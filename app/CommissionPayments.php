<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionPayments extends Model
{
    protected $table='commissionPayments';
	protected $primaryKey='id';
	
	
	public $timestamps=false;
	
	protected $fillable=array(
		'idFile',
		'payoutDate',
		'amount',
		'affiliateCompanyId',
		'comments',
		'iduploadFile',
		'affiliateCompanyName',
		'provider',
		'statusCode',
		'feeID',
		'eftTransactionID'
	);
}
