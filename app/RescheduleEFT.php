<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RescheduleEFT extends Model
{
  protected $table = "reschedule_efts";  
  protected $primaryKey = 'id';
  protected $fillable = ['idFile','eftTransactionID','newEftTransactionID'];
}
