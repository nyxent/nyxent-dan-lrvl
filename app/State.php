<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $table = "state";
    protected $primaryKey = 'idState';
    protected $fillable = ['abbreviation','name'];
    protected $hidden = ['idState'];

}
