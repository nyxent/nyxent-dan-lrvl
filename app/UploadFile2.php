<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadFile extends Model
{
     protected $table='uploadFile';
	 protected $primaryKey='id';
	 
     protected $fillable = [
      'uploadDate'
    ];
}
