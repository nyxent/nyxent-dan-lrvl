<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebtHolderNote extends Model {

    protected $table = "debtHolderNotes";
    protected $primaryKey = 'debtHolderNoteId';
    protected $fillable = ['idDebtHolder', 'note', 'creationDate'];
    protected $hidden = ['debtHolderNoteId'];
    public $timestamps = false;

    public function debtHolders() {
		return $this->belongsTo('App\DebtHolder', 'idDebtHolder', 'idDebtHolder');
	}

}
