<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {
	
	protected $table = "file";
	protected $primaryKey = 'idFile';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'firstName', 'lastName', 'homePhone', 'businessPhone', 'cellPhone', 'email', 'dateBirth', 'socialSecurity', 'maritalStatus', 'address', 'city', 'county', 'zip', 'idFileStatus',
		'scFirstName', 'scLastName', 'scHomePhone', 'scBusinessPhone', 'scCellPhone', 'scEmail', 'scDateBirth', 'scSocialSecurity', 'scMaritalStatus', 'scAddress', 'scCity', 'scCounty', 'scZip', 'testFile', 'idFileStatus',
		'enrollmentApprovedDate'
	];
	
	public function debtHolders() {
		return $this->hasMany('App\DebtHolder', 'idFile', 'idFile');
	}
	
	public function histories(){
		return $this->hasMany('App\History','idFile','idFile');
	}
	
	public function fileStatus(){
		return $this->belongsTo('App\FileStatus', 'idFileStatus','idFileStatus');
	}
	
	public function clientPayments(){
		return $this->hasMany('App\ClientPayments', 'idFile','idFile');
	}
	
	public function getInactiveDate(){
		if($this->idFileStatus==env('INACTIVE_IDFILESTATUS')){
			return $this->histories()->where('title',env('INACTIVE_TITLE'))->first();
		}
		return null;
	}
	
	public function getFirstPayment(){
		return $this->clientPayments()->whereIn('statusCode',array('P','T','S'))->orderBy('depositDate')->first();
	}
	
}
