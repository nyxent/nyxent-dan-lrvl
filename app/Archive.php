<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model {

    protected $table = "archive";
    protected $primaryKey = 'idArchive';
    protected $fillable = [ 'idFile', 'archiveName', 'archiveType', 'archiveFileName',
        'archiveDate', 'idDebtHolder', 'viewed', 'idLegalMatter','hash'];
    protected $hidden = ['idArchive','created_at','updated_at'];

}
