<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileStatus extends Model
{
  protected $table = "fileStatus"; 
  protected $primaryKey = 'idFileStatus'; 
  protected $fillable = [ 'name',];

  public $timestamps = false;
  
}
