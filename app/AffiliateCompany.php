<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateCompany extends Model {
	
	protected $table = "affiliateCompany";
	protected $primaryKey = 'affiliateCompanyId';
		
}
