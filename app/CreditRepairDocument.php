<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditRepairDocument extends Model
{
  protected $table = "creditRepairDocument"; 
  protected $primaryKey = 'idDocument'; 
  protected $fillable = ['idFile', 'name', 'fileName'];

}
