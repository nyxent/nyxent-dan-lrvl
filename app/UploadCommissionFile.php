<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadCommissionFile extends Model
{
    protected $table='uploadCommissionFile';
	 protected $primaryKey='id';
	 
     protected $fillable = [
      'type',
      'payment_date',
      'client',
      'commission',
      'company',
	  'matchClient',
	  'matchCompany'
    ];
}
