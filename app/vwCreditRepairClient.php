<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vwCreditRepairClient extends Model
{
  protected $table = "vwCreditRepairClient";
  protected $fillable = ['idFile', 'firstName', 'lastName', 'provider', 'contractSignDateCr', 
      'idFileDAN', 'email', 'homePhone', 'businessPhone', 'cellPhone', 'enrollmentApprovedDate', 
      'idUser', 'sands', 'sandsCreationDate', 'goldCreationDate', 'statusName', 'hide', 'userHide', 'sentDate'];
  public $timestamps = false;
  
}
