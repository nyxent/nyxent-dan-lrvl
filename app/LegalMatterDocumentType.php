<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalMatterDocumentType extends Model {

    protected $table = "legalMatterDocumentType";
    protected $primaryKey = 'idLegalMatterDocumentType';
    protected $fillable = ['name'];
    protected $hidden = ['idLegalMatterDocumentType'];

    public $timestamps = false;

}
