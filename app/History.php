<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model {

    protected $table = "history";
    protected $primaryKey = 'id';
    protected $fillable = ['idFile', 'title', 'detail', 'idUser'];

    // Do not set updated_at timestamp.
    const UPDATED_AT = null;
    const CREATED_AT = 'timeStamp';

    public function getInactiveDate() {
        if ($this->title == 'Flow Change to Inactive') {
            return $this->timeStamp;
        } else {
            return null;
        }
    }

}
