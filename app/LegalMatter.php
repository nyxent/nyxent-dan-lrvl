<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalMatter extends Model {

    protected $table = "legalMatter";
    protected $primaryKey = 'idLegalMatter';
    protected $fillable = ['idFile', 'idUser', 'idDebtHolder', 'description', 'openDate',
        'closeDate', 'idLegalMatterStatus', 'plantiff', 'defenderFirstName', 'defenderLastName', 'lmJurisdiction',
        'lmAddress', 'lmCity', 'lmZip', 'idState', 'lmName', 'lmLawFirm', 'lmEmail',
        'lmPhone', 'lmFax', 'ref', 'ammount', 'appearingDate', 'idLegalMatterDocumentType', 'served'];
    protected $hidden = ['idLegalMatter', 'created_at', 'updated_at'];

    public function file() {
        return $this->belongsTo('App\Client', 'idFile', 'idFile');
    }

    public function debtHolder() {
        return $this->belongsTo('App\DebtHolder', 'idDebtHolder', 'idDebtHolder');
    }

    public function state() {
        return $this->belongsTo('App\State', 'idState', 'idState');
    }

    public function archives() {
        return $this->hasMany('App\Archive', 'idLegalMatter', 'idLegalMatter');
    }

    public function documentType() {
        return $this->belongsTo('App\LegalMatterDocumentType', 'idLegalMatterDocumentType', 'idLegalMatterDocumentType');
    }

}
