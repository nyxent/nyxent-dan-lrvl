<?php



namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\CommissionPaymentsController;

use Input;



class ForecastController extends Controller

{

    public function index(Request $request)

    {	

		/*

			* set  maximun execution  time

		*/

		ini_set('max_execution_time', 1400); 

		$page = Input::get('page', 1);

		$paginate = 500;

		$data=array();

		if($request)

    	{

			$CommisionPaymentsController=new CommissionPaymentsController();

			$searchClient=trim($request->GET('searchClient'));

			$orderby = $request->GET('orderby');

			$client=$CommisionPaymentsController->clientEPPS();			

			/*

				* SELECT IDS READY SAVE

			*/

			$where='';

			if($searchClient!='')

			{

				$where.="  AND clientPayments.idFile='".$searchClient."' ";

			}

			$Query="SELECT MAX(cantidad) AS CantidadMayor FROM 

										(

										SELECT `file`.idfile,`firstName`,`lastName`,COUNT(*) AS cantidad FROM `clientPayments`

										INNER JOIN `file` 

										ON `clientPayments`.idfile=file.idfile

										WHERE  clientPayments.provider='EPPS' AND statusCode IN ('S','P','R') {where}

										GROUP BY `file`.idfile,`firstName`,`lastName` 

										) AS t";

			$CantidadMayor =DB::select(str_replace("{where}",$where,$Query));

			

			$Query="SELECT `file`.idfile,`firstName`,`lastName`,COUNT(*) AS cantidad ,

								CONCAT('$',FORMAT(ppDownPayment, 2)) aS ppDownPayment,

								CONCAT('$',ppMontlyPayment) AS ppMontlyPayment

								FROM `clientPayments`

								INNER JOIN `file` 

								ON `clientPayments`.idfile=file.idfile

								WHERE  clientPayments.provider='EPPS'  {where}

								GROUP BY `file`.idfile,`firstName`,`lastName`,ppDownPayment,ppMontlyPayment

								{orderby}

									";

			$orderbyText='';

			if($orderby!='')

			{

				$orderbyText=$CommisionPaymentsController->queryOrderCommission($orderby);

			}

			$Query=str_replace("{orderby}",$orderbyText,$Query);



			$data=DB::select(str_replace("{where}",$where,$Query));

			$Query="SELECT idfile,DATE_FORMAT(depositDate, '%m/%d/%Y') as depositDate,

										CONCAT('$',FORMAT(amount/100, 2)) as amount,statusCode,lastMessage 

										FROM `clientPayments`

										WHERE provider='EPPS'  AND statusCode IN ('S','P','R')  {where}

										ORDER BY  DATE_FORMAT(depositDate, '%Y/%m/%d') 

									";

			$ClientPayments =DB::select(str_replace("{where}",$where,$Query));

			$ClientPaymentsResult=array();

			foreach($ClientPayments  as $indice=>$valor)

			{

				$ClientPaymentsResult[(integer)$valor->idfile][]=$valor;

			}

		

		

		}

		$itemsFile=array();


		if(isset($data))

		{
	
			$offSet = ($page * $paginate) - $paginate;

			
			$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);



			$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);

	

		}

		return view('form.Forecast.index',["offset"=>$offSet,"itemsFile"=>$itemsFile,'client'=>$client,'CantidadMayor'=>$CantidadMayor,'ClientPayments'=>$ClientPaymentsResult,'orderby'=>$orderby,'searchClient'=>$searchClient]);	

	}

}

