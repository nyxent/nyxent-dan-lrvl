<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AffiliateCompany;
use App\Client;
use Maatwebsite\Excel\Facades\Excel;

class CommisionReportAffilateController extends Controller {

    public function index(Request $request) {
        ini_set('memory_limit', '5120M');
        set_time_limit(0);

        $paginate = $request->download ? null : 30;

        $commisions = null;
        if ($request->showReport) {

            $query = DB::table('file as f')
                    ->join('user as u', 'f.fileOwner', '=', 'u.idUser')
                    ->join('clientPayments as p', 'f.idFile', '=', 'p.idFile')
                    ->leftJoin('commissionPayments as c', function($join) use ($request) {
                        $join->on('c.eftTransactionID', '=', 'p.id');
                        $join->where('c.statusCode', '!=', 'V');
//                        $join->onRaw(DB::raw("C.sttusCode != 'V'"));

                        if (!empty($request->get('searchAffiliate'))) {
                            $join->on('c.affiliateCompanyId', '=', DB::raw($request->get('searchAffiliate')));
                        } else {
                            $join->on('c.affiliateCompanyId', '=', DB::raw('26'));
                        }
                    })
//                    ->leftJoin('commentClientPayments as m', 'p.eftTransactionID', '=', 'm.clientPaymentID')
                    ->leftJoin('contracts as s', function($join) {
                        $join->on('s.idFile', '=', 'f.idFile');
                        $join->on('s.active', '=', DB::raw('1'));
                    });
                    // ->where('c.amount', '<>', 0)->where('c.statusCode', '=', 'T');
//                    ->where('c.amount', '<>', 0);

            $query->where('p.provider', '=', 'EPPS');



            if (!empty($request->get('date1'))) {
                $query->where(DB::raw("DATE_FORMAT(p.depositDate, '%m/%d/%Y')"), '>=', $request->get('date1'));
            }
            if (!empty($request->get('date2'))) {
                $query->where(DB::raw("DATE_FORMAT(p.depositDate, '%m/%d/%Y')"), '<=', $request->get('date2'));
            }
            if (!empty($request->get('searchClient'))) {
                $query->where('f.idFile', '=', $request->get('searchClient'));
            }

//            $query->where('p.statusCode','S');
//            $query->where('c.statusCode','T');
            $query->whereNotIn('p.statusCode', array('V', 'PFV', 'FV', 'P', 'IP'));
//
//
//            echo ($query->toSql());

            $query->select(
                    DB::raw("CONCAT(u.firstName,' ',	u.lastName) as affiliateName"), 
                    'f.idFile', 
                    'f.firstName', 
                    'f.lastName', 
                    'f.ppTotalDeb', 
                    'f.ppProgramLenght', 
                    DB::raw("DATE_FORMAT(c.payoutDate, '%m/%d/%Y') as payoutDate"), 
                    DB::raw("DATE_FORMAT(p.depositDate, '%m/%d/%Y') as depositDate"), 
                    'c.amount',
                    'c.affiliateCompanyId', 
                    'c.provider',
                    DB::raw("DATE_FORMAT(f.enrollmentApprovedDate, '%m/%d/%Y') as enrollmentApprovedDate"),
                    's.ppTotalDeb as contractTotalDebt', 
                    's.ppProgramLenght as contractProgramLenght', 
                    'f.idFileDAN', 
                    'p.paymentNumber', 
                    'p.statusCode'
            )->orderByRaw('f.idFile,p.depositDate');

            // echo($query->toSql());
            // dd($query->toSql());

            $commisions = $paginate ? $query->paginate($paginate) : $query->get();
        }
        $clients = Client::all();

        $affiliates = AffiliateCompany::all();

        if ($request->download) {
            Excel::create('Commision Report ', function($excel) use($commisions) {
                $excel->sheet('Commision Report', function($sheet) use($commisions) {
                    $sheet->loadView('form.commisionReportAffilate.table', compact('commisions'));
                });
            })->download('xlsx');
        } else {

            return view('form.commisionReportAffilate.index', compact('commisions', 'clients', 'affiliates', 'request'));
        }
    }

}
