<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use Auth;
use App\Faq;
use App\FaqCategory;
use View;
use Input;
use Session;
Use Redirect;
use DateTime;

class DonaReportController extends Controller
{
  public function index(Request $request){
    

    $dwn =  $request->get('download');    
    $keyword = $request->get('keyword');
    $from = $request->get('from');
    $to = $request->get('to'); 
    $checked = $request->get('checked'); 
    $sortField = $request->get('sortField'); 
    $sortOrder = $request->get('sortOrder'); 
    $perPage = 10;
    
    
    $query ="SELECT
    f.idFile,
    f.firstName,
    f.lastName,
    f.provider,
    c.idUser,
    c2.idUser as sands,
    DATE_FORMAT(f.enrollmentApprovedDate, '%m/%d/%Y') AS enrollmentApprovedDate,
    s.name AS statusName,
    a.hideInfoDonna1 as hide,
    u.hideInfoDonna1 as userHide
    FROM
    file f
    INNER JOIN fileStatus s ON
    f.idFileStatus = s.idFileStatus
    LEFT JOIN fileChecked c ON
    f.idFile = c.idFile
    AND c.idUser = 170
    LEFT JOIN fileChecked c2 ON
    f.idFile = c2.idFile
    AND c2.idUser = 140
    AND c2.idFile NOT IN (SELECT idFile from fileChecked WHERE idUser = 170)
    LEFT JOIN affiliateCompany a ON a.affiliateCompanyId = f.affiliateCompanyId
    LEFT JOIN user AS u ON u.idUser = f.fileOwner
    WHERE
    f.enrollmentApprovedDate IS NOT NULL
    AND f.enrollmentApprovedDate <> '1900-01-01'
    AND (f.enrollmentApprovedDate < DATE_SUB(NOW(),INTERVAL 730 DAY) OR f.idFile IN (SELECT idFile from fileChecked WHERE idUser =170 OR idUser =140))
    AND f.idFileStatus NOT IN (11,17) ";
    
    if(!empty($keyword)){
      $query = $query." AND (
        f.firstName LIKE '%".$keyword."%'
        OR f.lastName LIKE '%".$keyword."%'
        OR CONCAT(f.firstName, ' ', f.lastName) LIKE '%".$keyword."%'
        OR f.idFile LIKE '%".$keyword."%'
        OR f.idFileDAN LIKE '%".$keyword."%'
        OR f.email LIKE '%".$keyword."%'
        OR f.homePhone LIKE '%".$keyword."%'
        OR f.businessPhone LIKE '%".$keyword."%'
        OR f.cellPhone LIKE '%".$keyword."%') ";
      }
      
      if(!empty($checked)&& $checked==1){
        $query = $query. "AND c.idUser is not NULL ";
      }
      elseif(!empty($checked)&& $checked==2){
        $query = $query. "AND c.idUser is NULL ";
      }
      elseif(!empty($checked)&& $checked==3){
        $query = $query. "AND c.idUser is NULL AND c2.idUser = 140 ";
      }
      // dd($query);
      if(!empty($from)){
        $dateFrom = DateTime::createFromFormat('m/d/Y', $from);
        $query = $query. "AND f.enrollmentApprovedDate >= '".$dateFrom->format('Y-m-d')."' ";
      }
      if(!empty($to)){
        $dateTo = DateTime::createFromFormat('m/d/Y', $to);
        $query = $query. "AND f.enrollmentApprovedDate <= '".$dateTo->format('Y-m-d')."' ";
      }
      if(!empty($sortField)){
        $query = $query ."ORDER BY ".$sortField." ".$sortOrder;
      }
      else{
        $query = $query ."ORDER BY f.enrollmentApprovedDate DESC";
      }    
      
      $clients = \DB::select($query, [1]);      
      
      if($dwn){
        // $clients = array_map(function ($value) {return (array)$value; }, $clients);
        Excel::create('KC Report', function($excel) use($clients){
          $excel->sheet('KC Report',function($sheet) use($clients){
            $sheet->loadView('dona.excel',array('clients'=>$clients));
          });
        })->download('xlsx');
      }
      else{
        return View::make('dona.index')->with('clients', $clients);
      }    
      
    }
    public function arrayPaginator($array, $request){
      $page = Input::get('page', 1);
      $perPage = 20;
      $offset = ($page * $perPage) - $perPage;
      return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(), 'query' => $request->query()]);
    }
  }
  