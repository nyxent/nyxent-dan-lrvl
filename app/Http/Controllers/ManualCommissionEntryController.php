<?php



namespace App\Http\Controllers;



use Session;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Input;

use App\Http\Controllers\CommissionPaymentsController;

use App\Http\Controllers\CommissionEntryController;





class ManualCommissionEntryController extends Controller

{

    public function index(Request $request)

    {

		$page = Input::get('page', 1);

		$CommisionPaymentsController=new CommissionPaymentsController();

		$paginate =$CommisionPaymentsController->paginateGlobal();

		/*

			* FUNCTION IN ANOTHER CONTROLLER

		*/

		$CommisionPaymentsController=new CommissionPaymentsController();

		$client=$CommisionPaymentsController->clientSAS();

		

		if($request)

    	{

			

			$searchClient=trim($request->GET('searchClient'));

			$date1 = $request->GET('date1');

			$date2 = $request->GET('date2');

			$action = $request->GET('action');

			$dateDownload=date('m-d-Y').' All';

			$itemsFile=array();

			$Companys=array();

			$CompanysPayments=array();

			$where ='';

			if($searchClient!='' or ($date1!='' and $date2!=''))

			{

				if($searchClient!='')

				{

					$where.=" AND commentClientPayments.idFile='".$searchClient."' ";

					

				}

				if($date1!='' and $date2!='')

				{

					$dateDownload=$date1.' '.$date2;

					$where.=" AND depositDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."' ";

				}

				

				

					

				$data =DB::select($this->query().$where.$this->queryOrder());

				

				if($action=='export')

				{

					

					return $CommisionPaymentsController->ExportCSV($data,'ManualCommissionEntry'.$dateDownload.'.csv',10);	

				}	

				if(isset($data))

				{

					$offSet = ($page * $paginate) - $paginate;

					$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);

					$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator(

					$itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);

				

				}

			}

			return view('form.manCommissionEntry.index',

			["itemsFile"=>$itemsFile,'client'=>$client,

			'searchClient'=>$searchClient,

			'date1'=>$date1,'date2'=>$date2,

			'CompanysPayments'=>$CompanysPayments

			

			]

			);		

		}

    }

	public function query()

	{

			$query='

				SELECT  	

					(

						 SELECT `affiliateCompanyName` FROM `affiliateCompany`

						 WHERE `affiliateCompanyId` IN 

						  (

							SELECT affiliateCompanyId FROM `file` WHERE commentClientPayments.idfile=`file`.idfile

						  )

					) AS AffiliateName,

					lastName,

					firstName,

					CONCAT("$",FORMAT(amount/100, 2)) AS amount,

					commentClientPayments.idFile AS idFile,

					DATE_FORMAT(depositDate, "%m/%d/%Y") AS depositDate,

					REPLACE(FORMAT((

						SELECT COALESCE(SUM(C.`amount`),0) FROM 

						(

							SELECT  `file`.idfile,MAX(`payoutDate`)  AS payoutDate ,`commissionPayments`.affiliateCompanyId

							FROM `commissionPayments`

							INNER JOIN `file`

							ON `file`.idfile =`commissionPayments`.idfile AND `file`.affiliateCompanyId =`commissionPayments`.affiliateCompanyId

							WHERE statusCode="T" AND amount>0

							GROUP BY idfile,affiliateCompanyId

						) AS LastCommisions

						INNER JOIN `commissionPayments` C 

						ON C.idfile=LastCommisions.idfile AND C.`payoutDate`=LastCommisions.`payoutDate` AND 

						LastCommisions.affiliateCompanyId= C.affiliateCompanyId

						WHERE statusCode="T" AND amount>0 AND commentClientPayments.idfile=C.idfile

					)/100,2),",","") AS paymentAffiliate,

					

					(

						 SELECT   affiliateCompany2.affiliateCompanyName FROM `affiliateCompany`

						 INNER JOIN  `userAffiliateCompany` ON

						 `affiliateCompany`.`superAffiliateId`=`userAffiliateCompany`.`idUser`

						 INNER JOIN `file` ON `file`.affiliateCompanyId=`affiliateCompany`.affiliateCompanyId

						  INNER JOIN `affiliateCompany` AS affiliateCompany2 ON affiliateCompany2.affiliateCompanyId=`userAffiliateCompany`.`affiliateCompanyId`

						WHERE commentClientPayments.idfile=`file`.idfile

					) AS SuperAffiliateName,

					REPLACE(FORMAT((

						SELECT COALESCE(SUM(C.`amount`),0) FROM 

						(

							SELECT idfile,MAX(`payoutDate`)  AS payoutDate ,affiliateCompanyId

							FROM `commissionPayments`

							WHERE `affiliateCompanyId`=

							(

								SELECT   `userAffiliateCompany`.`affiliateCompanyId` FROM `affiliateCompany`

								INNER JOIN  `userAffiliateCompany` ON

								`affiliateCompany`.`superAffiliateId`=`userAffiliateCompany`.`idUser`

								INNER JOIN `file` ON `file`.affiliateCompanyId=`affiliateCompany`.affiliateCompanyId

								WHERE `commissionPayments`.idfile=`file`.idfile

							)

							 AND statusCode="T" AND amount>0 

							GROUP BY idfile,affiliateCompanyId

						) AS LastCommisions

						INNER JOIN `commissionPayments` C 

						ON C.idfile=LastCommisions.idfile AND C.`payoutDate`=LastCommisions.`payoutDate` AND 

						LastCommisions.affiliateCompanyId= C.affiliateCompanyId

						WHERE statusCode="T" AND amount>0 AND commentClientPayments.idfile=C.idfile

						

					)/100,2),",","") AS paymentSuperAffiliate ,

					

					REPLACE(FORMAT((

						

						SELECT COALESCE(SUM(C.`amount`),0) FROM 

						(

							SELECT idfile,MAX(`payoutDate`)  AS payoutDate ,affiliateCompanyId

							FROM `commissionPayments`

							WHERE `affiliateCompanyId`="24" AND statusCode="T" AND amount>0

							GROUP BY idfile,affiliateCompanyId

						) AS LastCommisions

						INNER JOIN `commissionPayments` C 

						ON C.idfile=LastCommisions.idfile AND C.`payoutDate`=LastCommisions.`payoutDate` AND 

						LastCommisions.affiliateCompanyId= C.affiliateCompanyId

						WHERE statusCode="T" AND amount>0 AND commentClientPayments.idfile=C.idfile

						

					)/100,2),",","") AS paymentNyxent,

					

						REPLACE(FORMAT((

						SELECT COALESCE(SUM(C.`amount`),0) FROM 

						(

							SELECT idfile,MAX(`payoutDate`)  AS payoutDate ,affiliateCompanyId

							FROM `commissionPayments`

							WHERE `affiliateCompanyId`="21" AND statusCode="T" AND amount>0

							GROUP BY idfile,affiliateCompanyId

						) AS LastCommisions

						INNER JOIN `commissionPayments` C 

						ON C.idfile=LastCommisions.idfile AND C.`payoutDate`=LastCommisions.`payoutDate` AND 

						LastCommisions.affiliateCompanyId= C.affiliateCompanyId

						WHERE statusCode="T" AND amount>0 AND commentClientPayments.idfile=C.idfile

						

					)/100,2),",","") AS PaymentDAN,

					

					

					"24" AS affiliateCompanyIdNyxent,

					REPLACE(FORMAT((

						SELECT COALESCE(SUM(C.`amount`),0) FROM 

						(

							SELECT idfile,MAX(`payoutDate`)  AS payoutDate ,affiliateCompanyId

							FROM `commissionPayments`

							WHERE `affiliateCompanyId`="29" AND statusCode="T" AND amount>0

							GROUP BY idfile,affiliateCompanyId

						) AS LastCommisions

						INNER JOIN `commissionPayments` C 

						ON C.idfile=LastCommisions.idfile AND C.`payoutDate`=LastCommisions.`payoutDate` AND 

						LastCommisions.affiliateCompanyId= C.affiliateCompanyId

						WHERE statusCode="T" AND amount>0 AND commentClientPayments.idfile=C.idfile

						

					)/100,2),",","") AS paymentFinder,

					"29" AS `affiliateCompanyIdFinder`, 

				

					"21" AS `affiliateCompanyIdDAN`,

					

					(

						SELECT affiliateCompanyId FROM `file` WHERE commentClientPayments.idfile=`file`.idfile

					)  AS affiliateCompanyId,

					

					(

						SELECT   `userAffiliateCompany`.`affiliateCompanyId` FROM `affiliateCompany`

						INNER JOIN  `userAffiliateCompany` ON

						`affiliateCompany`.`superAffiliateId`=`userAffiliateCompany`.`idUser`

						INNER JOIN `file` ON `file`.affiliateCompanyId=`affiliateCompany`.affiliateCompanyId

						WHERE commentClientPayments.idfile=`file`.idfile

					) AS SuperAffiliateCompanyId, 

					

					DATE_FORMAT(depositDate, "%Y-%m-%d") AS depositDate2,

						COALESCE(commissionPaid ,"N") AS commissionPaid 

				

					

				FROM 

				(

					SELECT `clientPaymentsSAS`.idfile,commissionPaid,`clientPaymentsSAS`.`depositDate`,`clientPaymentsSAS`.`amount`

					FROM `clientPaymentsSAS`

					WHERE COALESCE(commissionPaid,"")<>"Y"

				) AS commentClientPayments

				LEFT JOIN 

				(

					SELECT DISTINCT commissionPayments.idfile FROM `commissionPayments`

					INNER JOIN `commentCommissionPayments`

					ON

					`commissionPayments`.idfile=`commentCommissionPayments`.idfile AND

					`commissionPayments`.`payoutDate`=`commentCommissionPayments`.`dateTransaction` AND

					`commissionPayments`.`affiliateCompanyId`=`commentCommissionPayments`.`affiliateCompanyId`

					WHERE `statusCode`="T"

					GROUP BY idfile,paymentNumber

				

				)AS commentCommissionPayments

				ON  `commentClientPayments`.idfile=`commentCommissionPayments`.idfile

				

				INNER JOIN `file` ON `file`.idfile=commentClientPayments.idfile

				







					';

			return $query;

	}

	public function mmddyyyyToyyyymmdd($date)

	{

		$date = explode("/",substr($date,0,10));

		$date = $date[2].'-'.$date[0].'-'.$date[1];

		return $date;

	}

	public function queryOrder()

	{

			$query=" ORDER BY AffiliateName,lastName,firstName,DATE_FORMAT(depositDate, '%Y/%m/%d')";

			return $query;

	}

	public function store(Request $request)

    {

		$DatosToSave=$request->GET('DatosToSave');

		

		if(isset($DatosToSave))

		{

			foreach($DatosToSave as $indice=>$value2)

			{

				$NewFeeDate=$this->mmddyyyyToyyyymmdd(trim($value2['NewFeeDate']));

				$idFile=trim($value2['idFile']);

				$idDate=trim($value2['idDate']);

				$CompanysPayments=$value2['CompanysPayments'];

				$Note=$value2['Note'];

				

				

				foreach($CompanysPayments as $indice=>$value)

				{

					if(trim($value['affiliateCompanyId'])!='')

					{

						$CommentCurrent =DB::select("SELECT * 

												FROM commissionPayments 

												WHERE idfile='".$idFile."' AND payoutDate='".$NewFeeDate."' AND affiliateCompanyId='".$value['affiliateCompanyId']."' ");

					

						if(count($CommentCurrent)>0)	

						{

							DB::update("UPDATE commissionPayments 

										SET amount='".str_replace('.','',$value['amount'])."', provider='SAS',statusCode='T'

										WHERE idfile='".$idFile."' AND payoutDate='".$NewFeeDate."' AND affiliateCompanyId='".$value['affiliateCompanyId']."' ");

						}

						else

						{

							DB::insert("

							INSERT INTO `crmlexco_dan_dev`.`commissionPayments` 

									(

									`idFile`, 

									`payoutDate`, 

									`amount`, 

									`affiliateCompanyId`, 

									`provider`, 

									`statusCode`,

									 manualUpload

									)

								VALUES

								(

									'".$idFile."',

									'".$NewFeeDate."',

									REPLACE(FORMAT('".$value['amount']."', 2), ',', ''),

									'".$value['affiliateCompanyId']."',

									'SAS',

									'T',

									'Y'

								);

							");

						}

						

						$CommentCurrent =DB::select("SELECT * 

												FROM commentCommissionPayments 

												WHERE idfile='".$idFile."' AND dateTransaction='".$NewFeeDate."' AND affiliateCompanyId='".$value['affiliateCompanyId']."' ");

						$CommentCurrentPayment =DB::select("SELECT * 

														FROM commentClientPayments 

														WHERE idfile='".$idFile."' AND dateTransation='".$idDate."' ");

					

						if(count($CommentCurrent)>0)	

						{

							



							DB::update("UPDATE commentCommissionPayments 

										SET 

											comment='".$Note."'

										WHERE idfile='".$idFile."' AND dateTransaction='".$NewFeeDate."' AND affiliateCompanyId='".$value['affiliateCompanyId']."' ");

						}

						else

						{

							DB::insert("INSERT INTO commentCommissionPayments 

								(

									idFile,

									dateTransaction,

									affiliateCompanyId,

									comment

								)

								VALUES

								(

									'".$idFile."',

									'".$NewFeeDate."',

									'".$value['affiliateCompanyId']."',

									'".$Note."'

							

								);

							");

						}	

					}

				}

			

				$CommentCurrent =DB::select("SELECT * 

											FROM clientPaymentsSAS 

											WHERE idfile='".$idFile."' AND depositDate='".$idDate."' ");

				

				

				DB::update("UPDATE clientPaymentsSAS 

								SET commissionPaid='Y'

								WHERE idfile='".$idFile."' AND depositDate='".$idDate."' ");

				

				$CommissionEntryController=new CommissionEntryController();

				$CommissionEntryController->updatePaymentNumberCommission();

			}

		}

		

		echo json_encode(array('Save'=>'Ok'));

		exit;

	}

}

