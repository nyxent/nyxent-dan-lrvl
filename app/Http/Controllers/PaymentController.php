<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Utils\PaymentNumberUtil;
use App\ClientPayments;
use App\Client;

class PaymentController extends Controller {

    public function paymentConsecutive(Request $request) {

        if ($request->idFile) {
            PaymentNumberUtil::repairClientPayments($request->idFile);
        }

        dd($request->all());
    }
    
    public function repairAll(){
        $files = Client::all();
        foreach($files as $file){
            PaymentNumberUtil::repairClientPayments($file->idFile);
        }
    }

}
