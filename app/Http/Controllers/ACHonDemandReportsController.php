<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ACHonDemandReportsController extends Controller
{
    public function index(Request $request)
    {	
		$CommisionPaymentsController=new CommissionPaymentsController();
		
		$date1 = $request->GET('date1');
		$date2 = $request->GET('date2');
		$reportType = $request->GET('reportType');
		$dateDownload='';
		$where ='';
		if($date1!='' and $date2!='')
		{
			/*
				* EXPORT
			*/
			$dateDownload=$date1.' '.$date2;
			$where.=" WHERE depositDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."'";
		}
		if($reportType=='ach')
		{
			$data =DB::select($this-> queryACH().$where.' ORDER BY depositDate');
			
		}
		if($reportType=='returnedItem')
		{
			$data =DB::select($this-> queryRETURN().$where.' ORDER BY depositDate');
			
		}
		if($reportType=='forecast')
		{
			$data =DB::select($this-> queryFORCAST().$where.' ORDER BY depositDate');
			
		}
		
		if($reportType!='')
		{
			return $CommisionPaymentsController->ExportCSV($data,'ACHonDemandReports'.$reportType.$dateDownload.'.csv',11);	
		}
			
		return view('form.ACHonDemandReports.index',['date1'=>$date1,'date2'=>$date2]);
	}
	
	
	public function queryACH()
	{
		$query="
			SELECT clientPayments.idfile AS CRMID,
			`depositDate`,CONCAT(`lastName`,' ',`firstName`) AS clientName,
			CONCAT('$',FORMAT(`amount`/100,2)) AS amount,
			clientPayments.provider
			FROM `clientPayments`
			INNER JOIN `file`
			ON `clientPayments`.idfile=file.idfile
			WHERE clientPayments.provider='EPPS'
			";
		return $query;
	}
	public function queryRETURN()
	{
		$query="
			SELECT clientPayments.idfile AS CRMID,
			`depositDate`,CONCAT(`lastName`,' ',`firstName`) AS clientName,
			CONCAT('$',FORMAT(`amount`/100,2)) AS amount,
			clientPayments.provider,
			statusCodeDescription
			FROM `clientPayments`
			INNER JOIN `file`
			ON `clientPayments`.idfile=file.idfile
			WHERE statusCode in ('R','THR','PFV','PF')
			and clientPayments.provider='EPPS'
			";
		return $query;
	}
	public function queryFORCAST()
	{
		$query="
			SELECT clientPayments.idfile AS CRMID,
			`depositDate`,CONCAT(`lastName`,' ',`firstName`) AS clientName,
			CONCAT('$',FORMAT(`amount`/100,2)) AS amount,
			clientPayments.provider,
			statusCodeDescription
			FROM `clientPayments`
			INNER JOIN `file`
			ON `clientPayments`.idfile=file.idfile
			WHERE statusCode in ('P')
				and clientPayments.provider='EPPS'
			";
		return $query;
	}
}
