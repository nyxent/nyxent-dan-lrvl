<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AffiliateCompany;
use App\Client;
use Maatwebsite\Excel\Facades\Excel;

class CommisionReportSuperAffilateController extends Controller {
	
	public function index(Request $request) {
		ini_set('memory_limit', '5120M');
		
		$paginate = $request->download ? null : 30;
		
		$query = DB::table('file as f')
		->join('user as u', 'f.fileOwner', '=', 'u.idUser')
		->join('commissionPayments as c', 'f.idFile', '=', 'c.idFile')
		->join('clientPayments as p','c.eftTransactionID','=','p.id')
		->join('affiliateCompany as a', 'c.affiliateCompanyId','=','a.affiliateCompanyId')
		->join('userAffiliateCompany as r','a.superAffiliateId','=','r.idUser')
		->leftJoin('commentClientPayments as m','p.eftTransactionID','=','m.clientPaymentID')
		->leftJoin('contracts as s', function($join) {
			$join->on('s.idFile', '=', 'f.idFile');
			$join->on('s.active', '=', DB::raw('1'));
		})
		// ->where('c.amount', '<>', 0)->where('c.statusCode', '=', 'T');
		->where('c.amount', '<>', 0);
		
		$query->where('c.provider', '=', 'EPPS');
		
		if (!empty($request->get('searchAffiliate'))) {
			$query->where('c.affiliateCompanyId', '=', $request->get('searchAffiliate'));
		} else {
			$query->where('c.affiliateCompanyId', '=', 26);
		}
		if (!empty($request->get('date1'))) {
			$query->where(DB::raw("DATE_FORMAT(c.payoutDate, '%m/%d/%Y')"), '>=', $request->get('date1'));
		}
		if (!empty($request->get('date2'))) {
			$query->where(DB::raw("DATE_FORMAT(c.payoutDate, '%m/%d/%Y')"), '<=', $request->get('date2'));
		}
		if (!empty($request->get('searchClient'))) {
			$query->where('f.idFile', '=', $request->get('searchClient'));
		}
		
		$query->select(DB::raw("CONCAT(u.firstName,' ',	u.lastName) as affiliateName"), 'f.idFile', 'f.firstName', 'f.lastName', 'f.ppTotalDeb', 'f.ppProgramLenght', DB::raw("DATE_FORMAT(c.payoutDate, '%m/%d/%Y') as payoutDate"), 'c.amount', 'c.affiliateCompanyId', 'c.provider', DB::raw("DATE_FORMAT(f.enrollmentApprovedDate, '%m/%d/%Y') as enrollmentApprovedDate"), 's.ppTotalDeb as contractTotalDebt', 's.ppProgramLenght as contractProgramLenght','f.idFileDAN','m.paymentNumber','m.manualPaymentNumber')->orderByRaw('f.idFile,c.payoutDate');
		
		// echo($query->toSql());
		// dd($query->toSql());
		
		$commisions = $paginate ? $query->paginate($paginate) : $query->get();
		
		$clients = Client::all();

		$affiliates = AffiliateCompany::all();
		
		if ($request->download) {
			Excel::create('Commision Report ', function($excel) use($commisions) {
				$excel->sheet('Commision Report', function($sheet) use($commisions) {
					$sheet->loadView('form.commisionReportAffilate.table', compact('commisions'));
				});
			})->download('xlsx');
		} else {
			
			return view('form.commisionReportSuperAffilate.index', compact('commisions', 'clients', 'affiliates', 'request'));
		}
	}
	
}
