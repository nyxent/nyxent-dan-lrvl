<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Session;
use Mail;
use App\Announcement;
use App\Client;

class AnnouncementsController extends Controller {

    public function form($id = null) {
        $announcement = Announcement::find($id);
        if (Session::get('idFile')) {
            $user = Client::find(Session::get('idFile'));
            $to = $user->firstName . " " . $user->lastName;
        } else {
            $to = 'All Users';
        }
        return view('announcement.form', compact('announcement', 'to'));
    }

    public function save(Request $request) {
        $data = $request->all();
        
        $rules = array(
            'subject' => 'required|max:255',
            'message' => 'required|max:512'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput(); //get the data in all input formfields
            return "error";
        }
        
        
        $data['remitter_id'] = Session::get('iduser');
        if (Session::get('idFile') && !isset($data['all'])) {
            $data['recipient_id'] = Session::get('idFile');
        } else {
            $data['all'] = true;
        }

        $announcement = new Announcement($data);
        $announcement->save();
        Session::flash("message", "Announcement has been sent");
        return redirect()->route("announcement.create");
    }

}
