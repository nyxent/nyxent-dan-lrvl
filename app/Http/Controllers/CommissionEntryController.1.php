<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\UploadCommissionFile;
use App\ClientPayments;
use App\clientpaymentssas;
use App\CommissionPayments;
use App\UploadFile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoapClient;
use SimpleXMLElement;
use App\Http\Requests;
use Input;

class CommissionEntryController extends Controller {

    public function index(Request $request) {

        DB::update('UPDATE file SET dateChangeProvider=NOW()+ INTERVAL 1 DAY WHERE dateChangeProvider IS NULL AND  provider="EPPS"');

        if ($request) {



            if (Session::get('idFile')) {

                if (Session::get('Continue') != 'Y') {

                    $itemsFile = DB::table('uploadCommissionFile')->select('id', 'type', DB::raw('DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date'), 'client', DB::raw('FORMAT(commission/100, 2) AS commission'), 'company', 'iduploadfile', 'matchClient', 'matchCompany', 'matchIdClient', 'matchIdCompany', 'duplicate')
                            ->where('iduploadFile', Session::get('idFile'))
                            ->orderByRaw('IF(matchIdClient IS NULL OR matchIdCompany IS NULL,NULL,matchIdClient)')
                            ->orderBy(DB::raw('DATE_FORMAT(payment_date, "%Y/%m/%d")'))
                            ->orderBy("client")
                            ->paginate(20);
                } else {

                    $itemsFile = DB::table('uploadCommissionFile')->select('id', 'type', DB::raw('DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date'), 'client', DB::raw('FORMAT(commission/100, 2) AS commission'), 'company', 'iduploadfile', 'matchClient', 'matchCompany', 'matchIdClient', 'matchIdCompany', 'duplicate')
                            ->where('iduploadFile', Session::get('idFile'))
                            ->orderByRaw('IF(matchIdClient IS NULL OR matchIdCompany IS NULL,NULL,matchIdClient)')
                            ->orderBy(DB::raw('DATE_FORMAT(payment_date, "%Y/%m/%d")'))
                            ->orderBy("client")
                            ->paginate(20);
                }

                return view('form.commissionEntry.index', ["itemsFile" => $itemsFile, "Continue" => Session::get('Continue')]);
            } else {

                return view('form.commissionEntry.index');
            }
        }
    }

    /**

     * import a file in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(Request $request) {

        $accion = trim($request->GET('accion'));

        $number_send = trim($request->GET('number_send'));

        if ($accion == 'success') {

            return $this->export();
        }

        if ($accion == 'import') {

            return $this->uploadFile($request);
        }

        if ($accion == 'synchronizeSAS') {

            return $this->synchronizePaymentsSAS($number_send);
        }

        if ($accion == 'synchronizeEPPS') {

            return $this->synchronizePaymentsEPPS();
        }
    }

    /*

     * SYNCHRONIZE DATA SAS

     */

    public function synchronizePaymentsSAS($number_send) {

        /*

         * set  maximun execution  time

         */

        ini_set('max_execution_time', 720);

        /*         * ******************************* */

        $ClientPayments = new ClientPayments;

        $clientpaymentssas = new clientpaymentssas;

        if ($number_send == 1) {

            /*

             * SYNCRHONIZE ALL AND THE NEW CHANGE SAS TO EPPS

             */

            $ListAccountID = DB::select("SELECT `accountID`,f.idfile FROM `file` f

										INNER JOIN `transaction_service_security` tss

										ON f.idfile=tss.idfile

										WHERE  f.idfile<1300 and  provider='SAS' 

  									    UNION ALL 

									    SELECT `accountID`,f.idfile FROM `file` f

										INNER JOIN `transaction_service_security` tss

										ON f.idfile=tss.idfile

										WHERE   provider='EPPS' AND 

										(

										 (`dateChangeProvider` IS NOT NULL AND `dateSynchronize` IS NULL) 

										 OR

										 (`dateChangeProvider` IS NOT NULL AND dateChangeProvider >= dateSynchronize)

										)

										");



            /*

             * TEST

             */

            /*

              $ListAccountID=DB::select("SELECT `accountID`,f.idfile FROM `file` f

              INNER JOIN `transaction_service_security` tss

              ON f.idfile=tss.idfile

              WHERE f.idfile='2125'

              UNION ALL

              SELECT `accountID`,f.idfile FROM `file` f

              INNER JOIN `transaction_service_security` tss

              ON f.idfile=tss.idfile

              WHERE   provider='EPPS' AND

              (

              (`dateChangeProvider` IS NOT NULL AND `dateSynchronize` IS NULL)

              OR

              (`dateChangeProvider` IS NOT NULL AND dateChangeProvider >= dateSynchronize)

              )

              ");

              $number_send=0;

             */
        }

        if ($number_send == 2) {

            $ListAccountID = DB::select("SELECT `accountID`,f.idfile FROM `file` f

										INNER JOIN `transaction_service_security` tss

										ON f.idfile=tss.idfile

										WHERE  f.idfile>=1300  and  f.idfile<=1600 and  provider='SAS' 

										

										/* AND f.idfile='1357'*/

										");
            // 	$ListAccountID=DB::select("SELECT
            // 	`accountID`,
            // 	f.idfile
            // FROM
            // 	`file` f
            // INNER JOIN `transaction_service_security` tss ON
            // 	f.idfile = tss.idfile
            // WHERE
            // 	f.idfile = 1750
            // 								");
        }

        if ($number_send == 3) {

            $ListAccountID = DB::select("SELECT `accountID`,f.idfile FROM `file` f

										INNER JOIN `transaction_service_security` tss

										ON f.idfile=tss.idfile

										WHERE  f.idfile>=1600  and  f.idfile<=1900 and  provider='SAS' 

										

										/* AND f.idfile='1357'*/

										");
        }

        if ($number_send == 4) {

            $ListAccountID = DB::select("SELECT `accountID`,f.idfile FROM `file` f

										INNER JOIN `transaction_service_security` tss

										ON f.idfile=tss.idfile

										WHERE  f.idfile>=1900  and  provider='SAS' 

										

										/* AND f.idfile='1357'*/

										");

            $number_send = 0;
        }



        foreach ($ListAccountID as $indice => $AccountID) {


            // dd('ENTRA');
            /* ##################################################################

              #################DONWLOAD REJECTED##################################

              ################################################################### */

            $url = 'https://sas1.secureaccountservice.com/tas?page=flashSession&vendor=Seldix&expert=TrustAccounting&function=apiGetClientDeposits&accountID=SecureAccountService_0&login=danapiuser&password=DANCRMLEX1User&contactID=' . $AccountID->accountID;

            $PaymentsSAS = @file_get_contents($url, false, $context, -1, 10485760);

            $PaymentsSAS = str_replace('<!DOCTYPE HTML>"TransactionID","Date","Amount","Description"', '', $PaymentsSAS);

            $PaymentsSAS = preg_split("/\\r\\n|\\r|\\n/", $PaymentsSAS);

            if (count($PaymentsSAS) > 1) {



                DB::table('clientPayments')->where(['idfile' => $AccountID->idfile])->where(['provider' => 'SAS'])->delete();

                $dataArray = array();



                foreach ($PaymentsSAS as $indice2 => $FilePayments) {

                    if ($FilePayments != '') {

                        $FilePayments = explode(",", $FilePayments);



                        /* if(

                          (

                          (

                          (strpos( strtolower($FilePayments[3]),'deposit')==true) and (strpos( strtolower($FilePayments[3]),'rejected')==false)

                          )

                          or

                          (

                          (strpos( strtolower($FilePayments[3]),'closed')==true) and (strpos( strtolower($FilePayments[3]),'rejected')==false)

                          )

                          )

                          and str_replace('"','',$FilePayments[2])!=0

                          )

                          {

                          $dataArray[] = [

                          'idFile' =>$AccountID->idfile,

                          'depositDate' =>str_replace('"','',$FilePayments[1]),

                          'amount' =>str_replace('"','',$FilePayments[2]),

                          'provider'=>'SAS',

                          'statusCode'=>'S',

                          'statusCodeDescription'=>str_replace('"','',$FilePayments[3])

                          ];

                          }

                          else

                          {

                          if(strpos( strtolower($FilePayments[3]),'rejected')==true  and str_replace('"','',$FilePayments[2])!=0 )

                          {

                          $dataArray[] = [

                          'idFile' =>$AccountID->idfile,

                          'depositDate' =>str_replace('"','',$FilePayments[1]),

                          'amount' =>str_replace('"','',$FilePayments[2]),

                          'provider'=>'SAS',

                          'statusCode'=>'R',

                          'statusCodeDescription'=>str_replace('"','',$FilePayments[3])

                          ];

                          }

                          else

                          {

                          $dataArray[] = [

                          'idFile' =>$AccountID->idfile,

                          'depositDate' =>str_replace('"','',$FilePayments[1]),

                          'amount' =>str_replace('"','',$FilePayments[2]),

                          'provider'=>'SAS',

                          'statusCode'=>'SKI',

                          'statusCodeDescription'=>str_replace('"','',$FilePayments[3])

                          ];

                          }

                          } */



                        if (
                                (strpos(strtolower($FilePayments[3]), 'rejected') == true and str_replace('"', '', $FilePayments[2]) != 0 )

                                or strpos(strtolower($FilePayments[3]), 'skip') == true

                                or strpos(strtolower($FilePayments[3]), 'refund') == true
                        ) {

                            $dataArray[] = [

                                'idFile' => $AccountID->idfile,
                                'depositDate' => str_replace('"', '', $FilePayments[1]),
                                'amount' => str_replace('"', '', $FilePayments[2]),
                                'provider' => 'SAS',
                                'statusCode' => 'R',
                                'statusCodeDescription' => str_replace('"', '', $FilePayments[3])
                            ];
                        }
                    }
                }

                if ($dataArray != array()) {

                    $ClientPayments->insert($dataArray);
                }
            }

            /* ##################################################################

              #################DONWLOAD PAYMENTS##################################

              ################################################################### */



            $url = 'https://sas1.secureaccountservice.com/tas?page=flashSession&vendor=Seldix&expert=TrustAccounting&function=apiGetClientDepositSchedule&accountID=SecureAccountService_0&login=danapiuser&password=DANCRMLEX1User&contactID=' . $AccountID->accountID;

            $PaymentsSAS = @file_get_contents($url, false, $context, -1, 10485760);

            $PaymentsSAS = str_replace('<!DOCTYPE HTML>"ScheduledActionID","Recurring","DateStart","DateEnd","Amount","Description","Status"', '', $PaymentsSAS);

            $PaymentsSAS = preg_split("/\\r\\n|\\r|\\n/", $PaymentsSAS);

            if (count($PaymentsSAS) > 1) {


                $dataArray = array();



                foreach ($PaymentsSAS as $indice2 => $FilePayments) {

                    if ($FilePayments != '') {

                        $FilePayments = explode(",", $FilePayments);

                        if ($FilePayments != '') {

                            if ((((strpos(strtolower($FilePayments[5]), 'deposit') == true) and ( strpos(strtolower($FilePayments[6]), 'processed') == true))) and trim(str_replace('"', '', $FilePayments[4])) != 0) {

                                $dataArray[] = [

                                    'idFile' => $AccountID->idfile,
                                    'depositDate' => str_replace('"', '', $FilePayments[2]),
                                    'amount' => str_replace('"', '', $FilePayments[4]),
                                    'provider' => 'SAS',
                                    'statusCode' => 'S',
                                    'statusCodeDescription' => str_replace('"', '', $FilePayments[5] . ' - ' . $FilePayments[6])
                                ];
                            } else {

                                if (
                                        (

                                        strpos(strtolower($FilePayments[5]), 'deposit') == true

                                        and

                                        strpos(strtolower($FilePayments[6]), 'rejected') == true

                                        )

                                        and trim(str_replace('"', '', $FilePayments[4])) != 0
                                ) {

                                    $dataArray[] = [

                                        'idFile' => $AccountID->idfile,
                                        'depositDate' => str_replace('"', '', $FilePayments[2]),
                                        'amount' => str_replace('"', '', $FilePayments[4]),
                                        'provider' => 'SAS',
                                        'statusCode' => 'R',
                                        'statusCodeDescription' => str_replace('"', '', $FilePayments[5] . ' - ' . $FilePayments[6])
                                    ];
                                }
                            }
                        }
                    }
                }





                if ($dataArray != array()) {

                    $ClientPayments->insert($dataArray);
                }
            }/* ##################################################################

              #################DONWLOAD PAYMENTS##################################

              ################################################################### */

            $url = 'https://sas1.secureaccountservice.com/tas?page=flashSession&vendor=Seldix&expert=TrustAccounting&function=apiGetClientPaymentSchedule&accountID=SecureAccountService_0&login=danapiuser&password=DANCRMLEX1User&contactID=' . $AccountID->accountID;

            $PaymentsSAS = @file_get_contents($url, false, $context, -1, 10485760);

            $PaymentsSAS = str_replace('<!DOCTYPE HTML>"ScheduledActionID","Recurring","DateStart","DateEnd","Amount","Description","Status","OtherPayee"', '', $PaymentsSAS);

            $PaymentsSAS = preg_split("/\\r\\n|\\r|\\n/", $PaymentsSAS);

            if (count($PaymentsSAS) > 1) {

                DB::table('clientPaymentsSAS')->where(['idfile' => $AccountID->idfile])->delete();

                $dataArray = array();



                foreach ($PaymentsSAS as $indice2 => $FilePayments) {

                    if ($FilePayments != '') {

                        $FilePayments = explode(",", $FilePayments);

                        if ($FilePayments != '') {

                            if (
                                    (

                                    (strpos(strtolower($FilePayments[6]), 'processed') == true)

                                    )

                                    and trim(str_replace('"', '', $FilePayments[4])) != 0
                            ) {

                                $dataArray[] = [

                                    'idFile' => $AccountID->idfile,
                                    'depositDate' => str_replace('"', '', $FilePayments[3]),
                                    'amount' => str_replace('"', '', $FilePayments[4]),
                                ];
                            }
                        }
                    }
                }





                if ($dataArray != array()) {

                    $clientpaymentssas->insert($dataArray);
                }
            }

            /*

             * SAVE DATE SYNCRHONIZE

             */

            DB::update('UPDATE file SET dateSynchronize=NOW() WHERE idfile="' . $AccountID->idfile . '"');
        }

        if ($number_send != '0') {

            $errorSAS = '';
        } else {

            $errorSAS = 'Success';
        }

        /* 	

          DB::update("

          UPDATE

          `clientPayments`

          INNER JOIN

          (



          SELECT Total2.* FROM

          (

          SELECT Resultados.* ,

          IF(Resultados.statusCode='R',rank-1,rank) AS Conteo

          FROM

          (

          SELECT

          idFile,

          `depositDate`,

          statusCode,

          (

          CASE idFile

          WHEN @curType

          THEN @curRow := @curRow + 1

          ELSE @curRow := 1 AND @curType := idFile END

          ) + 0 AS rank

          FROM

          (

          SELECT  idFile,depositDate,statusCode

          FROM `clientPayments`

          WHERE  statusCode IN ('S','R') AND provider='SAS'

          ORDER BY  idFile,depositDate

          ) p,

          (SELECT @curRow := 0, @curType := '') r



          ) AS Resultados

          WHERE statusCode='R'

          ) AS Total

          INNER JOIN

          (

          SELECT

          idFile,

          `depositDate`,

          statusCode,

          (

          CASE idFile

          WHEN @curType2

          THEN @curRow2 := @curRow2 + 1

          ELSE @curRow2 := 1 AND @curType2 := idFile END

          ) + 0 AS rank

          FROM

          (

          SELECT  idFile,depositDate,statusCode

          FROM `clientPayments`

          WHERE  statusCode IN ('S','R')  AND provider='SAS'

          ORDER BY  idFile,depositDate

          ) p,

          (SELECT @curRow2 := 0, @curType2 := '') r



          ) AS Total2

          ON  Total.idfile=Total2.idfile AND Total.Conteo=Total2.rank

          ) AS Total3

          ON Total3.idfile=clientPayments.idfile AND clientPayments.`depositDate`=Total3.`depositDate`

          SET clientPayments.statusCode='R'

          "); */





        /*

          UPDATE PAYMENT NUMBER

         */

        $this->updatePaymentNumber();







        return view('form.commissionEntry.index', ['number_send' => $number_send, "Continue" => $errorSAS]);
    }

    /*

     * SYNCHRONIZE DATA SAS

     */

    public function synchronizePaymentsEPPS() {



        /*

         * set  maximun execution  time

         */


        /*         * ******************************* */

        $ClientPayments = new ClientPayments;

        $CommissionPayments = new CommissionPayments;



        $ListAccountID = DB::select("SELECT DATE_FORMAT(NOW(), '%Y%m%d'),f.idfile FROM `file` f

									WHERE provider='EPPS' AND ( DATE_FORMAT(`dateSynchronize`, '%Y%m%d')<>DATE_FORMAT(NOW(), '%Y%m%d') OR dateSynchronize IS NULL) ");


        foreach ($ListAccountID as $indice => $AccountID) {



            /*

             * DEPOSITE

             */

            $opts = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => "Content-Type: text/xml\r\n",
                    'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					   <FindEftByID xmlns="http://tempuri.org/">

						 <UserName>'. env('API_USER').'</UserName>

						 <PassWord>'.env('API_PASSWORD').'</PassWord>

						 <CardHolderID>' . $AccountID->idfile . '</CardHolderID>

					   </FindEftByID>

					 </soap:Body>

					</soap:Envelope>',
                    'timeout' => 60
                )
            );



            $context = stream_context_create($opts);

            $url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

            $resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);



            $resultPaymentsEPPS = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindEftByIDResponse xmlns="http://tempuri.org/">', '', $resultPaymentsEPPS);



            $resultPaymentsEPPS = str_replace('<FindEftByIDResult>', '', $resultPaymentsEPPS);



            $resultPaymentsEPPS = str_replace('</FindEftByIDResult>', '', $resultPaymentsEPPS);



            $resultPaymentsEPPS = str_replace('<Message>Success</Message></FindEftByIDResponse></soap:Body></soap:Envelope>', '', $resultPaymentsEPPS);



            $PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);





            if (count($PaymentsEPPS) > 1) {

                DB::delete("DELETE FROM clientPayments WHERE idfile='" . $AccountID->idfile . "' AND provider='EPPS'");

                $dataArray = array();



                foreach ($PaymentsEPPS as $indice2 => $FilePayments) {

                    if ($FilePayments->StatusCode != 'Voided') {

                        if ($FilePayments->StatusCode == 'Settled') {

                            $StatusCode = 'S';
                        }

                        if ($FilePayments->StatusCode == 'Create EFT Pending') {

                            $StatusCode = 'P';
                        }

                        if ($FilePayments->StatusCode == 'Voided') {

                            $StatusCode = 'V';
                        }

                        if ($FilePayments->StatusCode == 'Returned') {

                            $StatusCode = 'R';
                        }

                        if ($FilePayments->StatusCode == 'Prenote Fail Void') {

                            $StatusCode = 'PFV';
                        }

                        if ($FilePayments->StatusCode == 'Transmitted High-Risk') {

                            $StatusCode = 'THR';
                        }

                        if ($FilePayments->StatusCode == 'Prenote Fail') {

                            $StatusCode = 'PF';
                        }

                        $dataArray[] = [

                            'idFile' => $AccountID->idfile,
                            'depositDate' => $FilePayments->EftDate,
                            'amount' => str_replace(".", '', $FilePayments->EftAmount),
                            'provider' => 'EPPS',
                            'statusCode' => $StatusCode,
                            'eftTransactionID' => $FilePayments->EftTransactionID,
                            'statusCodeDescription' => $FilePayments->StatusCode,
                            'routingNumber' => $FilePayments->AccountNumber,
                            'accountNumber' => $FilePayments->RoutingNumber,
                            'lastMessage' => $FilePayments->LastMessage
                        ];
                    }
                }

                if ($dataArray != array()) {

                    $ClientPayments->insert($dataArray);
                }
            }



            /*             * ********************************************************************************************* */

            /*

             * COMMISSION

             */

            $opts = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => "Content-Type: text/xml\r\n",
                    'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					   <FindFeeByCardHolderID xmlns="http://tempuri.org/">

						 <UserName>'.env('API_USER').'</UserName>

						 <PassWord>'.env('API_PASSWORD').'</PassWord>

						 <CardHolderID>' . $AccountID->idfile . '</CardHolderID>

					   </FindFeeByCardHolderID>

					 </soap:Body>

					</soap:Envelope>',
                    'timeout' => 60
                )
            );



            $context = stream_context_create($opts);

            $url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

            $resultCommisionEPPS = file_get_contents($url, false, $context, -1, 10485760);



            $resultCommisionEPPS = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindFeeByCardHolderIDResponse xmlns="http://tempuri.org/">', '', $resultCommisionEPPS);



            $resultCommisionEPPS = str_replace('<FindFeeByCardHolderIDResult>', '', $resultCommisionEPPS);



            $resultCommisionEPPS = str_replace('</FindFeeByCardHolderIDResult>', '', $resultCommisionEPPS);



            $resultCommisionEPPS = str_replace('<Message>Success</Message></FindFeeByCardHolderIDResponse></soap:Body></soap:Envelope>', '', $resultCommisionEPPS);





            $CommisionEPPS = simplexml_load_string($resultCommisionEPPS);





            if (count($CommisionEPPS) > 1) {

                DB::delete("DELETE FROM commissionPayments WHERE idfile='" . $AccountID->idfile . "' AND provider='EPPS'");



                $dataArray = array();



                foreach ($CommisionEPPS as $indice2 => $Commission) {

                    if ($Commission->StatusCode == 'Transmitted' or $Commission->StatusCode == 'Pending' or $Commission->StatusCode == 'Settled') {

                        if ($Commission->StatusCode == 'Transmitted') {

                            $StatusCode = 'T';
                        }

                        if ($Commission->StatusCode == 'Pending') {

                            $StatusCode = 'P';
                        }

                        if ($Commission->StatusCode == 'Settled') {

                            $StatusCode = 'S';
                        }

                        if ($Commission->Party == 'EPPS') {

                            $PaidToName = 'EPPS';
                        } else {

                            $PaidToName = ($Commission->PaidToName == 'Debt Assistance Network')? 'DAN':$Commission->PaidToName;
                        }

                        $dataArray[] = [

                            'idFile' => $AccountID->idfile,
                            'payoutDate' => $Commission->Fee_Date,
                            'amount' => str_replace(".", '', $Commission->FeeAmount),
                            'affiliateCompanyName' => $PaidToName,
                            'provider' => 'EPPS',
                            'statusCode' => $StatusCode
                        ];
                    }
                }

                if ($dataArray != array()) {

                    $CommissionPayments->insert($dataArray);
                }
            }







            /*

             * SAVE DATE SYNCRHONIZE

             */

            DB::update('UPDATE file SET dateSynchronize=NOW() WHERE idfile="' . $AccountID->idfile . '"');

            /*

             * SAVE BALANCE

             */

            $AccountInfo = $this->FindAccountBalance($AccountID->idfile);

            foreach ($AccountInfo as $indice2 => $Info) {

                DB::update('UPDATE file SET balanceEPPS=' . str_replace(".", '', (string) $Info->AccountBalance) . ' WHERE idfile="' . $AccountID->idfile . '"');
            }
        }



        DB::update('UPDATE commissionPayments CP

							INNER JOIN affiliateCompany  AC ON

							TRIM(REPLACE(CP.affiliateCompanyName," ",""))=TRIM(REPLACE(CONCAT(AC.affiliateCompanyName)," ",""))

							SET CP.affiliateCompanyId=AC.affiliateCompanyId

							WHERE CP.affiliateCompanyId IS NULL

									');

        $CompanyNameEmpty = DB::select("SELECT `affiliateCompanyName` FROM  commissionPayments 

					 WHERE provider='EPPS' AND `affiliateCompanyId` IS NULL

									");





        DB::insert("		

						INSERT INTO `commentCommissionPayments`

						(

							`idFile`,

							`dateTransaction`,

							`affiliateCompanyId`

						)

						SELECT DISTINCT

							c .idFile,

							c.`payoutDate`,

							c.`affiliateCompanyId`

						FROM `commissionPayments` c 

						LEFT JOIN `commentCommissionPayments` ccp

						ON c.idFile= ccp.idFile AND c.`payoutDate`= ccp.`dateTransaction` AND c.`affiliateCompanyId`= ccp.`affiliateCompanyId`

						WHERE c.affiliateCompanyId IS NOT NULL AND ccp.idFile IS NULL AND 

							  ccp.`dateTransaction` IS NULL AND ccp.`affiliateCompanyId` IS NULL");





        /*

          UPDATE PAYMENT NUMBER

         */

        $this->updatePaymentNumber();



        /*         * ********************************************************************************************* */

        $page = Input::get('page', 1);

        $paginate = 100;

        /*

         * QUERY

         */

        $data = DB::select($this->queryErrorCompany());

        if (isset($data)) {

            $offSet = ($page * $paginate) - $paginate;



            $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);



            $itemsFile = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page, ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
        }



        if ($itemsFile->count() > 0) {

            $errorEPPS = 'EPPS_Y';
        } else {

            $errorEPPS = 'Success';
        }

        return view('form.commissionEntry.index', ["itemsFile" => $itemsFile, "Continue" => $errorEPPS]);
    }

    public function updatePaymentNumber() {

        /*

         * UPDATE PAYMENT NUMBER

         */

        DB::insert("

					INSERT INTO commentClientPayments

					(

						idFile, 

						dateTransation, 

						COMMENT

					)

					SELECT 

					DISTINCT 

					c .idFile,

					c.depositDate,

					MIN(statusCodeDescription)

					FROM clientPayments c 

					LEFT JOIN commentClientPayments ccp

					ON c.idFile= ccp.idFile AND c.depositDate= ccp.dateTransation

					WHERE 

					statusCodeDescription IS NOT NULL AND ccp.dateTransation IS NULL

					GROUP BY c .idFile,c.depositDate

					");



        DB::update("

				UPDATE `commentClientPayments`

				SET paymentNumber=NULL 

				WHERE COALESCE(manualPaymentNumber,'')<>'Y'");



        DB::update("

				

				UPDATE `commentClientPayments` c

				INNER JOIN 

				(

					SELECT   

						  idFile,

						  `depositDate`,

						 ( 

							CASE idFile 

							WHEN @curType 

							THEN @curRow := @curRow + 1

							ELSE @curRow := 1 AND @curType := idFile END

						  ) + 0 AS rank

					FROM  

					(

						SELECT  clientPayments.idFile,depositDate

						FROM `clientPayments`

						INNER JOIN commentClientPayments 

						ON clientPayments.idfile=commentClientPayments.idfile and 

						clientPayments.depositDate=commentClientPayments.dateTransation

						WHERE  statusCode='S' AND COALESCE(manualPaymentNumber,'')<>'Y'

						ORDER BY idFile,depositDate

					) p,

						  (SELECT @curRow := 0, @curType := '') r

				

				) AS t

				ON c.idfile=t.idfile AND c.dateTransation=t.`depositDate`

				SET `paymentNumber`=(rank-1)



			 ");
    }

    public function updatePaymentNumberCommission() {



        DB::insert("INSERT INTO `commentCommissionPayments`

			(

				`idFile`,

				`dateTransaction`,

				`affiliateCompanyId`

			)

			SELECT DISTINCT

				c .idFile,

				c.`payoutDate`,

				c.`affiliateCompanyId`

			FROM `commissionPayments` c 

			LEFT JOIN `commentCommissionPayments` ccp

			ON c.idFile= ccp.idFile AND c.`payoutDate`= ccp.`dateTransaction` AND c.`affiliateCompanyId`= ccp.`affiliateCompanyId`

			WHERE c.affiliateCompanyId IS NOT NULL AND ccp.idFile IS NULL AND 

				  ccp.`dateTransaction` IS NULL AND ccp.`affiliateCompanyId` IS NULL; ");



        DB::update("UPDATE `commentCommissionPayments`

			SET `paymentNumber`=NULL;");





        DB::update("UPDATE `commentCommissionPayments` cp2

				INNER JOIN 

				(

			

					SELECT   

						idfile, payoutDate,affiliateCompanyId, amount,

							 ( 

							CASE idFile

							WHEN @curType 

							THEN @curRow := @curRow + 1 

							ELSE @curRow := 1 AND @curType :=idFile END

							  ) + -1 AS rank2

						FROM  

						(

							SELECT `idFile`,`payoutDate`,affiliateCompanyId,SUM(amount) AS amount

							FROM `commissionPayments`

							WHERE statusCode='T' 

							AND affiliateCompanyId='24' 

							AND affiliateCompanyId IS NOT NULL 

							GROUP BY `idFile`,`payoutDate`,affiliateCompanyId

							ORDER BY`idFile`,`payoutDate` 			

						) p,

							  (SELECT @curRow := 0, @curType := '') r

						ORDER BY  idFile,affiliateCompanyId, payoutDate

				) AS T2

					ON cp2.idfile=T2.idfile AND  

					T2.payoutDate=cp2.`dateTransaction` AND 

					cp2.`affiliateCompanyId`=T2.`affiliateCompanyId`

				SET `paymentNumber`=rank2;");





        DB::update("UPDATE `commentCommissionPayments`

			INNER JOIN (

				SELECT otrasCommisiones.idFile,

				otrasCommisiones.dateTransaction,

				otrasCommisiones.affiliateCompanyId,

				commisionDAN.`paymentNumber`  AS paymentNumber2

				FROM 

				(

					SELECT * FROM `commentCommissionPayments`

					WHERE `affiliateCompanyId`='24'

				) AS commisionDAN

				INNER JOIN 

				(

					SELECT * FROM `commentCommissionPayments`

					WHERE `affiliateCompanyId`<>'24'

				) AS otrasCommisiones

				ON commisionDAN.idfile=otrasCommisiones.idfile AND  

				commisionDAN.`dateTransaction`=otrasCommisiones.`dateTransaction` 

			)AS T

			ON

			T.idFile=commentCommissionPayments.idfile AND 

			T.dateTransaction=commentCommissionPayments.dateTransaction AND 

			T.affiliateCompanyId=commentCommissionPayments.affiliateCompanyId

			SET `paymentNumber`=paymentNumber2;	");
    }

    public function queryErrorCompany() {

        $query = '

				SELECT DISTINCT 

				"COM" AS type ,

				"" AS payment_date,

				CONCAT(file.firstName, " ", file.lastName) AS client,

				"" AS commission,

				affiliateCompanyName AS company,

				commissionPayments.idFile AS matchClient,

				"N" AS matchCompany,

				"Y" AS matchIdClient,

				NULL AS matchIdCompany,

				"N" AS duplicate

				FROM commissionPayments

				INNER JOIN `file`  ON commissionPayments.idFile=file.idFile

				WHERE commissionPayments.provider="EPPS"	AND commissionPayments.affiliateCompanyId IS NULL	

			';

        return $query;
    }

    /*

     * FUNCTION UPLOAD FILE

     */

    public function uploadFile($request) {

        /*

         * set  maximun execution  time

         */

        ini_set('max_execution_time', 720);

        /*         * ******************************* */

        DB::table('uploadFile')->delete();

        DB::table('uploadCommissionFile')->delete();

        if ($request->file('imported-file')) {

            $path = $request->file('imported-file')->getRealPath();

            $data = Excel::load($path, function($reader) {
                        
                    })->get();



            $NameFile = explode(".", $request->file('imported-file')->getClientOriginalName());

            $Extension = end($NameFile);



            if (!empty($data) && $data->count()) {

                $UploadFile = new UploadFile;

                $UploadFile->uploadDate = date('Y-m-d H:i:s');

                $UploadFile->save();

                $idFile = $UploadFile->id;



                /*

                 * insert row

                 */

                $UploadCommissionFile = new UploadCommissionFile;

                /*                 * ************** */

                if (count($data->toArray()) > 10) {

                    $RowTotal = $data->toArray();
                } else {

                    $RowTotal = $data->toArray();

                    $RowTotal = $RowTotal[0];
                }

                /*                 * ************** */

                foreach ($RowTotal as $row) {



                    if (!empty($row) and $row['payment_date'] != '') {

                        $commission = str_replace(' ', '', trim($row['commission']));

                        if ($commission == '$-') {

                            $commission = '';
                        } else {

                            $commission = $commission;
                        }

                        if (isset($row['type'])) {

                            $type = $row['type'];
                        } else {

                            $type = $row[0];
                        }

                        /*

                         * ONLY COMMISSION

                         */

                        $NewDate = explode("/", $row['payment_date']);

                        if (count($NewDate) > 0) {

                            $NewDate3 = strtotime($row['payment_date']);

                            $NewDate2 = date('Y-m-d', $NewDate3);
                        } else {

                            $NewDate2 = $row['payment_date']->format('Y-m-d');
                        }







                        if ($type != 'DEP') {

                            $dataArray[] = $data = [

                                'type' => $type,
                                'payment_date' => $NewDate2,
                                'client' => $row['client'],
                                'commission' => $commission,
                                'company' => $row['company'],
                                'iduploadFile' => $idFile
                            ];
                        }
                    }
                }

                $UploadCommissionFile->insert($dataArray);

                /*

                 * Match client

                 */

                DB::update('UPDATE uploadCommissionFile UCF

									LEFT JOIN file  F ON TRIM(REPLACE(UCF.client," ",""))=TRIM(REPLACE(CONCAT(lastName,firstName)," ",""))

									SET matchClient=IF(F.lastName IS NULL,"N","Y"), UCF.matchIdClient=F.idFile,

									UCF.matchCompany=IF(UCF.company IS NULL,"Y",NULL)

									

									WHERE iduploadFile=?  AND provider IS NOT NULL

									', [$idFile]);



                DB::update('UPDATE uploadCommissionFile

								INNER JOIN clientPayments

								ON clientPayments.idFile=uploadCommissionFile.matchIdClient

								AND clientPayments.depositDate=uploadCommissionFile.payment_date

								AND clientPayments.amount=uploadCommissionFile.commission

								SET duplicate="Y"

								WHERE uploadCommissionFile.type="DEP" and iduploadFile=? ', [$idFile]);

                /*

                 * Match company

                 */

                DB::update('UPDATE  uploadCommissionFile UCF

							LEFT JOIN affiliateCompany  AC ON TRIM(REPLACE(UCF.company," ",""))=TRIM(REPLACE(CONCAT(affiliateCompanyName)," ",""))

							SET matchCompany=IF(AC.affiliateCompanyName IS NULL,"N","Y"), matchIdCompany=AC.affiliateCompanyId

							WHERE iduploadFile=? and type="COM"

							', [$idFile]);



                DB::update('UPDATE uploadCommissionFile

								INNER JOIN commissionPayments

								ON commissionPayments.payoutDate=uploadCommissionFile.payment_date

								AND commissionPayments.amount=IF(uploadCommissionFile.commission="",0,uploadCommissionFile.commission)

								AND commissionPayments.affiliateCompanyId=uploadCommissionFile.matchIdCompany

								AND commissionPayments.idfile=uploadCommissionFile.matchIdClient

								SET duplicate="Y"

								WHERE uploadCommissionFile.type="COM" and iduploadFile=? ', [$idFile]);

                /*

                 *  Count row

                 */

                $Continue = DB::select("SELECT IF(COUNT(*)=0,'Y','N') AS Continue2

									 FROM uploadCommissionFile

									 WHERE iduploadFile=? AND (matchClient='N' and type='DEP') OR (matchCompany='N'  and type='COM')", [$idFile]);

                $Continue = $Continue[0]->Continue2;



                /*

                 * Return row BD ->where('matchClient','N')->orwhere('matchCompany','N')

                 */

                if ($Continue != 'Y') {

                    $itemsFile = DB::table('uploadCommissionFile')->select('id', 'type', DB::raw('DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date'), 'client', 'commission', 'company', 'iduploadfile', 'matchClient', 'matchCompany', 'matchIdClient', 'matchIdCompany', 'duplicate')
                            ->where('iduploadFile', $idFile)
                            ->where(function($q) {

                                $q->where('matchClient', 'N')
                                ->orWhere('matchCompany', 'N');
                            })
                            ->orderByRaw('IF(matchIdClient IS NULL OR matchIdCompany IS NULL,NULL,matchIdClient)')
                            ->orderBy(DB::raw('DATE_FORMAT(payment_date, "%Y/%m/%d")'))
                            ->orderBy("client")
                            ->paginate(20);
                } else {



                    $itemsFile = DB::table('uploadCommissionFile')->select('id', 'type', DB::raw('DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date'), 'client', DB::raw('FORMAT(commission/100, 2) AS commission'), 'company', 'iduploadfile', 'matchClient', 'matchCompany', 'matchIdClient', 'matchIdCompany', 'duplicate')
                            ->where('iduploadFile', $idFile)
                            ->orderByRaw('IF(matchIdClient IS NULL OR matchIdCompany IS NULL,NULL,matchIdClient)')
                            ->orderBy(DB::raw('DATE_FORMAT(payment_date, "%Y/%m/%d")'))
                            ->orderBy("client")
                            ->paginate(20);
                }

                Session::put('idFile', $idFile);

                Session::put('Continue', $Continue);





                return view('form.commissionEntry.index', ["itemsFile" => $itemsFile, "Continue" => $Continue]);
            }
        } else {

            return view('form.commissionEntry.index', ["Mensaje" => "please select a file"]);
        }
    }

    /**

     * INSERT ROW VALID

     */
    public function export() {





        DB::insert('INSERT INTO `crmlexco_dan`.`commissionPayments` 

				(

				`idFile`, 

				`payoutDate`, 

				`amount`, 

				`affiliateCompanyId`,

				 provider,

				 statusCode

				)

				SELECT 

					matchIdClient, 

					payment_date,

					IF(commission="","0",commission),

					`matchIdCompany`,

					"SAS",

					"T"

				FROM

				uploadCommissionFile

				WHERE iduploadfile=? AND TYPE="COM" AND COALESCE(duplicate,"")<>"Y" AND matchClient="Y";

		  ', [Session::get('idFile')]);



        /*

          DB::insert('CALL SpInsertCommssionPayments(?)', [Session::get('idFile')]);

         */

        DB::table('uploadFile')->delete();

        DB::table('uploadCommissionFile')->delete();



        return view('form.commissionEntry.index', ["Continue" => 'Success']);
    }

    public function FindAccountBalance($searchClient) {

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-Type: text/xml\r\n",
                'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					   <FindCardHolderByID xmlns="http://tempuri.org/">

						 <UserName>'. env('API_USER').'</UserName>

						 <PassWord>'.env('API_PASSWORD').'</PassWord>

						 <CardHolderID>' . $searchClient . '</CardHolderID>

					   </FindCardHolderByID>

					 </soap:Body>

					</soap:Envelope>',
                'timeout' => 60
            )
        );



        $context = stream_context_create($opts);

        $url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

        $resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);



        $resultPaymentsEPPS = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindCardHolderByIDResponse xmlns="http://tempuri.org/">', '', $resultPaymentsEPPS);



        $resultPaymentsEPPS = str_replace('<FindCardHolderByIDResult>', '', $resultPaymentsEPPS);



        $resultPaymentsEPPS = str_replace('</FindCardHolderByIDResult>', '', $resultPaymentsEPPS);



        $resultPaymentsEPPS = str_replace('<Message>Success</Message></FindCardHolderByIDResponse></soap:Body></soap:Envelope>', '', $resultPaymentsEPPS);



        $PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);



        return $PaymentsEPPS;
    }

}

/*



UPDATE `file`

SET dateChangeProvider=NOW() + INTERVAL 3 DAY

WHERE provider='EPPS';



UPDATE  `file`

SET datesynchronize=DATE_FORMAT(NOW()- INTERVAL 3 DAY, '%Y%m%d')

WHERE provider='epps'





 INSERT INTO commentClientPayments

		(

			idFile, 

			dateTransation, 

			COMMENT

		)

		SELECT 

		DISTINCT 

		c .idFile,

		c.depositDate,

		MIN(statusCodeDescription)

		FROM clientPayments c 

		LEFT JOIN commentClientPayments ccp

		ON c.idFile= ccp.idFile AND c.depositDate= ccp.dateTransation

		WHERE 

		statusCodeDescription IS NOT NULL AND ccp.dateTransation IS NULL;

		GROUP BY c .idFile,c.depositDate

		

 UPDATE `commentClientPayments`

	SET paymentNumber=NULL and COALESCE(manualPaymentNumber,'')<>'Y';

	

UPDATE `commentClientPayments` c

	INNER JOIN 

	(

		SELECT   

			  idFile,

			  `depositDate`,

			 ( 

				CASE idFile 

				WHEN @curType 

				THEN @curRow := @curRow + 1

				ELSE @curRow := 1 AND @curType := idFile END

			  ) + 0 AS rank

		FROM  

		(

			SELECT  clientPayments.idFile,depositDate

			FROM `clientPayments`

			INNER JOIN commentClientPayments 

			ON clientPayments.idfile=commentClientPayments.idfile and 

			clientPayments.depositDate=commentClientPayments.dateTransation

			WHERE  statusCode='S' AND COALESCE(manualPaymentNumber,'')<>'Y'

			ORDER BY idFile,depositDate

		) p,

			  (SELECT @curRow := 0, @curType := '') r

	

	) AS t

	ON c.idfile=t.idfile AND c.dateTransation=t.`depositDate`

	SET `paymentNumber`=(rank-1);

































INSERT INTO `commentCommissionPayments`

(

	`idFile`,

	`dateTransaction`,

	`affiliateCompanyId`

)

SELECT DISTINCT

	c .idFile,

	c.`payoutDate`,

	c.`affiliateCompanyId`

FROM `commissionPayments` c 

LEFT JOIN `commentCommissionPayments` ccp

ON c.idFile= ccp.idFile AND c.`payoutDate`= ccp.`dateTransaction` AND c.`affiliateCompanyId`= ccp.`affiliateCompanyId`

WHERE c.affiliateCompanyId IS NOT NULL AND ccp.idFile IS NULL AND 

	  ccp.`dateTransaction` IS NULL AND ccp.`affiliateCompanyId` IS NULL;

	  

UPDATE `commentCommissionPayments`

SET `paymentNumber`=NULL;





UPDATE `commentCommissionPayments` cp2

	INNER JOIN 

	(



		SELECT   

			idfile, payoutDate,affiliateCompanyId, amount,

				 ( 

				CASE idFile

				WHEN @curType 

				THEN @curRow := @curRow + 1 

				ELSE @curRow := 1 AND @curType :=idFile END

				  ) + -1 AS rank2

			FROM  

			(

				SELECT `idFile`,`payoutDate`,affiliateCompanyId,SUM(amount) AS amount

				FROM `commissionPayments`

				WHERE statusCode='T' AND affiliateCompanyId='24' AND affiliateCompanyId IS NOT NULL AND (amount>0 OR (amount=0 AND manualUpload='Y'))

				GROUP BY `idFile`,`payoutDate` 

				ORDER BY`idFile`,`payoutDate` 			

			) p,

				  (SELECT @curRow := 0, @curType := '') r

			ORDER BY  idFile,affiliateCompanyId, payoutDate

	) AS T2

		ON cp2.idfile=T2.idfile AND  

		T2.payoutDate=cp2.`dateTransaction` AND 

		cp2.`affiliateCompanyId`=T2.`affiliateCompanyId`

	SET `paymentNumber`=rank2;





UPDATE `commentCommissionPayments`

INNER JOIN (

	SELECT otrasCommisiones.idFile,

	otrasCommisiones.dateTransaction,

	otrasCommisiones.affiliateCompanyId,

	commisionDAN.`paymentNumber`  AS paymentNumber2

	FROM 

	(

		SELECT * FROM `commentCommissionPayments`

		WHERE `affiliateCompanyId`='24'

	) AS commisionDAN

	INNER JOIN 

	(

		SELECT * FROM `commentCommissionPayments`

		WHERE `affiliateCompanyId`<>'24'

	) AS otrasCommisiones

	ON commisionDAN.idfile=otrasCommisiones.idfile AND  

	commisionDAN.`dateTransaction`=otrasCommisiones.`dateTransaction` 

)AS T

ON

T.idFile=commentCommissionPayments.idfile AND 

T.dateTransaction=commentCommissionPayments.dateTransaction AND 

T.affiliateCompanyId=commentCommissionPayments.affiliateCompanyId

SET `paymentNumber`=paymentNumber2;



*/

