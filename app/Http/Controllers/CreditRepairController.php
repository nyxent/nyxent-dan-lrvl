<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use App\CreditRepairDocument;
use View;
use Input;
use Session;
Use Redirect;
use DateTime;
use ZipArchive;
use App\Client;
use Response;
use App\vwCreditRepairClient;

class CreditRepairController extends Controller {

    public function index(Request $request) {

        $keyword = $request->get('keyword');
        $from = $request->get('from');
        $to = $request->get('to');
        $checked = $request->get('checked');
        $sortField = $request->get('sortField');
        $sortOrder = $request->get('sortOrder');
        $perPage = 10;


//        $query = "
//        SELECT
//            f.idFile,
//            f.firstName,
//            f.lastName,
//            f.provider,
//            f.contractSignDateCr,
//            c.idUser,
//            c2.idUser as sands,
//            c2.creationDate as sandsCreationDate,
//            c.creationDate as goldCreationDate,
//            DATE_FORMAT(f.enrollmentApprovedDate, '%m/%d/%Y') AS enrollmentApprovedDate,
//            s.name AS statusName,
//            a.hideInfoDonna1 as hide,
//            u.hideInfoDonna1 as userHide
//        FROM
//        
//        file f
//        INNER JOIN fileStatus s ON f.idFileStatus = s.idFileStatus
//    
//        LEFT JOIN fileChecked c ON f.idFile = c.idFile AND c.idUser = 170
//    
//        LEFT JOIN fileChecked c2 ON f.idFile = c2.idFile AND c2.idUser = 140
//        AND c2.idFile NOT IN (SELECT idFile from fileChecked WHERE idUser = 170)
//    
//        LEFT JOIN affiliateCompany a ON a.affiliateCompanyId = f.affiliateCompanyId
//	
//        LEFT JOIN user AS u ON u.idUser = f.fileOwner
//    
//        WHERE
//            f.enrollmentApprovedDate IS NOT NULL
//            AND f.enrollmentApprovedDate <> '1900-01-01'
//            AND (f.enrollmentApprovedDate < DATE_SUB(NOW(),INTERVAL 730 DAY) OR f.idFile IN (SELECT idFile from fileChecked WHERE idUser =170 OR idUser =140))
//            AND f.idFileStatus NOT IN (11,17)
//            ";
        
        

        $query = vwCreditRepairClient::query();

        if (!empty($keyword)) {
//            $query = $query . " AND (
//				f.firstName LIKE '%" . $keyword . "%'
//				OR f.lastName LIKE '%" . $keyword . "%'
//				OR CONCAT(f.firstName, ' ', f.lastName) LIKE '%" . $keyword . "%'
//				OR f.idFile LIKE '%" . $keyword . "%'
//				OR f.idFileDAN LIKE '%" . $keyword . "%'
//				OR f.email LIKE '%" . $keyword . "%'
//				OR f.homePhone LIKE '%" . $keyword . "%'
//				OR f.businessPhone LIKE '%" . $keyword . "%'
//				OR f.cellPhone LIKE '%" . $keyword . "%') ";
            
            $query->where(function ($subQ) use ($keyword){
                $subQ->where('firstName','like',"%{$keyword}%");
                $subQ->orWhere('lastName','like',"%{$keyword}%");
                $subQ->orWhereRaw("CONCAT(firstName, ' ', lastName) LIKE '%{$keyword}%'");
                $subQ->orWhere('idFile','like',"%{$keyword}%");
                $subQ->orWhere('idFileDAN','like',"%{$keyword}%");
                $subQ->orWhere('email','like',"%{$keyword}%");
                $subQ->orWhere('homePhone','like',"%{$keyword}%");
                $subQ->orWhere('businessPhone','like',"%{$keyword}%");
                $subQ->orWhere('cellPhone','like',"%{$keyword}%");
            });
            
            
        }

        if (!empty($checked) && $checked == 1) {
//            $query = $query . "AND c.idUser is not NULL ";
            $query->whereNotNull('idUser');
        } elseif (!empty($checked) && $checked == 2) {
//            $query = $query . "AND c.idUser is NULL ";
            $query->whereNull('idUser');
        } elseif (!empty($checked) && $checked == 3) {
//            $query = $query . "AND c.idUser is NULL AND c2.idUser = 140 ";
            $query->whereNull('idUser');
            $query->where('sands',140);
        }
        // dd($query);
        if (!empty($from)) {
            $dateFrom = DateTime::createFromFormat('m/d/Y', $from);
//            $query = $query . "AND f.enrollmentApprovedDate >= '" . $dateFrom->format('Y-m-d') . "' ";
            $query->where('enrollmentApprovedDate','>=',$dateFrom);
        }
        if (!empty($to)) {
            $dateTo = DateTime::createFromFormat('m/d/Y', $to);
//            $query = $query . "AND f.enrollmentApprovedDate <= '" . $dateTo->format('Y-m-d') . "' ";
            $query->where('enrollmentApprovedDate','<=',$dateTo);
        }
        if (!empty($sortField)) {
//            $query = $query . "ORDER BY " . $sortField . " " . $sortOrder;
            $query->orderBy($sortField,$sortOrder);
        } else {
//            $query = $query . "ORDER BY f.enrollmentApprovedDate ASC";
            $query->orderBy('sentDate','DESC');
        }

//        $clients = \DB::select($query, [1]);
//        $clients = $this->arrayPaginator($clients, $request);
        $clients = $query->paginate(20);

        return View::make('creditrepair.index')->with('clients', $clients);
        ;
    }

    public function arrayPaginator($array, $request) {
        $page = Input::get('page', 1);
        $perPage = 20;
        $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function documents(Request $request) {
        $documents = CreditRepairDocument::where('idFile', Session::get('idFile'))->get();
        return view('creditrepair.documents', compact('documents'));
    }

    public function saveDocument(Request $request) {
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $extension = $file->extension();
                $originalName = $file->getClientOriginalName();
                $fileName = "file_" . time() . "." . $extension;
                $path = $file->storeAs($request->idFile, $fileName, 'server');
                if ($path) {
                    $_document = CreditRepairDocument::create([
                                'idFile' => $request->idFile,
                                'name' => $request->name,
                                'fileName' => $fileName
                    ]);
                }
            }
        }
        return redirect()->back();
    }

    public function deleteDocument($id) {
        CreditRepairDocument::destroy($id);
        return redirect()->back();
    }

    public function downloadArchiveZip($idFile) {
        $client = Client::find($idFile);
        if (count($client->creditRepairDocuments)) {
            $_urlBase = base_path() . "/../public_html/dan_dev/creditRepairDocuments";
            $zipFileName = 'files_' . time() . '.zip';
            $zipFile = $_urlBase . '/zipFiles/' . $zipFileName;
            $zip = new ZipArchive();
            if ($zip->open($zipFile, ZipArchive::CREATE) === TRUE) {
                foreach ($client->creditRepairDocuments as $archive) {
                    $filePath = $_urlBase . '/' . $archive->idFile . '/' . $archive->fileName;
                    $zip->addFile($filePath, $archive->fileName);
                }
                // Close ZipArchive     
                $zip->close();
                return Response::download($zipFile, $zipFileName);
            } else {
                echo "Zip not open<br/><br/>";
            }
        }
        echo "<h1>File error</h1>";
    }

    public function saveCSDate(Request $request){
        
        $client = Client::find($request->idFile);
        if($client){
            $client->contractSignDateCr = $request->csDate ? DateTime::createFromFormat('m/d/Y', $request->csDate)->format('Y-m-d'): null;
            $client->save();
            echo $request->csDate ? $request->csDate : ' -- ';
        }
    }
    
}
