<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Controllers\CommissionPaymentsController;
use App\ClientPayments;
use App\CommissionPayments;
use App\RescheduleEFT;
use App\ActiveLog;
use App\Client;
use App\History;
use DateTime;
use App\Http\Controllers\CommissionEntryController;


class ReactivateUserController extends BaseSoapController
{
	public function reactivateUser(Request $request){
		
		$idClient = Session::get('idFilesearch');
		$client = Client::find($idClient);
		$payments =DB::select("SELECT * FROM paymentsStatus WHERE idFile= ".$idClient." AND scheduledPaymentDate >= '".$request->date."'");		
		$this->service = InstanceSoapClient::init();
		$count=0;
		foreach($payments as $p){

			$newDate = date("Y-m-d\TH:i:s",strtotime($request->new.' +'.$count.' month'));			
			
			$amount = $p->paymentFinder + $p->paymentSuperAffiliate + $p->paymentAffiliate + $p->paymentNyxent + $p->paymentDAN + $p->paymentSAS + $p->paymentUSMarketers +$p->paymentKC
			+ $p->paymentFinder2 + $p->paymentSuperAffiliate2;
			
			
			$eftData = array(
				'UserName' => env('API_USER'),
				'PassWord' => env('API_PASSWORD'),
				'CardHolderID' => $client->idFile,
				'EftDate' => $newDate,
				'EftAmount' => $amount,
				'EftFee' => 0,
				'BankName' => $client->ppBankName,
				'BankCity' => '',
				'BankState' => '',
				'AccountNumber' => $client->ppAccountNumber,
				'RoutingNumber' => $client->ppRoutingNumber,
				'AccountType' => 'Checking',
				'Memo' => ''
			);
			$this->service->AddEft($eftData	);
			
			//Commision Affiliate
			$company =DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID =".$p->affiliateCompanyId)[0];
			$amount = $p->paymentAffiliate;
			$feeData = array(
				'UserName' => env('API_USER'),
				'PassWord' => env('API_PASSWORD'),
				'CardHolderID' => $client->idFile,
				'FeeDate' => $newDate,
				'FeeAmount' => $amount,
				'Description' => '',
				'FeeType' => 'SettlementPayment',
        'PaidToName' => $company->affiliateCompanyName,
        'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
        'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
        'PaidToStreet2' => '',
        'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
        'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
        'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
				'ContactName' => '',
				'PaidToCustomerNumber' => ''
			);
			$this->service->AddFee($feeData	);
			// var_dump($feeData);
			
			//Commision KC
			if($p->paymentKC != 0){
				$company =DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 29")[0];
				$amount = $p->paymentKC;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData	);
				// var_dump($feeData);
			}
					
			//Commision NYXENT
			if($p->paymentNyxent != 0){
				$company =DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 24")[0];
				$amount = $p->paymentNyxent;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision DAN
			if($p->paymentDAN != 0){
				$company =DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 21")[0];
				$amount = $p->paymentDAN;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision USMARKETERS
			if($p->paymentUSMarketers != 0){
				$company =DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 16")[0];
				$amount = $p->paymentUSMarketers;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision SUPER AFFILIATE
			if($p->paymentSuperAffiliate != 0){
				$company =DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
				LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$p->superAffiliateId)[0];
				$amount = $p->paymentSuperAffiliate;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision FINDER
			if($p->paymentFinder != 0){
				$company =DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
				LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$p->finderId)[0];
				$amount = $p->paymentFinder;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision SUPER AFFILIATE 2
			if($p->paymentSuperAffiliate2 != 0){
				$company =DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
				LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$p->superAffiliateId2)[0];
				$amount = $p->paymentSuperAffiliate2;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}
			
			//Commision FINDER 2
			if($p->paymentFinder2 != 0){
				$company =DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
				LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$p->finderId2)[0];
				$amount = $p->paymentFinder2;
				$feeData = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID' => $client->idFile,
					'FeeDate' => $newDate,
					'FeeAmount' => $amount,
					'Description' => '',
					'FeeType' => 'SettlementPayment',
					'PaidToName' => $company->affiliateCompanyName,
					'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
					'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
					'PaidToStreet2' => '',
					'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
					'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
					'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
					'ContactName' => '',
					'PaidToCustomerNumber' => ''
				);
				$this->service->AddFee($feeData);
				// var_dump($feeData);
			}			
			$count++; 
		}

		$client->idFileStatus = 24;
		$client->save();

		/* save into active log*/
		$data = array(
			'idFile' =>$client->idFile,
			'title' => 'Flow change to Reinstated',
			'detail' => 'Flow change to Reinstated',
			'idUser' => Session::get('iduser')
		);
		$log = new History($data);
		$log->save();	

		echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
		exit; 		
	}		
}