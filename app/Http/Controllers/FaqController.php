<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Faq;
use App\FaqCategory;
use View;
use Input;
use Session;
Use Redirect;

class FaqController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return Response
  */
  public function index(Request $request)
  {
    $keyword = $request->get('search');
    $idFaqCategory = $request->get('idFaqCategory');
		$perPage = 10;
    $categories = FaqCategory::orderBy('name')->get();
    $faqQuery = Faq::join('faqCategory','faq.idFaqCategory','=','faqCategory.idFaqCategory');
    if (!empty($idFaqCategory)) {
      $faqQuery = $faqQuery->where('faq.idFaqCategory','=',$idFaqCategory);
    }

		if (!empty($keyword)) {
      $faqQuery = $faqQuery->where(function($query) use ($keyword){
        $query->where('faq.question', 'LIKE', "%$keyword%");
        $query->orWhere('faq.answer', 'LIKE', "%$keyword%");
        $query->orWhere('faq.tags', 'LIKE', "%$keyword%");
      });
    } 

    
    $faqs = $faqQuery->orderByRaw('faqCategory.name,ISNULL(faq.order), faq.order ASC, faq.question')->select('faq.*')->paginate($perPage);
    
    // load the view and pass the faqs
    return view('faqs.index',compact('faqs','categories'));
  }
  
  /**
  * Show the form for creating a new resource.
  *
  * @return Response
  */
  public function create()
  {
    // load the create form (app/views/faqs/create.blade.php)
    $categories = FaqCategory::orderBy('name')->pluck('name', 'idFaqCategory');
    return view('faqs.create',compact('categories'));
    
  }
  
  /**
  * Store a newly created resource in storage.
  *
  * @return Response
  */
  public function store()
  {
    // validate
    // read more on validation at http://laravel.com/docs/validation
    $rules = array(
      'question'       => 'required'
    );
    $validator = Validator::make(Input::all(), $rules);
    
    // process the login
    if ($validator->fails()) {
      return Redirect::to('faqs/create')
      ->withErrors($validator)
      ->withInput(Input::except('password'));
    } else {
      // store
      $faq = new Faq;
      $faq->idFaqCategory = Input::get('idFaqCategory');
      $faq->question = Input::get('question');
      $faq->answer = Input::get('answer');
      $faq->tags = Input::get('tags');
      $faq->order = Input::get('order');
      $faq->save();
      
      // redirect
      Session::flash('message', 'FAQ Successfully created!');
      return Redirect::to('faqs');
    }
  }
  
  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
    //
  }
  
  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return Response
  */
  public function edit($id)
  {
    // get the faq
    $faq = Faq::find($id);
    $categories = FaqCategory::orderBy('name')->pluck('name', 'idFaqCategory');
    
    // show the edit form and pass the faq
    return view('faqs.edit',compact('faq','categories'));
  }
  
  /**
  * Update the specified resource in storage.
  *
  * @param  int  $id
  * @return Response
  */
  public function update($id)
  {
    // validate
    // read more on validation at http://laravel.com/docs/validation
    $rules = array(
      'question'       => 'required'
    );
    $validator = Validator::make(Input::all(), $rules);
    
    // process the login
    if ($validator->fails()) {
      return Redirect::to('faqs/' . $id . '/edit')
      ->withErrors($validator)
      ->withInput(Input::except('password'));
    } else {
      // store
      $faq = Faq::find($id);
      $faq->idFaqCategory = Input::get('idFaqCategory');
      $faq->question = Input::get('question');
      $faq->answer = Input::get('answer');
      $faq->tags = Input::get('tags');
      $faq->order = Input::get('order');
      $faq->save();
      
      // redirect
      Session::flash('message', 'FAQ Successfully updated!');
      return Redirect::to('faqs');
    }
  }
  
  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return Response
  */
  public function destroy($id)
  {
    // delete
    $faq = Faq::find($id);
    $faq->delete();
    
    // redirect
    Session::flash('message', 'Successfully deleted the faq!');
    return Redirect::to('faqs');
  }
}
