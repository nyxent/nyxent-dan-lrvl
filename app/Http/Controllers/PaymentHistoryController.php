<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Controllers\CommissionPaymentsController;

class PaymentHistoryController extends Controller
{
    public function index(Request $request)
    {
		$page = Input::get('page', 1);
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
	
		/*
			* FUNCTION IN ANOTHER CONTROLLER
		*/
		$CommisionPaymentsController=new CommissionPaymentsController();
		$client=$CommisionPaymentsController->client();
		$company=$CommisionPaymentsController->company();
		
		if($request)
    	{
			$searchText=trim($request->GET('searchText'));
			$searchClient=trim($request->GET('searchClient'));
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			$where ='';
			if($searchText!='')
			{
				$where.=" AND t.affiliateCompanyId='".$searchText."' ";
			 	
			}
			if($searchClient!='')
			{
				$where.=" AND t.idFile='".$searchClient."' ";
				
			}
			if($date1!='' and $date2!='')
			{
				$where.=" AND payoutDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."' ";
			}
					
			$data =DB::select($this->query().$where.$this->queryOrder());
						 
					
			if(isset($data))
			{
				$offSet = ($page * $paginate) - $paginate;
				
				$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
				
				$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
		
			}
			return view('form.paymentHistory.index',["itemsFile"=>$itemsFile,'client'=>$client,'company'=>$company,'searchClient'=>$searchClient,'date1'=>$date1,'date2'=>$date2,'searchText'=>$searchText]);	
		}
    }
	public function query()
	{
		$query="
			SELECT `type`, lastName, firstName, affiliateCompanyName,DATE_FORMAT(`payoutDate`, '%m/%d/%Y') AS payoutDate,amount ,comments
							FROM 
							(
								SELECT 'COM' AS `type`, lastName, firstName, affiliateCompany.affiliateCompanyName,payoutDate,FORMAT(amount/100, 2) AS amount,comments,commissionPayments.affiliateCompanyId,commissionPayments.idFile
								FROM `commissionPayments`
								INNER JOIN `file` ON commissionPayments.idFile=file.idFile
								INNER JOIN affiliateCompany ON commissionPayments.affiliateCompanyId=affiliateCompany.affiliateCompanyId
								WHERE commissionPayments.statusCode='T'
								UNION ALL
								SELECT 'DEP' AS `type`,lastName,firstName,'' AS affiliateCompanyName,depositDate AS  payoutDate,FORMAT(amount/100, 2) AS amount,comments,'' as affiliateCompanyId,clientPayments.idFile
								FROM `clientPayments`
								INNER JOIN `file` ON clientPayments.idFile=file.idFile
								WHERE clientPayments.statusCode='S'
							) AS t 
							WHERE 1=1
							
			";
		return $query;
	}
	public function queryOrder()
	{
			$query=" ORDER BY DATE_FORMAT(payoutDate, '%Y/%m/%d'), type desc";
			return $query;
	}
	
}
