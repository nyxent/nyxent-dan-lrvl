<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;

class DiscrepancyReportEPPSController extends Controller

{

   public function index(Request $request){

		/** set  maximun execution  time*/

		ini_set('max_execution_time', 1400); 
		$page = Input::get('page', 1);
		$paginate = 10000;		

		/*********************************************/		

		$data =DB::select("SELECT
		TYPE,
		idFile,
		lastName,
		firstName,
		DATE_FORMAT(payoutDate, '%m/%d/%Y') AS payoutDate,
		amount,
		statusCode,
		eftTransactionID,
		dateIshigher ,
		DATE_FORMAT(payoutDate, '%Y%m%d') AS payoutDate2,
		FORMAT(balanceEPPS / 100,2) as balanceEPPS
	FROM
		(
		SELECT
			'DEP' AS TYPE,
			clientPayments.idFile,
			lastName,
			firstName,
			depositDate AS payoutDate,
			FORMAT(SUM(amount)/ 100,2) AS amount,
			clientPayments.statusCode,
			eftTransactionID,
			IF(depositDate>NOW(),'S','N') AS dateIshigher,
			'' AS affiliateCompanyName,
			balanceEPPS
		FROM
			clientPayments
		INNER JOIN `file` ON
			clientPayments.idFile = file.idFile
		INNER JOIN (
			SELECT
				commissionPayments.payoutDate,
				commissionPayments.idfile
			FROM
				commissionPayments
			WHERE
				statusCode IN ('P','T')
				AND `provider` = 'EPPS'
			GROUP BY
				commissionPayments.idfile,
				commissionPayments.payoutDate
			HAVING
				MIN(statusCode)= 'P'
				AND MAX(statusCode)= 'T' ) AS cp ON
			cp.payoutDate = clientPayments.depositDate
			AND cp.idfile = clientPayments.idfile
		WHERE
			statusCode = 'S'
		GROUP BY
			clientPayments.idFile,
			lastName,
			firstName,
			depositDate,
			clientPayments.statusCode,
			eftTransactionID,
			balanceEPPS
	UNION ALL
		SELECT
			commissionPayments.*
		FROM
			(
			SELECT
				DISTINCT 'COM' AS TYPE,
				commissionPayments.idFile,
				lastName,
				firstName,
				payoutDate,
				FORMAT(0,2) AS amount,
				'' AS statusCode,
				DATE_FORMAT(payoutDate, '%Y-%m-%d'),
				IF(payoutDate>NOW(),'S','N') AS dateIshigher,
				'' AS affiliateCompanyName,
				balanceEPPS
			FROM
				commissionPayments
			INNER JOIN `file` ON
				commissionPayments.idFile = file.idFile
			LEFT JOIN (
				SELECT
					clientPayments.*
				FROM
					clientPayments
				INNER JOIN (
					SELECT
						commissionPayments.payoutDate,
						commissionPayments.idfile
					FROM
						commissionPayments
					WHERE
						statusCode IN ('P','T')
						AND `provider` = 'EPPS'
					GROUP BY
						commissionPayments.idfile,
						commissionPayments.payoutDate
					HAVING
						MIN(statusCode)= 'P'
						AND MAX(statusCode)= 'T' ) AS cp ON
					cp.payoutDate = clientPayments.depositDate
					AND cp.idfile = clientPayments.idfile
				WHERE
					statusCode = 'S'
					AND provider = 'EPPS' ) AS clientPayments2 ON
				clientPayments2.idFile = commissionPayments.idFile
				AND clientPayments2.depositDate = commissionPayments.payoutDate
			WHERE
				clientPayments2.idFile IS NULL ) AS commissionPayments
		INNER JOIN (
			SELECT
				cp2.payoutDate,
				cp2.idfile
			FROM
				commissionPayments AS cp2
			WHERE
				cp2.statusCode IN ('P','T')
				AND cp2.`provider` = 'EPPS'
			GROUP BY
				cp2.idfile,
				cp2.payoutDate
			HAVING
				MIN(cp2.statusCode)= 'P'
				AND MAX(cp2.statusCode)= 'T' ) AS cp2 ON
			cp2.payoutDate = commissionPayments.payoutDate
			AND cp2.idfile = commissionPayments.idfile ) AS T
	ORDER BY
		DATE_FORMAT(payoutDate, '%Y/%m/%d')");

		$Commission =DB::select("SELECT
		DATE_FORMAT(commissionPayments.payoutDate, '%Y%m%d') AS payoutDate2,
		FORMAT(commissionPayments.amount / 100,2) AS amount,
		commissionPayments.idFile,
		commissionPayments.affiliateCompanyName,
		commissionPayments.affiliateCompanyId,
		commissionPayments.statusCode
	FROM
		commissionPayments
	INNER JOIN (
		SELECT
			cp2.payoutDate,
			cp2.idfile
		FROM
			commissionPayments AS cp2
		WHERE
			cp2.statusCode IN ('P','T')
			AND cp2.`provider` = 'EPPS'
		GROUP BY
			cp2.idfile,
			cp2.payoutDate
		HAVING
			MIN(cp2.statusCode)= 'P'
			AND MAX(cp2.statusCode)= 'T' ) AS cp2 ON
		cp2.payoutDate = commissionPayments.payoutDate
		AND cp2.idfile = commissionPayments.idfile");

		$CompanysPaymentsDate=array();
		$ClientDiscrepancy=array();

		foreach($Commission as $indice=>$value){

			if(!in_array($value->idFile, $ClientDiscrepancy)){
				$ClientDiscrepancy[]=$value->idFile;
			}

			if($value->affiliateCompanyId==21){
				$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['DAN']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
			}

			else{
				if($value->affiliateCompanyId==24){
					$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['Nyxent']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
				}

				else{
					if($value->affiliateCompanyId==29){
						$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['KcCredit']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
					}

					else{
						if($value->affiliateCompanyId==55){
							$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['EPPS']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
						}
						else{
							if(!array_key_exists((string)$value->payoutDate2,$CompanysPaymentsDate)){
								$CompanysPaymentsDate[(string)$value->payoutDate2]=array();
							}
							if(!array_key_exists((string)$value->idFile,$CompanysPaymentsDate[(string)$value->payoutDate2])){
								$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]=array();
							}

							if(!array_key_exists('C1',$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile])){
								$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['C1']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
							}
							else{
								$CompanysPaymentsDate[(string)$value->payoutDate2][(string)$value->idFile]['C2']=array('amount'=>$value->amount,'affiliateCompanyName'=>$value->affiliateCompanyName,'statusCode'=>$value->statusCode);
							}			
						}
					}
				}
			}
		}

		$itemsFile=array();
		if(isset($data)){
				$offSet = ($page * $paginate) - $paginate;
				$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
				$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
			}
		return view('form.DiscrepancyReportEPPSController.index',["itemsFile"=>$itemsFile,'CompanysPaymentsDate'=>$CompanysPaymentsDate]);	
	}

	public function FindAccountBalance($searchClient){
		$opts = array('http' =>
					  array(
						'method'  => 'POST',
						'header'  => "Content-Type: text/xml\r\n",
						'content' => '<?xml version="1.0" encoding="utf-8"?>
					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					 <soap:Body>
					   <FindCardHolderByID xmlns="http://tempuri.org/">
						 <UserName>'.env('API_USER').'</UserName>
						 <PassWord>'.env('API_PASSWORD').'</PassWord>
						 <CardHolderID>'.$searchClient.'</CardHolderID>
					   </FindCardHolderByID>
					 </soap:Body>
					</soap:Envelope>',
						'timeout' => 60
					  )
					);			
		$context  = stream_context_create($opts);
		$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';
		$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);
		$resultPaymentsEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindCardHolderByIDResponse xmlns="http://tempuri.org/">','',$resultPaymentsEPPS);
		$resultPaymentsEPPS=str_replace('<FindCardHolderByIDResult>','',$resultPaymentsEPPS);
		$resultPaymentsEPPS=str_replace('</FindCardHolderByIDResult>','',$resultPaymentsEPPS);
		$resultPaymentsEPPS=str_replace('<Message>Success</Message></FindCardHolderByIDResponse></soap:Body></soap:Envelope>','',$resultPaymentsEPPS);
		$PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);	
		return $PaymentsEPPS;
	}
}