<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CommissionPaymentsController;
use Input;

class clientReportController extends Controller
{
    public function index(Request $request)
    {	
		$page = Input::get('page', 1);
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
		
		if($request)
    	{
			
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			
			$where ='';
			
			if($date1!='' and $date2!='')
			{
				$where.=" WHERE dateIn Between '".$this->mmddyyyyToyyyymmdd($date1)."' AND  '".$this->mmddyyyyToyyyymmdd($date2)."' or dateOut Between '".$this->mmddyyyyToyyyymmdd($date1)."' AND  '".$this->mmddyyyyToyyyymmdd($date2)."' ";
			}
			
			$data =DB::select($this->query().$where.$this->queryOrder());
			
			if(isset($data))
			{
				$offSet = ($page * $paginate) - $paginate;
				$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
				$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator(
				$itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
			
			}
			return view('form.clientReport.index',
			["itemsFile"=>$itemsFile,'date1'=>$date1,'date2'=>$date2]
			);		
		}
	}
	public function mmddyyyyToyyyymmdd($date)
	{
		$date = explode("-",$date);
		$date = $date[2].$date[0].$date[1];
		return $date;
	}

	public function query()
	{
			$query='SELECT * FROM `clientFileId`
					';
			return $query;
	}
	public function queryOrder()
	{
			$query=" ORDER BY DATE_FORMAT(dateIn, '%Y/%m/%d')";
			return $query;
	}
}
