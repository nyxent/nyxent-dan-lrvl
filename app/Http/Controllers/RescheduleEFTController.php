<?php



namespace App\Http\Controllers;



use Session;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\CommissionPaymentsController;

use Input;

class RescheduleEFTController extends Controller

{

    public function index(Request $request)

    {	

		/*

			* set  maximun execution  time

		*/

		ini_set('max_execution_time', 1400); 

		$page = Input::get('page', 1);

		$paginate = 100;

		$data=array();

		if($request)

    	{

			$date1 = $request->GET('date1');

			$date2 = $request->GET('date2');

			if($date1=='' || $date2=='')

			{

				$date1 = date("Y/m/d");

				$dateO  = strtotime($date1);

				//$dateO  = strtotime("-10 day",$dateO);

				$date  = strtotime("-7 day", $dateO);

				$date1 = date('m-d-Y', $date);

				$date2 = date('m-d-Y', $dateO);

			}

			if($date1!='' and $date2!='')

			{

				$clientResult=array();

				$CommisionPaymentsController=new CommissionPaymentsController();

				$client=$CommisionPaymentsController->clientEPPS();

				foreach($client as $indice=>$value)

				{

				

					$clientResult[$value->idFile]=$value->client2;

				}

			

			

				/*

					* SELECT IDS READY SAVE

				*/

				$ListIdtransactionSave =DB::select("SELECT DATE_FORMAT(rescheduleDate, '%Y-%m-%d') as idTransaction FROM `paymentRescheduleLog` WHERE `type`='P'");



				$ListIdtransactionSave2=array();

				foreach($ListIdtransactionSave as $indice=>$value)

				{

					$ListIdtransactionSave2[]=$value->idTransaction;

				}

				

				$FindEftByStatusDateResult=array();

				//$FindEftByStatusDate=$this->FindEftByStatusDate($date1,$date2);

				$FindEftByStatusDate =DB::select("

						SELECT clientPayments.idfile AS CardHolderId,

								`depositDate` AS EftDate,

								FORMAT(amount/100, 2) AS EftAmount,

								`eftTransactionID`  AS EftTransactionID,

								`statusCode` AS  StatusCode,

								`routingNumber` AS RoutingNumberEPPS,

								`accountNumber` AS AccountNumberEPPS,

								`lastMessage` AS `LastMessage`,

								 `ppRoutingNumber` as RoutingNumber,

								 `ppAccountNumber` as AccountNumber

								FROM `clientPayments`

								INNER JOIN `file` ON file.idfile=clientPayments.idfile

								WHERE statusCode IN ('R','PF','PFV')");

								

				foreach($FindEftByStatusDate as $indice=>$value)

				{

					

					// PRODUCTION

					//if($value->StatusCode=='R' or $value->StatusCode=='PF' or $value->StatusCode=='PFV' or $value->StatusCode=='THR' )

					if($value->StatusCode=='R')

					{

						if(array_key_exists((integer)$value->CardHolderId, $clientResult))

						{

							if (!in_array(substr($value->EftDate,0,10), $ListIdtransactionSave2))

							{

								$clientResult[(integer)$value->CardHolderId];

								$value->client=$clientResult[(integer)$value->CardHolderId];

								$value->EftDate=date('m/d/Y',strtotime($value->EftDate));

								$FindEftByStatusDateResult[]=$value;

							}

						}

						

						

					}

					

					

					// TEST

					/*if($value->StatusCode=='Create EFT Pending')

					{

						if(array_key_exists((integer)$value->CardHolderId, $clientResult))

						{

							if (!in_array(substr($value->EftDate,0,10), $ListIdtransactionSave2))

							{

								$clientResult[(integer)$value->CardHolderId];

								$value->client=$clientResult[(integer)$value->CardHolderId];

								$value->EftDate=date('m/d/Y',strtotime($value->EftDate));

								$FindEftByStatusDateResult[]=$value;

							}

						}

						

					}*/

					

				}

				$data=$FindEftByStatusDateResult;

			

			}

		}

		$itemsFile=array();

		if(isset($data))

		{

			$offSet = ($page * $paginate) - $paginate;

			

			$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);

			

			$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);

	

		}

		return view('form.rescheduleEFT.index',["itemsFile"=>$itemsFile,'date1'=>$date1,'date2'=>$date2,'FindEftByStatusDateResult'=>json_encode($data)]);	

	}

	public function FindEftByStatusDate($date1,$date2)

	{

		

		$opts = array('http' =>

					  array(

						'method'  => 'POST',

						'header'  => "Content-Type: text/xml\r\n",

						'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					 	<FindEftByStatusDate xmlns="http://tempuri.org/">

						  <UserName> '.env('API_USER').'</UserName>

						  <PassWord> '.env('API_PASSWORD').'</PassWord>

						  <StatusDateFrom>'.$this->mmddyyyyToyyyymmdd2($date1).'</StatusDateFrom>

						  <StatusDateTo>'.$this->mmddyyyyToyyyymmdd2($date2).'</StatusDateTo>

						</FindEftByStatusDate>

					 </soap:Body>

					</soap:Envelope>',

						'timeout' => 180

					  )

					);			

					

		$context  = stream_context_create($opts);

		$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

		$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);



		

		if($resultPaymentsEPPS=='<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindEftByStatusDateResponse xmlns="http://tempuri.org/"><FindEftByStatusDateResult><EFTList /><Message>Cannot query a date range longer than 7 days.</Message></FindEftByStatusDateResult></FindEftByStatusDateResponse></soap:Body></soap:Envelope>')

		{

			echo 	$resultPaymentsEPPS;

			exit;

		}

		$resultPaymentsEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindEftByStatusDateResponse xmlns="http://tempuri.org/">','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('<FindEftByStatusDateResult>','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('</FindEftByStatusDateResult>','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('<Message>Success</Message></FindEftByStatusDateResponse></soap:Body></soap:Envelope>','',$resultPaymentsEPPS);

		

		$PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);	

		

		return $PaymentsEPPS;

	}

	public function yyyymmddTommddyyyy($date)

	{

		$date = explode("-",substr($date,0,10));

		$date = $date[1].'-'.$date[0].'-'.$date[0];

		return $date;

	}

	public function mmddyyyyToyyyymmdd2($date)

	{

		$date = explode("-",substr($date,0,10));

		$date = $date[2].'-'.$date[0].'-'.$date[1];

		return $date;

	}

	public function mmddyyyyToyyyymmdd($date)

	{

		$date = explode("/",substr($date,0,10));

		$date = $date[2].'-'.$date[0].'-'.$date[1];

		return $date;

	}

	public function store(Request $request)

    {

		$FindEftByStatusDateResult=$request->GET('FindEftByStatusDateResult');

		$NewEftDate=trim($request->GET('NewEftDate'));

		$NewEftAmount=trim($request->GET('NewEftAmount'));

		$UpdateNextAccount=trim($request->GET('UpdateNextAccount'));

		

		$CardHolderId=$FindEftByStatusDateResult['CardHolderId'];

		$OriginAmountEft=$FindEftByStatusDateResult['EftAmount'];

		$EftTransactionID=$FindEftByStatusDateResult['EftTransactionID'];

		$EftDateOringin=$FindEftByStatusDateResult['EftDate'];

		$NewAccountNumber=trim($request->GET('NewAccountNumber'));

		$NewRoutingNumber=trim($request->GET('NewRoutingNumber'));

		$NewNote=trim($request->GET('NewNote'));

		/*

			* CREAR EFT

		*/

		$opts = array('http' =>

					  array(

						'method'  => 'POST',

						'header'  => "Content-Type: text/xml\r\n",

						'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					 

					 	<AddEft xmlns="http://tempuri.org/">

						  <UserName> '.env('API_USER').'</UserName>

						  <PassWord> '.env('API_PASSWORD').'</PassWord>

						  <CardHolderID>'.$CardHolderId.'</CardHolderID>

						  <EftDate>'.$this->mmddyyyyToyyyymmdd($NewEftDate).'</EftDate>

						  <EftAmount>'.$NewEftAmount.'</EftAmount>

						  <EftFee>0</EftFee>

						  <BankName></BankName>

						  <BankCity></BankCity>

						  <BankState></BankState>

						  <AccountNumber>'.$NewAccountNumber.'</AccountNumber>

						  <RoutingNumber>'.$NewRoutingNumber.'</RoutingNumber>

						  <AccountType>Checking</AccountType>

						  <Memo></Memo>

						</AddEft>

					 </soap:Body>

					</soap:Envelope>',

						'timeout' => 180

					  )

					);			



					$context  = stream_context_create($opts);

					$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

					$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);

					

					$resultPaymentsEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><AddEftResponse xmlns="http://tempuri.org/">','',$resultPaymentsEPPS);

			

					

					$resultPaymentsEPPS=str_replace('</AddEftResponse></soap:Body></soap:Envelope>','',$resultPaymentsEPPS);

					

					error_log($resultPaymentsEPPS);

					$PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);

					$EftTransactionIDSave=$PaymentsEPPS->EftTransactionID;

					error_log("

					INSERT INTO `crmlexco_dan_dev`.`paymentRescheduleLog` 

						(

						`dateTransaction`, 

						`type`, 

						`idTransaction`, 

						`idUser`, 

						`idFile`,

						rescheduleDate

						)

						VALUES

						(

						CURDATE(), 

						'P', 

						'".$EftTransactionID."', 

						'".Session::get('iduser')."', 

						'".$CardHolderId."',

						'".$this->mmddyyyyToyyyymmdd($EftDateOringin)."'

						);

					");

					DB::insert("

					INSERT INTO `crmlexco_dan_dev`.`paymentRescheduleLog` 

						(

						`dateTransaction`, 

						`type`, 

						`idTransaction`, 

						`idUser`, 

						`idFile`,

						rescheduleDate

						)

						VALUES

						(

						CURDATE(), 

						'P', 

						'".$EftTransactionID."', 

						'".Session::get('iduser')."', 

						'".$CardHolderId."',

						'".$this->mmddyyyyToyyyymmdd($EftDateOringin)."'

						);

					");

					

					$CadenaPaymentsChanges=

					' EftDate=>'.$NewEftDate.

					'| Amount=>'.$NewEftAmount.

					'| AccountNumber=>'.$NewAccountNumber.

					'| RoutingNumber=>'.$NewRoutingNumber;

					

					DB::insert("INSERT INTO history 

						(

						idFile, 

						title, 

						detail, 

						iduser

						)

						VALUES

						(

						'".$CardHolderId."', 

						'Add Eft', 

						'".$CadenaPaymentsChanges."', 

						'".Session::get('iduser')."'

						);

					");

		/**************************************************************/

		/*

			* FIND BY CARDHOLDER

		*/

		$CommisionEPPS=$this->FindFeeByCardHolderID($CardHolderId);

		if(count($CommisionEPPS)>1)

		{	

			

			$dataArray=array();

			$CommissionEPPSTrasantion=array();

			$SumDAN='N';

			$FeeToSave=array();

			foreach($CommisionEPPS as $indice2=>$Commission)

			{

				if($Commission->StatusCode=='Pending')

				{

					if(substr($Commission->Fee_Date,0,10)==$this->mmddyyyyToyyyymmdd($EftDateOringin))

					{

						$opts = array('http' =>

							  array(

								'method'  => 'POST',

								'header'  => "Content-Type: text/xml\r\n",

								'content' => '<?xml version="1.0" encoding="utf-8"?>

							<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

							 <soap:Body>

							   <VoidFee  xmlns="http://tempuri.org/">

								 <UserName> '.env('API_USER').'</UserName>

								 <PassWord> '.env('API_PASSWORD').'</PassWord>

								  <FeeID>'.$Commission->FeeID.'</FeeID>

							   </VoidFee >

							 </soap:Body>

							</soap:Envelope>',

								'timeout' => 60

							  )

							);			

					

							$context  = stream_context_create($opts);

							$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

							$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);

							

							$CadenaPaymentsChanges=

								' FeeDate=>'.$Commission->Fee_Date.

								'| AffiliateCompany=>'.$Commission->PaidToName.

								'| FeeID=>'.$Commission->FeeID.

								'| Amount=>'.$Commission->FeeAmount;

								

							DB::insert("INSERT INTO history 

									(

									idFile, 

									title, 

									detail, 

									iduser

									)

									VALUES

									(

									'".$CardHolderId."', 

									'VoidFee', 

									'".$CadenaPaymentsChanges."', 

									'".Session::get('iduser')."'

									);

								");

							/*

								* FEE DIFERENCE

							*/

							$NewFeeAmount=$Commission->FeeAmount;

							$Difference=($NewEftAmount-$OriginAmountEft);

							if($Difference>0 and $Commission->PaidToName=='DAN' and $SumDAN=='N')

							{

								$SumDAN='S';

								$NewFeeAmount=($NewFeeAmount+$Difference);

								/*echo $FeeAmount;*/

							}

							

							if(array_key_exists((string)$Commission->PaidToName,$FeeToSave))

							{

								$FeeToSave[(string)$Commission->PaidToName]['amount']=$FeeToSave[(string)$Commission->PaidToName]['amount']+$NewFeeAmount;

							}

							else

							{

								$FeeToSave[(string)$Commission->PaidToName]=array('amount'=>$NewFeeAmount);

							}

					}

				}

			}

		}

		if($NewNote!='')

		{

			

		 	DB::update('UPDATE file F

		 			 SET F.paymentIssues=CONCAT(COALESCE(F.paymentIssues,""),"\n'.$NewNote.' '.$NewEftDate.'")

					 WHERE idFile="'.$CardHolderId.'"

									');

		}

		/*

			* CREATE FEE

		*/

		

		foreach($FeeToSave as $indice=>$FeeToSaveAmount)

		{

			$opts = array('http' =>

				  array(

					'method'  => 'POST',

					'header'  => "Content-Type: text/xml\r\n",

					'content' => '<?xml version="1.0" encoding="utf-8"?>

				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

				 <soap:Body>

				 

					<AddFee xmlns="http://tempuri.org/">

					  <UserName> '.env('API_USER').'</UserName>

					  <PassWord> '.env('API_PASSWORD').'</PassWord>

					  <CardHolderID>'.$CardHolderId.'</CardHolderID>

					  <FeeDate>'.$this->mmddyyyyToyyyymmdd($NewEftDate).'</FeeDate>

					  <FeeAmount>'.$FeeToSaveAmount['amount'].'</FeeAmount>

					  <Description>Reschedule|'.$EftTransactionIDSave.'</Description>

					  <FeeType>SettlementPayment</FeeType>

					  <PaidToName>'.$indice.'</PaidToName>

					  <PaidToPhone></PaidToPhone>

					  <PaidToStreet></PaidToStreet>

					  <PaidToStreet2></PaidToStreet2>

					  <PaidToCity></PaidToCity>

					  <PaidToState></PaidToState>

					  <PaidToZip></PaidToZip>

					  <ContactName></ContactName>

					  <PaidToCustomerNumber></PaidToCustomerNumber>

					</AddFee>

					

				 </soap:Body>

				</soap:Envelope>',

					'timeout' => 180

				  )

				);	

				$context  = stream_context_create($opts);

				$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

			

				$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);

				

				

				$CadenaPaymentsChanges=

				' FeeDate=>'.$NewEftDate.

				'| AffiliateCompany=>'.$indice.

				'| Amount=>'.$FeeToSaveAmount['amount'];

				

				DB::insert("INSERT INTO history 

					(

					idFile, 

					title, 

					detail, 

					iduser

					)

					VALUES

					(

					'".$CardHolderId."', 

					'Add Fee', 

					'".$CadenaPaymentsChanges."', 

					'".Session::get('iduser')."'

					);

				");

		}

								

		

		

				

		if($UpdateNextAccount=='S')

		{

			$PaymentsEPPS=$this->FindEftByID($CardHolderId);

			$CadenaPaymentsChanges='';

			foreach($PaymentsEPPS as $indice2=>$FilePayments)

			{

				if($FilePayments->StatusCode=='Create EFT Pending')

				{

					

					$opts = array('http' =>

						  array(

							'method'  => 'POST',

							'header'  => "Content-Type: text/xml\r\n",

							'content' => '<?xml version="1.0" encoding="utf-8"?>

						<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

						 <soap:Body>

						   <UpdateEft xmlns="http://tempuri.org/">

							 

							 <UserName> '.env('API_USER').'</UserName>

							 <PassWord> '.env('API_PASSWORD').'</PassWord>

							  <EftTransactionID>'.$FilePayments->EftTransactionID.'</EftTransactionID>

							  <EftDate>'.$FilePayments->EftDate.'</EftDate>

							  <EftAmount>'.$FilePayments->EftAmount.'</EftAmount>

							  <EftFee>0</EftFee>

							  <BankName></BankName>

							  <BankCity></BankCity>

							  <BankState></BankState>

							  <AccountNumber>'.$NewAccountNumber.'</AccountNumber>

							  <RoutingNumber>'.$NewRoutingNumber.'</RoutingNumber>

							  <AccountType>Checking</AccountType>

							  <Memo>message</Memo>

							 

							 

						   </UpdateEft>

						 </soap:Body>

						</soap:Envelope>',

							'timeout' => 60

						  )

						);			

					

					$context  = stream_context_create($opts);

					$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

					$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);

					

					$CadenaPaymentsChanges.=

					' -- EftTransactionID=>'.$FilePayments->EftTransactionID.

					'| EftDate=>'.$this->yyyymmddTommddyyyy($FilePayments->EftDate).

					'| AccountNumber Origin=>'.$FilePayments->AccountNumber.

					'| AccountNumber Current=>'.$NewAccountNumber.

					'| RoutingNumber Origin=>'.$FilePayments->RoutingNumber.

					'| RoutingNumber Current=>'.$NewRoutingNumber;

					

				}

			}

			if($CadenaPaymentsChanges!='')

			{

				DB::insert("INSERT INTO history 

					(

					idFile, 

					title, 

					detail, 

					iduser

					)

					VALUES

					(

					'".$CardHolderId."', 

					'Change EFT Payments', 

					'".$CadenaPaymentsChanges."', 

					'".Session::get('iduser')."'

					);

				");

			}

			DB::update('UPDATE file F

						 SET F.ppRoutingNumber="'.$NewRoutingNumber.'",

							 F.ppAccountNumber="'.$NewAccountNumber.'"

						 WHERE idFile="'.$CardHolderId.'"

									');

			

		}

		echo json_encode(array('Save'=>'Ok'));

		exit;

	}

	/*

		* FIND EFT BY ID

	*/

	public function FindEftByID($searchClient)

	{

		$opts = array('http' =>

					  array(

						'method'  => 'POST',

						'header'  => "Content-Type: text/xml\r\n",

						'content' => '<?xml version="1.0" encoding="utf-8"?>

					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

					 <soap:Body>

					   <FindEftByID xmlns="http://tempuri.org/">

						 <UserName> '.env('API_USER').'</UserName>

						 <PassWord> '.env('API_PASSWORD').'</PassWord>

						 <CardHolderID>'.$searchClient.'</CardHolderID>

					   </FindEftByID>

					 </soap:Body>

					</soap:Envelope>',

						'timeout' => 60

					  )

					);			

					

		$context  = stream_context_create($opts);

		$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

		$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);

		

		$resultPaymentsEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindEftByIDResponse xmlns="http://tempuri.org/">','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('<FindEftByIDResult>','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('</FindEftByIDResult>','',$resultPaymentsEPPS);

		

		$resultPaymentsEPPS=str_replace('<Message>Success</Message></FindEftByIDResponse></soap:Body></soap:Envelope>','',$resultPaymentsEPPS);

		

		$PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);	

		

		return $PaymentsEPPS;

	}

	/*

		* FIND FEE BY CARD HOLDER ID

	*/

	public function FindFeeByCardHolderID($searchClient)

	{

		$opts = array('http' =>

			  array(

				'method'  => 'POST',

				'header'  => "Content-Type: text/xml\r\n",

				'content' => '<?xml version="1.0" encoding="utf-8"?>

			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

			 <soap:Body>

			   <FindFeeByCardHolderID xmlns="http://tempuri.org/">

					  <UserName> '.env('API_USER').'</UserName>

					  <PassWord> '.env('API_PASSWORD').'</PassWord>

				 <CardHolderID>'.$searchClient.'</CardHolderID>

			   </FindFeeByCardHolderID>

			 </soap:Body>

			</soap:Envelope>',

				'timeout' => 60

			  )

			);			

			

			$context  = stream_context_create($opts);

			$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';

			$resultCommisionEPPS = file_get_contents($url, false, $context, -1, 10485760);

			

			$resultCommisionEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindFeeByCardHolderIDResponse xmlns="http://tempuri.org/">','',$resultCommisionEPPS);

			

			$resultCommisionEPPS=str_replace('<FindFeeByCardHolderIDResult>','',$resultCommisionEPPS);

			

			$resultCommisionEPPS=str_replace('</FindFeeByCardHolderIDResult>','',$resultCommisionEPPS);

			

		$resultCommisionEPPS=str_replace('<Message>Success</Message></FindFeeByCardHolderIDResponse></soap:Body></soap:Envelope>','',$resultCommisionEPPS);

			

			

			$CommisionEPPS = simplexml_load_string($resultCommisionEPPS);

			

			

			return $CommisionEPPS;

	}

}

