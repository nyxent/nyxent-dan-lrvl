<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Models\ClientModel;
use App\FileStatus;

class DownloadDataReportController extends Controller {
	
	public function index(Request $request) {
		
		$fileStatus = FileStatus::orderBy('name')->get();
		$paginate = $request->download ? null : 20;
		$files = $request->submited ? ClientModel::getClients($request->all(),  $paginate): array();
		
		if ($request->download) {
			// $clients = array_map(function ($value) {return (array)$value; }, $clients);
			Excel::create('Download Data report', function($excel) use($request, $files) {
				$excel->sheet('KC Report', function($sheet) use($request, $files) {
					$sheet->loadView('downloadDataReport.excel', compact('request', 'files'));
				});
			})->download('xlsx');
		} else {
			return view('downloadDataReport.index', compact('request', 'files', 'fileStatus'));
		}
	}
	
	public function detailedInfo(Request $request) {
		
		$fileStatus = FileStatus::orderBy('name')->get();
		$paginate = $request->download ? null : 20;
		$files = $request->submited ? ClientModel::getClients($request->all(),  $paginate): array();
		
		if ($request->download) {
			// $clients = array_map(function ($value) {return (array)$value; }, $clients);
			Excel::create('Download Data report', function($excel) use($request, $files) {
				$excel->sheet('KC Report', function($sheet) use($request, $files) {
					$sheet->loadView('detailedInfo.excel', compact('request', 'files'));
				});
			})->download('xlsx');
		} else {
			return view('detailedInfo.index', compact('request', 'files', 'fileStatus'));
		}
	}
	
}
