<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\CommissionPaymentsController;
use Input;
use Response;

class CommissionPaymentsController extends Controller
{
	public function paginateGlobal()
	{
		$paginate = 50;
		return $paginate;
	}
    public function index(Request $request)
    {
		$page = Input::get('page', 1);
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
		/*
			* FUNCTION LOCAL
		*/
		$client=$this->client();
		$company=$this->company();
		if($request)
    	{
			
			$searchText=trim($request->GET('searchText'));
			$searchClient=trim($request->GET('searchClient'));
			
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			

			$where ='';
			if($searchText!='')
			{
				$where.=" AND commissionPayments.affiliateCompanyId='".$searchText."' ";
			}
			if($searchClient!='')
			{
				$where.=" AND commissionPayments.idFile='".$searchClient."' ";
			}
			if($date1!='' and $date2!='')
			{
				$where.=" AND DATE_FORMAT(payoutDate, '%Y%m%d') Between '".$this->mmddyyyyToyyyymmdd($date1)."' AND  '".$this->mmddyyyyToyyyymmdd($date2)."' ";
			}
					
						
			$data =DB::select($this->query().$where.$this->queryOrder());
					
			if(isset($data))
			{
			$offSet = ($page * $paginate) - $paginate;
			
			$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
			
			$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
		
			}
			return view('form.commissionsByClient.index',["itemsFile"=>$itemsFile,'client'=>$client,'company'=>$company,'searchClient'=>$searchClient,'date1'=>$date1,'date2'=>$date2,'searchText'=>$searchText]);	
		}
    }
	
	public function query()
	{
			$query=" SELECT lastName,firstName,affiliateCompany.affiliateCompanyName,DATE_FORMAT(payoutDate, '%m/%d/%Y') AS payoutDate,CONCAT('$',FORMAT(amount/100, 2)) AS amount
			FROM commissionPayments
			INNER JOIN `file` ON commissionPayments.idFile=file.idFile
			INNER JOIN affiliateCompany ON commissionPayments.affiliateCompanyId=affiliateCompany.affiliateCompanyId 
			WHERE commissionPayments.statusCode='T'
			";
			return $query;
	}
	public function queryOrder()
	{
			$query=" ORDER BY DATE_FORMAT(payoutDate, '%Y/%m/%d'),lastName,firstName";
			return $query;
	}
	
	/*
		* THESE FUNCIONS ARE CALLED IN OTHER CONTROLLER
	*/
	public function client()
	{
		$client=DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file	
								WHERE provider IS NOT NULL
								ORDER BY CONCAT(TRIM(lastName),' ',firstName) 
							
						");
		
		return $client;	
	
	}
	public function clientAffiliate($affiliateCompanyId)
	{
		$client=DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file	
								WHERE   affiliateCompanyId='".$affiliateCompanyId."'
								ORDER BY CONCAT(TRIM(lastName),' ',firstName) 
								
							");
		return $client;	
	
	}
	public function clientAffiliateKcCredit()
	{
		$client=DB::select("
								SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM `file`
								WHERE idFile IN 
								(
									SELECT DISTINCT idFile 
									FROM commissionPayments 
									WHERE commissionPayments.affiliateCompanyId='29'
								)	
						  ");
		return $client;	
	
	}
	public function clientAffiliateActionSolutionCredit()
	{
		$client=DB::select("
								SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM `file`
								WHERE idFile IN 
								(
									SELECT DISTINCT idFile 
									FROM commissionPayments 
									WHERE commissionPayments.affiliateCompanyId='28'
								)	
						  ");
		return $client;	
	
	}
	/*
		* THESE FUNCIONS ARE CALLED IN OTHER CONTROLLER
		* CLIENT SAS
	*/
	public function clientSAS()
	{
		$client=DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file	
							WHERE provider='SAS'
							ORDER BY CONCAT(TRIM(lastName),' ',firstName) 
							");
		return $client;	
	
	}
	public function clientEPPS()
	{
		$client=DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file	
							WHERE provider='EPPS'
							ORDER BY CONCAT(TRIM(lastName),' ',firstName) 
							");
		return $client;	
	
	}
	public function company()
	{
		$client=DB::select("SELECT affiliateCompanyId,affiliateCompanyName FROM affiliateCompany ORDER BY affiliateCompanyName	");
		return $client;	
	}
	public function mmddyyyyToyyyymmdd($date)
	{
		$date = explode("-",$date);
		$date = $date[2].$date[0].$date[1];
		return $date;
	}
	/*
		* EXPORT CSV
	*/
	public function ExportCSV($data,$filename,$countColumn=100)
	{
		/*$table=array(array('tweet_text'=>'1', 'screen_name'=>'1', 'name'=>'1', 'created_at'=>'1'));*/

		$handle = fopen($filename, 'w+');
		
		foreach($data as $indice=>$row) 
		{
			if($indice==0)
			{
				$handle2=array();
				$count=0;
				foreach($row as $indice2=>$row2)
				{
					if($count<=$countColumn)
					{
						$handle2[]=$indice2;
					}
					$count++;
				}
				fputcsv($handle, $handle2);
			}
			$field=array();
			$count=0;
			foreach($row as $indice2=>$row2)
			{
				if($count<=$countColumn)
				{
					$field[]=$row2;
				}
				$count++;
			}
			fputcsv($handle, $field);
		}
		fclose($handle);
		$headers = array(
			'Content-Type' => 'text/csv',
		);
		return Response::download($filename, $filename, $headers);	
	}
	/*
		* ORDER COMMISSION
	*/
	public function queryOrderCommission($orderby)
	{
		$pos = strpos($orderby, "Desc");
		if ($pos === false) 
		{
			$Des='';
		}
		else
		{
			$Des='Desc';
		}
		$orderby=str_replace('Desc','',$orderby);
		$query='';
		if($orderby=='')
		{
			$query=" ORDER BY DATE_FORMAT(payoutDate, '%Y/%m/%d') {DESC},CONCAT(trim(file.lastName),file.firstName)";
		}
		if($orderby=='Date')
		{
			$query=" ORDER BY DATE_FORMAT(payoutDate, '%Y/%m/%d') {DESC}";
		}
		if($orderby=='CRMID')
		{
			$query=" ORDER BY file.idfile {DESC}";
		}
		if($orderby=='ClienteName')
		{
			$query=" ORDER BY CONCAT(trim(file.lastName),trim(file.firstName)) {DESC}";
		}
		if($orderby=='AffiliateName')
		{
			$query=" ORDER BY CONCAT(trim(user.firstName),trim(user.lastName)) {DESC},CONCAT(trim(file.lastName),trim(file.firstName))";
		}
		if($orderby=='FirstName')
		{
			$query=" ORDER BY trim(file.firstName) {DESC}";
		}
		if($orderby=='LastName')
		{
			$query=" ORDER BY trim(file.LastName) {DESC}";
		}
		if($orderby=='affiliateCompanyName')
		{
			$query=" ORDER BY trim(affiliateCompany.affiliateCompanyName) {DESC}";
		}
		if($orderby=='amount')
		{
			$query=" ORDER BY commissionPayments.amount {DESC}";
		}
		$query=str_replace("{DESC}", $Des, $query);
		return $query;
	}
	
}
