<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Controllers\CommissionPaymentsController;


class CommisionReportController extends Controller
{
   public function index(Request $request)
    {
        ini_set('memory_limit', '2048M');
		$page = Input::get('page', 1);
		
		/*
			* FUNCTION IN ANOTHER CONTROLLER
		*/
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
		$client=$CommisionPaymentsController->client();
		$company=$CommisionPaymentsController->company();
		
		if($request)
    	{
			$searchText=trim($request->GET('searchText'));
			$searchClient=trim($request->GET('searchClient'));
			$dateDownload=date('m-d-Y').' All';
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			$action = $request->GET('action');
			$orderby = $request->GET('orderby');
			$where ='';
			if($searchText!='')
			{
			 	$where.=" AND affiliateCompany.affiliateCompanyId='".$searchText."' ";
			}
			if($searchClient!='')
			{
				$where.="  AND file.idFile='".$searchClient."' ";
			}
			if($date1!='' and $date2!='')
			{
				$dateDownload=$date1.' '.$date2;
				$where.=" AND payoutDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."'";
			}
			$data =DB::select($this->query().$where.$CommisionPaymentsController->queryOrderCommission($orderby));
			/*
				* EXPORT
			*/
			if($action=='export')
			{
				
				return $CommisionPaymentsController->ExportCSV($data,'CommissionReport'.$dateDownload.'.csv',10);	
			}
			/*
				* PAGINATE
			*/
			if(isset($data))
			{
			$offSet = ($page * $paginate) - $paginate;
			
			$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
			
			$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
		
			}
			return view('form.commisionReport.index',["itemsFile"=>$itemsFile,'client'=>$client,'company'=>$company,'searchClient'=>$searchClient,'date1'=>$date1,'date2'=>$date2,'searchText'=>$searchText,'orderby'=>$orderby]);	
		}
    }
	public function query()
	{
		$query="
			SELECT
			file.idFile AS CRM_ID,
			IF(file.fileowner=0,CONCAT(file.lastName,' ',file.firstName),CONCAT(user.lastName,' ',user.`firstName`)) as SalesPerson,
			file.firstName,
			file.lastName,
			DATE_FORMAT(commissionPayments.payoutDate, '%m/%d/%Y') Transactiondate,
			FORMAT(commissionPayments.amount/100,2) AS amount,
			commentCommissionPayments.paymentNumber,
			file.ppProgramLenght as Nofpayments,
			FORMAT(file.ppTotalDeb,2) AS totalEnrolledDebt,
			DATE_FORMAT(enrollmentApprovedDate, '%m/%d/%Y') as enrollmentApprovedDate,
			if(fileStatus.idFileStatus IN(11,9,17,4,18),fileStatus.name,'') as fileStatus,
			
			DATE_FORMAT(file.enrollmentApprovedDate , '%m/%d/%Y') AS approvedDate,
			affiliateCompany.affiliateCompanyName,
			commissionPayments.provider,
			commentCommissionPayments.comment,
			
			
			
			
			if(commissionPayments.amount<0,'S','N') AS negative,
			commissionPayments.affiliateCompanyId,
			payoutDate
			
			FROM
			file 
			LEFT JOIN transaction_service_security
			ON file.idFile = transaction_service_security.idFile
			
			INNER JOIN commissionPayments
			ON file.idFile = commissionPayments.idFile	
			
			INNER JOIN affiliateCompany
			ON commissionPayments.affiliateCompanyId = affiliateCompany.affiliateCompanyId
			
			INNER JOIN affiliateCompany AS affiliateCompanyPayee
			ON commissionPayments.affiliateCompanyId = affiliateCompanyPayee.affiliateCompanyId
			
			INNER JOIN fileStatus ON fileStatus.idFileStatus=file.idFileStatus
			
			INNER JOIN user ON file.fileowner=user.idUser
			
			LEFT JOIN 
					(
						SELECT idFile,dateTransaction,affiliateCompanyId,MIN(comment) AS comment,MIN(paymentNumber) AS paymentNumber
						FROM commentCommissionPayments
						GROUP BY idFile,dateTransaction,affiliateCompanyId
					) AS commentCommissionPayments
		    ON commentCommissionPayments.idFile=commissionPayments.idFile AND 
				commentCommissionPayments.dateTransaction=commissionPayments.payoutDate AND
				commentCommissionPayments.affiliateCompanyId=commissionPayments.affiliateCompanyId 
			WHERE commissionPayments.statusCode='T' 
			";
		return $query;
	}
	
}
