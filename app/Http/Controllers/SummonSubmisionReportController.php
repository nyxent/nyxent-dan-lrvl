<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

use App\Http\Models\LegalMatterModel;

class SummonSubmisionReportController extends Controller {

    public function index(Request $request) {

        $paginate = $request->download ? null : 20;
        
        $legalMatters = LegalMatterModel::getLegalMatters($request->all(), $paginate) ;

        if ($request->download) {
            // $clients = array_map(function ($value) {return (array)$value; }, $clients);
            Excel::create('Summon submission report', function($excel) use($request, $legalMatters) {
                $excel->sheet('Summon submission', function($sheet) use($request, $legalMatters) {
                    $sheet->loadView('summonSubmissionReport.excel', compact('request', 'legalMatters'));
                });
            })->download('xlsx');
        } else {
            return view('summonSubmissionReport.index', compact('request', 'legalMatters'));
        }
    }

}
