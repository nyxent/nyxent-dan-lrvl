<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use View;
use Input;
use Session;
Use Redirect;
use App\RescheduleEFT;

class SoapController extends BaseSoapController
{
  private $service;
  
  public function index()
  {
    $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
    return view('soap.febsdForm', compact('client'));
  }
  
  public function FindEftByStatusDate(){
    try {
      $this->service = InstanceSoapClient::init();
      $params = array(
        'UserName' => env('API_USER'),
        'PassWord' => env('API_PASSWORD'),
        'StatusDateFrom'=> date("Y-m-d\TH:i:s",strtotime(Input::get('date1'))),
        'StatusDateTo' => date("Y-m-d\TH:i:s",strtotime(Input::get('date2')))
      );      
      $result = $this->service->FindEftByStatusDate($params);
      
      if(isset($result->FindEftByStatusDateResult->EFTList->EFTTransactionDetail)){
        $efts = $result->FindEftByStatusDateResult->EFTList->EFTTransactionDetail;        
        $eftClient = array();
        foreach($efts as $eft){
          if($eft->StatusCode=='Voided' && $eft->CardHolderId==Input::get('searchClient') && $eft->EftDate > date('Y-m-d',strtotime("+3 days"))){
            
            $message = $this->service->AddEft(
              [
                'UserName' => env('API_USER'),
                'PassWord' => env('API_PASSWORD'),
                'CardHolderID' => $eft->CardHolderId,
                'EftDate' => $eft->EftDate,
                'EftAmount' => $eft->EftAmount,
                'EftFee' => 0,
                'BankName' => $eft->BankName,
                'BankCity' => $eft->BankCity,
                'BankState' => $eft->BankState,
                'AccountNumber' => $eft->AccountNumber,
                'RoutingNumber' => $eft->RoutingNumber,
                'AccountType' => 'Checking',
                'Memo' => $eft->Memo
                ]
              );
            }
          }
          
          $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
          Session::flash('message', 'All Voided EFT Have been in EPPS Pending');
          return view('soap.febsdForm', compact('client'));
          
        }
        else{
          $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
          Session::flash('message', 'No EFT in EPPS with dates');
          return view('soap.febsdForm', compact('client'));
        }
        
      }
      catch(\Exception $e) {
        return $e->getMessage();
      }
    }
    

    public function reScheduleNSF(){
      $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
      return view('soap.nsfForm', compact('client')); 
    }
    
    public function reScheduleEFT(){
      $cardHolderID = Input::get('searchClient');
      try{
        $this->service = InstanceSoapClient::init();
        $params = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID'=> $cardHolderID
        );
        $result = $this->service->FindEftByID($params);
        foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft){
          if($eft->StatusCode == 'Returned' && !RescheduleEFT::where('EftTransactionID','=',$eft->EftTransactionID)->first()){
            $message = $this->service->AddEft(
              [
                'UserName' => env('API_USER'),
                'PassWord' => env('API_PASSWORD'),
                'CardHolderID' => $eft->CardHolderId,
                'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft->EftDate.' +3 days')),
                'EftAmount' => $eft->EftAmount,
                'EftFee' => 0,
                'BankName' => $eft->BankName,
                'BankCity' => $eft->BankCity,
                'BankState' => $eft->BankState,
                'AccountNumber' => $eft->AccountNumber,
                'RoutingNumber' => $eft->RoutingNumber,
                'AccountType' => 'Checking',
                'Memo' => $eft->Memo
                ]
              );
              
              $rescheduleEFT = new RescheduleEFT;
              $rescheduleEFT->EftTransactionID = $eft->EftTransactionID;
              $rescheduleEFT->save();
              Session::flash('message', 'NSF are Re Schedule');
              return Redirect::to('reScheduleNSF');
            }            
          }
          $result = $this->service->FindEftByID($params);
          Session::flash('message', 'No NSF found');
          return Redirect::to('reScheduleNSF');
        }
        catch(\Exception $e) {
          return $e->getMessage();
        }
      }
    }