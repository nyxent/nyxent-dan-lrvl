<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CommissionPaymentsController;
use Input;

class ClientPaymentsController extends Controller
{

    public function index(Request $request)
    {
		$page = Input::get('page', 1);
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
		/*
			* FUNCTION IN ANOTHER CONTROLLER
		*/
		$CommisionPaymentsController=new CommissionPaymentsController();
		$client=$CommisionPaymentsController->client();
		
		if($request)
    	{
			$searchClient=trim($request->GET('searchClient'));
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			
			$where ='';
			if($searchClient!='')
			{
				$where.=" AND clientPayments.idFile='".$searchClient."' ";
				
			}
			if($date1!='' and $date2!='')
			{
				$where.=" AND depositDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."' ";
			}
				
				
			$data =DB::select($this->query().$where.$this->queryOrder());
				
			
			if(isset($data))
			{
				$offSet = ($page * $paginate) - $paginate;
				$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
				$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator(
				$itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
			
			}
			return view('form.paymentsByClient.index',
			["itemsFile"=>$itemsFile,'client'=>$client,'searchClient'=>$searchClient,'date1'=>$date1,'date2'=>$date2]
			);		
		}
    }
	public function query()
	{
			$query='SELECT lastName,firstName,commentClientPayments.paymentNumber,paymentSequence,
					DATE_FORMAT(depositDate, "%m/%d/%Y") AS depositDate,CONCAT("$",FORMAT(amount/100, 2)) AS amount
					,commentClientPayments.comment,
					commentClientPayments.paymentNumber,
					statusCode
					FROM clientPayments
					INNER JOIN `file` ON clientPayments.idFile=file.idFile
					LEFT JOIN 
					(
						SELECT idFile,dateTransation,MIN(comment) AS comment,MIN(paymentNumber) AS paymentNumber
						FROM commentClientPayments
						GROUP BY idFile,dateTransation
					)
					commentClientPayments ON commentClientPayments.idFile=clientPayments.idFile AND 
					commentClientPayments.dateTransation=clientPayments.depositDate
					WHERE 1=1
					';
			return $query;
	}
	public function queryOrder()
	{
			$query=" ORDER BY DATE_FORMAT(depositDate, '%Y/%m/%d'),lastName,firstName";
			return $query;
	}
}
