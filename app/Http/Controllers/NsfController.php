<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;


use Auth;
use View;
use Input;
use Session;
Use Redirect;
use Mail;
use Response;
use Carbon\Carbon;
use App\RescheduleEFT;
use App\Client;

class NsfController extends BaseSoapController
{
  
  public function index(){
    $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
    return view('soap.nsfForm', compact('client')); 
  }
  
  public function voidClient($client){
    $this->service = InstanceSoapClient::init();
    $params = array(
      'UserName' => env('API_USER'),
      'PassWord' => env('API_PASSWORD'),
      'CardHolderID'=> $client->idFile
    );

    $result = $this->service->FindEftByID($params);

    foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft1){
      if($eft1->StatusCode == 'Create EFT Pending'){
        $message = $this->service->VoidEft(
          [
            'UserName' => env('API_USER'),
            'PassWord' => env('API_PASSWORD'),
            'EftTransactionID' => $eft1->EftTransactionID
            ]
          );
        }
      }
      
      $resultFee = $this->service->FindFeeByCardHolderID($params);
      foreach($resultFee->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
        if($fee->StatusCode=='Pending'){
          $message = $this->service->VoidFee(
            [
              'UserName' => env('API_USER'),
              'PassWord' => env('API_PASSWORD'),
              'FeeID' => $fee->FeeID
              ]
            );
          }
        }
    dd('TERMINA VOID');
  }
  
  public static function syncNSF(){
    
    ini_set('max_execution_time', 999999999);
    ini_set('memory_limit', '200048M');
    
    Log::useFiles(storage_path().'/logs/syncNSF.log');
    Log::info('Run SyncNSF');
    //Get Clients
    // $clients = Client::where('provider','=','EPPS')->get();
    $clients = Client::where('idFile','=',1124)->get();
    foreach($clients as $client){
      (new self)->nsfReSchedule($client);
    }
    Log::info('Finish SyncNSF');
    dd('TERMINA');
  }
  
  private function findNSF($idFile,$eftTransactionID){
    return RescheduleEFT::where('idFile','=',$idFile)
    ->where('EftTransactionID','=',$eftTransactionID)->first();
  }
  
  private function findNSFLastMonth($idFile){
    $date = Carbon::yesterday();
    // $date = Carbon::create(2020, 01, 24, 0, 0, 0);
    //dd($date->subMonthsNoOverflow(1));
    return RescheduleEFT::where('idFile','=',$idFile)
    ->whereMonth('created_at', '=', $date->subMonthsNoOverflow(1)->month)->first();
  }
  
  private function findNSFSameMonth($idFile){
    $date = Carbon::yesterday();
    // $date = Carbon::create(2019, 01, 24, 0, 0, 0);
    return RescheduleEFT::where('idFile','=',$idFile)
    ->whereMonth('created_at', '=', $date->month)->whereYear('created_at', $date->year)->first();
  }
  
  public function nsfReSchedule($client){
    
    ini_set('max_execution_time', 999999999);
    ini_set('memory_limit', '200048M');
    
    //Get Client
    //$client = Client::find($request->searchClient);
    
    // Init Soap Service
    $this->service = InstanceSoapClient::init();
    $params = array(
      'UserName' => env('API_USER'),
      'PassWord' => env('API_PASSWORD'),
      'CardHolderID'=> $client->idFile
    );
    
    //Get all EFT for Client
    $result = $this->service->FindEftByID($params);
    
    //Loop on EFTs
     $nowDate = Carbon::yesterday();
    //  $nowDate = Carbon::create(2019, 01, 24, 0, 0, 0);
    //  $nowDate = Carbon::create(2019, 05, 28, 0, 0, 0);
    
    //  dd($nowDate);
    
    if(isset($result->FindEftByIDResult->EFTList->EFTTransactionDetail)){

      
      foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft){
        // dd($eft);
        if($eft->StatusCode == 'Returned'){
          
          $eftDate = Carbon::createFromTimestamp(strtotime($eft->ReturnedDate));
          
          // dd($eftDate);
          
          // dd($eftDate>=$nowDate);
          
          //Check if EFT is this month
          if($eftDate >= $nowDate  && !$this->findNSF($client->idFile,$eft->EftTransactionID)){

             //dd($eft);
            
            //Check if EFT is New NSF
            if($eft->NSFReturnCode == 'R01'){

              // dd($eft);
              
              //Check if exists NSF on previous month
              if($this->findNSFLastMonth($client->idFile)){
              // if(1){

                // dd($eft);
                
                // Change Others EFT +3 month
                foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft1){
                  if($eft1->StatusCode == 'Create EFT Pending'&& Carbon::createFromTimestamp(strtotime($eft1->EftDate)) > $nowDate){
                    $message = $this->service->UpdateEft(
                      [
                        'UserName' => env('API_USER'),
                        'PassWord' => env('API_PASSWORD'),
                        'CardHolderID' => $eft1->CardHolderId,
                        'EftTransactionID' => $eft1->EftTransactionID,
                        'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft1->EftDate.' +3 month')),
                        'EftAmount' => $eft1->EftAmount,
                        'EftFee' => 0,
                        'BankName' => $eft1->BankName,
                        'BankCity' => $eft1->BankCity,
                        'BankState' => $eft1->BankState,
                        'AccountNumber' => $eft1->AccountNumber,
                        'RoutingNumber' => $eft1->RoutingNumber,
                        'AccountType' => 'Checking',
                        'Memo' => $eft1->Memo
                        ]
                      );
                    }
                  }
                  
                  // Put EFT on +3 month
                  
                  $message = $this->service->AddEft(
                    [
                      'UserName' => env('API_USER'),
                      'PassWord' => env('API_PASSWORD'),
                      'CardHolderID' => $eft->CardHolderId,
                      'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft->EftDate.' +3 month')),
                      'EftAmount' => $eft->EftAmount,
                      'EftFee' => 0,
                      'BankName' => $eft->BankName,
                      'BankCity' => $eft->BankCity,
                      'BankState' => $eft->BankState,
                      'AccountNumber' => $eft->AccountNumber,
                      'RoutingNumber' => $eft->RoutingNumber,
                      'AccountType' => 'Checking',
                      'Memo' => $eft->Memo
                      ]
                    );
                    
                    $resultFee = $this->service->FindFeeByCardHolderID($params);
                    foreach($resultFee->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
                      if($fee->StatusCode=='Pending' && Carbon::createFromTimestamp(strtotime($fee->Fee_Date)) > $nowDate){
                        $message = $this->service->UpdateFee(
                          [
                            'UserName' => env('API_USER'),
                            'PassWord' => env('API_PASSWORD'),
                            'FeeID' => $fee->FeeID,
                            'FeeDate' => date("Y-m-d\TH:i:s",strtotime($fee->Fee_Date.' +3 month')),
                            'FeeAmount' => $fee->FeeAmount,
                            'Description' => $fee->Description,
                            'FeeType' => $fee->FeeType,
                            'PaidToName' => $fee->PaidToName,
                            'PaidToPhone' => $fee->PaidToPhone,
                            'PaidToStreet' => $fee->PaidToStreet,
                            'PaidToStreet2' => $fee->PaidToStreet2,
                            'PaidToCity' => $fee->PaidToCity,
                            'PaidToState' => $fee->PaidToState,
                            'PaidToZip' => $fee->PaidToZip,
                            'PaidToCustomerNumber' => $fee->PaidToContactName,
                            'ContactName' => $fee->PaidToName,
                            ]
                          );
                        }
                    }
                      
                      
                      //Email to client
                      $data = array(
                        'client' => $client->firstName.' '.$client->lastName,
                        'amount' =>$eft->EftAmount,
                        'date'=> date("m/d/Y",strtotime($eft->EftDate)),
                        'reason'=>$eft->StatusCode
                      );
                      $sendClient = Mail::send('emails.suspendClient', $data, function ($message) use ($client,$eft) {
                        $message->from('accounting@danprogram.com');
                        $message->to($client->email);
                        $message->subject('Your payment on '.date("m/d/Y",strtotime($eft->EftDate)).' has returned as '.$eft->LastMessage);
                      });

                      //Email to consumer care
                      $dataMail= array(
                        'subject' =>$client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended',
                        'reason' => $eft->LastMessage
                      );
                      
                      //Email to consumer care
                      $sendConsumer = Mail::send('emails.suspendDan', $dataMail, function ($message) use ($client) {
                        $message->to('accounting@danprogram.com');
                        $message->from('system@danprogram.com');
                        $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been NSF Suspended');
                      });

                      $sendConsumer = Mail::send('emails.suspendDan', $dataMail, function ($message) use ($client) {
                        $message->to('steven@nyxent.com');
                        $message->from('system@danprogram.com');
                        $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been NSF Suspended');
                      });
                      
                      //Suspended Client
                      $client->idFileStatus = 18;
                      $client->save();
                      
                      $rescheduleEFT = new RescheduleEFT;
                      $rescheduleEFT->idFile = $client->idFile;
                      $rescheduleEFT->EftTransactionID = $eft->EftTransactionID;
                      $rescheduleEFT->save();
                      
                      \DB::insert("INSERT INTO history ( idFile, title,detail,iduser) VALUES ('".$client->idFile."','NSF Suspension','".$eft->LastMessage."',0);");
                      // Session::flash('message', 'NSF Suspension');
                      // return Redirect::to('nsf');
                    }
                    else{

                      // dd('MISMO MES');
                      
                      //check if exist another NSF in same month
                      if($this->findNSFSameMonth($client->idFile)){
                      //  if(1){
                        // dd($eft);
                        // Change Others EFT +1 month
                        foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft1){
                          if($eft1->StatusCode == 'Create EFT Pending' && Carbon::createFromTimestamp(strtotime($eft1->EftDate)) > $nowDate){
                            $message = $this->service->UpdateEft(
                              [
                                'UserName' => env('API_USER'),
                                'PassWord' => env('API_PASSWORD'),
                                'CardHolderID' => $eft1->CardHolderId,
                                'EftTransactionID' => $eft1->EftTransactionID,
                                'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft1->EftDate.' +1 month')),
                                'EftAmount' => $eft1->EftAmount,
                                'EftFee' => 0,
                                'BankName' => $eft1->BankName,
                                'BankCity' => $eft1->BankCity,
                                'BankState' => $eft1->BankState,
                                'AccountNumber' => $eft1->AccountNumber,
                                'RoutingNumber' => $eft1->RoutingNumber,
                                'AccountType' => 'Checking',
                                'Memo' => $eft1->Memo
                                ]
                              );
                            }
                          }
                          // Put EFT on next month
                          
                          $message = $this->service->AddEft(
                            [
                              'UserName' => env('API_USER'),
                              'PassWord' => env('API_PASSWORD'),
                              'CardHolderID' => $eft->CardHolderId,
                              'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft->EftDate.' +1 month')),
                              'EftAmount' => $eft->EftAmount,
                              'EftFee' => 0,
                              'BankName' => $eft->BankName,
                              'BankCity' => $eft->BankCity,
                              'BankState' => $eft->BankState,
                              'AccountNumber' => $eft->AccountNumber,
                              'RoutingNumber' => $eft->RoutingNumber,
                              'AccountType' => 'Checking',
                              'Memo' => $eft->Memo
                              ]
                            ); 
                            $resultFee = $this->service->FindFeeByCardHolderID($params);
                            foreach($resultFee->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
                              if($fee->StatusCode=='Pending' && Carbon::createFromTimestamp(strtotime($fee->Fee_Date)) > $nowDate){
                                $message = $this->service->UpdateFee(
                                  [
                                    'UserName' => env('API_USER'),
                                    'PassWord' => env('API_PASSWORD'),
                                    'FeeID' => $fee->FeeID,
                                    'FeeDate' => date("Y-m-d\TH:i:s",strtotime($fee->Fee_Date.' +1 month')),
                                    'FeeAmount' => $fee->FeeAmount,
                                    'Description' => $fee->Description,
                                    'FeeType' => $fee->FeeType,
                                    'PaidToName' => $fee->PaidToName,
                                    'PaidToPhone' => $fee->PaidToPhone,
                                    'PaidToStreet' => $fee->PaidToStreet,
                                    'PaidToStreet2' => $fee->PaidToStreet2,
                                    'PaidToCity' => $fee->PaidToCity,
                                    'PaidToState' => $fee->PaidToState,
                                    'PaidToZip' => $fee->PaidToZip,
                                    'PaidToCustomerNumber' => $fee->PaidToContactName,
                                    'ContactName' => $fee->PaidToName,
                                    ]
                                  );
                                }
                              }
                              
                              
                              //Email to client
                              $data = array(
                                'client' => $client->firstName.' '.$client->lastName,
                                'amount' =>$eft->EftAmount,
                                'date'=> date("m/d/Y",strtotime($eft->EftDate)),
                                'newAmount'=>$eft->EftAmount+50,
                                'newDate'=>date("m/d/Y",strtotime($eft->EftDate.' +1 month'))
                              );
                              $sendClient = Mail::send('emails.rerunClient', $data, function ($message) use ($client) {
                                $message->from('accounting@danprogram.com');
                                $message->to($client->email);
                                $message->subject('Your rerun payment has RETURNED as NSF');
                              });
                              
                              //Email to consumer care
                              $sendConsumer = Mail::send('emails.rerunConsumer',[], function ($message) use ($client) {
                                $message->to('accounting@danprogram.com');
                                $message->from('system@danprogram.com');
                                $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') rerun draft has returned as NSF');
                              });
                              $sendConsumer = Mail::send('emails.rerunConsumer',[], function ($message) use ($client) {
                                $message->to('steven@nyxent.com');
                                $message->from('system@danprogram.com');
                                $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') rerun draft has returned as NSF');
                              });
                              
                              $rescheduleEFT = new RescheduleEFT;
                              $rescheduleEFT->EftTransactionID = $eft->EftTransactionID;
                              $rescheduleEFT->idFile = $client->idFile;
                              $rescheduleEFT->save();
                              
                              //Payment issues
                              $client->idFileStatus = 9;
                              $client->save();
                              \DB::insert("INSERT INTO history ( idFile, title,detail,iduser) VALUES ('".$client->idFile."','NSF are Re Schedule to next month','',0);");
                              // Session::flash('message', 'NSF are Re Schedule to next month');
                              // return Redirect::to('nsf');
                              
                            }
                            else{
                              
                              //  dd('2Days');
                              // Re schedule EFT 2 days later
                              
                              $message = $this->service->AddEft(
                                [
                                  'UserName' => env('API_USER'),
                                  'PassWord' => env('API_PASSWORD'),
                                  'CardHolderID' => $eft->CardHolderId,
                                  'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft->ReturnedDate.' +3 days')),
                                  'EftAmount' => $eft->EftAmount,
                                  'EftFee' => 0,
                                  'BankName' => $eft->BankName,
                                  'BankCity' => $eft->BankCity,
                                  'BankState' => $eft->BankState,
                                  'AccountNumber' => $eft->AccountNumber,
                                  'RoutingNumber' => $eft->RoutingNumber,
                                  'AccountType' => 'Checking',
                                  'Memo' => $eft->Memo
                                  ]
                                ); 
                                // Send Email to client
                                $data = array(
                                  'client' => $client->firstName.' '.$client->lastName,
                                  'amount' =>$eft->EftAmount,
                                  'date'=> date("m/d/Y",strtotime($eft->EftDate)),
                                  'newAmount'=>$eft->EftAmount+25,
                                  'newDate'=>date("m/d/Y",strtotime($eft->EftDate.' +3 days'))
                                );
                                $send = Mail::send('emails.2days', $data, function ($message) use ($client) {
                                  $message->from('accounting@danprogram.com');
                                  $message->to($client->email);
                                  $message->subject('Your payment has RETURNED as NSF');
                                });
                                $rescheduleEFT = new RescheduleEFT;
                                $rescheduleEFT->idFile = $client->idFile;
                                $rescheduleEFT->EftTransactionID = $eft->EftTransactionID;
                                $rescheduleEFT->save();
                                \DB::insert("INSERT INTO history ( idFile, title,detail,iduser) VALUES ('".$client->idFile."','NSF are Re Schedule 3 Days','',0);");
                                // Session::flash('message', 'NSF are Re Schedule 3 Days');
                                // return Redirect::to('nsf');
                              }
                            }
                          }
                          else{
                            
                            if($eft->NSFReturnCode == 'R02'||$eft->NSFReturnCode == 'R03'||$eft->NSFReturnCode == 'R04'){
                              
                              //dd('3Months');
                              // Change Others EFT +3 month
                              foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft1){
                                if($eft1->StatusCode == 'Create EFT Pending'&& Carbon::createFromTimestamp(strtotime($eft1->EftDate)) > $nowDate){
                                  $message = $this->service->UpdateEft(
                                    [
                                      'UserName' => env('API_USER'),
                                      'PassWord' => env('API_PASSWORD'),
                                      'CardHolderID' => $eft1->CardHolderId,
                                      'EftTransactionID' => $eft1->EftTransactionID,
                                      'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft1->EftDate.' +3 month')),
                                      'EftAmount' => $eft1->EftAmount,
                                      'EftFee' => 0,
                                      'BankName' => $eft1->BankName,
                                      'BankCity' => $eft1->BankCity,
                                      'BankState' => $eft1->BankState,
                                      'AccountNumber' => $eft1->AccountNumber,
                                      'RoutingNumber' => $eft1->RoutingNumber,
                                      'AccountType' => 'Checking',
                                      'Memo' => $eft1->Memo
                                      ]
                                    );
                                  }
                                }
                                
                                // Put EFT on +3 month
                                
                                $message = $this->service->AddEft(
                                  [
                                    'UserName' => env('API_USER'),
                                    'PassWord' => env('API_PASSWORD'),
                                    'CardHolderID' => $eft->CardHolderId,
                                    'EftDate' => date("Y-m-d\TH:i:s",strtotime($eft->EftDate.' +3 month')),
                                    'EftAmount' => $eft->EftAmount,
                                    'EftFee' => 0,
                                    'BankName' => $eft->BankName,
                                    'BankCity' => $eft->BankCity,
                                    'BankState' => $eft->BankState,
                                    'AccountNumber' => $eft->AccountNumber,
                                    'RoutingNumber' => $eft->RoutingNumber,
                                    'AccountType' => 'Checking',
                                    'Memo' => $eft->Memo
                                    ]
                                  );
                                  
                                  
                                  $resultFee = $this->service->FindFeeByCardHolderID($params);
                                  foreach($resultFee->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
                                    if($fee->StatusCode=='Pending' && Carbon::createFromTimestamp(strtotime($fee->Fee_Date)) > $nowDate){
                                      $message = $this->service->UpdateFee(
                                        [
                                          'UserName' => env('API_USER'),
                                          'PassWord' => env('API_PASSWORD'),
                                          'FeeID' => $fee->FeeID,
                                          'FeeDate' => date("Y-m-d\TH:i:s",strtotime($fee->Fee_Date.' +3 month')),
                                          'FeeAmount' => $fee->FeeAmount,
                                          'Description' => $fee->Description,
                                          'FeeType' => $fee->FeeType,
                                          'PaidToName' => $fee->PaidToName,
                                          'PaidToPhone' => $fee->PaidToPhone,
                                          'PaidToStreet' => $fee->PaidToStreet,
                                          'PaidToStreet2' => $fee->PaidToStreet2,
                                          'PaidToCity' => $fee->PaidToCity,
                                          'PaidToState' => $fee->PaidToState,
                                          'PaidToZip' => $fee->PaidToZip,
                                          'PaidToCustomerNumber' => $fee->PaidToContactName,
                                          'ContactName' => $fee->PaidToName,
                                          ]
                                        );
                                      }
                                    } 
                                    
                                    //Suspended Client
                                    $client->idFileStatus = 19;
                                    $client->save();
                                    $from ='accounting@danprogram.com';
                                  }
                                  else{
                                    
                                    //voidAll
                                    foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft1){
                                      if($eft1->StatusCode == 'Create EFT Pending'&& Carbon::createFromTimestamp(strtotime($eft1->EftDate)) > $nowDate){
                                        $message = $this->service->VoidEft(
                                          [
                                            'UserName' => env('API_USER'),
                                            'PassWord' => env('API_PASSWORD'),
                                            'EftTransactionID' => $eft1->EftTransactionID
                                            ]
                                          );
                                        }
                                      }
                                      
                                      $resultFee = $this->service->FindFeeByCardHolderID($params);
                                      foreach($resultFee->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
                                        if($fee->StatusCode=='Pending' && Carbon::createFromTimestamp(strtotime($fee->Fee_Date)) > $nowDate){
                                          $message = $this->service->VoidFee(
                                            [
                                              'UserName' => env('API_USER'),
                                              'PassWord' => env('API_PASSWORD'),
                                              'FeeID' => $fee->FeeID
                                              ]
                                            );
                                          }
                                        }
                                        //Suspended Client
                                        $client->idFileStatus = 19;
                                        $client->save();
                                        $from ='accounting@danprogram.com';
                                      }
                                      
                                      //Email to client
                                      $data = array(
                                        'client' => $client->firstName.' '.$client->lastName,
                                        'amount' =>$eft->EftAmount,
                                        'date'=> date("m/d/Y",strtotime($eft->EftDate)),
                                        'reason'=>$eft->LastMessage,
                                      );
                                      $sendClient = Mail::send('emails.suspendClient', $data, function ($message) use ($client,$eft) {
                                        $message->from('accounting@danprogram.com');
                                        $message->to($client->email);
                                        $message->subject('Your payment on '.date("Y-m-d",strtotime($eft->EftDate)).' has returned as '.$eft->LastMessage);
                                      });
                                      
                                      //Email to consumer care
                                      $dataMail= array(
                                        'subject' =>$client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended',
                                        'reason' => $eft->LastMessage
                                      );
                                      $sendConsumer = Mail::send('emails.suspendDan',$dataMail, function ($message) use ($client,$from) {
                                        $message->to($from);
                                        $message->from('system@danprogram.com');
                                        $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended');
                                      });

                                    //Email to consumer care
                                    $dataMail= array(
                                      'subject' =>$client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended',
                                      'reason' => $eft->LastMessage
                                    );
                                    $sendConsumer = Mail::send('emails.suspendDan',$dataMail, function ($message) use ($client,$from) {
                                      $message->to('steven@nyxent.com');
                                      $message->from('system@danprogram.com');
                                      $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended');
                                    });

                                    //Email to consumer care
                                    $dataMail= array(
                                      'subject' =>$client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended',
                                      'reason' => $eft->LastMessage
                                    );
                                    $sendConsumer = Mail::send('emails.suspendDan',$dataMail, function ($message) use ($client,$from) {
                                      $message->to('sandscynthia@gmail.com');
                                      $message->from('system@danprogram.com');
                                      $message->subject($client->firstName.' '.$client->lastName.' ('.$client->idFile.') has been suspended');
                                    });
                                      
                                      //Email to epps
                                      /*
                                      $data = array(
                                        'client' => $client->firstName.' '.$client->lastName,
                                        'idFile'=>$client->idFile
                                      );
                                      $sendConsumer = Mail::send('emails.suspendEpps',$data, function ($message) use ($client) {
                                        $message->to('adunham@eppscard.com');
                                        $message->from('sandscynthia@gmail.com');
                                        $message->subject('Please suspend client '.$client->firstName.' '.$client->lastName.' ('.$client->idFile.') temporarily');
                                      });*/
                                      
                                      $rescheduleEFT = new RescheduleEFT;
                                      $rescheduleEFT->idFile = $client->idFile;
                                      $rescheduleEFT->EftTransactionID = $eft->EftTransactionID;
                                      $rescheduleEFT->save();
                                      
                                      \DB::insert("INSERT INTO history ( idFile, title,detail,iduser) VALUES ('".$client->idFile."','Suspension','".$eft->LastMessage."',0);");
                                      // Session::flash('message', 'Suspension '.$eft->LastMessage);
                                      // return Redirect::to('nsf');
                                    }
                                  }
                                }
                              }//cierra foreach de los EFts
                            } // cierra if isset efts
                            unset($client);
                            unset($result);
                            unset($params);
                            unset($message);
                            unset($resultFee);
                          }
                        }
                        