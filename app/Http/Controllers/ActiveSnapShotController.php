<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ActiveSnapShotController extends BaseSoapController
{
		public function index()
		{
			$client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
			return view('snapshot.form', compact('client')); 
    }

		public function activesnap(Request $request)
		{
			$client = Client::Find($request->GET('searchClient'));

			// Init Soap Service
			$this->service = InstanceSoapClient::init();

			// Set params for client
			$params = array(
					'UserName' => env('API_USER'),
					'PassWord' => env('API_PASSWORD'),
					'CardHolderID'=> $client->idFile
			);
			
			//Get all EFT for Client
			$resultEFT = $this->service->FindEftByID($params)->FindEftByIDResult->EFTList->EFTTransactionDetail;
			$resultFee = $this->service->FindFeeByCardHolderID($params)->FindFeeByCardHolderIDResult->FeeList->Fee;

			return view('snapshot.table', compact('resultEFT', 'resultFee'));
    }
}
