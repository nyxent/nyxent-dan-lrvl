<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Controllers\CommissionPaymentsController;
use App\ClientPayments;
use App\CommissionPayments;
use App\RescheduleEFT;
use App\ActiveLog;
use App\History;
use App\Client;
use DateTime;
use App\Http\Controllers\CommissionEntryController;

class RepairCommissionController extends BaseSoapController {

    public function sortByDate($a, $b) {
        $a = $a['date'];
        $b = $b['date'];

        if ($a == $b)
            return 0;
        return ($a < $b) ? -1 : 1;
    }

    public function index(Request $request) {


        /*
         * set  maximun execution  time
         */
        ini_set('max_execution_time', 1400);
        $page = Input::get('page', 1);
        $paginate = 100;
        /*
         * FUNCTION IN ANOTHER CONTROLLER
         */
        $CommisionPaymentsController = new CommissionPaymentsController();
        $client = $CommisionPaymentsController->client();
        /*         * ********************************** */
        $ClientPayments = new ClientPayments;
        $CommissionPayments = new CommissionPayments;

        $Companys = array();
        $CompanysPaymentsDate = array();
        $PaymentsEPPSTrasantion = array();
        $CommissionEPPSTrasantion = array();
        /*         * ********************************* */
        $searchClient = trim($request->GET('searchClient'));
        Session::put('idFilesearch', $searchClient);
        $AccountNumber = '';
        $RoutingNumber = '';
        $accountBalance = 0;
        //$searchClient='1234';

        $NameClient = '';
        $idFile = '';
        $idFileStatus = '';
        $fileStatusName = '';
        $paymentData = array();
        $companies = array();

        if ($searchClient) {

            $payments = DB::select("select distinct
											c.depositDate as date,
											'dep' as type,
											CONCAT_WS( '-', c.amount, c.eftTransactionID) amount,
											c.provider,
											c.statusCode,
											c.statusCodeDescription,
											c.paymentNumber,
                                                                                        c.id,
											p.manualPaymentNumber
										from
											clientPayments c
										left join
											commentClientPayments p
										on c.idFile = p.idFile AND IF(c.provider = 'SAS', CONCAT( c.depositDate,'_', c.amount), c.eftTransactionID) = p.clientPaymentID
										where
											c.idFile = " . $searchClient . "
											and c.statusCode NOT IN ('RF','V','PFV','PF')
										order by
											date,c.id");
            $payments = array_map(function ($value) {
                return (array) $value;
            }, $payments);

            $affiliates = DB::select('SELECT
			DISTINCT
				a.affiliateCompanyId,
				a.affiliateCompanyName
			FROM
				affiliateCompany a
			INNER JOIN commissionPayments c ON
				a.affiliateCompanyId = c.affiliateCompanyId
			WHERE
				c.idFile =' . $searchClient);
            $commisions = array();
            foreach ($affiliates as $a) {
                $commisionsAffiliate = DB::select("SELECT
				c.payoutDate as date,
				c.statusCode as statusCode,
				c.provider as provider,
                                c.id,
                                c.eftTransactionID,
				CONCAT_WS('_',MAX(c.statusCode),SUM(c.amount),GROUP_CONCAT(c.feeID)) as amount
				FROM
					commissionPayments c
				WHERE
					c.idFile = " . $searchClient . "
				AND c.affiliateCompanyId = " . $a->affiliateCompanyId . "
				AND c.amount > 0
				AND c.statusCode <> 'V'
				GROUP BY
				c.payoutDate,
				c.statusCode,
				c.provider,
                                c.id,
                                c.eftTransactionID
				ORDER BY
					c.payoutDate");
                foreach ($commisionsAffiliate as $b) {

                    $commisions[$b->date]['date'] = $b->date;
                    $commisions[$b->date]['statusCode'] = $b->statusCode;
                    $commisions[$b->date]['provider'] = $b->provider;
                    $commisions[$b->date]['type'] = 'com';
                    $companyName = str_replace(' ', '_', str_replace(',', '', str_replace('-', '_', str_replace('.', '', $a->affiliateCompanyName))));
                    $commisions[$b->date][$companyName] = $b->amount;
                    $commisions[$b->date]["{$companyName}-id"] = $b->id;
                    $commisions[$b->date]["{$companyName}-eftTransactionID"] = $b->eftTransactionID;
                }
            }
            // $commisions =DB::select('CALL active_commision('.$searchClient.')');
            $commisions = array_map(function ($value) {
                return (array) $value;
            }, $commisions);
            $fields = array_merge($payments, $commisions);
            $result = array_multisort(array_column($fields, 'date'), SORT_ASC, array_column($fields, 'type'), SORT_DESC, $fields);

            $NameClientResult = DB::select("SELECT CONCAT(a.lastName,' ',a.firstName) AS name,
												 a.ppRoutingNumber as RoutingNumber,
												 a.ppAccountNumber as AccountNumber,
												 a.idFile,
												 a.idFileStatus,
												 a.affiliateCompanyId,
												 b.name as fileStatusName
				FROM file a
				INNER JOIN fileStatus b
				ON a.idFileStatus = b.idFileStatus
				WHERE a.idFile='" . $searchClient . "'
			");
            $NameClient = $NameClientResult[0]->name;


            $idFile = $NameClientResult[0]->idFile;


            $affiliateIds = DB::select("SELECT
					CONCAT_WS(',',a.finderId,
					a.superAffiliateId,
					a.finderId2,
					a.superAffiliateId2) as ids
				FROM
					affiliateCompany a
				INNER JOIN
				file f
				ON a.affiliateCompanyId = f.affiliateCompanyId
				WHERE
				f.idFile =" . $searchClient);

            $companySql = "SELECT
                        a.affiliateCompanyId,
                        a.affiliateCompanyName
                FROM
                        user u
                LEFT JOIN userAffiliateCompany x ON
                        u.idUser = x.idUser
                LEFT JOIN affiliateCompany a ON
                        x.affiliateCompanyId = a.affiliateCompanyId
                WHERE
                        u.idUser IN (" . $affiliateIds[0]->ids . ")
                UNION
                SELECT 
                        a.affiliateCompanyId,
                        a.affiliateCompanyName
                FROM 
                        affiliateCompany a
                WHERE
                        a.affiliateCompanyId IN (16,21,24,29," . $NameClientResult[0]->affiliateCompanyId . ")
									";
            $Companys = DB::select($companySql);


            $companies = array_unique(array_merge($affiliates, $Companys), SORT_REGULAR);

            foreach ($payments as $_payment) {
                $paymentData[$_payment['id']] = $_payment;
            }
        }

        if (!isset($fields)) {
            $fields = array();
        }



        $data = [
            "itemsFile" => $fields,
            'client' => $client,
            'Companys' => $companies,
            'idFile' => $idFile,
            'payments' => $paymentData,
            'NameClient' => $NameClient];

        return view('form.repairCommission.index', $data);
    }

    public function saveRepairCommission(Request $request) {

        foreach ($request->commissions as $commission) {
            $commission = CommissionPayments::find($commission);
            if ($commission) {
                $commission->eftTransactionID = $request->idPayment;
                $commission->save();
            }
        }
    }

}
