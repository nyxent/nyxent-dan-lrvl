<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
     public function index(Request $request)
    {	
		return view('login.index');	
	}
	public function store(Request $request)
    {
		$user=trim($request->GET('user'));
		$password=trim($request->GET('password'));
		
		if($user and  $password and $request->session()->get('loginClient') ===null)
		{
			if( $user=='RobertA' and $password=='R0b3rt4' )
			{
				$request->session()->put('loginClient','Ok');
				return redirect('/form/clientReport');
			}
		}
		if($request->session()->get('loginClient')=='Ok')
		{
			return redirect('/form/clientReport');
		}
		if($request->session()->get('login') ===null)
		{
			return redirect('/login');
		}
        return $next($request);

	}
}
