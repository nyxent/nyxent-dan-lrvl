<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

use Auth;
use View;
use Input;
use Session;
Use Redirect;
use Mail;
use Response;
use Carbon\Carbon;
use App\RescheduleEFT;
use App\Client;
use App\ClientPayments;
use App\CommissionPayments;
use App\State;
use App\History;

class EppsController extends BaseSoapController{
  private $service;

  public function createNewFee($split,$idFile,$EftTransactionID,$affiliateCompanyID){
    $message = $this->service->AddFee($split);
    $dataArray = [
      'idFile' => $idFile,
      'payoutDate' => $split['FeeDate'],
      'amount' => $split['FeeAmount']*100,
      'affiliateCompanyName' => $split['PaidToName'],
      'provider' => 'EPPS',
      'affiliateCompanyId' => $affiliateCompanyID,
      'statusCode'=>'P',
      'feeID'=>$message->AddFeeResult->FeeID
    ];
    $commissionPayment = CommissionPayments::where('feeID', $message->AddFeeResult->FeeID)->first();
    if ($commissionPayment == null) {
      $commissionPayment = CommissionPayments::create($dataArray);
    } else {
      $commissionPayment->update($dataArray);
    }
    return $message;
  }

  public function updateFee($feeID,$split,$idFile,$EftTransactionID,$affiliateCompanyID){
    $message = $this->service->UpdateFee(
      [
      'UserName' => env('API_USER'),
      'PassWord' => env('API_PASSWORD'),
      'FeeID' => $feeID,
      'FeeDate' => date("Y-m-d\TH:i:s",strtotime($split['FeeDate'])),
      'FeeAmount' => $split['FeeAmount'],
      'Description' =>$split['Description'],
      'FeeType' => $split['FeeType'],
      'PaidToName' => $split['PaidToName'],
      'PaidToPhone' =>$split['PaidToPhone'],
      'PaidToStreet' => $split['PaidToStreet'],
      'PaidToStreet2' => $split['PaidToStreet2'],
      'PaidToCity' =>$split['PaidToCity'],
      'PaidToState' => $split['PaidToState'],
      'PaidToZip' => $split['PaidToZip'],
      'PaidToCustomerNumber' =>$split['PaidToCustomerNumber'],
      'ContactName' => $split['PaidToName'],
      ]
    );
    $dataArray = [
      'idFile' => $idFile,
      'payoutDate' => $split['FeeDate'],
      'amount' => $split['FeeAmount']*100,
      'affiliateCompanyName' => $split['PaidToName'],
      'provider' => 'EPPS',
      'statusCode'=>'P',
      'affiliateCompanyId' => $affiliateCompanyID,
      'eftTransactionID' => $EftTransactionID,
      'feeID'=>$feeID
    ];
    $commissionPayment = CommissionPayments::where('feeID', $feeID)->first();
    if ($commissionPayment == null) {
      $commissionPayment = CommissionPayments::create($dataArray);
    } else {
      $commissionPayment->update($dataArray);
    }
    
    return $message;
  }

  public function startEPPSPayments($idFile){
    //Init soap EPPS API service
    $this->service = InstanceSoapClient::init();

    //Get Client Info
    $client = Client::Find($idFile);

    //Get Modification contract info and payments
    $contract = \DB::table('contracts')->where('idFile',$client->idFile)->where('doc_DAN_AND_SAS_Status','completed')->where('active',1)->first();
    $payments = \DB::table('paymentsStatus')->where('idFile',$client->idFile)->get();

    //Init counters to EFTS and FEES
    $countEFT=0;
    $countFEE=0;

    $count = 0;

    //Create Account to EPPS
    $clientArray= array(
      'UserName' => env('API_USER'),
      'PassWord' => env('API_PASSWORD'),
      'CardHolderID'=> $client->idFile,
      'FirstName' => $client->firstName,
      'LastName'=> $client->lastName,
      'DateOfBirth'=> date("Y-m-d\TH:i:s",strtotime($client->dateBirth)),
      'SSN'=> preg_replace('/[^0-9]/', '',str_replace(' ', '-', $client->socialSecurity)),
      'Street'=> $client->address,
      'City'=> $client->city,
      'State'=> State::find($client->idState)->name,
      'Zip'=> $client->zip,
      'PhoneNumber'=> preg_replace('/[^0-9]/', '',str_replace(' ', '-', $client->homePhone)),
      'EmailAddress'=> $client->email
    );
    $result = $this->service->AddCardHolder($clientArray);

    if($result->AddCardHolderResult->StatusCode == 'Error'){
      return $result->AddCardHolderResult->Message;
    }    

    //Set Params to get info from EPPS
    $params = array(
      'UserName' => env('API_USER'),
      'PassWord' => env('API_PASSWORD'),
      'CardHolderID'=> $client->idFile
    );


    //Get EFTS from EPPS with Pending status to reuse
    $efts = array();
    $result = $this->service->FindEftByID($params);
    if(isset($result->FindEftByIDResult->EFTList->EFTTransactionDetail)){
      if(is_array($result->FindEftByIDResult->EFTList->EFTTransactionDetail)){
        foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft){
          if(strpos($eft->StatusCode, 'Pending') !== false){
            $efts[] = $eft;
          }
        }
      }else{
        $eft = $result->FindEftByIDResult->EFTList->EFTTransactionDetail;
        $efts[] = $eft;
      }
    }

    //Get FEES from EPPS with Pending status to reuse
    $fees= array();
    $result = $this->service->FindFeeByCardHolderID($params);
    if(isset($result->FindFeeByCardHolderIDResult->FeeList->Fee)){
      foreach($result->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
        if($fee->StatusCode=='Pending'){
          $fees[] = $fee;
        }
      }
    }


    //Loop to use payments 
    foreach($payments as $payment){

      //EFT
      $amount = $payment->paymentFinder + $payment->paymentSuperAffiliate + $payment->paymentAffiliate + $payment->paymentNyxent + $payment->paymentDAN
                  + $payment->paymentSAS + $payment->paymentUSMarketers + $payment->paymentKC+ $payment->paymentFinder2 + $payment->paymentSuperAffiliate2;
      if(isset($efts[$countEFT])){
        $message = $this->service->UpdateEft(
          [
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' =>  $client->idFile,
          'EftTransactionID' => $efts[$countEFT]->EftTransactionID,
          'EftDate' => date("Y-m-d\TH:i:s",strtotime($payment->scheduledPaymentDate)),
          'EftAmount' => $amount,
          'EftFee' => 0,
          'BankName' => $contract->ppBankName,
          'BankCity' => '',
          'BankState' => '',
          'AccountNumber' => $contract->ppAccountNumber,
          'RoutingNumber' =>$contract->ppRoutingNumber,
          'AccountType' => 'Checking',
          'Memo' => ''
          ]
        );    
        if($message->UpdateEftResult->StatusCode == 'Error'){
          return $message->UpdateEftResult->Message;
        }
        $EftTransactionID=$efts[$countEFT]->EftTransactionID;
              
        $dataArray = [
          'idFile' => $client->idFile,
          'depositDate' => $payment->scheduledPaymentDate,
          'amount' => str_replace(".", '', $amount),
          'provider' => 'EPPS',
          'eftTransactionID' => $EftTransactionID,
          'routingNumber' => $contract->ppRoutingNumber,
          'accountNumber' => $contract->ppAccountNumber,
          'statusCode'=>'P',
          'statusCodeDescription'=>'Create EFT Pending',
          'paymentNumber'=>$payment->paymentMonth?$payment->paymentMonth:0
        ];
        $clientPayment = ClientPayments::where('eftTransactionID', $EftTransactionID)->first();
        if ($clientPayment == null) {          
          $newClientPayment = ClientPayments::create($dataArray);
          $EftTransactionID=$newClientPayment->id;
        }else{
          $clientPayment->update($dataArray);
          $EftTransactionID=$clientPayment->id;
        }
        $countEFT++; 
      }else{
        $message = $this->service->AddEft(
          [
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'EftDate' => date("Y-m-d\TH:i:s",strtotime($payment->scheduledPaymentDate)),
          'EftAmount' => $amount,
          'EftFee' => 0,
          'BankName' => $contract->ppBankName,
          'BankCity' => '',
          'BankState' => '',
          'AccountNumber' => $contract->ppAccountNumber,
          'RoutingNumber' => $contract->ppRoutingNumber,
          'AccountType' => 'Checking',
          'Memo' => ''
          ]
        );
        if($message->AddEftResult->StatusCode == 'Error'){
          return $message->AddEftResult->Message;
        }
        $EftTransactionID = $message->AddEftResult->EftTransactionID;
        $dataArray = [
          'idFile' => $client->idFile,
          'depositDate' => $payment->scheduledPaymentDate,
          'amount' => str_replace(".", '', $amount),
          'provider' => 'EPPS',
          'eftTransactionID' => $EftTransactionID,
          'routingNumber' => $contract->ppRoutingNumber,
          'accountNumber' => $contract->ppAccountNumber,
          'statusCode'=>'P',
          'statusCodeDescription'=>'Create EFT Pending',
          'paymentNumber'=>$payment->paymentMonth?$payment->paymentMonth:0
        ];
        $clientPayment = ClientPayments::where('eftTransactionID', $EftTransactionID)->first();
        if ($clientPayment == null) {
          $newClientPayment = ClientPayments::create($dataArray);
          $EftTransactionID=$newClientPayment->id;
        }else{
          $clientPayment->update($dataArray);
          $EftTransactionID=$clientPayment->id;
        }
        $countEFT++;
      }

      //FEES 
      $newDate = date("Y-m-d\TH:i:s",strtotime($payment->scheduledPaymentDate));
      //Commision Affiliate

      if($payment->paymentAffiliate!=0 && $payment->affiliateCompanyId!=0){
        $company = \DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID =".$payment->affiliateCompanyId)[0];
        $amount = $payment->paymentAffiliate;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,$payment->affiliateCompanyId);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,$payment->affiliateCompanyId);
          $countFEE++;
        }
      }
      //Commision KC
      if($payment->paymentKC != 0){
        $company = \DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 29")[0];
        $amount = $payment->paymentKC;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,29);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,29);
          $countFEE++;
        }
      }

      //Commision NYXENT
      if($payment->paymentNyxent != 0){
        $company = \DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 24")[0];
        $amount = $payment->paymentNyxent;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,24);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,24);
          $countFEE++;
        }
      }
      
      //Commision DAN
      if($payment->paymentDAN != 0){
        $company = \DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 21")[0];
        $amount = $payment->paymentDAN;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,21);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,21);
          $countFEE++;
        }
      }
      
      //Commision USMARKETERS
      if($payment->paymentUSMarketers != 0){
        $company = \DB::select("SELECT * FROM affiliateCompany WHERE affiliateCompanyID = 16")[0];
        $amount = $payment->paymentUSMarketers;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,16);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,16);
          $countFEE++;
        }
      }
      
      //Commision SUPER AFFILIATE
      if($payment->paymentSuperAffiliate != 0 && $payment->superAffiliateId!=0){
        $company = \DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
        LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$payment->superAffiliateId)[0];
        $amount = $payment->paymentSuperAffiliate;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }
      }
      
      //Commision FINDER
      if($payment->paymentFinder != 0 && $payment->finderId !=0){
        $company = \DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
        LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$payment->finderId)[0];
        $amount = $payment->paymentFinder;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }
      }
      
      //Commision SUPER AFFILIATE 2
      if($payment->paymentSuperAffiliate2 != 0 && $payment->superAffiliateId2!=0){
        $company = \DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
        LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$payment->superAffiliateId2)[0];
        $amount = $payment->paymentSuperAffiliate2;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }
      }
      
      //Commision FINDER 2
      if($payment->paymentFinder2 != 0 && $payment->finderId2!=0){
        $company = \DB::select("SELECT affiliateCompany.*	FROM user	LEFT JOIN userAffiliateCompany ON user.idUser = userAffiliateCompany.idUser
        LEFT JOIN affiliateCompany ON userAffiliateCompany.affiliateCompanyId = affiliateCompany.affiliateCompanyId	WHERE	user.idUser = ".$payment->finderId2)[0];
        $amount = $payment->paymentFinder2;
        $feeData = array(
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'CardHolderID' => $client->idFile,
          'FeeDate' => $newDate,
          'FeeAmount' => $amount,
          'Description' => '',
          'FeeType' => 'SettlementPayment',
          'PaidToName' => $company->affiliateCompanyName,
          'PaidToPhone' => (isset($company->affiliateCompanyPhone))?$company->affiliateCompanyPhone:'',
          'PaidToStreet' => (isset($company->affiliateCompanyStreet))?$company->affiliateCompanyStreet:'',
          'PaidToStreet2' => '',
          'PaidToCity' => (isset($company->affiliateCompanyCity))?$company->affiliateCompanyCity:'',
          'PaidToState' => (isset($company->affiliateCompanyState))?$company->affiliateCompanyState:'',
          'PaidToZip' => (isset($company->affiliateCompanyZip))?$company->affiliateCompanyZip:'',
          'ContactName' => '',
          'PaidToCustomerNumber' => ''
        );
        if(isset($fees[$countFEE])){
          $this->updateFee($fees[$countFEE]->FeeID,$feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }else{
          $this->createNewFee($feeData,$client->idFile,$EftTransactionID,$company->affiliateCompanyId);
          $countFEE++;
        }
      }

    }

    //Void other efts
    if($countEFT < count($efts)){
      for($i=$countEFT;$i<count($efts);$i++){
        $message = $this->service->VoidEft([
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'EftTransactionID' => $efts[$i]->EftTransactionID ]);
      }
    }

    //Void other fees
    if($countFEE < count($fees)){
      for($i=$countFEE; $i < count($fees);$i++){
        $message = $this->service->VoidFee(
        [
          'UserName' => env('API_USER'),
          'PassWord' => env('API_PASSWORD'),
          'FeeID' => $fees[$i]->FeeID
          ]
        );
      }
    }
    return 1;    
  }

  public function voidInactiveClient($idFile){
    ini_set('max_execution_time', 11400);
    ini_set('memory_limit', '20048M');
    $transmittedDate = '';
    $this->service = InstanceSoapClient::init();

    $inactive = Client::Find($idFile);
    try{
      $params = array(
        'UserName' => env('API_USER'),
        'PassWord' => env('API_PASSWORD'),
        'CardHolderID'=> $inactive->idFile
      );
      $result = $this->service->FindEftByID($params);
      if(isset($result->FindEftByIDResult->EFTList->EFTTransactionDetail)){
        foreach($result->FindEftByIDResult->EFTList->EFTTransactionDetail as $eft){
          if(strpos($eft->StatusCode, 'Pending') !== false){
            $message = $this->service->VoidEft([
              'UserName' => env('API_USER'),
              'PassWord' => env('API_PASSWORD'),
              'EftTransactionID' => $eft->EftTransactionID ]);
          }
          // elseif($idFile ==2753){
          elseif($eft->StatusCode =='Transmitted'  ||  $eft->StatusCode =='In Prenote' ){
            $transmittedDate = $eft->EftDate;
            History::create(array('idFile' => $idFile,
						'title' => 'No Payment Void',
						'detail' => 'While Inactivating the client, '.$eft->EftDate.'- '.$eft->EftTransactionID.' payment could not be voided.',
						'idUser' => 0));
          }
          unset($message);
        }
      }
      unset($result);
      $result = $this->service->FindFeeByCardHolderID($params);
      if(isset($result->FindFeeByCardHolderIDResult->FeeList->Fee)){
        foreach($result->FindFeeByCardHolderIDResult->FeeList->Fee as $fee){
          if(strpos($fee->StatusCode, 'Pending') !== false && $fee->Fee_Date != $transmittedDate){
            $message = $this->service->VoidFee([
              'UserName' => env('API_USER'),
              'PassWord' => env('API_PASSWORD'),
              'FeeID' => $fee->FeeID ]);
          }
          unset($message);
        }
      }
      unset($result);
    }
    catch(\Exception $e) {
      return $e->getMessage();
    }

    if(!empty($transmittedDate)){
      return $transmittedDate;
    }
    else{
      return 1;
    }

    $client= \DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file WHERE provider IS NOT NULL ORDER BY CONCAT(TRIM(lastName),' ',firstName)");
    return view('soap.febsdForm', compact('client'));
  }
}