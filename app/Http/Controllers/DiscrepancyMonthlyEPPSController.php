<?php



namespace App\Http\Controllers;



use Session;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\CommissionPaymentsController;

use Input;



class DiscrepancyMonthlyEPPSController extends Controller

{

    public function index(Request $request)

    {

		/*

			* set  maximun execution  time

		*/

		ini_set('max_execution_time', 1400); 

		$page = Input::get('page', 1);

		$paginate = 10000;

		if ($request->GET('month') && $request->GET('year')) {

		$year = $request->GET('year');
		$month= $request->GET('month');

		/*********************************************/		

		$data =DB::select("
		SELECT
		f.idFile,
		f.firstName,
		f.lastName,
		p.depositDate,
		FORMAT(f.ppDownPayment,2) as ppDownPayment,
		FORMAT(f.ppMontlyPayment,2) as ppMontlyPayment,
		(p.amount / 100) as amountReal2,
		((SUM(c.amount))/ 100) as  amountCommission2,
		(((SUM(c.amount))/ 100 + 12.50)) as comepps
	FROM
		file f
	inner join 
		clientPayments p
		on f.idFile = p.idFile
		and p.eftTransactionID is not NULL
		and p.statusCode = 'P'
		AND MONTH(p.depositDate) = ".$month."
		AND YEAR(p.depositDate) = ".$year."
	inner JOIN commissionPayments c on
		f.idFile = c.idFile
		and c.affiliateCompanyName <> 'EPPS'
		and c.statusCode = 'P'
		AND MONTH(c.payoutDate) =".$month."
		AND YEAR(c.payoutDate) = ".$year."
	where
		f.provider = 'EPPS'
	group by
		f.idFile,
		f.firstName,
		f.lastName,
		p.depositDate,
		ppDownPayment,
		ppMontlyPayment,	
		amountReal2
	ORDER BY
		DATE_FORMAT(p.depositDate, '%Y/%m/%d')			
	");


	}

		

		$itemsFile=array();

		if(isset($data))

			{

				$offSet = ($page * $paginate) - $paginate;

				

				$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);

				

				$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);

		

			}

		

		return view('form.DiscrepancyMonthlyEPPS.index',["itemsFile"=>$itemsFile]);	

	}

}

