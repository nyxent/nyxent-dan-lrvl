<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

class CommisionReportAffilateKCController extends Controller
{
   public function index(Request $request)
    {
		$page = Input::get('page', 1);
		/*
			* FUNCTION IN ANOTHER CONTROLLER
		*/
		$CommisionPaymentsController=new CommissionPaymentsController();
		$paginate =$CommisionPaymentsController->paginateGlobal();
		$client=$CommisionPaymentsController->clientAffiliateKcCredit();
		
		if($request)
    	{
			$searchText=trim($request->GET('searchText'));
			$searchClient=trim($request->GET('searchClient'));
			$dateDownload=date('m-d-Y').' All';
			$date1 = $request->GET('date1');
			$date2 = $request->GET('date2');
			$action = $request->GET('action');
			$orderby = $request->GET('orderby');
			$where ='';
			if($searchClient!='')
			{
				$where.="  AND file.idFile='".$searchClient."' ";
			}
			if($date1!='' and $date2!='')
			{
				$dateDownload=$date1.' '.$date2;
				$where.=" AND payoutDate Between '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date1)."' AND  '".$CommisionPaymentsController->mmddyyyyToyyyymmdd($date2)."'";
			}
			$data =DB::select($this->query().$where.$CommisionPaymentsController->queryOrderCommission($orderby));
			/*
				* EXPORT
			*/
			if($action=='export')
			{
				return $CommisionPaymentsController->ExportCSV($data,'CommissionReportAffiliateKC'.$dateDownload.'.csv',6);	
			}
			/*
				* PAGINATE
			*/
			if(isset($data))
			{
			$offSet = ($page * $paginate) - $paginate;
			
			$itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
			
			$itemsFile= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page,['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
		
			}
			return view('form.commisionReportAffilateKC.index',["itemsFile"=>$itemsFile,'client'=>$client,'searchClient'=>$searchClient,'date1'=>$date1,'date2'=>$date2,'orderby'=>$orderby]);	
		}
    }
	public function query()
	{
		$query="
			SELECT
			file.idFile AS CRM_ID,
			file.firstName,
			file.lastName,
			DATE_FORMAT(commissionPayments.payoutDate, '%m/%d/%Y') payoutDate ,
			CONCAT('$', FORMAT(commissionPayments.amount/100,2)) AS Commission,
			commentCommissionPayments.paymentNumber,
			commentCommissionPayments.comment,
			commissionPayments.provider as Processor,
			if(commissionPayments.amount<0,'S','N') AS negative,
			commissionPayments.affiliateCompanyId,
			payoutDate
			FROM
			file 
			LEFT JOIN transaction_service_security
			ON file.idFile = transaction_service_security.idFile
			
			INNER JOIN commissionPayments
			ON file.idFile = commissionPayments.idFile	
			INNER JOIN affiliateCompany
			ON commissionPayments.affiliateCompanyId = affiliateCompany.affiliateCompanyId
			INNER JOIN affiliateCompany AS affiliateCompanyPayee
			ON commissionPayments.affiliateCompanyId = affiliateCompanyPayee.affiliateCompanyId
			LEFT JOIN 
					(
						SELECT idFile,dateTransaction,affiliateCompanyId,MIN(comment) AS comment,MIN(paymentNumber) AS paymentNumber
						FROM commentCommissionPayments
						GROUP BY idFile,dateTransaction,affiliateCompanyId
					) AS commentCommissionPayments
		    ON commentCommissionPayments.idFile=commissionPayments.idFile AND 
				commentCommissionPayments.dateTransaction=commissionPayments.payoutDate AND
				commentCommissionPayments.affiliateCompanyId=commissionPayments.affiliateCompanyId 
			WHERE commissionPayments.statusCode='T' 
			AND commissionPayments.affiliateCompanyId='29'
			";
		return $query;
	}
	
}
