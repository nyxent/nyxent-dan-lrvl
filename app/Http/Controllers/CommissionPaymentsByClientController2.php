<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use App\Http\Controllers\CommissionPaymentsController;
use App\ClientPayments;
use App\CommissionPayments;
use App\RescheduleEFT;
use App\ActiveLog;
use App\History;
use App\Client;
use DateTime;
use App\Http\Controllers\CommissionEntryController;
use App\Http\Utils\PaymentNumberUtil;


class CommissionPaymentsByClientController2 extends BaseSoapController
{

		public function sortByDate($a, $b)
		{
				$a = $a['date'];
				$b = $b['date'];
		
				if ($a == $b) return 0;
				return ($a < $b) ? -1 : 1;
		}


    public function index(Request $request)
    {

	
		/*
			* set  maximun execution  time
		*/
		ini_set('max_execution_time', 1400); 
		$page = Input::get('page', 1);
		$paginate = 100;
		/*
			* FUNCTION IN ANOTHER CONTROLLER
		*/
		$CommisionPaymentsController=new CommissionPaymentsController();
		$client=$CommisionPaymentsController->client();
		/*************************************/
		$ClientPayments=new ClientPayments;
		$CommissionPayments=new CommissionPayments;
		
		$Companys=array();
		$affiliates=array();
		$CompanysPaymentsDate=array();
		$PaymentsEPPSTrasantion=array();
		$CommissionEPPSTrasantion=array();
		/************************************/
		$searchClient=trim($request->GET('searchClient'));
		Session::put('idFilesearch', $searchClient); 
		$AccountNumber='';
    $RoutingNumber='';
		$accountBalance=0;
		
////////////////////////////////////////////////////////////////////////////////

			//Call new service sync with Jhon Method.

			$curl = curl_init();

			curl_setopt_array($curl, array(
					CURLOPT_URL => "http://keykoapp.nyxent.com/synchronizePaymentsEPPS/".$searchClient,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_TIMEOUT => 30000,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						// Set Here Your Requesred Headers
							'Content-Type: application/json',
					),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {
					echo "cURL Error #:" . $err;
			} else {
					// print_r(json_decode($response));
			}

		//$searchClient='1234';
		if($searchClient)
		{
      $this->service = InstanceSoapClient::init();
      $params = array(
        'UserName' => env('API_USER'),
        'PassWord' => env('API_PASSWORD'),
        'CardHolderID'=> $searchClient
      );

      $cardHolder = $this->service->FindCardHolderByID($params);
      $accountBalance = (isset($cardHolder->FindCardHolderByIDResult->CardHolderList->CardHolderDetail))? $cardHolder->FindCardHolderByIDResult->CardHolderList->CardHolderDetail->AccountBalance : 0;	
			/************************************/
			$PaymentsEPPS=$this->FindEftByID($searchClient);
			if(count($PaymentsEPPS)>1)
			{
				$dataArray=array();
				$PaymentsEPPSTrasantion=array();
				foreach($PaymentsEPPS as $indice2=>$FilePayments)
				{
					// var_dump($FilePayments);
					if($FilePayments->StatusCode!='Voided'&& $FilePayments->StatusCode!='Prenote Fail Void')
					{
						if($FilePayments->StatusCode=='Settled')
						{
							$StatusCode	='S';
						}
						if($FilePayments->StatusCode=='Create EFT Pending')
						{
							$StatusCode	='P';
						}
						if($FilePayments->StatusCode=='Voided')
						{
							$StatusCode	='V';
						}
						if($FilePayments->StatusCode=='Returned')
						{
							$StatusCode	='R';
						}
						if($FilePayments->StatusCode=='Prenote Fail Void')
						{
							$StatusCode	='PFV';
						}
						if($FilePayments->StatusCode=='Transmitted High-Risk')
						{
							$StatusCode	='THR';
						}
						if($FilePayments->StatusCode=='Prenote Fail')
						{
							$StatusCode	='PF';
						}
						if($FilePayments->StatusCode=='In Prenote')
						{
							$StatusCode	='IP';
						}
						if($FilePayments->StatusCode=='Transmitted')
						{
							$StatusCode	='T';
						}

						
						$dataArray[] = [
						  'idFile' =>$searchClient,
						  'depositDate' =>$FilePayments->EftDate,
						  'amount' =>str_replace(".",'',$FilePayments->EftAmount),
						  'provider'=>'EPPS',
						  'statusCode'=>$StatusCode,
						  'eftTransactionID'=>$FilePayments->EftTransactionID,
						  'statusCodeDescription'=>$FilePayments->StatusCode,
						  'routingNumber'=>$FilePayments->AccountNumber,
						  'accountNumber'=>$FilePayments->RoutingNumber,
						  'lastMessage'=>$FilePayments->LastMessage
						  
						];
						/*
							* SAVE DATA TRANSACTION
						*/
						$PaymentsEPPSTrasantion[(string)$FilePayments->EftTransactionID] =(array)$FilePayments;
						/***********************/
					}
				}
				// if($dataArray!=array())
				// {
				// 	$ClientPayments->insert($dataArray);
				// }
				
			}
		}
		/**********************************************/
		/*
			*  COMMISSION EPPS
		*/
		if($searchClient)
		{
			$CommisionEPPS=$this->FindFeeByCardHolderID($searchClient);
			
			if(count($CommisionEPPS)>1)
			{			
				$dataArray=array();
				$CommissionEPPSTrasantion=array();
				foreach($CommisionEPPS as $indice2=>$Commission)
				{
					if($Commission->StatusCode=='Transmitted' or $Commission->StatusCode=='Pending')
					{
						if($Commission->StatusCode=='Transmitted')
						{
							$StatusCode	='T';
						}
						if($Commission->StatusCode=='Pending')
						{
							$StatusCode	='P';
						}
						if($Commission->StatusCode=='Settled')
						{
							$StatusCode	='S';
						}
						$DescriptionReschedule=explode("|",$Commission->Description);
						$EftTransactionID=0;
						if($DescriptionReschedule[0]=='Reschedule')
						{
							 $EftTransactionID=	$DescriptionReschedule[1];
						}
						if($Commission->PaidToName=='No Debt Plan LLC'){
							$Commission->PaidToName='Mpower Wealth LLC';
						}
						$dataArray[] = [
						  'idFile' =>$searchClient,
						  'payoutDate' =>$Commission->Fee_Date,
						  'amount' =>str_replace(".",'',$Commission->FeeAmount),
						  'affiliateCompanyName'=>($Commission->PaidToName == 'Debt Assistance Network')? 'DAN':$Commission->PaidToName,
						  'provider'=>'EPPS',
						  'statusCode'=>$StatusCode,
						  'feeID'=>$Commission->FeeID,
						  'eftTransactionID'=>$EftTransactionID
						];
						$Commission->FeeAmountOrigin=$Commission->FeeAmount;
						/*
							* SAVE DATA TRANSATION
						*/
						$CommissionEPPSTrasantion[(string)$Commission->FeeID] =(array)$Commission;
						/***********************/
					}
				
				}
				// if($dataArray!=array())
				// {
				// 	$CommissionPayments->insert($dataArray);
				// }
			}
		 DB::update('UPDATE commissionPayments CP
							INNER JOIN affiliateCompany  AC ON
							TRIM(REPLACE(CP.affiliateCompanyName," ",""))=TRIM(REPLACE(CONCAT(AC.affiliateCompanyName)," ",""))
							SET CP.affiliateCompanyId=AC.affiliateCompanyId
							WHERE CP.affiliateCompanyId IS NULL
									');
		}
		$NameClient='';
		$idFile='';
		$idFileStatus='';
		$fileStatusName='';
		if($searchClient)
		{

			PaymentNumberUtil::repairClientPayments($searchClient);
			$payments =DB::select("select distinct
											c.depositDate as date,
											'dep' as type,
											CONCAT_WS( '-', c.amount, c.eftTransactionID) amount,
											c.provider,
											c.statusCode,
											c.statusCodeDescription,
											c.paymentNumber,
											c.eftTransactionID 
										from
											clientPayments c
										where
											c.idFile = ".$searchClient."
											and c.statusCode NOT IN ('RF','V','PFV','PF')
										order by
											date, c.eftTransactionID");
			$payments = array_map(function ($value) {
				return (array)$value;
			}, $payments);


			$affiliates = DB::select('SELECT
			DISTINCT
				a.affiliateCompanyId,
				a.affiliateCompanyName
			FROM
				affiliateCompany a
			INNER JOIN commissionPayments c ON
				a.affiliateCompanyId = c.affiliateCompanyId
			WHERE
				c.idFile ='.$searchClient);
			$commisions = array();
			foreach($affiliates as $a){
				$commisionsAffiliate = DB::select("SELECT
				c.payoutDate as date,
				c.statusCode as statusCode,
				c.provider as provider,
				CONCAT_WS('_',MAX(c.statusCode),SUM(c.amount),GROUP_CONCAT(c.feeID)) as amount
				FROM
					commissionPayments c
				WHERE
					c.idFile = ".$searchClient."
				AND c.affiliateCompanyId = ".$a->affiliateCompanyId."
				AND c.amount > 0
				AND c.statusCode <> 'V'
				GROUP BY
				c.payoutDate,
				c.statusCode,
				c.provider
				ORDER BY
					c.payoutDate");
				foreach($commisionsAffiliate as $b){
					$commisions[$b->date]['date']=$b->date; 
					$commisions[$b->date]['statusCode']=$b->statusCode; 
					$commisions[$b->date]['provider']=$b->provider; 
					$commisions[$b->date]['type']='com'; 
					$commisions[$b->date]['eftTransactionID']='99999999999'; 
					$commisions[$b->date][str_replace(' ','_',str_replace(',','',str_replace('-','_',str_replace('.','',$a->affiliateCompanyName))))]=$b->amount; 
				}
			}
			// $commisions =DB::select('CALL active_commision('.$searchClient.')');
			$commisions = array_map(function ($value) {
				return (array)$value;
			}, $commisions);
			$fields = array_merge($payments,$commisions);

			$result = array_multisort(array_column($fields, 'date') , SORT_ASC, array_column($fields, 'eftTransactionID'),  SORT_ASC, array_column($fields, 'type') , SORT_DESC,	$fields);
			//$result = usort($fields,array($this,'sortByDate'));


			
			$NameClientResult=DB::select("SELECT CONCAT(a.lastName,' ',a.firstName) AS name,
												 a.ppRoutingNumber as RoutingNumber,
												 a.ppAccountNumber as AccountNumber,
												 a.idFile,
												 a.idFileStatus,
												 a.affiliateCompanyId,
												 b.name as fileStatusName
				FROM file a
				INNER JOIN fileStatus b
				ON a.idFileStatus = b.idFileStatus
				WHERE a.idFile='".$searchClient."'
			");
			$NameClient=$NameClientResult[0]->name;
			
			
			$AccountNumber=$NameClientResult[0]->AccountNumber;
			$RoutingNumber=$NameClientResult[0]->RoutingNumber;
			$idFile=$NameClientResult[0]->idFile;
			$idFileStatus=$NameClientResult[0]->idFileStatus;
			$fileStatusName=$NameClientResult[0]->fileStatusName;

			$affiliateIds = DB::select("SELECT
					CONCAT_WS(',',a.finderId,
					a.superAffiliateId,
					a.finderId2,
					a.superAffiliateId2) as ids
				FROM
					affiliateCompany a
				INNER JOIN
				file f
				ON a.affiliateCompanyId = f.affiliateCompanyId
				WHERE
				f.idFile =".$searchClient);

			$Companys =DB::select("SELECT
										a.affiliateCompanyId,
										a.affiliateCompanyName
									FROM
										user u
									LEFT JOIN userAffiliateCompany x ON
										u.idUser = x.idUser
									LEFT JOIN affiliateCompany a ON
										x.affiliateCompanyId = a.affiliateCompanyId
									WHERE
										u.idUser IN (".$affiliateIds[0]->ids.")
									UNION
									SELECT 
										a.affiliateCompanyId,
										a.affiliateCompanyName
									FROM 
										affiliateCompany a
									WHERE
										a.affiliateCompanyId IN (16,21,24,29,".$NameClientResult[0]->affiliateCompanyId.")
									");
			
			$CompanysPayments=DB::select("SELECT CONCAT(DATE_FORMAT(payoutDate, '%m/%d/%Y'),affiliateCompanyId,if(eftTransactionID<>0,eftTransactionID,'')) AS payoutDate,FORMAT(SUM(amount)/100, 2) AS amount,Max(feeID) as feeID,if(eftTransactionID<>0,'S','N') as Reschule,if(count(*)>1,'S','N') as duplicate,statusCode
				FROM commissionPayments
				WHERE commissionPayments.idFile='".$searchClient."'
				GROUP BY payoutDate,affiliateCompanyId,eftTransactionID,statusCode
			");

		}
		if($searchClient!='')
		{
			$CompanysPaymentsDate=array();		 
			foreach($CompanysPayments as $indice=>$value)
			{
				$CompanysPaymentsDate[$value->payoutDate]=array('amountFee'=>$value->amount,'feeID'=>$value->feeID,'statusCode'=>$value->statusCode);
			}		 

		}	
		
		$itemsFile=array();
		if(!isset($fields)){$fields=array();}
		$resp = array_multisort(array_column($PaymentsEPPSTrasantion, 'EftDate'),  SORT_ASC, $PaymentsEPPSTrasantion);
		$commisionSort = array_multisort(array_column($CommissionEPPSTrasantion, 'Fee_Date'),  SORT_ASC, $CommissionEPPSTrasantion);
		return view('form.commisionPaymentsByClient2.index',["itemsFile"=>$fields,'client'=>$client,'Companys'=>array_unique(array_merge($affiliates,$Companys), SORT_REGULAR),'CompanysPayments'=>$CompanysPaymentsDate,'PaymentsEPPSTrasantion'=>json_encode($PaymentsEPPSTrasantion),'CommissionEPPSTrasantion'=>json_encode($CommissionEPPSTrasantion),'AccountNumber'=>$AccountNumber,'RoutingNumber'=>$RoutingNumber,'searchClient'=>$searchClient,'NameClient'=>$NameClient,'idFile'=>$idFile,'idFileStatus'=>$idFileStatus,'fileStatusName'=>$fileStatusName,'accountBalance'=>$accountBalance]);	
	}


	
	
	public function store(Request $request)
  {


		$action=trim($request->GET('action'));
		if($action=='saveChangePayment')
		{
				$searchClient=trim($request->GET('searchClient'));
				
				$NewNote=trim($request->GET('NewNote'));

				/*
					* SAVE LOG
				*/
				
				 
				if($request->GET('Reactive')){
					$client = Client::find(Session::get('idFilesearch'));
					$client->idFileStatus = 24;
					$client->save();
					$this->InsertHistory(Session::get('idFilesearch'),'Flow change to Reinstated',$NewNote);
				}
				else{
					$this->InsertHistory(Session::get('idFilesearch'),'Change payments active snapshot',$NewNote);
				}
				
				/*
					* set  maximun execution  time
				*/
				ini_set('max_execution_time', 1400); 
				$PaymentsEPPSChanges=json_decode($_POST['PaymentsEPPSChanges']);
				
		
				$CadenaPaymentsChanges='';
				if(isset($PaymentsEPPSChanges) and $PaymentsEPPSChanges!=array())
				{
					foreach($PaymentsEPPSChanges as $indice=>$value)
					{						
						/*
							* UPDATE EFT
						*/
						$this->FN_UpdateEFT($value->EftTransactionID,$value->EftDate,str_replace(",","",str_replace("$","",$value->EftAmount)),$value->AccountNumber,$value->RoutingNumber);
						
						$CadenaPaymentsChanges.=
						' -- EftTransactionID=>'.$value->EftTransactionID.
						'| EftDate=>'.$this->yyyymmddTommddyyyy($value->EftDate).
						'| Amount Origin=>'.$value->EftAmountOrigin.
						'| Amount Current=>'.$value->EftAmount;
						
					}
				}
				if($CadenaPaymentsChanges!='')
				{
					/*
						* SAVE LOG
					*/
					$this->InsertHistory(Session::get('idFilesearch'),'Change EFT Payments',$CadenaPaymentsChanges);
					
					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Change EFT Payments',
						'detail' => $CadenaPaymentsChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();	
				}
		
				$CadenaCommissionChanges='';
				$CommissionEPPSChanges=json_decode($_POST['CommissionEPPSChanges']);
				if(isset($CommissionEPPSChanges) and $CommissionEPPSChanges!=array())
				{
					foreach($CommissionEPPSChanges as $indice=>$value)
					{
						$dataXml='<UserName>'.$this->credentialsUser().'</UserName>
					 			  <PassWord>'.$this->credentialsPWD().'</PassWord>';
						foreach($value as $indice2=>$value2)
						{
							if($indice2=='FeeAmountOrigin')
							{
								$FeeAmountOrigin='| Amount Origin=>'.$value2;
							}
							else
							{
								if($indice2=='FeeDateOrigin')
								{
									$FeeDateOrigin='| Date Origin=>'.$this->yyyymmddTommddyyyy($value2);
								}
								else
								{
									if($indice2=='FeeDate')
									{
										$FeeDateCurrent='| Date Current=>'.$this->yyyymmddTommddyyyy($value2);
									}
									if($indice2=='FeeAmount')
									{
										$FeeAmount='| Amount Current=>'.$value2;
										$value2=str_replace(",","",str_replace("$","",$value2));
									}
									if($indice2=='FeeID')
									{
										$FeeID='FeeID=>'.$value2;
										$value2=str_replace(",","",str_replace("$","",$value2));
									}
				
									$dataXml.='<'.$indice2.'>'.$value2.'</'.$indice2.'>';
								}
							}
					
						}
						$opts = array('http' =>
							  array(
								'method'  => 'POST',
								'header'  => "Content-Type: text/xml\r\n",
								'content' => '<?xml version="1.0" encoding="utf-8"?>
							<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
							 <soap:Body>
							   <UpdateFee xmlns="http://tempuri.org/">
								 
								'.$dataXml.'
		
							   </UpdateFee>
							 </soap:Body>
							</soap:Envelope>',
								'timeout' => 60
							  )
							);			
						
						$context  = stream_context_create($opts);
						$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';
						$resultCommisionEPPS = file_get_contents($url, false, $context, -1, 10485760);
						
						$CadenaCommissionChanges.=
						' -- '.$FeeID.$FeeDateOrigin.$FeeDateCurrent.$FeeAmountOrigin.$FeeAmount;
					}
				}
				if($CadenaCommissionChanges!='')
				{
					/*
						* SAVE LOG
					*/
					$this->InsertHistory(Session::get('idFilesearch'),'Change Fee Commission',$CadenaCommissionChanges);
					
					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Change Fee Commission',
						'detail' => $CadenaCommissionChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();	
				}
				if($_SERVER['SERVER_NAME']=='127.0.0.1')
				{
					header('Location: /form/commisionPaymentsByClient2?searchClient='.Session::get('idFilesearch'));
					exit;
				}
				else
				{
					header('Location: /reportsTransactions/form/commisionPaymentsByClient2?searchClient='.Session::get('idFilesearch'));
					exit;
				}
		}
		if($action=='saveChangeAccountinfo')
		{
			$NewAccountNumber=trim($request->GET('NewAccountNumber'));
			$NewRoutingNumber=trim($request->GET('NewRoutingNumber'));
			
			$PaymentsEPPS=$this->FindEftByID(Session::get('idFilesearch'));
			$CadenaPaymentsChanges='';
			foreach($PaymentsEPPS as $indice2=>$FilePayments)
			{
				if($FilePayments->StatusCode=='Create EFT Pending')
				{			
					/*
						* UPDATE EFT
					*/
					$this->FN_UpdateEFT($FilePayments->EftTransactionID,$FilePayments->EftDate,$FilePayments->EftAmount,$NewAccountNumber,$NewRoutingNumber);
					
					$CadenaPaymentsChanges.=
					' -- EftTransactionID=>'.$FilePayments->EftTransactionID.
					'| EftDate=>'.$this->yyyymmddTommddyyyy($FilePayments->EftDate).
					'| AccountNumber Origin=>'.$FilePayments->AccountNumber.
					'| AccountNumber Current=>'.$NewAccountNumber.
					'| RoutingNumber Origin=>'.$FilePayments->RoutingNumber.
					'| RoutingNumber Current=>'.$NewRoutingNumber;
				}
			}
			if($CadenaPaymentsChanges!='')
			{
				/*
					* SAVE LOG
				*/
				$this->InsertHistory(Session::get('idFilesearch'),'Changed bank account info',$CadenaPaymentsChanges);

				/* save into active log*/
				$data = array(
					'idFile' => Session::get('idFilesearch'),
					'title' => 'Changed bank account info',
					'detail' => $CadenaPaymentsChanges,
					'idUser' => Session::get('iduser')
				);
				$log = new ActiveLog($data);
				$log->save();	
			}
			DB::update('UPDATE file F
						 SET F.ppRoutingNumber="'.$NewRoutingNumber.'",
							 F.ppAccountNumber="'.$NewAccountNumber.'"
						 WHERE idFile="'.Session::get('idFilesearch').'"
									');
									
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				header('Location: /form/commisionPaymentsByClient2?searchClient='.Session::get('idFilesearch'));
				exit;
			}
			else
			{
				header('Location: /reportsTransactions/form/commisionPaymentsByClient2?searchClient='.Session::get('idFilesearch'));
				exit;
			}
		}
		if($action=='CopyRow')
		{
			$NewEftDate=trim($request->GET('NewEftDate'));
			$NewRows=$request->GET('NewRow');
			/*
				* COPY ROW
			*/
			$this->CopyNewRow($NewRows,$NewEftDate);
			/*************************************/
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}	
		}
		if($action=='DeleteRow')
		{
			$NewRows=$request->GET('NewRow');
			foreach($NewRows as $indice=> $NewRow)
			{
				if($NewRow['type']=='EFT')
				{
						$NewEft=$NewRow['Oringen'];
						$opts = array('http' =>
						  array(
							'method'  => 'POST',
							'header'  => "Content-Type: text/xml\r\n",
							'content' => '<?xml version="1.0" encoding="utf-8"?>
								<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
								  <soap:Body>
									<VoidEft xmlns="http://tempuri.org/">
									   <UserName>'.$this->credentialsUser().'</UserName>
									   <PassWord>'.$this->credentialsPWD().'</PassWord>
									  <EftTransactionID>'.$NewEft['EftTransactionID'].'</EftTransactionID>
									</VoidEft>
								  </soap:Body>
								</soap:Envelope>',
							'timeout' => 180	
						  )
						);			
						
						$context  = stream_context_create($opts);
						$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';
						$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);
						/*
							* SAVE LOG
						*/
						//$this->InsertHistory(Session::get('idFilesearch'),'Void EFT',$NewEft['EftTransactionID']);

						/* save into active log*/
						$data = array(
							'idFile' => Session::get('idFilesearch'),
							'title' => 'Delete row',
							'detail' => $NewEft['EftTransactionID'],
							'idUser' => Session::get('iduser')
						);
						$log = new ActiveLog($data);
						$log->save();	
				}
			}
			foreach($NewRows as $indice=> $NewRow)
			{
				if($NewRow['type']=='FEE')
				{
						$NewFee=$NewRow['Oringen'];
						$opts = array('http' =>
						  array(
							'method'  => 'POST',
							'header'  => "Content-Type: text/xml\r\n",
							'content' => '<?xml version="1.0" encoding="utf-8"?>
								<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
								  <soap:Body>
								  	<VoidFee xmlns="http://tempuri.org/">
									   <UserName>'.$this->credentialsUser().'</UserName>
					 				   <PassWord>'.$this->credentialsPWD().'</PassWord>
									  <FeeID>'.$NewFee['FeeID'].'</FeeID>
									</VoidFee>
								  </soap:Body>
								</soap:Envelope>',
							'timeout' => 180	
						  )
						);			
						
						$context  = stream_context_create($opts);
						$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';
						$resultPaymentsEPPS = file_get_contents($url, false, $context, -1, 10485760);
						/*
							* SAVE LOG
						*/
						// $this->InsertHistory(Session::get('idFilesearch'),'Void Fee',$NewFee['FeeID']);

						/* save into active log*/
						$data = array(
							'idFile' => Session::get('idFilesearch'),
							'title' => 'Delete row',
							'detail' => $NewFee['FeeID'],
							'idUser' => Session::get('iduser')
						);
						$log = new ActiveLog($data);
						$log->save();	
				}
			}
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}	
					
		}
			
		if($action=='DisplaceRow')
		{
			$NewEftDate=trim($request->GET('NewEftDate'));
			$NewEftDateOring=$NewEftDate;
			$NewRows=$request->GET('NewRow');
			
			$NewEftDate = new DateTime($this->mmddyyyyToyyyymmdd($NewEftDate));
			$PaymentsEPPS=$this->FindEftByID(Session::get('idFilesearch'));
			$CadenaPaymentsChanges='';
			foreach($PaymentsEPPS as $indice2=>$FilePayments)
			{
				if($FilePayments->StatusCode=='Create EFT Pending')
				{
					$EftDateCurrent = new DateTime(substr($FilePayments->EftDate,0,10));
					if ($EftDateCurrent >= $NewEftDate ) 
					{ 
							$EftDateMore30Days = new DateTime($FilePayments->EftDate); // Y-m-d
							$time = strtotime($EftDateMore30Days->format('Y-m-d'));
							$EftDateMore30Days_New= date("Y-m-d", strtotime("+1 month", $time));
							/*
								* UPDATE EFT
							*/
							$this->FN_UpdateEFT($FilePayments->EftTransactionID,$EftDateMore30Days_New,$FilePayments->EftAmount,$FilePayments->AccountNumber,$FilePayments->RoutingNumber);
							
							$CadenaPaymentsChanges.=
							' -- EftTransactionID=>'.$FilePayments->EftTransactionID.
							'| EftDate Origin=>'.$this->yyyymmddTommddyyyy($FilePayments->EftDate).
							'| EftDate Current=>'.$this->yyyymmddTommddyyyy($EftDateMore30Days->format('Y-m-d'));
							/********/
								
					}
				}
			}
			if($CadenaPaymentsChanges!='')
			{
				/*
					* SAVE LOG
				*/
				// $this->InsertHistory(Session::get('idFilesearch'),'Update Eft',$CadenaPaymentsChanges);

				/* save into active log*/
				$data = array(
					'idFile' => Session::get('idFilesearch'),
					'title' => 'Update EFT',
					'detail' => $CadenaPaymentsChanges,
					'idUser' => Session::get('iduser')
				);
				$log = new ActiveLog($data);
				$log->save();	
			}
			$CommisionEPPS=$this->FindFeeByCardHolderID(Session::get('idFilesearch'));
			if(count($CommisionEPPS)>1)
			{	
				
				$dataArray=array();
				$CommissionEPPSTrasantion=array();
				$FeeToSave=array();
				foreach($CommisionEPPS as $indice2=>$Commission)
				{
					if($Commission->StatusCode=='Pending')
					{
						$FeeDateCurrent = new DateTime(substr($Commission->Fee_Date,0,10));
						if ($FeeDateCurrent >= $NewEftDate ) 
						{ 
							$FeeDateMore30Days = new DateTime($Commission->Fee_Date); // Y-m-d
							$time = strtotime($FeeDateMore30Days->format('Y-m-d'));
							$FeeDateMore30Days_New= date("Y-m-d", strtotime("+1 month", $time));							
							/*
								* UPDATE FEE
							*/
							$this->FN_UpdateFee($Commission->FeeID,$FeeDateMore30Days_New,$Commission->FeeAmount,$Commission->Description,$Commission->FeeType,$Commission->PaidToName,$Commission->PaidToPhone,$Commission->PaidToStreet,$Commission->PaidToStreet2,$Commission->PaidToCity,$Commission->PaidToState,$Commission->PaidToZip);
							
							$CadenaPaymentsChanges=
							' FeeID=>'.$Commission->FeeID.
							'| Amount Oringin=>'.$this->yyyymmddTommddyyyy($Commission->Fee_Date).
							'| Amount Current=>'.$this->yyyymmddTommddyyyy($EftDateMore30Days->format('Y-m-d'));
							
						}
					}
				}
				/*
					* SAVE LOG
				*/
				// $this->InsertHistory(Session::get('idFilesearch'),'Update Fee',$CadenaPaymentsChanges);

				/* save into active log*/
				$data = array(
					'idFile' => Session::get('idFilesearch'),
					'title' => 'Update FEE',
					'detail' => $CadenaPaymentsChanges,
					'idUser' => Session::get('iduser')
				);
				$log = new ActiveLog($data);
				$log->save();
			}
			/*
				* COPY ROW
			*/
			$this->CopyNewRow($NewRows,$NewEftDateOring);
			/*************************************/
									
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}	
		}
		if($action=='MergeRow')
		{
			$NewEftDate=trim($request->GET('NewEftDate'));
			$NewRows=$request->GET('NewRow');
			/*
				* MERGE EFT
			*/
			$PaymentsEPPS=$this->FindEftByID(Session::get('idFilesearch'));
			$CadenaPaymentsChanges='';
			foreach($PaymentsEPPS as $indice2=>$FilePayments)
			{
				if($FilePayments->StatusCode=='Create EFT Pending')
				{
					if(substr($FilePayments->EftDate,0,10)==$this->mmddyyyyToyyyymmdd($NewEftDate))
					{
						
						/*
							* UPDATE EFT
						*/
						foreach($NewRows as $indice=> $NewRow)
						{
							if($NewRow['type']=='EFT')
							{
								$NewEft=$NewRow['Oringen'];
								$NewRows[$indice]['Merge']='S';
								/*
									* UPDATE EFT
								*/
								$this->FN_UpdateEFT($FilePayments->EftTransactionID,$FilePayments->EftDate,($FilePayments->EftAmount+$NewEft['EftAmount']),$FilePayments->AccountNumber,$FilePayments->RoutingNumber);
								
								$CadenaPaymentsChanges=
								' UpdateEft=>'.$NewEftDate.
								'| Amount Oringin=>'.$FilePayments->EftAmount.
								'| Amount Current=>'.($FilePayments->EftAmount+$NewEft['EftAmount']);
								/*
									* SAVE LOG
								*/
								// $this->InsertHistory(Session::get('idFilesearch'),'Update Eft',$CadenaPaymentsChanges);	

								/* save into active log*/
								$data = array(
									'idFile' => Session::get('idFilesearch'),
									'title' => 'Update EFT',
									'detail' => $CadenaPaymentsChanges,
									'idUser' => Session::get('iduser')
								);
								$log = new ActiveLog($data);
								$log->save();
							}
						}
					}
					
				}
			}
			foreach($NewRows as $indice=> $NewRow)
			{
				if($NewRow['type']=='EFT'  and $NewRow['Merge']=='N')
				{
					$NewEft=$NewRow['Oringen'];
					$BankName='';
					if(isset($NewEft['BankName']))
					{
						$BankName=$NewEft['BankName'];
					}
					$BankCity='';
					if(isset($NewEft['BankCity']))
					{
						$BankCity=$NewEft['BankCity'];
					}
					$BankState='';
					if(isset($NewEft['BankState']))
					{
						$BankState=$NewEft['BankState'];
					}
					/*	
						* ADD FEE
					*/
					$this->FN_AddEFT($NewEft['CardHolderId'],$this->yyyymmddTommddyyyy2($NewEftDate),$NewEft['EftAmount'],$BankName,$BankCity,$BankState,$NewEft['AccountNumber'],$NewEft['RoutingNumber']);
					
					$CadenaPaymentsChanges=
					' EftDate=>'.$NewEftDate.
					'| Amount=>'.$NewEft['EftAmount'];
					/*
						* SAVE LOG
					*/
					// $this->InsertHistory(Session::get('idFilesearch'),'Add Eft',$CadenaPaymentsChanges);
					
					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Add EFT',
						'detail' => $CadenaPaymentsChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();

				}
			}
			/*
				* MERGE FEE
			*/
			$CommisionEPPS=$this->FindFeeByCardHolderID(Session::get('idFilesearch'));
			if(count($CommisionEPPS)>1)
			{	
				
				$dataArray=array();
				$CommissionEPPSTrasantion=array();
				$FeeToSave=array();
				foreach($CommisionEPPS as $indice2=>$Commission)
				{
					if($Commission->StatusCode=='Pending')
					{
						if(substr($Commission->Fee_Date,0,10)==$this->mmddyyyyToyyyymmdd($NewEftDate))
						{
							/*
								* UPDATE FEE
							*/
							foreach($NewRows as $indice=> $NewRow)
							{
								if($NewRow['type']=='FEE' )
								{
									
									$NewFee=$NewRow['Oringen'];
									if($NewFee['PaidToName']==$Commission->PaidToName)
									{
										$NewRows[$indice]['Merge']='S';
										/*
											* UPDATE FEE
										*/
										$this->FN_UpdateFee($Commission->FeeID,$Commission->Fee_Date,($Commission->FeeAmount+$NewFee['FeeAmount']),$Commission->Description,$Commission->FeeType,$Commission->PaidToName,$Commission->PaidToPhone,$Commission->PaidToStreet,$Commission->PaidToStreet2,$Commission->PaidToCity,$Commission->PaidToState,$Commission->PaidToZip);
										
										$CadenaPaymentsChanges=
										' UpdateFee=>'.$NewEftDate.
										'| PaidToName=>'.$NewFee['PaidToName'].
										'| Amount Oringin=>'.$Commission->FeeAmount.
										'| Amount Current=>'.($Commission->FeeAmount+$NewFee['FeeAmount']);
										/*
											* SAVE LOG
										*/
										// $this->InsertHistory(Session::get('idFilesearch'),'Update Fee',$CadenaPaymentsChanges);

										/* save into active log*/
										$data = array(
											'idFile' => Session::get('idFilesearch'),
											'title' => 'Update FEE',
											'detail' => $CadenaPaymentsChanges,
											'idUser' => Session::get('iduser')
										);
										$log = new ActiveLog($data);
										$log->save();
									}
									
									
								}
							}
						}
					}
				}
			}
			foreach($NewRows as $indice=> $NewRow)
			{
				if($NewRow['type']=='FEE' and $NewRow['Merge']=='N')
				{
					$NewFee=$NewRow['Oringen'];		
					/*	
						* ADD FEE
					*/
					$this->FN_AddFee($NewFee['CardHolderID'],$this->yyyymmddTommddyyyy2($NewEftDate),$NewFee['FeeAmount'],$NewFee['PaidToName']);
					$CadenaPaymentsChanges=
					' FeeDate=>'.$NewEftDate.
					'| AffiliateCompany=>'.$NewFee['PaidToName'].
					'| Amount=>'.$NewFee['FeeAmount'];
					/*
						* SAVE LOG
					*/
					// $this->InsertHistory(Session::get('idFilesearch'),'Add Fee',$CadenaPaymentsChanges);
					
					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Add FEE',
						'detail' => $CadenaPaymentsChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();
				}
			}
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}				
		}
		if($action=='AddNewFEE')
		{
			$NewAccountNumber=trim($request->GET('NewAccountNumber'));
			$NewRoutingNumber=$request->GET('NewRoutingNumber');
			$NewEftDate=$request->GET('NewEftDate');
			$NewFEEAmount=$request->GET('NewFEEAmount');
			$AffiliateCompanyName=$request->GET('AffiliateCompanyName');
			$this->FN_AddFee(Session::get('idFilesearch'),$this->yyyymmddTommddyyyy2($NewEftDate),$NewFEEAmount,$AffiliateCompanyName);
			
			/* save into active log*/
			$CadenaPaymentsChanges=	' FeeDate=>'.$NewEftDate.	'| AffiliateCompany=>'.$AffiliateCompanyName.	'| Amount=>'.$NewFEEAmount;
			$data = array(
				'idFile' => Session::get('idFilesearch'),
				'title' => 'Add FEE',
				'detail' => $CadenaPaymentsChanges,
				'idUser' => Session::get('iduser')
			);
			$log = new ActiveLog($data);
			$log->save();
			
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}	
		}
		if($action=='AddNewFEESAS')
		{
			$NewEftDate=$request->GET('NewEftDate');
			$NewFEEAmount=$request->GET('NewFEEAmount')*100;
			$affiliateCompanyId=$request->GET('affiliateCompanyId');
			
			DB::insert("INSERT INTO commissionPayments 
						(
						`idFile`,
						`payoutDate`,
						`amount`,
						`affiliateCompanyId`,
						 dateUpdate,
						 manualUpload,
						 statusCode,
						 provider
						)
						VALUES
						(
						'".Session::get('idFilesearch')."', 
						'".$this->yyyymmddTommddyyyy2($NewEftDate)."', 
						'".$NewFEEAmount."', 
						'".$affiliateCompanyId."',
						NOW(),
						'Y',
						'T',
						'SAS'
						);
					");
			
			/*
				*  CREAR COMMENT
			*/
			$CommissionEntryController=new CommissionEntryController();
			$CommissionEntryController->updatePaymentNumberCommission();
							  
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}	
		}
		if($action=='Renumbercommision')
		{
			/*
				*  CREAR COMMENT
			*/
			$CommissionEntryController=new CommissionEntryController();
			$CommissionEntryController->updatePaymentNumberCommission();
							  
			if($_SERVER['SERVER_NAME']=='127.0.0.1')
			{
				echo json_encode(array('Save'=>'/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
			else
			{
				echo json_encode(array('Save'=>'/reportsTransactions/form/commisionPaymentsByClient2','idfile'=>Session::get('idFilesearch')));
				exit;
			}
		}
	}
	public function client()
	{
		$client=DB::select("SELECT idFile,CONCAT(lastName,' ',firstName) AS client2 FROM file	");
		return $client;	
	
	}
	
	public function FindEftByID($searchClient)
	{
		// DB::delete("DELETE FROM clientPayments WHERE idfile='".$searchClient."' AND provider='EPPS'");
		$stringXML='<FindEftByID xmlns="http://tempuri.org/">
						<UserName>'.$this->credentialsUser().'</UserName>
						<PassWord>'.$this->credentialsPWD().'</PassWord>
						<CardHolderID>'.$searchClient.'</CardHolderID>
					 </FindEftByID>';
					   
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		$resultPaymentsEPPS=$this->ExecFunctionAPI($opts);
		
		$resultPaymentsEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindEftByIDResponse xmlns="http://tempuri.org/">','',$resultPaymentsEPPS);
		
		$resultPaymentsEPPS=str_replace('<FindEftByIDResult>','',$resultPaymentsEPPS);
		
		$resultPaymentsEPPS=str_replace('</FindEftByIDResult>','',$resultPaymentsEPPS);
		
		$resultPaymentsEPPS=str_replace('<Message>Success</Message></FindEftByIDResponse></soap:Body></soap:Envelope>','',$resultPaymentsEPPS);
		
		$PaymentsEPPS = simplexml_load_string($resultPaymentsEPPS);	
		
		return $PaymentsEPPS;
	}
	
	public function FindFeeByCardHolderID($searchClient)
	{
		// DB::delete("DELETE FROM commissionPayments WHERE idfile='".$searchClient."' AND provider='EPPS'");
		
		$stringXML='<FindFeeByCardHolderID xmlns="http://tempuri.org/">
						<UserName>'.$this->credentialsUser().'</UserName>
						<PassWord>'.$this->credentialsPWD().'</PassWord>
						<CardHolderID>'.$searchClient.'</CardHolderID>
					 </FindFeeByCardHolderID>';
					   
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		$resultCommisionEPPS=$this->ExecFunctionAPI($opts);
		
		$resultCommisionEPPS=str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FindFeeByCardHolderIDResponse xmlns="http://tempuri.org/">','',$resultCommisionEPPS);
			
		$resultCommisionEPPS=str_replace('<FindFeeByCardHolderIDResult>','',$resultCommisionEPPS);
		$resultCommisionEPPS=str_replace('</FindFeeByCardHolderIDResult>','',$resultCommisionEPPS);
		$resultCommisionEPPS=str_replace('<Message>Success</Message></FindFeeByCardHolderIDResponse></soap:Body></soap:Envelope>','',$resultCommisionEPPS);
		$CommisionEPPS = simplexml_load_string($resultCommisionEPPS);

		return $CommisionEPPS;
		
		
	}
	
	public function CopyNewRow($NewRows,$NewEftDate)
	{
		/*
				* CREATE EFT AND FEE
			*/
			foreach($NewRows as $NewRow)
			{
				if($NewRow['type']=='EFT')
				{
					$NewEft=$NewRow['Oringen'];
					$BankName='';
					if(isset($NewEft['BankName']))
					{
						$BankName=$NewEft['BankName'];
					}
					$BankCity='';
					if(isset($NewEft['BankCity']))
					{
						$BankCity=$NewEft['BankCity'];
					}
					$BankState='';
					if(isset($NewEft['BankState']))
					{
						$BankState=$NewEft['BankState'];
					}
					/*	
						* ADD FEE
					*/
					$this->FN_AddEFT($NewEft['CardHolderId'],$this->yyyymmddTommddyyyy2($NewEftDate),$NewEft['EftAmount'],$BankName,$BankCity,$BankState,$NewEft['AccountNumber'],$NewEft['RoutingNumber']);
					
					$CadenaPaymentsChanges=
					' EftDate=>'.$NewEftDate.
					'| Amount=>'.$NewEft['EftAmount'];
					/*
						* SAVE LOG
					*/
					// $this->InsertHistory(Session::get('idFilesearch'),'Add Eft',$CadenaPaymentsChanges);
					
					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Add EFT',
						'detail' => $CadenaPaymentsChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();

				}
				else
				{
					$NewFee=$NewRow['Oringen'];
					/*	
						* ADD FEE
					*/
					$this->FN_AddFee($NewFee['CardHolderID'],$this->yyyymmddTommddyyyy2($NewEftDate),$NewFee['FeeAmount'],$NewFee['PaidToName']);
					$CadenaPaymentsChanges=
					' FeeDate=>'.$NewEftDate.
					'| AffiliateCompany=>'.$NewFee['PaidToName'].
					'| Amount=>'.$NewFee['FeeAmount'];
					/*
						* SAVE LOG
					*/
					// $this->InsertHistory(Session::get('idFilesearch'),'Add Fee',$CadenaPaymentsChanges);

					/* save into active log*/
					$data = array(
						'idFile' => Session::get('idFilesearch'),
						'title' => 'Add FEE',
						'detail' => $CadenaPaymentsChanges,
						'idUser' => Session::get('iduser')
					);
					$log = new ActiveLog($data);
					$log->save();
				
				}
			}	
	}
	public function FN_AddEFT($CardHolderID,$NewEftDate,$EFTAmount,$BankName,$BankCity,$BankState,$AccountNumber,$RoutingNumber)
	{
		$stringXML='<AddEft xmlns="http://tempuri.org/">
						 <UserName>'.$this->credentialsUser().'</UserName>
						  <PassWord>'.$this->credentialsPWD().'</PassWord>
						  <CardHolderID>'.$CardHolderID.'</CardHolderID>
						  <EftDate>'.$NewEftDate.'</EftDate>
						  <EftAmount>'.$EFTAmount.'</EftAmount>
						  <EftFee>0</EftFee>
						  <BankName>'.$BankName.'</BankName>
						  <BankCity>'.$BankCity.'</BankCity>
						  <BankState>'.$BankState.'</BankState>
						  <AccountNumber>'.$AccountNumber.'</AccountNumber>
						  <RoutingNumber>'.$RoutingNumber.'</RoutingNumber>
						  <AccountType>Checking</AccountType>
						  <Memo></Memo>
						</AddEft>';
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		$this->ExecFunctionAPI($opts);
	}
	public function FN_AddFee($CardHolderID,$NewEftDate,$FeeAmount,$PaidToName)
	{
		$stringXML='<AddFee xmlns="http://tempuri.org/">
						  <UserName>'.$this->credentialsUser().'</UserName>
						  <PassWord>'.$this->credentialsPWD().'</PassWord>
						  <CardHolderID>'.$CardHolderID.'</CardHolderID>
						  <FeeDate>'.$NewEftDate.'</FeeDate>
						  <FeeAmount>'.$FeeAmount.'</FeeAmount>
						  <Description>Debt Program fee</Description>
						  <FeeType>SettlementPayment</FeeType>
						  <PaidToName>'.$PaidToName.'</PaidToName>
						  <PaidToPhone></PaidToPhone>
						  <PaidToStreet></PaidToStreet>
						  <PaidToStreet2></PaidToStreet2>
						  <PaidToCity></PaidToCity>
						  <PaidToState></PaidToState>
						  <PaidToZip></PaidToZip>
						  <ContactName></ContactName>
						  <PaidToCustomerNumber></PaidToCustomerNumber>
						</AddFee>';
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		$this->ExecFunctionAPI($opts);
	}
	public function FN_UpdateFee($FeeID,$Fee_Date,$FeeAmount,$Description,$FeeType,$PaidToName,$PaidToPhone,$PaidToStreet,$PaidToStreet2,$PaidToCity,$PaidToState,$PaidToZip)
	{
		$stringXML='<UpdateFee xmlns="http://tempuri.org/">
				  <UserName>'.$this->credentialsUser().'</UserName>
				  <PassWord>'.$this->credentialsPWD().'</PassWord>
				  <FeeID>'.$FeeID.'</FeeID>
				  <FeeDate>'.$Fee_Date.'</FeeDate>
				  <FeeAmount>'.$FeeAmount.'</FeeAmount>
				  <Description>'.$Description.'</Description>
				  <FeeType>'.$FeeType.'</FeeType>
				  <PaidToName>'.$PaidToName.'</PaidToName>
				  <PaidToPhone>'.$PaidToPhone.'</PaidToPhone>
				  <PaidToStreet>'.$PaidToStreet.'</PaidToStreet>
				  <PaidToStreet2>'.$PaidToStreet2.'</PaidToStreet2>
				  <PaidToCity>'.$PaidToCity.'</PaidToCity>
				  <PaidToState>'.$PaidToState.'</PaidToState>
				  <PaidToZip>'.$PaidToZip.'</PaidToZip>
				  <PaidToCustomerNumber></PaidToCustomerNumber>
				  <ContactName></ContactName>
				</UpdateFee>';			
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		$this->ExecFunctionAPI($opts);
	}
	
	public function FN_UpdateEFT($EftTransactionID,$EftDate,$EftAmount,$AccountNumber,$RoutingNumber)
	{
		$stringXML='<UpdateEft xmlns="http://tempuri.org/">
					  <UserName>'.$this->credentialsUser().'</UserName>
					  <PassWord>'.$this->credentialsPWD().'</PassWord>
					  <EftTransactionID>'.$EftTransactionID.'</EftTransactionID>
					  <EftDate>'.$EftDate.'</EftDate>
					  <EftAmount>'.$EftAmount.'</EftAmount>
					  <EftFee>0</EftFee>
					  <BankName></BankName>
					  <BankCity></BankCity>
					  <BankState></BankState>
					  <AccountNumber>'.$AccountNumber.'</AccountNumber>
					  <RoutingNumber>'.$RoutingNumber.'</RoutingNumber>
					  <AccountType>Checking</AccountType>
					  <Memo>message</Memo>
				</UpdateEft>';			
		$opts=$this->bodyXML();
		$opts['http']['content']=str_replace('{BodyXML}',$stringXML,$opts['http']['content']);
		error_log(print_r($opts,1),0);
		$this->ExecFunctionAPI($opts);
	}
									
	public function bodyXML()
	{
		$opts = array('http' =>
					  array(
						'method'  => 'POST',
						'header'  => "Content-Type: text/xml\r\n",
						'content' => '<?xml version="1.0" encoding="utf-8"?>
					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					 <soap:Body>
					 	{BodyXML}
					 </soap:Body>
					</soap:Envelope>',
						'timeout' => 180
					  )
					);
		return $opts;
	}
	public function credentialsUser()
	{
		return env('API_USER');
	}
	public function credentialsPWD()
	{
		return env('API_PASSWORD');
	}
	public function yyyymmddTommddyyyy($date)
	{
		$date = explode("-",substr($date,0,10));
		$date = $date[2].'-'.$date[0].'-'.$date[1];
		return $date;
	}
	public function yyyymmddTommddyyyy2($date)
	{
		$date = explode("/",substr($date,0,10));
		$date = $date[2].'-'.$date[0].'-'.$date[1];
		return $date;
	}
	public function mmddyyyyToyyyymmdd($date)
	{
		$date = explode("/",substr($date,0,10));
		$date = $date[2].'-'.$date[0].'-'.$date[1];
		return $date;
	}
	public function ExecFunctionAPI($opts)
	{
		$context  = stream_context_create($opts);
		$url = 'https://www.securpaycardportal.com/proxy/proxy.incoming/eftservice.asmx';	
		$resultPaymentsEPPS = file_get_contents($url, false, $context, 0, 10485760);
		return $resultPaymentsEPPS ;
	}
	public function InsertHistory($CardHolderID,$title,$CadenaPaymentsChanges)
	{

		/* save into active log*/
		$data = array(
			'idFile' =>$CardHolderID,
			'title' => $title,
			'detail' => $CadenaPaymentsChanges,
			'idUser' => Session::get('iduser')
		);
		$log = new History($data);
		$log->save();	
	}
	
}

/**/
