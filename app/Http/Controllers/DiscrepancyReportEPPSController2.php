<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;

class DiscrepancyReportEPPSController2 extends Controller

{
	public function index(){
		$rows = DB::select('SELECT
			c.idFile,
			CONCAT(f.firstName," ",f.lastName) as name,
			c.depositDate as date,
			c.amount depositeAmount,
			b.amount commisionAmount,
			1250 as eppsAmount,
			c.amount - (b.amount + 1250) as resta,
			c.provider,
			c.statusCode
			FROM
				clientPayments c
			INNER JOIN file f
				on c.idFile = f.idFile
			INNER JOIN (SELECT idfile,payoutDate, sum(amount) as amount FROM commissionPayments GROUP BY idFile, payoutDate) b 
			ON c.idFile = b.idFile AND c.depositDate = b.payoutDate
			WHERE
				c.statusCode IN ("T","P")
				and c.provider = "EPPS"
				and c.amount - (b.amount + 1250) <> 0 ORDER BY c.idFile, c.depositDate');
		
		return view('form.DiscrepancyReportEPPSController2.index',compact('rows'));
	}
}