<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CommissionPaymentsController;
use Input;

use App\CommentClientPayments;
use App\ClientPayments;

class TestController extends Controller {

    public function updateClientPaymentID($idFile = null){
        
        $commentsQuery = CommentClientPayments::whereNull('clientPaymentID');
        
        if($idFile){
            $commentsQuery->where('idFile',$idFile);
        }
        
        $comments = $commentsQuery->limit(20000)->get();
        
        foreach($comments as $comment){
            $payment = ClientPayments::where([
                ['idFile',$comment->idFile],
                ['depositDate',$comment->dateTransation]
            ])->first();
            if($payment){
                $comment->clientPaymentID = $payment->provider == 'SAS' ? "{$payment->depositDate}_{$payment->amount}":$payment->eftTransactionID;
                $comment->save();
                echo "<br />- <b>$comment->idComment</b> updated";
            }else{
                echo "<br />- No payment for <b>$comment->idComment</b>";
            }
        }
    }


    public function updateComments($idFile = null){
        $commentsQuery = DB::select('SELECT clientPaymentID, count(clientPaymentID) from commentClientPayments  GROUP by clientPaymentID HAVING COUNT(clientPaymentID) >1');
        foreach($commentsQuery as $commentID){
            $commentsClient = CommentClientPayments::where('clientPaymentID',$commentID->clientPaymentID)->get();
            $current = null;
            foreach($commentsClient as $c){
                if($current){
                    $c->delete();
                }
                else{
                    $current = $c->clientPaymentID;
                    echo "<br />- No payment for <b>$c->clientPaymentID</b>";
                }
            }
        }
    }

}
