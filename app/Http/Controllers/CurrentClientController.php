<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ClientPayments;
use App\CommentClientPayments;

class CurrentClientController extends Controller {

    public function index(Request $request) {

        if ($request) {
            $searchClient = Session::get('idFile');

           //Call new service sync with Jhon Method.

			$curl = curl_init();

			curl_setopt_array($curl, array(
					CURLOPT_URL => "http://keykoapp.nyxent.com/synchronizePaymentsEPPS/".$searchClient,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_TIMEOUT => 30000,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						// Set Here Your Requesred Headers
							'Content-Type: application/json',
					),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {
					echo "cURL Error #:" . $err;
			} else {
					// print_r(json_decode($response));
			}

            $MessageClientEmpty = 'N';
            $itemsFile = array();
            if ($searchClient == '' || $searchClient == '0') {
                $MessageClientEmpty = 'Y';
                $data = array();
            } else {

                //$dataResult = DB::select($this->query() . $where . $this->queryOrder());
                $dataResult = ClientPayments::where([
                            ['idFile', $searchClient]
                        ])->whereNotIn('statusCode',array('V','PF','PFV'))->orderBy('depositDate')->get();

                $currentCount = 0;
                $currentPos = 0;
                foreach ($dataResult as $dR) {

                    
                    $newData = $dR->toArray();
                    $newData['depositDate'] = date_format(date_create($newData['depositDate']), 'm/d/Y');
                    $newData['amount'] = "$".  number_format($newData['amount']/100,2);
                    $newData['localPN'] = $currentCount;
                    $newData['file'] = $dR->file;
                    $newData['depositDate2'] = date_format(date_create($newData['depositDate']), 'Ymd');
                    $newData['comment'] = $dR->getComments();
                    $newData['manualPaymentNumber'] = $newData['comment'] ? $newData['comment']->manualPaymentNumber : '';
                    $newData['paymentNumber'] = $newData['statusCode'] != 'R' && $newData['statusCode'] != 'RF' ? $currentCount : null;
                    $newData['commentID'] = $dR->getIDComment();
                    $itemsFile[$currentPos++] = $newData;
                    
                    if($newData['statusCode'] != 'R' && $newData['statusCode'] != 'RF'){
                        $currentCount++;
                    }
                    
                }

                foreach ($itemsFile as $pos => $data) {
                    if ($data['manualPaymentNumber'] == 'Y') {
                        $newPos = intval($data['comment']->paymentNumber);
                        for ($count = $pos; $count < count($itemsFile); $count++) {
                            if ($itemsFile[$count]['statusCode'] != 'R' && $itemsFile[$count]['statusCode'] != 'RF') {
                                $itemsFile[$count]['paymentNumber'] = $newPos++;
                            }
                        }
                    }
                }
                
            }

            return view('form.currentClient.index', ["itemsFile" => $itemsFile,'idFile' => $searchClient, 'MessageClientEmpty' => $MessageClientEmpty]);
        }
    }

    public function sinchronizeCustomClientSAS($idFile){
        $CommisionPaymentsController=new CommissionEntryController();
        $CommisionPaymentsController->synchronizePaymentsSAS(5,$idFile);
        return redirect('/form/paymentCurrentClient');
    }
    
    
    public function store(Request $request) {
        $action = trim($request->action);

        switch ($action) {
            case 'renumber':
                return $this->index($request);
                break;
            case 'SavePaymentComment':
                $this->savePaymentComment($request);
                break;
            case 'SavePaymentNumber':
                $this->savePaymentNumber($request);
                break;
            case 'DeletePaymentNumber':
                $this->deletePaymentNumber($request);
                break;
        }
    }

    private function savePaymentComment($request) {

        $IdRow = trim($request->IdRow);
        $idCP = $request->idClientPayment;
        $clientPayment = ClientPayments::find($idCP);

        if ($clientPayment) {
            
            $comment = trim($request->comment);
            $IdFile = $clientPayment->idFile;
            $clientPaymentID = $clientPayment->getIDComment();


            $CommentCurrent = CommentClientPayments::where([
                        ['idFile', $IdFile],
                        ['clientPaymentID', $clientPaymentID]
                    ])->first();
            
            if ($CommentCurrent) {
                $CommentCurrent->comment = $comment;
                $CommentCurrent->manualEntry = 'Y';
                $CommentCurrent->save();
            } else {
                $create = CommentClientPayments::create(array(
                    'idFile' => $IdFile,
                    'comment' => $comment,
                    'manualEntry' => 'Y',
                    'dateTransation' => $clientPayment->depositDate,
                    'clientPaymentID' => $clientPaymentID
                ));
                
//                dd($create);
            }
        }

        echo json_encode(array('Save' => 'Ok', 'Comment' => $comment, 'IdRow' => $IdRow));
    }

    private function savePaymentNumber($request) {
        $IdRow = trim($request->IdRow);
        $paymentNumber = trim($request->paymentNumber);
        $idCP = $request->idClientPayment;
        $clientPayment = ClientPayments::find($idCP);

        if ($clientPayment) {
            $IdFile = $clientPayment->idFile;
            $clientPaymentID = $clientPayment->getIDComment();

            $CommentCurrent = CommentClientPayments::where([
                        ['idFile', $IdFile],
                        ['clientPaymentID', $clientPaymentID]
                    ])->first();

            if ($CommentCurrent) {
                $CommentCurrent->paymentNumber = $paymentNumber;
                $CommentCurrent->manualPaymentNumber = 'Y';
                $CommentCurrent->save();
            } else {

                CommentClientPayments::create(array(
                    'idFile' => $IdFile,
                    'paymentNumber' => $paymentNumber,
                    'manualPaymentNumber' => 'Y',
                    'dateTransation' => $clientPayment->depositDate,
                    'clientPaymentID' => $clientPaymentID
                ));
            }
            echo json_encode(array('Save' => 'Ok', 'paymentNumber' => $paymentNumber, 'IdRow' => $IdRow));
        }
    }

    private function deletePaymentNumber($request) {

        $IdRow = trim($request->IdRow);
        $idCP = $request->idClientPayment;
        $clientPayment = ClientPayments::find($idCP);

        if ($clientPayment) {
            $IdFile = $clientPayment->idFile;
            $clientPaymentID = $clientPayment->getIDComment();

            $CommentCurrent = CommentClientPayments::where([
                        ['idFile', $IdFile],
                        ['clientPaymentID', $clientPaymentID]
                    ])->first();

            if ($CommentCurrent) {
                $CommentCurrent->paymentNumber = null;
                $CommentCurrent->manualPaymentNumber = null;
                $CommentCurrent->save();


                echo json_encode(array('Save' => 'Ok', 'paymentNumber' => 0, 'IdRow' => $IdRow));
            }
        }
    }

}
