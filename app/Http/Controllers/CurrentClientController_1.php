<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\CommissionPaymentsController;
use App\Http\Controllers\CommissionEntryController;
use Input;
use App\ClientPayments;


class CurrentClientController_1 extends Controller {

    public function index(Request $request) {
        $page = Input::get('page', 1);
        $CommisionPaymentsController = new CommissionPaymentsController();
        $paginate = $CommisionPaymentsController->paginateGlobal();
        /*
         * FUNCTION IN ANOTHER CONTROLLER
         */
        $CommisionPaymentsController = new CommissionPaymentsController();
        if ($request) {
            $searchClient = Session::get('idFile');
            /* $searchClient= '2133'; */
            $MessageClientEmpty = 'N';
            $itemsFile = array();
            if ($searchClient == '' || $searchClient == '0') {
                $MessageClientEmpty = 'Y';
                $data = array();
            } else {
                $where = '';
                if ($searchClient != '') {
                    $where.=" AND clientPayments.idFile='" . $searchClient . "' ";
                }
                //$dataResult = DB::select($this->query() . $where . $this->queryOrder());
                $dataResult = ClientPayments::where([
                    ['idFile',$searchClient],
                    []
                ]);

                $data = array();
                $currentCount = 0; 
                foreach($dataResult as $dR){
                    dd($dR);
                    $newData = $dR->toArray();
                    $newData['localPN'] = $currentCount;
                    $data[$currentCount] = $newData;
                    
                }
                dd($data);
                
                

                if (isset($data)) {
                    $offSet = ($page * $paginate) - $paginate;
                    $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
                    $itemsFile = new \Illuminate\Pagination\LengthAwarePaginator(
                            $itemsForCurrentPage, count($data), $paginate, $page, ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]);
                }
            }
            
            return view('form.currentClient.index', ["itemsFile" => $itemsFile, 'MessageClientEmpty' => $MessageClientEmpty]);
        }
    }

    public function query() {
        $query = 'SELECT 
					file.idFile,
					lastName,
					firstName,
					commentClientPayments.paymentNumber,
					paymentSequence,
					DATE_FORMAT(depositDate, "%Y%m%d") AS depositDate2,
					DATE_FORMAT(depositDate, "%m/%d/%Y") AS depositDate,
					CONCAT("$",FORMAT(amount/100, 2)) AS amount,
					commentClientPayments.comment,
					IF(
					clientPayments.statusCodeDescription="Returned",
					
					CONCAT(clientPayments.statusCodeDescription,"--",clientPayments.lastMessage),clientPayments.statusCodeDescription
					
					) AS statusCodeDescription,
					clientPayments.statusCode,
					clientPayments.provider,
					commentClientPayments.paymentNumber,
					commentClientPayments.manualPaymentNumber						
					FROM clientPayments
					INNER JOIN `file` ON clientPayments.idFile=file.idFile
					LEFT JOIN 
					(
						SELECT idFile,dateTransation,MIN(comment) AS comment,MIN(paymentNumber) AS paymentNumber,manualPaymentNumber
						FROM commentClientPayments
						GROUP BY idFile,dateTransation,manualPaymentNumber
					)
					commentClientPayments ON commentClientPayments.idFile=clientPayments.idFile AND 
					commentClientPayments.dateTransation=clientPayments.depositDate
					WHERE clientPayments.statusCode<>"P" and "S"="S"
					';
        return $query;
    }

    public function queryOrder() {
        $query = " ORDER BY DATE_FORMAT(depositDate, '%Y/%m/%d'),lastName,firstName";
        return $query;
    }

    public function store(Request $request) {
        $action = trim($request->GET('action'));

        if ($action == 'SavePaymentComment') {
            $IdFile = trim($request->GET('IdFile'));
            $IdDate = trim($request->GET('IdDate'));
            $comment = trim($request->GET('comment'));
            $IdRow = trim($request->GET('IdRow'));


            $CommentCurrent = DB::select("SELECT * 
										FROM commentClientPayments 
										WHERE idfile='" . $IdFile . "' AND dateTransation='" . $IdDate . "' ");

            if (count($CommentCurrent) > 0) {
                DB::update("UPDATE commentClientPayments 
							SET comment='" . $comment . "',manualEntry='Y'
							WHERE idfile='" . $IdFile . "' AND dateTransation='" . $IdDate . "' ");
            } else {
                DB::insert("INSERT INTO commentClientPayments 
					(
						idFile,
						dateTransation,
						comment,
						manualEntry
					)
					VALUES
					(
						'" . $IdFile . "',
						'" . $IdDate . "',
						'" . $comment . "',
						'Y'
					);
				");
            }
            echo json_encode(array('Save' => 'Ok', 'Comment' => $comment, 'IdRow' => $IdRow));
            exit;
        }
        if ($action == 'SavePaymentNumber') {

            $IdFile = trim($request->GET('IdFile'));
            $IdDate = trim($request->GET('IdDate'));
            $paymentNumber = trim($request->GET('paymentNumber'));
            $IdRow = trim($request->GET('IdRow'));


            $CommentCurrent = DB::select("SELECT * 
										FROM commentClientPayments 
										WHERE idfile='" . $IdFile . "' AND dateTransation='" . $IdDate . "'  ");

            if (count($CommentCurrent) > 0) {
                DB::update("UPDATE commentClientPayments 
							SET paymentNumber='" . $paymentNumber . "', manualPaymentNumber='Y'
							WHERE idfile='" . $IdFile . "' AND dateTransation='" . $IdDate . "' ");
            } else {
                DB::insert("INSERT INTO commentClientPayments 
					(
						idFile,
						dateTransation,
						paymentNumber,
						manualPaymentNumber
					)
					VALUES
					(
						'" . $IdFile . "',
						'" . date("Y/m/d", strtotime($IdDate)) . "',
						'" . $paymentNumber . "',
						'Y'
					);
				");
            }
            echo json_encode(array('Save' => 'Ok', 'paymentNumber' => $paymentNumber, 'IdRow' => $IdRow));
            exit;
        }
        if ($action == 'renumber') {
            $CommissionEntryController = new CommissionEntryController();
            $CommissionEntryController->updatePaymentNumber();
            return $this->index($request);
        }
        if ($action == 'DeletePaymentNumber') {

            $IdFile = trim($request->GET('IdFile'));
            $IdDate = trim($request->GET('IdDate'));
            $paymentNumber = trim($request->GET('paymentNumber'));
            $IdRow = trim($request->GET('IdRow'));

            DB::update("UPDATE commentClientPayments 
						SET  manualPaymentNumber=NULL
						WHERE idfile='" . $IdFile . "' AND dateTransation='" . $IdDate . "' ");
            $CommissionEntryController = new CommissionEntryController();
            $CommissionEntryController->updatePaymentNumber();

            echo json_encode(array('Save' => 'Ok', 'paymentNumber' => $paymentNumber, 'IdRow' => $IdRow));
            exit;
        }
    }

}
