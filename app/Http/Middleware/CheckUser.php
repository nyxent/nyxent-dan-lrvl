<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\DB;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$hash=trim($request->GET('hash'));
		if($hash and $request->session()->get('login') ===null)
		{
			$hashResult =DB::select("SELECT hash,iduser,idFile,userIdProfile FROM shareSession
									   WHERE hash='".$hash."'
				");
			
			if(count($hashResult)>0)
			{
				$request->session()->put('login','Ok');
				$request->session()->put('iduser',$hashResult[0]->iduser);
				$request->session()->put('idFile',$hashResult[0]->idFile);
				$request->session()->put('userIdProfile',$hashResult[0]->userIdProfile);
				$request->session()->put('affiliateCompanyAdmin','N');
				/*
					* SELECT  affiliateCompanyAdmin
				*/
				$UserAdmin =DB::select("SELECT IF(`affiliateCompanyAdmin`=1,'S','N') AS affiliateCompanyAdmin FROM `userAffiliateCompany` WHERE `idUser`='".$request->session()->get('iduser')."' ");
				if(count($UserAdmin)>0)
				{
					$request->session()->put('affiliateCompanyAdmin',$UserAdmin[0]->affiliateCompanyAdmin);
				}
			}	
				
				/***********************************/
			    DB::delete("DELETE  FROM shareSession");
		}
		else
		{
			if($hash)
			{
				$hashResult =DB::select("SELECT hash,iduser,idFile,userIdProfile FROM shareSession
										   WHERE hash='".$hash."'
					");
				if(count($hashResult)>0)
				{
					$request->session()->put('iduser',$hashResult[0]->iduser);
					$request->session()->put('idFile',$hashResult[0]->idFile);
					$request->session()->put('userIdProfile',$hashResult[0]->userIdProfile);
					$request->session()->put('affiliateCompanyAdmin','N');
					/*
						* SELECT  affiliateCompanyAdmin
					*/
					$UserAdmin =DB::select("SELECT IF(`affiliateCompanyAdmin`=1,'S','N') AS affiliateCompanyAdmin FROM `userAffiliateCompany` WHERE `idUser`='".$request->session()->get('iduser')."' ");
					if(count($UserAdmin)>0)
					{
						$request->session()->put('affiliateCompanyAdmin',$UserAdmin[0]->affiliateCompanyAdmin);
					}
				}	
					
					/***********************************/
				DB::delete("DELETE  FROM shareSession");
			}
		}

		if($request->session()->get('login') ===null)
		{
			return redirect('/login');
		}
        return $next($request);
    }
}
