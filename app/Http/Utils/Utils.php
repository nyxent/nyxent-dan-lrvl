<?php

namespace App\Http\Utils;

use App\CommissionPayments;
use Carbon\Carbon;

class Utils {

    public static function formatDate($date) {
        if ($date) {
            $_date = \DateTime::createFromFormat('m/d/Y', $date);
            return date_format($_date, 'Y-m-d');
        }
        return null;
    }

    public static function dateTimeFormat($date) {
        if ($date) {
            $_date = strtotime($date);
            return date('m/d/Y', $_date);
        }
        return "";
    }

    public static function maskAccount($account) {
        $account = preg_replace('/\s+/', '', $account);
        $length = strlen($account);
        for ($i = 0; $i < $length - 4; $i++) {
            $account[$i] = 'X';
        }
        return $account;
    }

    public static function isDownPayment($date, $idFile) {
        if($date) {
            $_date = Carbon::createFromFormat('m/d/Y', $date);
            $usMarketers = CommissionPayments::where('idFile', $idFile)->where('affiliateCompanyId', env('USMARKS_ID', 16))->where('payoutDate', $_date->format('Y-m-d'))->where('amount', '>', 0)->first();
            if (isset($usMarketers)) {
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }

}
