<?php

namespace App\Http\Utils;

use App\CommissionPayments;
use Carbon\Carbon;
use App\ClientPayments;

class PaymentNumberUtil {

    public static function repairClientPayments($idFile) {
        // set oayemnt number to null
        ClientPayments::where('idFile', $idFile)->update(['paymentNumber' => null]);

        $payments = ClientPayments::where([
                    ['idFile', $idFile]
                ])->whereNotIn('statusCode', array('V', 'PF', 'PFV'))->orderBy('depositDate')->get();

        $cont = 0;
        foreach ($payments as $payment) {

            if ($payment->statusCode != 'R' && $payment->statusCode != 'RF') {

                $comment = $payment->getComments();
                if ($comment && $comment->manualPaymentNumber == 'Y') {
                    $cont = $comment->paymentNumber;
                }

                $payment->paymentNumber = $cont++;

                $payment->save();
            }
        }
    }

}
