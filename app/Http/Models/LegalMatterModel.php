<?php

namespace App\Http\Models;

use App\Http\Utils\Utils;

use App\LegalMatter;

class LegalMatterModel {

    public static function getLegalMatters($filters = array(), $paginate = 20) {

        $sql = LegalMatter::query();

        if (isset($filters['dateFrom'])) {
            $sql->where('openDate', '>=', Utils::formatDate($filters['dateFrom']));
        }

        if (isset($filters['dateTo'])) {
            $sql->where('openDate', '<=', Utils::formatDate($filters['dateTo']));
        }

        
        return $paginate ? $sql->paginate($paginate) : $sql->get();
    }

}
