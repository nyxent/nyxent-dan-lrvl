<?php

namespace App\Http\Models;

use App\Client;
use App\Http\Utils\Utils;

class ClientModel {

    public static function getClients($filters = array(), $paginate = 20) {

        $sql = Client::query();

        if (isset($filters['dateFrom'])) {
            $sql->where('enrollmentApprovedDate', '>=', Utils::formatDate($filters['dateFrom']));
        }

        if (isset($filters['dateTo'])) {
            $sql->where('enrollmentApprovedDate', '<=', Utils::formatDate($filters['dateTo']));
        }

        if (isset($filters['fileStatus'])) {
            $sql->whereIn('idFileStatus', $filters['fileStatus']);
        }

        if (isset($filters['keyword'])) {
            $sql->where(function($subQ) use ($filters) {
                $subQ->where('firstName', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('initial', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('lastName', 'like', "%{$filters['keyword']}%");
                $subQ->orWhereRaw("CONCAT(firstName, ' ', lastName) LIKE ?", array("%{$filters['keyword']}%"));
                $subQ->orWhere('idFile', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('idFileDan', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('email', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('homePhone', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('businessPhone', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('cellPhone', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('scFirstName', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('scInitial', 'like', "%{$filters['keyword']}%");
                $subQ->orWhere('scLastName', 'like', "%{$filters['keyword']}%");
            });
        }

//        dd($filters);
//        dd($sql->toSql());
        return $paginate ? $sql->paginate($paginate) : $sql->get();
    }

}
