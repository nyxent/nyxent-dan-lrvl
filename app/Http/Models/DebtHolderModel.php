<?php

namespace App\Http\Models;

use App\DebtHolder;
use App\Http\Utils\Utils;

class DebtHolderModel {
	
	public static function getDebtHolders($filters = array(), $paginate = 20) {

		$sql = \DB::table('file as f')
						->leftJoin('debtHolder as d','f.idFile','=','d.idFile')
						->leftJoin(\DB::raw("(SELECT idFile,max(depositDate) as lastPayment FROM clientPayments WHERE statusCode = 'S' GROUP BY idFile) as c"),'f.idFile','=','c.idFile')
						->leftJoin('state as s','f.idState','=','s.idState');
					
		
		if (isset($filters['dateFrom'])) {
			$sql->where('f.enrollmentApprovedDate', '>=', Utils::formatDate($filters['dateFrom']));
		}
		
		if (isset($filters['dateTo'])) {
			$sql->where('f.enrollmentApprovedDate', '<=', Utils::formatDate($filters['dateTo']));
		}
		
		if (isset($filters['fileStatus'])) {
			$sql->whereIn('idFileStatus', $filters['fileStatus']);
		}
		
		if (isset($filters['keyword'])) {
			$sql->where(function($subQ) use ($filters) {
				$subQ->where('f.firstName', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.initial', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.lastName', 'like', "%{$filters['keyword']}%");
				$subQ->orWhereRaw("CONCAT(f.firstName, ' ', f.lastName) LIKE ?", array("%{$filters['keyword']}%"));
				$subQ->orWhere('f.idFile', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.idFileDan', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.email', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.homePhone', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.businessPhone', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.cellPhone', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.scFirstName', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.scInitial', 'like', "%{$filters['keyword']}%");
				$subQ->orWhere('f.scLastName', 'like', "%{$filters['keyword']}%");
			});
		}
		
		$sql->select('f.*', 'd.*', 's.name as state','c.lastPayment');

		$sql->get();

		return $paginate ? $sql->paginate($paginate) : $sql->get();
	}
	
}
