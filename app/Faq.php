<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    protected $table = "faq";
    protected $primaryKey = 'idfaq';
    protected $fillable = ['idFaqCategory', 'question', 'answer', 'tags', 'order'];

    public function faqCategory() {
        return $this->belongsTo('App\FaqCategory', 'idFaqCategory', 'idFaqCategory');
    }

}
