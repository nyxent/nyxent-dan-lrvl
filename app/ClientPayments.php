<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CommentClientPayments;

class ClientPayments extends Model {

    protected $table = 'clientPayments';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = array(
        'idFile',
        'paymentNumber',
        'paymentSequence',
        'depositDate',
        'amount',
        'comments',
        'provider',
        'statusCode',
        'statusCodeDescription',
        'routingNumber',
        'accountNumber',
        'lastMessage',
        'eftTransactionID'
    );
    
    public function file(){
        return $this->belongsTo('App\Client','idFile','idFile');
    }

    public function getComments(){
        $idComment = $this->getIDComment();        
        return CommentClientPayments::where([
            ['idFile',$this->idFile],
            ['clientPaymentID',$idComment]
        ])->first();
    }
    
    /**
     * Retorna el id que relaciona con la tabla comments
     * - si es epps se usa el campo eftTransactionID 
     * - Si es SAS se usa los campos depositDate + '_' + amount
     * @return String
     */
    public function getIDComment(){
        return $this->provider == 'SAS' ? "{$this->depositDate}_{$this->amount}":$this->eftTransactionID;
    }
}
