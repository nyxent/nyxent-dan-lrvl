<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadFile extends Model
{
    protected $table='uploadFile';
	protected $primaryKey='id';
	 
	public $timestamps=false;
	 
     protected $fillable = [
      'uploadDate'
    ];
}
