<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\CommissionEntryController;

class syncEPPS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:EPPS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Payments and Commissions with EPPS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = CommissionEntryController::synchronizePaymentsEPPS();
    }
}
