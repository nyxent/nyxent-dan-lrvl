<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentClientPayments extends Model {

    protected $table = 'commentClientPayments';
    protected $primaryKey = 'idComment';
    protected $fillable = array('idFile', 'dateTransation', 'comment', 'commissionPaid', 'paymentNumber', 'manualEntry', 'dateUpdate', 'manualPaymentNumber', 'clientPaymentID');
    public $timestamps = false;

}