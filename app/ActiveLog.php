<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveLog extends Model
{
  protected $table = "activeLog"; 
  protected $primaryKey = 'id'; 
  protected $fillable = ['idFile', 'title', 'detail','idUser'];

}
