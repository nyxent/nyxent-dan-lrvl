<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model {

    protected $table = "faqCategory";
    protected $primaryKey = 'idFaqCategory';
    protected $fillable = ['name',];

    public function tabs() {
        return $this->hasMany('app\Faq', 'idFaqCategory', 'idFaqCategory')->whereNotNull('answer')->orderBy('order');
    }

}
