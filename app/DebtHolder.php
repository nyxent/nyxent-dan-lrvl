<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebtHolder extends Model {

    protected $table = "debtHolder";
    protected $primaryKey = 'idDebtHolder';
    protected $fillable = ['idFile', 'name', 'accountNumber', 'balance', 'notes', 'status',
        'type', 'address', 'city', 'zip', 'idState', 'typeOfDebt', 'ref', 'lastPaymentDate',
        'accountStatus', 'bp2NameOnStatement', 'bp2StatementReceivedDate', 'bp2OfferMailedDate',
        'bp2OfferCheckNumber', 'bp2OfferClearedDate', 'bp2AddressChanged', 'bp2MinPmtDate1', 'bp2Check1',
        'bp2MinPmtDate2', 'bp2Check2', 'bp2MinPmtDate3', 'bp2Check3', 'bp2MinPmtDate4', 'bp2Check4',
        'bp2MinPmtDate5', 'bp2Check5', 'bp2MinPmtDate6', 'bp2Check6', 'debtorsName','businessName'];
    protected $hidden = ['idDebtHolder'];
    public $timestamps = false;

    public function debtHolderNotes() {
		return $this->hasMany('App\DebtHolderNote', 'idDebtHolder', 'idDebtHolder');
    }
    
    public function client(){
		return $this->belongsTo('App\Client', 'idFile','idFile');
	}

}
