<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
  protected $table = "announcements"; 
  protected $primaryKey = 'idannouncement'; 
  protected $fillable = ['remitter_id', 'message','subject', 'recipient_id','all'];

  public function  getRemitter(){
    return User::find($this->remitter_id);
  }
}
