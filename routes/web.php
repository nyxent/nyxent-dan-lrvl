<?php

/*

  |--------------------------------------------------------------------------

  | Web Routes

  |--------------------------------------------------------------------------

  |

  | Here is where you can register web routes for your application. These

  | routes are loaded by the RouteServiceProvider within a group which

  | contains the "web" middleware group. Now create something great!

  |

 */



Route::get('/', function () {

    return view('welcome');
});

Route::resource('form/commisionReportAffilate', 'CommisionReportAffilateController');
Route::resource('form/commisionReportSuperAffilate', 'CommisionReportSuperAffilateController');
Route::get('paymentConsecutive', 'PaymentController@paymentConsecutive');
Route::get('repairAll', 'PaymentController@repairAll');


Route::get('form/repairCommission', 'RepairCommissionController@index');
Route::post('saveRepairCommission', 'RepairCommissionController@saveRepairCommission');

Route::group(['middleware' => 'checkuser'], function() {

    
    
    Route::resource('form/commissionEntry', 'CommissionEntryController');
    Route::resource('form/paymentsByClient', 'ClientPaymentsController');
    Route::resource('form/CommissionPayments', 'CommissionPaymentsController');
    Route::resource('form/paymentHistory', 'PaymentHistoryController');
    Route::resource('form/commisionPaymentsByClient', 'CommissionPaymentsByClientController');
    Route::resource('form/commisionPaymentsByClient2', 'CommissionPaymentsByClientController2');
    Route::resource('form/commisionReport', 'CommisionReportController');
    Route::resource('form/paymentCurrentClient', 'CurrentClientController');

    Route::resource('form/commisionReportAffilateAS', 'CommisionReportAffilateASController');
    Route::resource('form/commisionReportAffilateKC', 'CommisionReportAffilateKCController');
    Route::resource('form/manCommissionEntry', 'ManualCommissionEntryController');
    Route::resource('form/rescheduleEFT', 'RescheduleEFTController');
    Route::resource('form/DiscrepancyReportEPPS', 'DiscrepancyReportEPPSController');
    Route::resource('form/Forecast', 'ForecastController');
    Route::resource('form/ForecastSAS', 'ForecastControllerSAS');
    Route::resource('form/DiscrepancyMonthly', 'DiscrepancyMonthlyEPPSController');
    Route::resource('form/ACHonDemandReports', 'ACHonDemandReportsController');

    //Client Portal Announcements
    Route::get('announcement/create', 'AnnouncementsController@form')->name('announcement.create');
    Route::post('announcement', 'AnnouncementsController@save')->name('announcement.save');

    // Client Portal FAQ
    Route::get('faqs', 'FaqController@index')->name('faqs.index');
    Route::get('faqs/create', 'FaqController@create')->name('faqs.create');
    Route::post('faqs/', 'FaqController@store')->name('faqs.store');
    Route::get('faqs/{id}/edit', 'FaqController@edit')->name('faqs.edit');
    Route::post('faqs/{id}', 'FaqController@update')->name('faqs.update');
    Route::get('faqs/{id}/delete', 'FaqController@destroy')->name('faqs.destroy');

    Route::get('updateClientPaymentID', 'TestController@updateClientPaymentID')->name('faqs.destroy');

    Route::get('donaReport', 'DonaReportController@index')->name('donaReport');
    Route::get('creditRepair', 'CreditRepairController@index')->name('creditRepair');
    Route::get('creditRepairDocuments', 'CreditRepairController@documents')->name('creditRepairDocuments');
    Route::post('creditRepairDocumentSave', 'CreditRepairController@saveDocument')->name('creditRepairDocumentSave');
    Route::get('creditRepairDelete/{id}', 'CreditRepairController@deleteDocument')->name('creditRepairDelete');
    Route::get('downloadDocumentZip/{id}', 'CreditRepairController@downloadArchiveZip')->name('downloadArchiveZip');
    Route::post('checkClient', 'CreditRepairController@checkClient')->name('checkClient');
    Route::post('saveCSDate', 'CreditRepairController@saveCSDate')->name('saveCSDate');
    
    // download data report
    Route::get('profileDownload', 'DownloadDataReportController@detailedInfo')->name('profileDownload');
    Route::get('detailedInfo', 'DownloadDataReportController@index')->name('detailedInfo');
    
    // summon submision report
    Route::get('summonsubmisison', 'SummonSubmisionReportController@index')->name('summonsubmisison');
});



Route::get('form/reactivateUser', 'ReactivateUserController@reactivateUser');

Route::post('form/savePaymentNumber', 'CurrentClientController@store');


// soapClient
Route::get('voids', 'SoapController@index')->name('soap.find');
Route::post('voids', 'SoapController@FindEftByStatusDate')->name('soap.findEftByStatusDate');

Route::get('nsf', 'NsfController@index')->name('nsf');
Route::post('nsf', 'NsfController@nsfReSchedule')->name('rescheduleNSF');
Route::get('nsfSync', 'NsfController@syncNSF');
Route::get('sendMail', 'NsfController@sendMail');

//EPPS 
Route::get('inactives', 'EppsController@voidInactives')->name('epps.voidInactives');
Route::get('inactive/{idFile}', 'EppsController@voidInactiveClient')->name('epps.voidInactiveClient');
Route::get('startEPPSPayments/{idFile}', 'EppsController@startEPPSPayments')->name('epps.startEPPSPayments');

Route::group(['middleware' => 'checkuserclient'], function() {

    Route::resource('form/clientReport', 'clientReportController');
});

// Route::get('commisionReportAffilate', 'CommisionReportAffilateController@index');

Route::get('startPayments/{id}', 'EppsController@startPayments')->name('epps.startpayments');


Route::get('sinchronizeCustomClientSAS/{id}', 'CurrentClientController@sinchronizeCustomClientSAS');


Route::resource('cron', 'CronCrontoller');

Route::resource('login', 'LoginController');







/*Route::resource('form/commissionEntry','CommissionEntryController');

Route::resource('form/commissionEntry','CommissionEntryController');*/



/*

Route::post('items/import', 'ItemController@import');

*/