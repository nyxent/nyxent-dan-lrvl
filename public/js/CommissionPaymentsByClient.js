function ShowChangeDateFee(dateFeeTransaction)
{
	$("#DateFee"+dateFeeTransaction).attr('onclick','');
	$.date=$("#DateFee"+dateFeeTransaction).html();

	$("#DateFee"+dateFeeTransaction).html(
	'<input id="dateFee1'+dateFeeTransaction+'" value="'+$.date+'" readonly="readonly" class="date" type="text" style="width:100px;height:20px;" />');
		
	$('#dateFee1'+dateFeeTransaction).datepicker({  
       format: 'mm/dd/yyyy',
	   startDate: CurrentDate(),
	    autoclose: true,
     });  
	 $('#dateFee1'+dateFeeTransaction).focus();
	 
	 /*
	 	* ONCHANGE
	 */
	 $('#dateFee1'+dateFeeTransaction).change(function () {
		 	
			if($.date==$('#dateFee1'+dateFeeTransaction).val())
			{
				$("#DateFee"+dateFeeTransaction).html($('#dateFee1'+dateFeeTransaction).val());
				$("#DateFee"+dateFeeTransaction).attr('onclick','ShowChangeDateFee('+"'"+dateFeeTransaction+"'"+')');
			}
			else
			{
				$("#DateFee"+dateFeeTransaction).html($('#dateFee1'+dateFeeTransaction).val());
				$("#DateFee"+dateFeeTransaction).attr('onclick','ShowChangeDateFee('+"'"+dateFeeTransaction+"'"+')');
				$.dateCurrent=(($("#DateFee"+dateFeeTransaction).html()).substring(0, 10)).split("/");
				$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
				var Contenido='Do you want to change all following dates?';
				MostrarPopup('Confirm',
							Contenido,
							'AceptChangeFollowingDateFee('+"'"+dateFeeTransaction+"'"+')',
							'AceptChangeCurrentDateFee('+"'"+dateFeeTransaction+"','"+$.CurrentDate+"'"+')'
							,'','primary','N','N','','Change all following dates','Only selection');
				
			}
			
      });
	 /*  $('#dateFee1'+dateFeeTransaction).blur(function () {
		   $('#dateFee1'+dateFeeTransaction).trigger('change');
	   })*/
}
function AceptChangeFollowingDateFee(dateFeeTransaction)
{
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.Fee_Date).substring(0, 10)).split("-");
		$.lastDay=new Date($.dateFormat[0], $.dateFormat[1], 0).getDate();
				
		$.dateCurrent=(($("#DateFee"+dateFeeTransaction).html()).substring(0, 10)).split("/");
		$.lastDayCurrent=new Date($.dateCurrent[2], $.dateCurrent[0], 0).getDate();

		if(dateFeeTransaction==((value.Fee_Date).substring(0, 10)))
		{
			$.FeeDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];	
			$("#msg"+dateFeeTransaction).html($("#img").html());
			$.CommissionEPPSTrasantion[key]['change']='Y';
			$.CommissionEPPSTrasantion[key]['changeDate']=$.FeeDate;
		}
		else
		{
			if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
			{
				$.dayCurrent=$.dateCurrent[1];
				if($.lastDayCurrent==$.dayCurrent)
				{
					$.day=$.lastDay;
				}
				else
				{
					$.day=$.dayCurrent;
				}
				$("#DateFee"+((value.Fee_Date).substring(0, 10))).html($.dateFormat[1]+'/'+$.dayCurrent+'/'+$.dateFormat[0]);
				
				$.FeeDate=$.dateFormat[0]+'-'+$.dateFormat[1]+'-'+$.day;	
				$("#msg"+((value.Fee_Date).substring(0, 10))).html($("#img").html());
				$.CommissionEPPSTrasantion[key]['change']='Y';
				$.CommissionEPPSTrasantion[key]['changeDate']=$.FeeDate;
			}
		}
	});
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}
function AceptChangeCurrentDateFee(dateFeeTransaction,CurrentDate)
{
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{				
		if(dateFeeTransaction==((value.Fee_Date).substring(0, 10)))
		{
			$("#msg"+dateFeeTransaction).html($("#img").html());
			$.CommissionEPPSTrasantion[key]['change']='Y';
			$.CommissionEPPSTrasantion[key]['changeDate']=CurrentDate;
		}
	});
	SaveCommisionEPPSChangeToArray();
}
function SaveCommisionEPPSChangeToArray()
{
	$.CommissionEPPSChanges=Array();
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{
		if(value.change=='Y')
		{
			$.CommissionEPPSChanges.push({
						FeeID:key,
						FeeAmountOrigin:($.isEmptyObject(value.FeeAmount)==true ? "" :value.FeeAmountOrigin),
						FeeDateOrigin:($.isEmptyObject(value.FeeAmount)==true ? "" :value.Fee_Date),
						FeeDate:($.isEmptyObject(value.changeDate)==true ? "" : value.changeDate),
						FeeAmount:($.isEmptyObject(value.FeeAmount)==true ? "" :value.FeeAmount),
						Description:($.isEmptyObject(value.Description)==true ? "" : value.Description),
						FeeType:($.isEmptyObject(value.FeeType)==true ? "" : value.FeeType),
						PaidToName:($.isEmptyObject( value.PaidToName)==true ? "" : value.PaidToName),
						PaidToPhone:($.isEmptyObject( value.PaidToPhone)==true ? "" : value.PaidToPhone),
						PaidToStreet:($.isEmptyObject( value.PaidToStreet)==true ? "" : value.PaidToStreet),
						PaidToStreet2:($.isEmptyObject(value.PaidToStreet2)==true ? "" : value.PaidToStreet2),
						PaidToCity:($.isEmptyObject(value.PaidToCity)==true ? "" : value.PaidToCity),
						PaidToState:($.isEmptyObject(value.PaidToState)==true ? "" : value.PaidToState),
						PaidToZip:($.isEmptyObject(value.PaidToZip)==true ? "" : value.PaidToZip),
						PaidToCustomerNumber:'',
						ContactName:''
					});	
		}
	});
	$("#CommissionEPPSChanges").val(JSON.stringify($.CommissionEPPSChanges));	
}
/******************************************************************
*******************************************************************
*******************************************************************/
function ShowChangeDate(eftTransactionID)
{
	$("#"+eftTransactionID).attr('onclick','');
	$.date=$("#"+eftTransactionID).html();

	$("#"+eftTransactionID).html(
	'<input id="date1'+eftTransactionID+'" value="'+$.date+'"  readonly="readonly" class="date" type="text" style="width:100px;height:20px;" />');
		
	$('#date1'+eftTransactionID).datepicker({  
       format: 'mm/dd/yyyy',
	   startDate: CurrentDate(),
	    autoclose: true,
     });  
	 $('#date1'+eftTransactionID).focus();
	 
	 /*
	 	* ONCHANGE
	 */
	 $('#date1'+eftTransactionID).change(function () {
		 	
			if($.date==$('#date1'+eftTransactionID).val())
			{
				$("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
				$("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
			}
			else
			{
				var Contenido='Do you want to change all following dates?';
				Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
				MostrarPopup('Confirm',Contenido,'AceptChangeFollowingDate('+eftTransactionID+')','AceptChangeCurrentDate('+eftTransactionID+')','','primary','N','N','','Change all following dates','Only selection');
				$("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
				$("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
				$("#CountMonth").val($.CountMonth);
			}
      });
	 /* $('#date1'+eftTransactionID).blur(function () { 
		  $("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
		  $("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
	   })*/
	  
	
}
function AceptChangeFollowingDate(eftTransactionID)
{
	$.CountReplay=1;
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.EftDate).substring(0, 10)).split("-");
		$.lastDay=new Date($.dateFormat[0], $.dateFormat[1], 0).getDate();
				
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		$.lastDayCurrent=new Date($.dateCurrent[2], $.dateCurrent[0], 0).getDate();
		if($.CountReplay<=$("#CountMonth").val())
		{
			if(eftTransactionID==value.EftTransactionID)
			{
				$.CountReplay++;
				/*
					*  Date Current YYYY-MM-DD
				*/
				$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
				/*
					* Update Fee
				*/
				AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
				$("#msg"+value.EftTransactionID).html($("#img").html());
				$.AmountCurrent=$('#amount'+eftTransactionID).html();
				$.PaymentsEPPSTrasantion[key]['change']='Y';
				$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
				$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
			}
			else
			{
				if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
				{
					$.CountReplay++;
					$.dayCurrent=$.dateCurrent[1];
					if($.lastDayCurrent==$.dayCurrent)
					{
						$.day=$.lastDay;
					}
					else
					{
						$.day=$.dayCurrent;
					}
					$("#"+value.EftTransactionID).html($.dateFormat[1]+'/'+$.day+'/'+$.dateFormat[0]);
					
					$("#msg"+value.EftTransactionID).html($("#img").html());
					/*
						*  Date Current YYYY-MM-DD
					*/
					$.CurrentDate=$.dateFormat[0]+'-'+$.dateFormat[1]+'-'+$.day;
					/*
						* Update Fee
					*/
					AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
					$.AmountCurrent=$('#amount'+eftTransactionID).html();
					$.PaymentsEPPSTrasantion[key]['change']='Y';
					$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
					$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
				}
			}
		}
	});
	SavePaymentsEPPSChangeToArray()
	CerrarPopup();
}
function AceptChangeCurrentDate(eftTransactionID)
{
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{	
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		if(eftTransactionID==value.EftTransactionID)
		{
			/*
				*  Date Current YYYY-MM-DD
			*/
			$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
			/*
				* Update Fee
			*/
			AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
			$.AmountCurrent=$('#amount'+eftTransactionID).html();
			$("#msg"+value.EftTransactionID).html($("#img").html());
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
		}
	});
	SavePaymentsEPPSChangeToArray()
	CerrarPopup();
}

function SavePaymentsEPPSChangeToArray()
{							
	$.PaymentsEPPSChanges=Array();
	$.each($.PaymentsEPPSTrasantion, function( key, value ) 
	{
		if(value.change=='Y')
		{
			$.PaymentsEPPSChanges.push({
			EftAmountOrigin:value.EftAmount,
			EftTransactionID:value.EftTransactionID,
			AccountNumber:value.AccountNumber,
			RoutingNumber:value.RoutingNumber,
			EftDate:value.changeDate,
			EftAmount:value.changeEftAmount
			})
		}
	});	
	$("#PaymentsEPPSChanges").val(JSON.stringify($.PaymentsEPPSChanges));
}
/******************************************************************
*******************************************************************
*******************************************************************/
function ShowChangeAmount(eftTransactionID)
{
	$("#amount"+eftTransactionID).attr('onclick','');
	$.amount=$("#amount"+eftTransactionID).html();

	$("#amount"+eftTransactionID).html(
	'<input id="amount2'+eftTransactionID+'" value="'+($.amount).replace("$","")+'" class="text" type="text" style="width:100px;height:20px;" />');
	$('#amount2'+eftTransactionID).focus();
	 /*
	 	* ONCHANGE
	 */
	 $('#amount2'+eftTransactionID).blur(function () {
		 	if(($.amount).replace("$","")==($('#amount2'+eftTransactionID).val()).replace('$',''))
			{
				$("#amount"+eftTransactionID).html("$"+$('#amount2'+eftTransactionID).val());
				$("#amount"+eftTransactionID).attr('onclick','ShowChangeAmount('+eftTransactionID+')');
			}
			else
			{
				var Contenido='Do you want to change all following dates?';
				Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
				MostrarPopup('Confirm',Contenido,'AceptChangeFollowingAmount('+eftTransactionID+')','AceptChangeCurrentAmount('+eftTransactionID+')','','primary','N','N','','Change all following dates','Only selection');
				$("#amount"+eftTransactionID).html("$"+$('#amount2'+eftTransactionID).val());
				$("#amount"+eftTransactionID).attr('onclick','ShowChangeAmount('+eftTransactionID+')');
				$("#CountMonth").val($.CountMonth);
			}
      });
	
}
function AceptChangeFollowingAmount(eftTransactionID)
{
	$.CountReplay=1;
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.EftDate).substring(0, 10)).split("-");
		$.lastDay=new Date($.dateFormat[0], $.dateFormat[1], 0).getDate();
				
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		$.lastDayCurrent=new Date($.dateCurrent[2], $.dateCurrent[0], 0).getDate();

		$.AmountCurrent=$('#amount'+eftTransactionID).html();
		
		
		if($.CountReplay<=$("#CountMonth").val())
		{
			if(eftTransactionID==value.EftTransactionID)
			{
				$.CountReplay++;
				$.dateCurrent2=(($("#"+value.EftTransactionID).html()).substring(0, 10)).split("/");
				$.CurrentDate=$.dateCurrent2[2]+'-'+$.dateCurrent2[0]+'-'+$.dateCurrent2[1];
			
				$.PaymentsEPPSTrasantion[key]['change']='Y';
				$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
				$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
				$("#msg"+value.EftTransactionID).html($("#img").html());
			}
			else
			{
				if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
				{
					$.CountReplay++;
					$.dateCurrent2=(($("#"+value.EftTransactionID).html()).substring(0, 10)).split("/");
					$.CurrentDate=$.dateCurrent2[2]+'-'+$.dateCurrent2[0]+'-'+$.dateCurrent2[1];
					
					if($.lastDayCurrent==$.dayCurrent)
					{
						$("#amount"+value.EftTransactionID).html($.AmountCurrent);
					}
					else
					{
						$("#amount"+value.EftTransactionID).html($.AmountCurrent);
					}
					$.PaymentsEPPSTrasantion[key]['change']='Y';
					$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
					$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
					$("#msg"+value.EftTransactionID).html($("#img").html());
				}
			}
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
}
function AceptChangeCurrentAmount(eftTransactionID)
{
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{				
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		$.AmountCurrent=$('#amount'+eftTransactionID).html();
		$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
		if(eftTransactionID==value.EftTransactionID)
		{
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
			$("#msg"+value.EftTransactionID).html($("#img").html());
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
}
/******************************************************************
*******************************************************************
*******************************************************************/
function ShowChangeAmountFee(dateFeeTransaction,FeeID,affiliateCompanyId)
{
	$("#amountFee"+FeeID).attr('onclick','');
	$.amount=$.trim($("#amountFee"+FeeID).html());

	$("#amountFee"+FeeID).html(
	'<input id="amountFee2'+FeeID+'" value="'+($.amount).replace("$","")+'" class="text" type="text" style="width:100px;height:20px;" />');
	$('#amountFee2'+FeeID).focus();
	 /*
	 	* ONCHANGE
	 */
	 $('#amountFee2'+FeeID).blur(function () {
			if(($.amount).replace("$","")==($('#amountFee2'+FeeID).val()).replace('$',''))
			{
				$("#amountFee"+FeeID).html("$"+$('#amountFee2'+FeeID).val());
				$("#amountFee"+FeeID).attr('onclick','ShowChangeAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')');
			}
			else
			{
				if(!formatCurrency($('#amountFee2'+FeeID).val()))
				{
					var Contenido='Please enter a proper amount';
					MostrarPopup('Warning',Contenido,'N','S','N','warning','N','N','','Yes','No');
					return false;
				}
				var Contenido='Do you want to change all following dates?';
				Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
				MostrarPopup('Confirm',Contenido,'AceptChangeFollowingAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')','AceptChangeCurrentAmountFee('+FeeID+','+"'"+affiliateCompanyId+"'"+')','','primary','N','N','','Change all following dates','Only selection');
				$("#amountFee"+FeeID).html("$"+$('#amountFee2'+FeeID).val());
				$("#amountFee"+FeeID).attr('onclick','ShowChangeAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')');
				$("#CountMonth").val($.CountMonth);
			}
      });	
}
function AceptChangeFollowingAmountFee(dateFeeTransaction,FeeID,affiliateCompanyId)
{
	$.CountReplay=1;
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.Fee_Date).substring(0, 10)).split("-");
		$.dateCurrent=(dateFeeTransaction).split("/");

		
		$.AmountCurrent=$('#amountFee'+FeeID).html();
		console.log(value);
		if(value.Party != 'Account Adjustment'){
			console.log(replaceAll((value.PaidToName).toLowerCase(),' ','')+'-------------'+replaceAll((affiliateCompanyId).toLowerCase(),' ',''));
			if($.CountReplay<=$("#CountMonth").val())
			{
				if(replaceAll((value.PaidToName).toLowerCase(),' ','')==replaceAll((affiliateCompanyId).toLowerCase(),' ',''))
				{
				
					if(FeeID==((value.FeeID).substring(0, 10)))
					{
						$.CountReplay++;
						$('#amountFee'+value.FeeID).html($.AmountCurrent);
							$.CommissionEPPSTrasantion[key]['change']='Y';
							$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
							if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined')
							{
								$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
							}
					}
					else
					{
						if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
						{
							$.CountReplay++;
							$('#amountFee'+value.FeeID).html($.AmountCurrent);
							$.CommissionEPPSTrasantion[key]['change']='Y';
							$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
							if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined')
							{
								$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
							}
						}
					}
				}
			}
		}
	});
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}
function AceptChangeCurrentAmountFee(FeeID,affiliateCompanyId)
{
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{		
		$.AmountCurrent=$('#amountFee'+FeeID).html();
		if(value.PaidToName==affiliateCompanyId)
		{
			if(FeeID==value.FeeID)
			{
				$('#amountFee'+value.FeeID).html($.AmountCurrent);
				$.CommissionEPPSTrasantion[key]['change']='Y';
				$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
				if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined')
				{
					$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
				}
			}
		}
	});
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}

function CurrentDate()
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	
	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = mm+'/'+dd+'/'+yyyy;
	return today;
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function formatCurrency(money) {
  var regex = /^\d+(?:\.\d{0,2})$/;
				return regex.test(money); 
}

function ChangeAccountInfo()
{
	$.Contenido='<div class="form-group">';
		$.Contenido+='<div class="form-group"> <label for="client">Account Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido+='<input id="NewAccountNumber2" onkeypress="javascript:return validarNro(event)" value="'+($.AccountNumber)+'" class="text" type="text" style="width:140px;" />';
		$.Contenido+='</div>';
		$.Contenido+='<div class="form-group"> <label for="client">Routing Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido+='<input id="NewRoutingNumber2" onkeypress="javascript:return validarNro(event)" value="'+($.RoutingNumber)+'" class="text" type="text" style="width:140px;" />';
		$.Contenido+='</div>';		
	$.Contenido+='</div>';
				MostrarPopup('Change account info',$.Contenido,'ConfirmSaveAccountInfo()','S','N','primary','N','N');
				$('.Newdate').datepicker({  
				  		format: 'mm/dd/yyyy',
	  					startDate: CurrentDateMoreTwoDays(),
	    				autoclose: true,
				 });  
}

function ConfirmSaveAccountInfo()
{
	$("#NewAccountNumber").val($("#NewAccountNumber2").val());
	$("#NewRoutingNumber").val($("#NewRoutingNumber2").val());
	
	$("#formAccountInfo").submit();	
}