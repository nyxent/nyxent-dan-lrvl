// JavaScript Document
/* POPUP BOOTSTRAP */
Comilla="'"
function MostrarPopup(Header,Contenido,FuncionAcept,FuncionCancel,Equis,TipoPopup,QuitarOpacidad,QuitarFondo,NivelPopup,NombreBotonAcept,NombreBotonCancel,Ancho)
{	
	
	var valorAncho='600px';

	if(Ancho!='' && Ancho!=undefined)
	{
		valorAncho=Ancho;
	}
	if(NivelPopup==undefined || $.isNumeric(NivelPopup)==false)
	{
		NivelPopup='';
	}
	$("#Header"+NivelPopup).removeProp("class");
	$("#Header"+NivelPopup).addClass("modal-header");
	/*
		* NOMBRE DEFAULT BOTON Accept
	*/
	$("#PopupAcept"+NivelPopup).html('Accept');
	$("#PopupCancel"+NivelPopup).html('Close');
	/*
		* NOMBRE BOTON Cancel Y Accept
	*/
	if(NombreBotonAcept!=undefined && NombreBotonAcept!="")
	{
		$("#PopupAcept"+NivelPopup).html(NombreBotonAcept);
	}
	/********************************************************/
	if(NombreBotonCancel!=undefined && NombreBotonCancel!="")
	{
		$("#PopupCancel"+NivelPopup).html(NombreBotonCancel);
	}
	$("#PopupAcept"+NivelPopup).removeProp("class");
	$("#PopupAcept"+NivelPopup).addClass("btn");
	/*********************************************************/
	switch(TipoPopup) {
		case 'primary':
			$("#Header"+NivelPopup).addClass("bg-primary");
			$("#PopupAcept"+NivelPopup).addClass("btn-primary");
			break;
		case 'wait':
			$("#Header"+NivelPopup).addClass("bg-wait");
			$("#PopupAcept"+NivelPopup).addClass("btn-wait");
			break;
		case 'success':
			$("#Header"+NivelPopup).addClass("bg-success");
			$("#PopupAcept"+NivelPopup).addClass("btn-success");
			break;
		case 'danger':
			$("#Header"+NivelPopup).addClass("bg-danger");
			$("#PopupAcept"+NivelPopup).addClass("btn-danger");
			break;
		case 'warning':
			$("#Header"+NivelPopup).addClass("bg-warning");
			$("#PopupAcept"+NivelPopup).addClass("btn-warning");
			break;
		case '':
			$("#PopupAcept"+NivelPopup).addClass("");
			break;
		default:
			$("#Header"+NivelPopup).addClass("bg-default");
	}
	$(".modal-header").removeClass("bg-danger");
	$("#PopupTitle"+NivelPopup).html(Header);
	$("#PopupBody"+NivelPopup).html(Contenido);
	if(FuncionAcept=='N')
	{
		$("#PopupAcept"+NivelPopup).hide();
	}
	else
	{
		$("#PopupAcept"+NivelPopup).show();
		$("#PopupAcept"+NivelPopup).attr('onclick',FuncionAcept);	
	}
	if(FuncionCancel=='N')
	{
		$("#PopupCancel"+NivelPopup).hide();
	}
	else
	{
		if(FuncionCancel!='S')
		{
			$("#PopupCancel"+NivelPopup).attr('onclick',FuncionCancel);	
		}
		$("#PopupCancel"+NivelPopup).show();
	}
	if(Equis=='N')
	{
		$("#Equis"+NivelPopup).hide();	
	}
	$("#Popup"+NivelPopup).modal('show');
	$("#Popup"+NivelPopup+" .modal-dialog").width(valorAncho);
	$("#Popup"+NivelPopup+" .modal-body").css({'max-height':'calc(100vh - 210px)','overflow-y':'auto'});
	if(QuitarOpacidad!=undefined && QuitarOpacidad=='S')
	{
		$(".modal-backdrop").css("opacity",'0');
	}
	if(QuitarFondo!=undefined && QuitarFondo=='S')
	{
		$(".modal-content").css('background-color','transparent');
		$(".modal-header").css('border-bottom','none');
		$(".modal-footer").css('border-top','none');
		$(".modal-content").css('box-shadow','none');
	}	
	else
	{
		$(".modal-content").css('background-color','#FFFFFF');
	}
}
function CerrarPopup(NivelPopup)
{
	if(NivelPopup==undefined)
	{
		NivelPopup='';
	}
	$("#Popup"+NivelPopup).modal('hide');
}

/*
	* MOSTRAR LOADING
*/
function ShowLoading()
{
	var Contenido='<div id="over_loader" class="overbox_loader">'+
						'<div class="content_loader" id="content_loader">'+
							'<div id="imagen_loader">'+
								'<div  class="loader">'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div><br><br>	';
	MostrarPopup('',Contenido,'N','N','N','','N','S');
	
}
/*
	* CLEAR FILEDS
*/
function clearFields()
{
	$("#date1").val('');
	$("#date2").val('');
	$(".selectpicker").val('default');
	$('.selectpicker').selectpicker('refresh');
}
/*FUNCION GENERAL PARA ENVIAR PETICIONES AJAX */
function EnviarPeticion(datos,decidir)
{

	 /* FUNCCION GENERAL QUE VALIDA CAMPOS REQUERIDOS POR SI HTML 5 FALLA */
		 /**************/
		console.log( 'Enviando peticion' );
		 $.BandPeticion=1;
		$.ajax({
			type : 'POST',
			url : $.Url,
			dataType : 'json',
			data: datos,
			/*async:false,*/
			success : function(data)
			{	
				 /************/
				$.BandPeticion=0;
				/*
					* IMPRIME MENSAJE QUE DEVUELVE EL SERVIDOR
				*/
				if(data!=null)
				{
					if(data.Mensaje!=undefined)
					{
						alert(data.Mensaje);
					}
					if(data.Error!=undefined && data.Error==1)
					{
						return false;
					}
				}
				
				if(decidir!="")
				{
					 DecidirAccion(data,decidir);	
				}
			},
			complete: function( ){
				$.BandPeticion=0;
				console.log( 'Termino petici��n' );
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				 $.BandPeticion=0;
				alert('Error');
			}
		});	
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function formatCurrency(money) {
   var regex = /^[1-9]{0,2}(,{0,1})(\d{2},)*(\d{3})*(?:\.\d{0,2})$/;
	return regex.test(money); 
}

function CurrentDate2()
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	
	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = mm+'-'+dd+'-'+yyyy;
	return today;
}

function CurrentDateMoreTwoDays()
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	dd=dd+3;
	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = mm+'/'+dd+'/'+yyyy;
	return today;
}
function validarNro(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
  return false;
}
function OrderBy(field)
{
	$("#orderby").val(field);
	$("#formFilters").submit();
}
function LoadOrderby(field)
{
	if(field.search("Desc")!=-1)
	{
		$("#"+field).attr('onclick',"OrderBy('"+field.replace("Desc","")+"')");
	}
	else
	{
		$("#"+field).attr('onclick',"OrderBy('"+field+"Desc')");
	}
}
function selectElementContents(el) 
{
	var body = document.body, range, sel;
	if (document.createRange && window.getSelection) {
		range = document.createRange();
		sel = window.getSelection();
		sel.removeAllRanges();
		try {
			range.selectNodeContents(el);
			sel.addRange(range);
		} catch (e) {
			range.selectNode(el);
			sel.addRange(range);
		}
	} else if (body.createTextRange) {
		range = body.createTextRange();
		range.moveToElementText(el);
		range.select();
		range.execCommand("Copy");
	}
	 var ok = document.execCommand('copy');

     if (ok)
	 {
		 MostrarPopup('Copy','Copy Success','N','S','S','primary','N','N');
	 }
	 else
	 {
		  MostrarPopup('Copy','Unable to copy! Please copy with secondary click of your mouse.','N','S','S','primary','N','N');
	 }
}