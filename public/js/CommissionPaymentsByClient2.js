function ReactivateUser(eftDate){
	console.log(eftDate);
	$.date=eftDate;
	$("#reactivateDate").show();
	$('#reactivateDate').datepicker({  
			format: 'mm/dd/yyyy',
	   	startDate: CurrentDate(),
	    autoclose: true,
     });  
	 $('#reactivateDate').focus();
	 
		/*
		* ONCHANGE
		*/
		$('#reactivateDate').change(function () {
				var Contenido='Do you want to reactivate this client?';
				MostrarPopup('Confirm',Contenido,'AcceptReactiveUser("'+eftDate+'","'+$('#reactivateDate').val()+'")','','','primary','N','N','','Reactivate!','');
		});
}
/****************************************************/


function AcceptReactiveUser(eftDate,newDate){
	console.log(eftDate);
	$.Url = 'reactivateUser';
	datos = {
		date: eftDate,
		new: newDate,
		NewAccountNumber: $.AccountNumber,
		NewRoutingNumber: $.RoutingNumber,
		AffiliateCompanyName: $.affiliateCompanyName,
		_token: $('input[name="_token"]').val()
	}
	ShowLoading();
	setTimeout(function () {ShowLoading();}, 500);
	ShowLoading();
	$.ajax({
		type : 'GET',
		url : $.Url,
		dataType : 'json',
		data:datos,
		/*async:false, */
		success : function(data)
		{	
			window.location = data.Save + '?searchClient=' + data.idfile;
		},
		complete: function( ){
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			$.BandPeticion=0;
			alert('Error');
		}
	});
}

/****************************************************/


function ReactivateDate(eftTransactionID)
{
	$.date=$("#"+eftTransactionID).html();
	$("#reactivateDate").show();
	$('#reactivateDate').datepicker({  
			format: 'mm/dd/yyyy',
	   	startDate: CurrentDate(),
	    autoclose: true,
     });  
	 $('#reactivateDate').focus();
	 
	 /*
	 	* ONCHANGE
	 */
	 $('#reactivateDate').change(function () {
		 	
			if($.date==$('#reactivateDate').val())
			{
				$("#"+eftTransactionID).html($('#reactivateDate').val());
				$("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
			}
			else
			{
				var Contenido='Do you want to reactivate this client?';
				Contenido+='<input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="hidden" style="width:140px;" />';
				$.Reactive =1;
				MostrarPopup('Confirm',Contenido,'AcceptReactivateAll('+eftTransactionID+')','','','primary','N','N','','Reactivate!','');
				$("#"+eftTransactionID).html($('#reactivateDate').val());
				$("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
				$("#CountMonth").val($.CountMonth);
			}
      });
	 /* $('#date1'+eftTransactionID).blur(function () { 
		  $("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
		  $("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
	   })*/
	  
	
}


/******************************************************** */


function AcceptReactivateAll(eftTransactionID){
	var currentDate = new Date($("#"+eftTransactionID).html());
	var count = 0;
	$.each($.PaymentsEPPSTrasantion,function(key,value){
		if(value.StatusCode == 'Create EFT Pending'){
			console.log(count);
			newDate = new Date(currentDate).addMonths(count);
			console.log(newDate);
			var month = ("0" + (newDate.getMonth() + 1)).slice(-2);
			var day = ("0" + (newDate.getDate())).slice(-2);
			$.CurrentDate = newDate.getFullYear()+'-'+month+'-'+day;
			$.CurrentDateHTML = month+'/'+day+'/'+newDate.getFullYear();
			console.log($.CurrentDate);
			AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=value.EftAmount;
			$("#"+value.EftTransactionID).html($.CurrentDateHTML);
			$("#msg"+value.EftTransactionID).html($("#img").html());
			count++;
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
	$('#NewNote').val('Flow Changes to Reactive');
	$('#Reactive').val(1);
	$.Enviar = 'S';
	ShowLoading();
	setTimeout(function () {ShowLoading();}, 500);
	ShowLoading();
	$("#FormConfirmSaveChange").submit();
}

/**************************************** */

function ShowChangeDateFee(dateFeeTransaction)
{
	console.log($('#fee-'+dateFeeTransaction));
	$('#fee-'+dateFeeTransaction).attr('onclick','');
	$.date=$('#fee-'+dateFeeTransaction).html();
	$('#fee-'+dateFeeTransaction).html('<input id="dateFee'+dateFeeTransaction+'" value="'+$.date+'" readonly="readonly" class="date" type="text" style="width:100px;height:20px;" />');
	$('#dateFee'+dateFeeTransaction).datepicker({  
		format: 'mm/dd/yyyy',
		startDate: CurrentDate(),
		autoclose: true,
	});  
	$('#dateFee'+dateFeeTransaction).focus();

	/** ONCHANGE */
	$('#dateFee'+dateFeeTransaction).change(function () {		 	
		if($.date==$('#dateFee'+dateFeeTransaction).val()){
			$('#fee-'+dateFeeTransaction).html($('#dateFee'+dateFeeTransaction).val());
			$('#fee-'+dateFeeTransaction).attr('onclick','ShowChangeDateFee('+"'"+dateFeeTransaction+"'"+')');
		}
		else{
			var Contenido='Do you want to change all following dates?';
			MostrarPopup('Confirm',Contenido,'AceptChangeFollowingDateFee('+"'"+dateFeeTransaction+"'"+')','AceptChangeCurrentDateFee('+"'"+dateFeeTransaction+"'"+')','','primary','N','N','','Change all following dates','Only selection');
			$('#fee-'+dateFeeTransaction).html($('#dateFee'+dateFeeTransaction).val());
			$('#fee-'+dateFeeTransaction).attr('onclick','ShowChangeDateFee('+"'"+dateFeeTransaction+"'"+')');		
		}		
	});
}

/******************************************************* */

function AceptChangeFollowingDateFee(dateFeeTransaction){
	var currentDate = new Date($('#fee-'+dateFeeTransaction).html());
	var count = 0;
	var actualDate = dateFeeTransaction;
	lastDate = actualDate;
	$.each($.CommissionEPPSTrasantion,function(key,value){
		var feeDate =value.Fee_Date.substring(0,10)
		if(value.StatusCode=='Pending'&&  feeDate >= actualDate){
			if(feeDate != lastDate){
				count++;
				lastDate = feeDate;
			}
			newDate = new Date(currentDate).addMonths(count);
			var month = ("0" + (newDate.getMonth() + 1)).slice(-2);
			var day = ("0" + (newDate.getDate())).slice(-2);
			$.CurrentDate = newDate.getFullYear()+'-'+month+'-'+day;
			$.CurrentDateHTML = month+'/'+day+'/'+newDate.getFullYear();

			$.CommissionEPPSTrasantion[key]['change']='Y';
			$.CommissionEPPSTrasantion[key]['changeDate']=$.CurrentDate;

			$("#fee-"+feeDate).html($.CurrentDateHTML);
			$("#msg"+feeDate).html($("#img").html());
			console.log($.CurrentDate);
			console.log(newDate);
		}
	});
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}
/************************************** */

function AceptChangeCurrentDateFee(dateFeeTransaction){
	console.log(dateFeeTransaction);
	console.log('ENTRA FEE');
	var newDate=(($('#fee-'+dateFeeTransaction).html()).substring(0, 10)).split("/");
	console.log(newDate);
	var CurrentDate=newDate[2]+'-'+newDate[0]+'-'+newDate[1];
	$.each($.CommissionEPPSTrasantion, function( key, value ){				
		if(dateFeeTransaction==((value.Fee_Date).substring(0, 10))){
			$("#msg"+dateFeeTransaction).html($("#img").html());
			$.CommissionEPPSTrasantion[key]['change']='Y';
			$.CommissionEPPSTrasantion[key]['changeDate']=CurrentDate;
		}
	});
	SaveCommisionEPPSChangeToArray();
}
/******************************************* */

function SaveCommisionEPPSChangeToArray(){
	$.CommissionEPPSChanges=Array();
	$.each($.CommissionEPPSTrasantion, function( key, value ){
		if(value.change=='Y'){
			$.CommissionEPPSChanges.push({
				FeeID:value.FeeID,
				FeeAmountOrigin:($.isEmptyObject(value.FeeAmount)==true ? "" :value.FeeAmountOrigin),
				FeeDateOrigin:($.isEmptyObject(value.FeeAmount)==true ? "" :value.Fee_Date),
				FeeDate:($.isEmptyObject(value.changeDate)==true ? "" : value.changeDate),
				FeeAmount:($.isEmptyObject(value.FeeAmount)==true ? "" :value.FeeAmount),
				Description:($.isEmptyObject(value.Description)==true ? "" : value.Description),
				FeeType:($.isEmptyObject(value.FeeType)==true ? "" : value.FeeType),
				PaidToName:($.isEmptyObject( value.PaidToName)==true ? "" : value.PaidToName),
				PaidToPhone:($.isEmptyObject( value.PaidToPhone)==true ? "" : value.PaidToPhone),
				PaidToStreet:($.isEmptyObject( value.PaidToStreet)==true ? "" : value.PaidToStreet),
				PaidToStreet2:($.isEmptyObject(value.PaidToStreet2)==true ? "" : value.PaidToStreet2),
				PaidToCity:($.isEmptyObject(value.PaidToCity)==true ? "" : value.PaidToCity),
				PaidToState:($.isEmptyObject(value.PaidToState)==true ? "" : value.PaidToState),
				PaidToZip:($.isEmptyObject(value.PaidToZip)==true ? "" : value.PaidToZip),
				PaidToCustomerNumber:'',
				ContactName:''
			});	
		}
	});
	$("#CommissionEPPSChanges").val(JSON.stringify($.CommissionEPPSChanges));	
}


/******************************************************************
*******************************************************************
*******************************************************************/


function ShowChangeDate(eftTransactionID){
  $("#"+eftTransactionID).attr('onclick','');
  $.date=$("#"+eftTransactionID).html();
  $("#"+eftTransactionID).html('<input id="date1'+eftTransactionID+'" value="'+$.date+'"  readonly="readonly" class="date" type="text" style="width:100px;height:20px;" />');
  $('#date1'+eftTransactionID).datepicker({  
    format: 'mm/dd/yyyy',
    startDate: CurrentDate(),
    autoclose: true});

  $('#date1'+eftTransactionID).focus();
  
  /** ON CHANGE */
  $('#date1'+eftTransactionID).change(function () {
    if($.date==$('#date1'+eftTransactionID).val()){
      $("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
      $("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
    }
    else{
      var Contenido='Do you want to change all following dates?';
      // Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
      MostrarPopup('Confirm',Contenido,'AcceptChangeFollowingDate('+eftTransactionID+')','AceptChangeCurrentDate('+eftTransactionID+')','','primary','N','N','','Change all following dates','Only selection');
      $("#"+eftTransactionID).html($('#date1'+eftTransactionID).val());
      $("#"+eftTransactionID).attr('onclick','ShowChangeDate('+eftTransactionID+')');
      $("#CountMonth").val($.CountMonth);
    }
  });	
}

/***************************************************** */

function AcceptChangeFollowingDate(eftTransactionID){
	console.log('ENTRA POR AQUI');
	var currentDate = new Date($("#"+eftTransactionID).html());
  var count = 0;
  var actualDate = '';
  $.each($.PaymentsEPPSTrasantion,function(key,value){
    if(value.EftTransactionID == eftTransactionID){
      actualDate = value.EftDate;
    }
  });
	$.each($.PaymentsEPPSTrasantion,function(key,value){
		if(value.StatusCode == 'Create EFT Pending' && value.EftDate >= actualDate){
			console.log(count);
			newDate = new Date(currentDate).addMonths(count);
			console.log(newDate);
			var month = ("0" + (newDate.getMonth() + 1)).slice(-2);
			var day = ("0" + (newDate.getDate())).slice(-2);
			$.CurrentDate = newDate.getFullYear()+'-'+month+'-'+day;
			$.CurrentDateHTML = month+'/'+day+'/'+newDate.getFullYear();
			console.log($.CurrentDate);
			console.log('PASA AQUI');
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=value.EftAmount;
			$("#"+value.EftTransactionID).html($.CurrentDateHTML);
			$("#"+value.EftTransactionID).closest('tr').next(".com").find(".row-date").html($.CurrentDateHTML);
			AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
			$("#msg"+value.EftTransactionID).html($("#img").html());
			count++;
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
}

/********************************************************** */

function AceptChangeCurrentDate(eftTransactionID){
	$.each( $.PaymentsEPPSTrasantion, function( key, value ){	
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		if(eftTransactionID==value.EftTransactionID)
		{
			/*
				*  Date Current YYYY-MM-DD
			*/
			$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
			/*
				* Update Fee
			*/
			console.log('ENTRA');console.log($("#"+eftTransactionID).html());
			$('#fee-'+(value.EftDate).substring(0,10)).html($("#"+eftTransactionID).html());
			AceptChangeCurrentDateFee((value.EftDate).substring(0,10),$.CurrentDate);
			
			$.AmountCurrent=$('#amount'+eftTransactionID).html();
			$("#msg"+value.EftTransactionID).html($("#img").html());
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
		}
	});
	SavePaymentsEPPSChangeToArray()
	CerrarPopup();
}

function SavePaymentsEPPSChangeToArray()
{							
	$.PaymentsEPPSChanges=Array();
	$.each($.PaymentsEPPSTrasantion, function( key, value ) 
	{
		if(value.change=='Y')
		{
			$.PaymentsEPPSChanges.push({
			EftAmountOrigin:value.EftAmount,
			EftTransactionID:value.EftTransactionID,
			AccountNumber:value.AccountNumber,
			RoutingNumber:value.RoutingNumber,
			EftDate:value.changeDate,
			EftAmount:value.changeEftAmount
			})
		}
	});	
	$("#PaymentsEPPSChanges").val(JSON.stringify($.PaymentsEPPSChanges));
}
/******************************************************************
*******************************************************************
*******************************************************************/
function ShowChangeAmount(eftTransactionID)
{
	$("#amount"+eftTransactionID).attr('onclick','');
	$.amount=$("#amount"+eftTransactionID).html();

	$("#amount"+eftTransactionID).html(
	'<input id="amount2'+eftTransactionID+'" value="'+($.amount).replace("$","")+'" class="text" type="text" style="width:100px;height:20px;" />');
	$('#amount2'+eftTransactionID).focus();
	 /*
	 	* ONCHANGE
	 */
	 $('#amount2'+eftTransactionID).blur(function () {
		 	if(($.amount).replace("$","")==($('#amount2'+eftTransactionID).val()).replace('$',''))
			{
				$("#amount"+eftTransactionID).html("$"+$('#amount2'+eftTransactionID).val());
				$("#amount"+eftTransactionID).attr('onclick','ShowChangeAmount('+eftTransactionID+')');
			}
			else
			{
				var Contenido='Do you want to change all following dates?';
				Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
				MostrarPopup('Confirm',Contenido,'AceptChangeFollowingAmount('+eftTransactionID+')','AceptChangeCurrentAmount('+eftTransactionID+')','','primary','N','N','','Change all following dates','Only selection');
				$("#amount"+eftTransactionID).html("$"+$('#amount2'+eftTransactionID).val());
				$("#amount"+eftTransactionID).attr('onclick','ShowChangeAmount('+eftTransactionID+')');
				$("#CountMonth").val($.CountMonth);
			}
      });
	
}
function AceptChangeFollowingAmount(eftTransactionID)
{
	$.CountReplay=1;
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.EftDate).substring(0, 10)).split("-");
		$.lastDay=new Date($.dateFormat[0], $.dateFormat[1], 0).getDate();
				
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		console.log($.dateCurrent);
		$.lastDayCurrent=new Date($.dateCurrent[2], $.dateCurrent[0], 0).getDate();

		$.AmountCurrent=$('#amount'+eftTransactionID).html();
		
		if($.CountReplay<=$("#CountMonth").val())
		{

			if(eftTransactionID==value.EftTransactionID)
			{
				$.CountReplay++;
				$.dateCurrent2=(($("#"+value.EftTransactionID).html()).substring(0, 10)).split("/");
				$.CurrentDate=$.dateCurrent2[2]+'-'+$.dateCurrent2[0]+'-'+$.dateCurrent2[1];
			
				$.PaymentsEPPSTrasantion[key]['change']='Y';
				$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
				$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
				$("#msg"+value.EftTransactionID).html($("#img").html());
			}
			else
			{
				if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
				{
					$.CountReplay++;
					$.dateCurrent2=(($("#"+value.EftTransactionID).html()).substring(0, 10)).split("/");
					$.CurrentDate=$.dateCurrent2[2]+'-'+$.dateCurrent2[0]+'-'+$.dateCurrent2[1];
					
					if($.lastDayCurrent==$.dayCurrent)
					{
						$("#amount"+value.EftTransactionID).html($.AmountCurrent);
					}
					else
					{
						$("#amount"+value.EftTransactionID).html($.AmountCurrent);
					}
					$.PaymentsEPPSTrasantion[key]['change']='Y';
					$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
					$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
					$("#msg"+value.EftTransactionID).html($("#img").html());
				}
			}
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
}
function AceptChangeCurrentAmount(eftTransactionID)
{
	$.each( $.PaymentsEPPSTrasantion, function( key, value ) 
	{				
		$.dateCurrent=(($("#"+eftTransactionID).html()).substring(0, 10)).split("/");
		$.AmountCurrent=$('#amount'+eftTransactionID).html();
		$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
		if(eftTransactionID==value.EftTransactionID)
		{
			$.PaymentsEPPSTrasantion[key]['change']='Y';
			$.PaymentsEPPSTrasantion[key]['changeDate']=$.CurrentDate;
			$.PaymentsEPPSTrasantion[key]['changeEftAmount']=$.AmountCurrent;
			$("#msg"+value.EftTransactionID).html($("#img").html());
		}
	});
	SavePaymentsEPPSChangeToArray();
	CerrarPopup();
}
/******************************************************************
*******************************************************************
*******************************************************************/
function ShowChangeAmountFee(dateFeeTransaction,FeeID,affiliateCompanyId)
{
	$("#amountFee"+FeeID).attr('onclick','');
	$.amount=$.trim($("#amountFee"+FeeID).html());

	$("#amountFee"+FeeID).html(
	'<input id="amountFee2'+FeeID+'" value="'+($.amount).replace("$","")+'" class="text" type="text" style="width:100px;height:20px;" />');
	$('#amountFee2'+FeeID).focus();
	 /*
	 	* ONCHANGE
	 */
	 $('#amountFee2'+FeeID).blur(function () {
			if(($.amount).replace("$","")==($('#amountFee2'+FeeID).val()).replace('$','')){
				$("#amountFee"+FeeID).html("$"+$('#amountFee2'+FeeID).val());
				$("#amountFee"+FeeID).attr('onclick','ShowChangeAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')');
			}
			else{
				if(!formatCurrency($('#amountFee2'+FeeID).val())){
					var Contenido='Please enter a proper amount';
					MostrarPopup('Warning',Contenido,'N','S','N','warning','N','N','','Yes','No');
					return false;
				}
				var Contenido='Do you want to change all following dates?';
				Contenido+='<br><br>Count month? <input id="CountMonth" onkeypress="javascript:return validarNro(event)"  class="text" type="text" style="width:140px;" />';
				MostrarPopup('Confirm',Contenido,'AceptChangeFollowingAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')','AceptChangeCurrentAmountFee('+FeeID+','+"'"+affiliateCompanyId+"'"+')','','primary','N','N','','Change all following dates','Only selection');
				$("#amountFee"+FeeID).html("$"+$('#amountFee2'+FeeID).val());
				$("#amountFee"+FeeID).attr('onclick','ShowChangeAmountFee('+"'"+dateFeeTransaction+"'"+','+FeeID+','+"'"+affiliateCompanyId+"'"+')');
				$("#CountMonth").val($.CountMonth);
			}
		});	
}
function AceptChangeFollowingAmountFee(dateFeeTransaction,FeeID,affiliateCompanyId)
{
	$.CountReplay=1;
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{
		$.dateFormat=((value.Fee_Date).substring(0, 10)).split("-");
		$.dateCurrent=(dateFeeTransaction).split("/");

		
		$.AmountCurrent=$('#amountFee'+FeeID).html();
		console.log(value);
		if(value.Party != 'Account Adjustment'){
			console.log(replaceAll((value.PaidToName).toLowerCase(),' ','')+'-------------'+replaceAll((affiliateCompanyId).toLowerCase(),' ',''));
			if($.CountReplay<=$("#CountMonth").val())
			{
				if(replaceAll((value.PaidToName).toLowerCase(),' ','')==replaceAll((affiliateCompanyId).toLowerCase(),' ',''))
				{
				
					if(FeeID==((value.FeeID).substring(0, 10)))
					{
						$.CountReplay++;
						$('#amountFee'+value.FeeID).html($.AmountCurrent);
							$.CommissionEPPSTrasantion[key]['change']='Y';
							$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
							if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined')
							{
								$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
							}
					}
					else
					{
						if(parseInt($.dateFormat[0]+''+$.dateFormat[1]+''+$.dateFormat[2])>=parseInt($.dateCurrent[2]+''+$.dateCurrent[0]+''+$.dateCurrent[1]))
						{
							$.CountReplay++;
							$('#amountFee'+value.FeeID).html($.AmountCurrent);
							$.CommissionEPPSTrasantion[key]['change']='Y';
							$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
							if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined')
							{
								$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
							}
						}
					}
				}
			}
		}
	});
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}
function AceptChangeCurrentAmountFee(FeeID,affiliateCompanyId){
	console.log(FeeID);
	$.AmountCurrent=$('#amountFee'+FeeID).html();
	$.each($.CommissionEPPSTrasantion, function( key, value ){
		if(FeeID==value.FeeID){
			$('#amountFee'+value.FeeID).html($.AmountCurrent);
			$.CommissionEPPSTrasantion[key]['change']='Y';
			$.CommissionEPPSTrasantion[key]['FeeAmount']=$.AmountCurrent;
			if(typeof $.CommissionEPPSTrasantion[key]['changeDate'] == 'undefined'){
				$.CommissionEPPSTrasantion[key]['changeDate']=((value.Fee_Date).substring(0, 10));
			}
		}
	});
	console.log($.CommissionEPPSTrasantion);
	SaveCommisionEPPSChangeToArray();
	CerrarPopup();
}

function CurrentDate()
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	
	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = mm+'/'+dd+'/'+yyyy;
	return today;
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function formatCurrency(money) {
  var regex = /^\d+(?:\.\d{0,2})$/;
				return regex.test(money); 
}

function ChangeAccountInfo()
{
	$.Contenido='<div class="form-group">';
		$.Contenido+='<div class="form-group"> <label for="client">Account Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido+='<input id="NewAccountNumber2" onkeypress="javascript:return validarNro(event)" value="'+($.AccountNumber)+'" class="text" type="text" style="width:140px;" />';
		$.Contenido+='</div>';
		$.Contenido+='<div class="form-group"> <label for="client">Routing Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido+='<input id="NewRoutingNumber2" onkeypress="javascript:return validarNro(event)" value="'+($.RoutingNumber)+'" class="text" type="text" style="width:140px;" />';
		$.Contenido+='</div>';		
	$.Contenido+='</div>';
				MostrarPopup('Change account info',$.Contenido,'ConfirmSaveAccountInfo()','S','N','primary','N','N');
				$('.Newdate').datepicker({  
				  		format: 'mm/dd/yyyy',
	  					startDate: CurrentDateMoreTwoDays(),
	    				autoclose: true,
				 });  
}

function ConfirmSaveAccountInfo()
{
	$("#NewAccountNumber").val($("#NewAccountNumber2").val());
	$("#NewRoutingNumber").val($("#NewRoutingNumber2").val());
	
	$("#formAccountInfo").submit();	
}