$(document).ready(function () {
    initDataRepair();
});

function initDataRepair() {
    $('.datePicker').datepicker({format: 'mm/dd/yyyy', autoclose: true, });
    $('.selectClient').click(function (e) {
        e.preventDefault();
        window.top.location.href = $(this).attr('href');
    });

    $('.check').change(function () {
        if ($(this).is(":checked")) {
            var check = 'NO';
        }
        else {
            var check = 'YES';
        }
        $.ajax({
            type: "GET",
            url: _DAN_URL + "/components/file.cfc",
            data: {
                method: "checkClient",
                idFile: $(this).attr('id'),
                checked: check
            },
            dataType: "json",
            success: function (data) {

            }
        });
    });
}

function sortTable(sortField, sortOrder) {
    $('#sortField').val(sortField);
    $('#sortOrder').val(sortOrder);
    $('#reportDwn').submit();
}

function editCSDate(idFile) {

    var prevDate = $('#csDateSel_' + idFile).html().replace('--','').trim();

    $('#csDate_' + idFile).val(prevDate);

    $('#divShowCSDate_' + idFile).hide();
    $('#divEditCSDate_' + idFile).show();
    return false;
}

function clearCSDate(idFile, url) {
    $('#csDate_' + idFile).val('');
    saveCSDate(idFile,url);
}
function saveCSDate(idFile, url) {

    

    $('#csDateSel_' + idFile).html('saving...');
    $.ajax({
        type: "post",
        url: url,
        data: {
            idFile: idFile,
            csDate: $('#csDate_' + idFile).val()
        },
        success: function (data) {
            $('#csDateSel_' + idFile).html(data);
            console.log('data: ' + data);
        }
    });

    $('#divEditCSDate_' + idFile).hide();
    $('#divShowCSDate_' + idFile).show();
    return false;
}