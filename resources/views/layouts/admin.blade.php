<!DOCTYPE html>
<html>
  <head>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
     <!--<link rel="stylesheet" href="{{asset('css/select2-bootstrap.css')}}"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
     <!-- datepicker style -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    
  
    <link rel="stylesheet" href="{{asset('css/plugin/datatables/datatables.bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/plugin/datatables/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/plugin/datatables/dataTables.bootstrap.min.css')}}">
    
    <!-- Custom style -->
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}">

    
    
    
      <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/numeral.js')}}"></script>
    
    <script src="{{asset('js/modernizr-custom.js')}}"></script>
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
        @stack('scripts')
    <!--<script src="{{asset('js/table.js')}}"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/app.min.js')}}"></script>
    <!-- Datepicker-->
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
   
    <script src="{{asset('js/functionGeneral.js?ver=11.0.1')}}"></script>    
    <script src="{{asset('js/utils.js')}}"></script>    

    <script>
        var _DAN_URL = '{{env('DAN_URL')}}';
        var _LOCAL_URL = '{{url('/')}}';
    </script>
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse"> 
  			 <!--POPUP BOOTSTRAP-->
            <!-- Modal -->
            <div class="modal fade" id="Popup" tabindex="-1" role="dialog" aria-labelledby="Popup" data-backdrop="static" data-keyboard="false">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div id="Header" class="modal-header">
            
                    <button id="Equis" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
                    </button>
                    <h4 class="modal-title" id="PopupTitle"></h4>
                  </div>
                  <div id="PopupBody" class="modal-body">
                  </div>
                  <div class="modal-footer">
                    <button id="PopupAcept" type="button" class="btn btn-primary">Acepta</button>
                    <button id="PopupCancel" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- POPUP BOOTSTRAP -->
            <!--
            <div class="bg-primary">...</div>
            <div class="bg-success">...</div>
            <div class="bg-info">...</div>
            <div class="bg-warning">...</div>
            <div class="bg-danger">...</div>-->
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                              <!--Contenido-->
                              @yield('contenido') 
                              <!--Fin Contenido-->
                           </div>
                        </div>
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
      
      <!--Fin-Contenido-->
  
   
  	<script type="text/javascript">
    $('.date').datepicker({  
       format: 'mm-dd-yyyy'
     });  
	</script>  
  </body>
</html>
