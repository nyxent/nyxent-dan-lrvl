<div class="col-md-6 col-sm-6 col-xs-6">
<div class="form-group">
	<div class="input-group">
        <select id="searchClient" class="selectpicker" name="searchClient" data-live-search="true" title="please select client" onchange="">
          @foreach ($client as $item)
                <option value="{{ $item->idFile }}">{{ $item->client2 }}</option>
            @endforeach
     	 </select>
	</div>
</div>
</div>

<div class="col-md-6 col-sm-6 col-xs-6">
<div class="form-group">
	<div class="input-group">
        <select id="searchText" class="selectpicker" name="searchText" data-live-search="true" title="please select a company" onchange="">
          @foreach ($company as $compan)
                <option value="{{ $compan->affiliateCompanyId }}">{{ $compan->affiliateCompanyName }}</option>
            @endforeach
     	 </select>
	</div>
</div>
</div>