<div class="col-md-12">
<div class="form-group">
	<div class="input-group">
        <select  id="searchClient" class="selectpicker"   name="searchClient" data-live-search="true" title="please select client" onchange="">
          @if(! empty($client))
              @foreach ($client as $item)
                    <option value="{{ $item->idFile }}">{{ $item->client2 }}</option>
              @endforeach
           @endif 
     	 </select>
	</div>
</div>
</div>