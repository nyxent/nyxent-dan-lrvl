@extends ('layouts.admin')

@section ('contenido')
<div class="container">
  
  <nav class="navbar">
    
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row top-buffer">
      <form method="POST" action="{{ url('creditRepairDocumentSave') }}" enctype="multipart/form-data" accept-charset="UTF-8" id="reportDwn" class="form-inline my-2 my-lg-0 float-right">
        {{ csrf_field() }}
        <input type="hidden" name="idFile" value="{{Session::get('idFile')}}">
        <div class="form-group">
          <label for="file">File:</label>
          <input type="file" class="form-control"  placeholder="" value="" name="files[]" multiple required>         
        </div>
        <div class="form-group">
          <label for="name">Name:</label>
          <input type="text" class="form-control"  placeholder="" value="" name="name" required>         
        </div>
        <button type="submit" class="btn btn-success">Upload</button>
      </form>
    </div>
    
    <div class="row top-buffer">
      <table id="faq-table" class="table table-bordered">
        <thead>
          <tr>
            <th>Document</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php $date = \Carbon\Carbon::now()->subYears(2)->startOfDay();?>
          @foreach($documents as $document)
          <tr>
            <td><a href="../creditRepairDocuments/{{$document->idFile.'/'.$document->fileName}}" target="_blank">{{ $document->name }}</a></td>
            <td>{{date('m/d/Y',strtotime($document->created_at))  }}</td>     
            <td><a class="btn btn-small btn-warning" href="{{ URL::to('creditRepairDelete/' . $document->idDocument)}}">Remove</a> </td>          
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>    
  </div>
  @endsection