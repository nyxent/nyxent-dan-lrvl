<table>
  <thead>
    <tr>
      <th>#</th>
      <th>Select</th>
      <th>File</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Approved Date</th>
      <th>Enrollment Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($clients as $client)
    <tr>
      <td>{{ $loop->iteration }}</td>
      <td>{{$client->checked}}</td>
      <td>{{ $client->idFile }}</td>
      <td>{{ $client->firstName }}</td>
      <td>{{ $client->lastName }}</td>                   
      <td>{{ $client->enrollmentApprovedDate }}</td>          
      <td>{{ $client->statusName }}</td>          
    </tr>
    @endforeach
  </tbody>
</table>