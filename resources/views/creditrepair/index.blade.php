@extends ('layouts.admin')

@section ('contenido')
<script src="{{asset('js/creditRepair.js')}}" type="text/javascript"></script>
<div class="container">
    <form method="GET" action="{{ url('creditRepair') }}" accept-charset="UTF-8" id="reportDwn" class="paginatedForm form-inline my-2 my-lg-0 float-right">
        

            <!-- will be used to show any messages -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="row top-buffer">

                <input type="hidden" id="sortField" name="sortField" value="{{ request('sortField') }}">
                <input type="hidden" id="sortOrder" name="sortOrder" value="{{ request('sortOrder') }}">
                <div class="form-group">
                    <label for="from">Keyword:</label>
                    <input type="text" class="form-control"  placeholder="" value="{{ request('keyword') }}" name="keyword">
                </div>
                <div class="form-group">
                    <label for="checked">Checked:</label>
                    <select class="form-control" name="checked" id="checked">
                        <option value="" <?php echo (request('checked') == '') ? 'selected' : ''; ?>>All</option>
                        <option value="1" <?php echo (request('checked') == '1') ? 'selected' : ''; ?>>Checked</option>
                        <option value="2" <?php echo (request('checked') == '2') ? 'selected' : ''; ?>>Uncheked</option>
                        <option value="3" <?php echo (request('checked') == '3') ? 'selected' : ''; ?>>Assigned</option>
                    </select>     
                </div>
                <div class="form-group">
                    <label for="from">From:</label>
                    <input type="text" class="form-control datePicker"  placeholder="" value="{{ request('from') }}" name="from">         
                </div>
                <div class="form-group">
                    <label for="to">To:</label>
                    <input type="text" class="form-control datePicker"  placeholder="" value="{{ request('to') }}" name="to">         
                </div>
                <button type="submit" class="btn btn-success"  onclick="$('#page').val(1);">Filter</button>

                {{-- <button class="btn btn-info save">Save Changes</button> --}}
            </div>

            <div class="row top-buffer">
                <table id="faq-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:30px;">Select</th>
                            <th onclick="sortTable('idFile', '<?php echo (request('sortField') == 'idFile' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">idFile <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'idFile') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>
                            <th onclick="sortTable('firstName', '<?php echo (request('sortField') == 'firstName' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">First Name <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'firstName') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>
                            <th onclick="sortTable('lastName', '<?php echo (request('sortField') == 'lastName' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">Last Name <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'lastName') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>
                            <th onclick="sortTable('enrollmentApprovedDate', '<?php echo (request('sortField') == 'enrollmentApprovedDate' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">Approved Date <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'enrollmentApprovedDate') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>
                            <th onclick="sortTable('statusName', '<?php echo (request('sortField') == 'name' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">Enrollment Status <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'name') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>
                            <th onclick="sortTable('sentDate', '<?php echo (request('sortField') == 'sentDate' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">Sent Date <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'sentDate') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>                        
                            <th onclick="sortTable('contractSignDateCr', '<?php echo (request('sortField') == 'contractSignDateCr' && request('sortOrder') == 'ASC') ? 'DESC' : 'ASC'; ?>');">Contract Signed Date <i class="fa fa-fw fa-sort<?php echo (request('sortField') == 'contractSignDateCr') ? '-' . strtolower(request('sortOrder')) : ''; ?> sortTable"></i></th>

                            <th style="width:30px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $date = \Carbon\Carbon::now()->subYears(2)->startOfDay(); ?>
                        @foreach($clients as $client)
                        <?php
                        $currentDate = \Carbon\Carbon::createFromFormat('Y-m-d', $client->enrollmentApprovedDate)->startOfDay();
                        $sentDate = null;
                        if ($client->idUser && ($date < $currentDate)) {
                            $class = 'gold';
                            $checked = 'checked';
                            // SentDate -> goldCreationDate
                            $sentDate = $client->goldCreationDate;
                        } elseif ($client->idUser) {
                            $class = '';
                            $checked = 'checked';
                            // SentDate -> fecha + 2 años
                            $sentDate = \Carbon\Carbon::createFromFormat('Y-m-d', $client->enrollmentApprovedDate)->addYears(2);
                        } elseif ($client->sands) {
                            $class = 'orange';
                            $checked = '';
                            // SentDate -> 
                            $sentDate = $client->sandsCreationDate;
                        } else {
                            $class = '';
                            $checked = '';
                            // SentDate -> fecha + 2 años ;
                            $sentDate = \Carbon\Carbon::createFromFormat('Y-m-d', $client->enrollmentApprovedDate)->addYears(2);
                        }
                        ?>
                        <tr class="{{$class}}">
                            <td style="text-align:center"><input class="check" type="checkbox" class="checkClient" name="clients[]" value="{{$client->idFile}}" id="{{$client->idFile}}" {{$checked}}></td>
                            <td>{{ $client->idFile }}</td>
                            <td>{{ $client->firstName }}</td>
                            <td>{{ $client->lastName }}</td>                                               
                            <td>{{ \Carbon\Carbon::parse($client->enrollmentApprovedDate)->format('m/d/Y') }}</td>          
                            <td>{{ $client->statusName }}</td>          
                            <td>{{ \Carbon\Carbon::parse($client->sentDate)->format('m/d/Y') }}</td>          
                            <td class="tdSpace">
                                <div class="col-md-12">
                                    <div id="divShowCSDate_{{$client->idFile}}" class="cursor-pointer col-md-12" onclick="editCSDate({{$client->idFile}});">
                                        <span id="csDateSel_{{$client->idFile}}">{{  $client->contractSignDateCr ? \Carbon\Carbon::parse($client->contractSignDateCr)->format('m/d/Y') : ' -- ' }}</span>
                                    </div>
                                    <div id="divEditCSDate_{{$client->idFile}}" class="divEditDate col-md-12" style="display: none;">
                                        <div class="col-md-6">
                                            <input id="csDate_{{$client->idFile}}" type="text" class="form-control datePicker "  placeholder="" value="{{ request('from') }}" name="from" readonly="readonly" >
                                        </div>
                                        <button onclick="return clearCSDate({{$client->idFile}},'{{url('saveCSDate')}}');" class="btn btn-sm col-md-3">Clear</button>
                                        <button onclick="return saveCSDate({{$client->idFile}},'{{url('saveCSDate')}}');" class="btn btn-sm col-md-3">Save</button>
                                    </div>
                                </div>
                            </td>
                            <td style="text-align:center; color:black">

                                <a class="selectClient" href="/headerFileInf.cfm?idFile={{$client->idFile}}&goToClient=1"><i class="fa fa-pencil" aria-hidden="true" style="color:black"></i></a>

                                @php 
                                /* en 2019-10-21 se pide quitar validación 
                                @if(!$client->hide && !$client->userHide)
                                <a class="selectClient" href="/headerFileInf.cfm?idFile={{$client->idFile}}&goToClient=1"><i class="fa fa-pencil" aria-hidden="true" style="color:black"></i></a>
                                @else 
                                {{'-'}}
                                @endif
                                */
                                @endphp

                            </td>          
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{$clients->links('layouts.paginators.formFilter')}}
                <div>
                    <ul>
                        <li>Orange: Assigned by DAN</li>
                        <li> Yellow: Earlier assigned clients</li>
                        <li>White: Normal</li>
                    </ul>
                </div>

            </div>

        
    </form>
</div>

                @endsection