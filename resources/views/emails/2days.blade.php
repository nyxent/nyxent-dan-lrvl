<h3>Dear {!! $client !!} </h3>
<p>Your payment of ${{number_format($amount,2)}} scheduled on {{$date}} was returned to us as Insufficient Funds (NSF).  
The payment processing company has automatically scheduled this payment to be rerun on {{$newDate}} in the amount of ${{number_format($amount,2)}} plus an NSF fee of $25.  
If you do not have the funds available for the second draw, please call the accounting department at 800-613-9906 or simply reply this email immediately to keep this draw from happening.</p>
<p>Thank you.</p>
<strong>DAN Program Accounting Team</strong>
