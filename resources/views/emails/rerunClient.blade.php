<h3>Dear {!! $client !!} </h3>
<p>Your payment of ${{number_format($amount,2)}} re-scheduled on {{$date}} was returned to us as Insufficient Funds (NSF) again.  
  We have postponed this payment to the end of the program and your next payment is scheduled as {{$newDate}} plus $50.00 return fees. 
  If you are not going to have the funds available on that redraw date, please call the accounting department at 800-613-9906 or simply reply to this email to reschedule your payment.</p>
<p>Thank you.</p>
<strong>DAN Program Accounting Team</strong>
