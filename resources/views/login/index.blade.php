@extends ('layouts.admin')
@section ('contenido')
	<div class="col-md-4  col-md-offset-4" >
    <div class="row">
        <div class="page-header">
            <h2>Login</h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('url' => url('login'), 'method' => 'post', 'files'=> true)) !!}
            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('text', "User", array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('user', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                </div>
            </div>
            <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                {!! Form::label('password', "Password", array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::password('password', array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 ">
                    <button  class="btn btn-primary" type="submit" style="margin-right: 15px;">
                        Login
                    </button>

                   
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection