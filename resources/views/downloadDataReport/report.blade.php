@php
use App\Http\Utils\Utils;
@endphp
@if($request->includeHistory)
	@php $max =0; 
	foreach($files as $file){
		foreach ($file->debtHolders as $debt){
			$count = 0;
			foreach($debt->debtHolderNotes as $history){
				$count++;
				if($count > $max){
					$max = $count; 
				}
			}			
		}
	}
	@endphp
@endif
<table class="table table-bordered table-hover table-condensed">
    <thead>
        <tr>
            <th>idFile</th>
						<th>First Process Payment Date</th>
						@if(!$request->includeHistory)
						<th>Most Recent Process Payment Date</th>
						@endif
            <th>Primary First Name</th>
						<th>Primary Last Name</th>
						@if(!$request->includeHistory)
            <th>Social Security</th>
            <th>Email</th>
            <th>Street Address</th>
            <th>City</th>
            <th>County</th>
            <th>State</th>
            <th>Zip</th>
            <th>Home Phone</th>                    
            <th>Cell Phone</th>
            <th>Work Phone</th>
            <th>Second Deb Name</th>
            <th>Second Debor's Social Security</th>
						<th>Contact Phone</th>
						@endif
            <th>Creditor Name</th>
            <th>Debtor Name</th>
            <th>Business Name</th>
            <th>Account Number</th>
						<th>Last 4</th>
						@if(!$request->includeHistory)
            <th>Ref</th>
            <th>Balance</th>
            <th>Account Status</th>
						<th>New Client</th>
						@endif
						@if($request->includeHistory)
							@for ($i = 0; $i < $max; $i++)
								<th>History {{$i+1}}</th>
							@endfor
						@endif
        </tr>
    </thead>
    <tbody>
        @php 
        $colorFlag = 1;
        @endphp

        @foreach($files as $file)
        @foreach($file->debtHolders as $debt)
				@if($debt->status == 1)
        <tr class="{{ $colorFlag == 1 ? 'oddRow':'' }}">
            <td>{{ $file->idFile }}</td>
						<td>{{$file->getFirstPayment() ? Utils::dateTimeFormat($file->getFirstPayment()->depositDate) : '-'}}</td>
						@if(!$request->includeHistory)
						<td>{{$file->getFirstPayment() ? Utils::dateTimeFormat($file->getFirstPayment()->depositDate) : '-'}}</td>
						@endif
            <td>{{ $file->firstName }}</td>
						<td>{{ $file->lastName }}</td>
						@if(!$request->includeHistory)
            <td>{{ $file->socialSecurity }}</td>
            <td>{{ $file->email }}</td>
            <td>{{ $file->address }}</td>
            <td>{{ $file->city }}</td>
						<td>{{ $file->county }}</td>            
						<td>{{ $file->state }}</td>
            <td>{{ $file->zip }}</td>
            <td>{{ $file->homePhone }}</td>
            <td>{{ $file->cellPhone }}</td>
            <td>{{ $file->businessPhone }}</td>
            <td>{{ $file->scFirstName }}</td>
            <td>{{ $file->scSocialSecurity }}</td>
						<td>{{ $file->scHomePhone }} - {{ $file->scBusinessPhone }} - {{ $file->scCellPhone }}</td>
						@endif
						<td>{{ $debt->name }}</td>
						<td>{{ $debt->debtorsName }}</td>
						<td>{{ $debt->businessName }}</td>
						<td>{{ preg_replace('/\s+/', '', $debt->accountNumber) }}</td>
						<td>{{ substr(preg_replace('/\s+/', '', $debt->accountNumber),-4) }}</td>
						@if(!$request->includeHistory)
						<td>{{ $debt->ref }}</td>
						<td>{{ $debt->balance }}</td>
						<td>{{ $debt->accountStatus }}</td>
						<td>{{ strpos($file->fileColor,'yellow')>0?1:0}}</td>
						@endif
						@if($request->includeHistory)
							@php $count = 0; @endphp
							@foreach($debt->debtHolderNotes as $history)
								<td>{{ Utils::dateTimeFormat($history->creationDate)}} - {{$history->note}}</td>
								@php $count++; @endphp
							@endforeach
							@for ($i = $count; $i < $max; $i++)
								<td></td>
							@endfor
						@endif
				</tr>
				@endif
				@endforeach
				@endforeach
    </tbody>
</table>