@extends ('layouts.admin')

<script src="{{asset('js/downloadDataReport.js')}}"></script>

@section ('contenido')
<div class="container">
	
	<h1>Accounts detailed info download</h1>
	
	<form method="GET" action="" accept-charset="UTF-8" id="downloadReporFrm" class="form-inline my-2 my-lg-0 float-right paginatedForm">
		
		<!-- will be used to show any messages -->
		@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
		@endif
		<div class="row top-buffer">
			<input type="hidden" name="submited" value="1"/>
			<input id="downloadInput" type="hidden" name="download" value="0">
			
			
			<div class="form-group">
				<label for="from">Keyword:</label>
				<input type="text" class="form-control"  placeholder="" value="{{ $request->keyword }}" name="keyword">         
			</div>
			<div class="form-group">
				<label for="from">From:</label>
				<input name="dateFrom" type="text" class="form-control datePicker" value="{{ $request->dateFrom }}" readonly>
			</div>
			<div class="form-group">
				<label for="to">To:</label>
				<input name="dateTo" type="text" class="form-control datePicker" value="{{ $request->dateTo }}" readonly>
			</div>
			<br /><br />
			<div class="well">	
				<div class="form-group">
					
					@foreach ($fileStatus as $fs)
					<div class="col-lg-3">
						@php $checked = $request->fileStatus && in_array($fs->idFileStatus,$request->fileStatus) ? 'checked': '' @endphp
						<input type="checkbox" class="fileStatusChk" name="fileStatus[]" value="{{$fs->idFileStatus}}" {{$checked}}> {{$fs->name}}
					</div>
					@endforeach
				</div>                
				<div class="row">
					<div class="form-group col-lg-3 col-lg-offset-9">
						<a class="btn btn-xs btn-info " onclick="fileStatusCheckAll();">Select All</a>
						<a class="btn btn-xs btn-info " onclick="fileStatusUncheckAll();">Clear Selection</a>
					</div>
				</div>
			</div>
			
			<div class="form-group col-lg-12">
				<input id="includeHistory" name="includeHistory" type="checkbox" value="1" {{$request->includeHistory ? 'checked' : ''}}>
				<label for="from">Include History</label>
			</div>
			<br /><br />
			
			<button type="submit" class="btn btn-success" onclick="$('#page').val(1);$('#downloadInput').val(0);">Filter</button>
			<a href="{{url('downloadDataReport')}}" class="btn btn-success" >Clear Filters</a>
			<button class="btn btn-info downloadBtn"  onclick="$('#downloadInput').val(1);">Download</button>
			
		</div>
		<br/>
		<div class="row">
			@if($request->submited)
			<div class="table-responsive">
							@include('downloadDataReport.report')
			</div>
			{{$files->links('layouts.paginators.formFilter')}}
			@endif
			
		</div>
		
	</form>
</div>
