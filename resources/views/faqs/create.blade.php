@extends ('layouts.admin')

@section ('contenido')
<div class="container">
  
  <nav class="navbar">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ URL::to('faqs') }}">Faqs</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="{{ URL::to('faqs') }}">View All faqs</a></li>
      <li><a href="{{ URL::to('faqs/create') }}">Create a faq</a>
      </ul>
    </nav>
    
    <h1>Create a Faq</h1>
    
   
    {{ Form::open(array('url' => 'faqs')) }}

    <div class="form-group">
      {{ Form::label('idFaqCategory', 'Category') }}
      {!! Form::select('idFaqCategory', $categories, null, ['id' => 'idFaqCategory','class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {{ Form::label('question', 'Question') }}
      {{ Form::text('question', Input::old('question'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
      {{ Form::label('answer', 'Answer') }}
      {{ Form::textarea('answer', Input::old('answer'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
      {{ Form::label('tags', 'Keywords (please enter keywords separated by a comma)') }}
      {{ Form::text('tags', Input::old('tags'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
      {{ Form::label('order', 'Order') }}
      {{ Form::text('order', Input::old('order'), array('class' => 'form-control')) }}
    </div>
    
    {{ Form::submit('Create the faq!', array('class' => 'btn btn-primary')) }}
    
    {{ Form::close() }}
  </div>
  @endsection