@extends ('layouts.admin')

@section ('contenido')
<div class="container">
  
  <nav class="navbar">
    <ul class="nav navbar-nav">
      <li><a href="{{ URL::to('faqs') }}">View All faqs</a></li>
      <li><a href="{{ URL::to('faqs/create') }}">Create a faq</a>
      </ul>
    </nav>
    
    <h1>All the faqs</h1>
    
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row top-buffer">
      <form method="GET" action="{{ url('faqs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
        <div class="input-group">
          <select name="idFaqCategory" class="form-control">
            <option value="">--Select a category --</option>
            @foreach($categories as $category)
            @php $selected = request('idFaqCategory')==$category->idFaqCategory?'selected':'';@endphp
            <option value="{{$category->idFaqCategory}}" {{$selected}}>{{$category->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="input-group">
          <input type="text" class="form-control"  placeholder="Keyword..." value="{{ request('search') }}" name="search"> <span class="input-group-btn">        
        </div>
        <div class="input-group">
            <button type="submit" class="btn btn-search">Search</button>        
        </div>
      </form>
    </div>
    
    <div class="row top-buffer">
      <table id="faq-table" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th class="col-md-1">Faq Id</th>
            <th class="col-md-1">Category</th>
            <th class="col-md-1">Order</th>
            <th class="col-md-3">Question</th>
            <th class="col-md-4">Answer</th>
            <th class="col-md-2">Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach($faqs as $key => $value)
          <tr>
            <td>{{ $value->idfaq }}</td>
            <td>{{ $value->faqCategory->name }}</td>
            <td>{{ $value->order }}</td>
            <td>{{ $value->question }}</td>
            <td>{{ $value->answer }}</td>          
            <td>              
              <div class="btn-group" role="group">
                <a class="btn btn-small btn-info" href="{{ URL::to('faqs/' . $value->idfaq . '/edit') }}">Edit</a> 
                <a class="btn btn-small btn-warning" href="{{ URL::to('faqs/' . $value->idfaq . '/delete') }}">Remove</a> 
              </div>       
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="pagination-wrapper"> {!! $faqs->appends(['search' => Request::get('search'),'idFaqCategory' => Request::get('idFaqCategory')])->render() !!} </div>
    </div>
    
  </div>
  @endsection