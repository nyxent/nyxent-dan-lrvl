@extends ('layouts.admin')

@section ('contenido')
<div class="container">

    <h1>Summons submission report</h1>

    <form method="GET" action="" accept-charset="UTF-8" id="" class="form-inline my-2 my-lg-0 float-right paginatedForm">
        <div class="row top-buffer">
            <input type="hidden" name="submited" value="1"/>
            <input id="downloadInput" type="hidden" name="download" value="0">

            
            <div class="form-group">
                <label for="from">From:</label>
                <input name="dateFrom" type="text" class="form-control datePicker" value="{{ $request->dateFrom }}" readonly>
            </div>
            <div class="form-group">
                <label for="to">To:</label>
                <input name="dateTo" type="text" class="form-control datePicker" value="{{ $request->dateTo }}" readonly>
            </div>
            <br /><br />
            
            <button type="submit" class="btn btn-success" onclick="$('#page').val(1); $('#downloadInput').val(0);">Filter</button>
            <a href="{{url('summonsubmisison')}}" class="btn btn-success" >Clear Filters</a>
            <button class="btn btn-info downloadBtn"  onclick="$('#downloadInput').val(1);">Download</button>

        </div>
        <br/>

        <br/>
        <div class="row">

            <div class="table-responsive">
                @include('summonSubmissionReport.report')
            </div>
            {{$legalMatters->links('layouts.paginators.formFilter')}}


        </div>

    </form>
</div>
