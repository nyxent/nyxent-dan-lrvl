@php
use App\Http\Utils\Utils;
@endphp

<table class="table table-bordered table-hover table-condensed">
    <thead>
        <tr>
            <th colspan="12">General info</th>
            <th colspan="8">Opposing Counsel</th>
        </tr>
        <tr>
            
            <th>List of Client's phone #</th>
            <th>List of client's email address</th>
            <th>ID</th>
            <th>Date DAN Received Lawsuit document</th>
            <th>Defendant First Name</th>
            <th>Defendant  Last Name</th>
            <th>Have you been served with a summons, petition or warrant?</th>
            <th>Date On Court Paperwork (File Date)</th>
            <th>Plaintiff Name</th>
            <th>3rd Party</th>
            <th>Account# on Lawsuit</th>
            <th>Account # enrolled with DAN</th>
            <th>Amount Sued For</th>
            <th>Who we assigned to deal this suit?</th>
            <th>Date Assigned</th>
            <th>State and county where the suit was filed</th>
            <th>Payment current?</th>
            <th>Plaintiff Law Firm</th>
            <th>Venue</th>
            <th>Lawsuit Case #</th>
            <th>Appearring Date</th>
            <th>Client contacted by Attorney</th>
            <th>Legal Plan Enrollment</th>
            <th>Lawsuit Result</th>
            <th>Current Status</th>
            <th>Method of Reception</th>
            <th>Tyep of Document</th>
            <th>Download Link</th>
        </tr>
    </thead>
    <tbody>
        @php 
        $colorFlag = 1;
        @endphp

        @foreach($legalMatters as $lm)
            <tr class="{{ $colorFlag == 1 ? 'oddRow':'' }}">
                
                <td>{{$lm->file->businessPhone}}<br/>{{$lm->file->cellPhone}}<br/>{{$lm->file->homePhone}}</td>
                <td>{{$lm->file->email}}</td>
                <td>{{$lm->idFile}}</td>
                <td>{{Utils::dateTimeFormat($lm->openDate)}}</td>
                <td>{{$lm->defenderFirstName}}</td>
                <td>{{$lm->defenderLastName}}</td>
                <td>{{$lm->served == 1 ? 'yes' : 'no'}}</td>
                <td></td>
                <td>{{$lm->plantiff}}</td>
                <td></td>
                <td></td>
                <td>{{$lm->debtHolder ? $lm->debtHolder->accountNumber: ''}}</td>
                <td>{{$lm->ammount}}</td>
                <td></td>
                <td></td>
                <td>{{$lm->city}} {{$lm->state ? $lm->state->name : ''}}</td>
                <td>?</td>
                <td>{{$lm->lmLawFirm}}</td>
                <td></td>
                <td>{{$lm->ref}}</td>
                <td>{{$lm->appearingDate}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Client's Portal</td>
                <td>{{ $lm->documentType ? $lm->documentType->name : null }}</td>

                
                
                @if(count($lm->archives))
                    @if($request->download)
                    
                        @foreach($lm->archives as $archive)
                            <td>
                                <a href="{{ env('DAN_URL')}}/archive/{{$lm->idFile}}/{{$archive->archiveFileName}}" >{{$archive->archiveName}}</a>
                            </td>
                        @endforeach
                        <td>
                            <a href="{{ env('CLIENT_PORTAL_URL')}}/downloadArchiveZip/{{$lm->idLegalMatter}}"  >Download All</a>
                        </td>
                    @else
                        <td>
                    
                            @foreach($lm->archives as $archive)
                            <div style="text-align: left; width: 300px;">- <a href="{{ env('DAN_URL')}}/archive/{{$lm->idFile}}/{{$archive->archiveFileName}}" target="_blank" style="cursor: pointer;">{{$archive->archiveName}}</a></div>
                            @endforeach
                            <div style="text-align: left; width: 300px;">- <a href="{{ env('CLIENT_PORTAL_URL')}}/downloadArchiveZip/{{$lm->idLegalMatter}}"  target="_blank" style="cursor: pointer;">Download All</a></div>

                        </td>
                    @endif
                @endif
                
                
            </tr>
            @php  $colorFlag *= -1; @endphp
        @endforeach
    </tbody>
</table>