<table id="faq-table" class="table table-bordered">
  <thead>
    <tr>
      <th>Checked</th>
      <th>idFile</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Approved Date</th>
      <th>Enrollment Status</th>
    </tr>
  </thead>
  <tbody>
    <?php $date = \Carbon\Carbon::now()->subYears(2)->startOfDay();?>
    @foreach($clients as $client)
    <?php
    $currentDate =   \Carbon\Carbon::createFromFormat('m/d/Y',$client->enrollmentApprovedDate)->startOfDay();
    if($client->idUser && ($date < $currentDate)){
      $class ='gold';
      $checked= 'checked';
    }
    elseif($client->idUser){
      $class ='';
      $checked= 'checked';
    }
    elseif($client->sands){
      $class ='orange';
      $checked= '';
    }
    else{
      $class ='';
      $checked= '';
    }
    ?>
    <tr class="{{$class}}">
      <td style="text-align:center">{{$checked}}</td>
      <td>{{ $client->idFile }}</td>
      <td>{{ $client->firstName }}</td>
      <td>{{ $client->lastName }}</td>                   
      <td>{{ $client->enrollmentApprovedDate }}</td>          
      <td>{{ $client->statusName }}</td>                   
    </tr>
    @endforeach
  </tbody>
</table>