@extends ('layouts.admin')

@section ('contenido')
<div class="container">
  
  <nav class="navbar">
   
    <h1>KC Credit Report Download</h1>
    
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="row top-buffer">
        <form method="GET" action="{{ url('donaReport') }}" accept-charset="UTF-8" id="reportDwn" class="form-inline my-2 my-lg-0 float-right">
          <input id="downloadInput" type="hidden" name="download" value="0">
          <input type="hidden" id="sortField" name="sortField" value="{{ request('sortField') }}">
          <input type="hidden" id="sortOrder" name="sortOrder" value="{{ request('sortOrder') }}">
          <div class="form-group">
            <label for="from">Keyword:</label>
            <input type="text" class="form-control"  placeholder="" value="{{ request('keyword') }}" name="keyword">         
          </div>
          <div class="form-group">
            <label for="checked">Checked:</label>
            <select class="form-control" name="checked" id="checked">
              <option value="" <?php echo (request('checked')=='')?'selected':''; ?>>All</option>
              <option value="1" <?php echo (request('checked')=='1')?'selected':''; ?>>Checked</option>
              <option value="2" <?php echo (request('checked')=='2')?'selected':''; ?>>Uncheked</option>
              <option value="3" <?php echo (request('checked')=='3')?'selected':''; ?>>Assigned</option>
            </select>     
          </div>
          <div class="form-group">
            <label for="from">From:</label>
            <input type="text" class="form-control datePicker"  placeholder="" value="{{ request('from') }}" name="from">         
          </div>
          <div class="form-group">
            <label for="to">To:</label>
            <input type="text" class="form-control datePicker"  placeholder="" value="{{ request('to') }}" name="to">         
          </div>
          <button type="submit" class="btn btn-success">Filter</button>
          <button class="btn btn-info downloadBtn">Download</button>
        </form>
    </div>
    
    <div class="row top-buffer">
      <table id="faq-table" class="table table-bordered">
        <thead>
          <tr>
            <th>Checked</th>
            <th onclick="sortTable('f.idFile','<?php echo (request('sortField')=='f.idFile' && request('sortOrder')=='ASC')?'DESC':'ASC';?>');">idFile <i class="fa fa-fw fa-sort<?php echo (request('sortField')=='f.idFile')? '-'.strtolower(request('sortOrder')):''; ?> sortTable"></i></th>
            <th onclick="sortTable('f.firstName','<?php echo (request('sortField')=='f.firstName' && request('sortOrder')=='ASC')?'DESC':'ASC';?>');">First Name <i class="fa fa-fw fa-sort<?php echo (request('sortField')=='f.firstName')? '-'.strtolower(request('sortOrder')):''; ?> sortTable"></i></th>
            <th onclick="sortTable('f.lastName','<?php echo (request('sortField')=='f.lastName' && request('sortOrder')=='ASC')?'DESC':'ASC';?>');">Last Name <i class="fa fa-fw fa-sort<?php echo (request('sortField')=='f.lastName')? '-'.strtolower(request('sortOrder')):''; ?> sortTable"></i></th>
            <th onclick="sortTable('f.enrollmentApprovedDate','<?php echo (request('sortField')=='f.enrollmentApprovedDate' && request('sortOrder')=='ASC')?'DESC':'ASC';?>');">Approved Date <i class="fa fa-fw fa-sort<?php echo (request('sortField')=='f.enrollmentApprovedDate')? '-'.strtolower(request('sortOrder')):''; ?> sortTable"></i></th>
            <th onclick="sortTable('s.name','<?php echo (request('sortField')=='s.name' && request('sortOrder')=='ASC')?'DESC':'ASC';?>');">Enrollment Status <i class="fa fa-fw fa-sort<?php echo (request('sortField')=='s.name')? '-'.strtolower(request('sortOrder')):''; ?> sortTable"></i></th>
          </tr>
        </thead>
        <tbody>
          <?php $date = \Carbon\Carbon::now()->subYears(2)->startOfDay();?>
          @foreach($clients as $client)
          <?php
          $currentDate =   \Carbon\Carbon::createFromFormat('m/d/Y',$client->enrollmentApprovedDate)->startOfDay();
          if($client->idUser && ($date < $currentDate)){
            $class ='gold';
            $checked= 'checked';
          }
          elseif($client->idUser){
            $class ='';
            $checked= 'checked';
          }
          elseif($client->sands){
            $class ='orange';
            $checked= '';
          }
          else{
            $class ='';
            $checked= '';
          }
          ?>
          <tr class="{{$class}}">
            <td style="text-align:center">{{$checked}}</td>
            <td>{{ $client->idFile }}</td>
            <td>{{ $client->firstName }}</td>
            <td>{{ $client->lastName }}</td>                   
            <td>{{ $client->enrollmentApprovedDate }}</td>          
            <td>{{ $client->statusName }}</td>                   
          </tr>
          @endforeach
        </tbody>
      </table>
      <div>
        <ul>
          <li>Orange: Assigned by DAN</li>
          <li> Yellow: Earlier assigned clients</li>
          <li>White: Normal</li>
        </ul>
      </div>
    </div>
    
  </div>
  <script>
  $('.datePicker').datepicker({format: 'mm/dd/yyyy',autoclose: true,}); 
  $('.downloadBtn').click(function(){
    $('#downloadInput').val(1);
    $('#reportDwn').submit();
    setTimeout(function(){
      $('#downloadInput').val(0);
    }, 2000);
  });
  function sortTable(sortField,sortOrder){
      $('#sortField').val(sortField);
      $('#sortOrder').val(sortOrder);
      $('#reportDwn').submit();
    }
  </script>
  @endsection