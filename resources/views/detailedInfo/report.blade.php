@php
use App\Http\Utils\Utils;
@endphp
<table class="table table-bordered table-hover table-condensed">
    <thead>
        <tr>
            <th>idFile</th>
            <th>First Process Payment Date</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Creditor Name</th>
            <th>Debtor Name</th>
            <th>Business Name</th>
            <th>Account Number</th>
            <th>Last 4</th>
            <th>Balance</th>
            <th>Account Status</th>                    
            <th>Inactive Date</th>
            <th>History</th>
        </tr>
    </thead>
    <tbody>
        @php 
        $colorFlag = 1;
        @endphp

        @foreach($files as $file)

        <tr class="{{ $colorFlag == 1 ? 'oddRow':'' }}">
            <td>{{ $file->idFile }}</td>
            <td>{{$file->getFirstPayment() ? Utils::dateTimeFormat($file->getFirstPayment()->depositDate) : '-'}}</td>
            <td>{{ $file->firstName }}</td>
            <td>{{ $file->lastName}}</td>	
            <td></td><td></td><td></td><td></td><td></td><td></td>
            <td>{{$file->getInactiveDate() ? Utils::dateTimeFormat($file->getInactiveDate()->timeStamp) : '-'}}</td>
            <td><b>File Status:</b> {{ $file->fileStatus->name }}</td>
        </tr>

        @foreach($file->debtHolders as $debtHolder)
        @if($debtHolder->status==1)
        <tr class="{{ $colorFlag == 1 ? 'oddRow':'' }}">
            <td>{{ $file->idFile }}</td>
            <td></td><td></td><td></td>	
            <td>{{ $debtHolder->name }}</td>
            <td>{{ $debtHolder->debtorsName }}</td>
            <td>{{ $debtHolder->businessName }}</td>
            <td>{{ $debtHolder->accountNumber }}</td>
            <td>{{ substr($debtHolder->accountNumber,-4) }}</td>
            <td>${{ number_format($debtHolder->balance,2) }}</td>
            <td>{{ $debtHolder->accountStatus }}</td>
            <td>-</td>
            <td></td>
        </tr>
        @endif
        @endforeach

        @if($request->includeHistory)

        @foreach($file->histories as $history)
        <tr class="{{ $colorFlag == 1 ? 'oddRow':'' }}">
            <td>{{ $file->idFile }}</td>
            <td></td><td></td><td></td>	<td></td>            
            <td></td><td></td><td></td><td></td>
            <td>{{Utils::datetimeFormat($history->getInactiveDate())}}</td>
            <td><b>{{Utils::dateTimeFormat($history->timeStamp)}}: </b>{{ $history->title }}</td>
        </tr>

        @endforeach
        @endif

        @php $colorFlag = (-1) * $colorFlag; @endphp
        @endforeach

    </tbody>
</table>