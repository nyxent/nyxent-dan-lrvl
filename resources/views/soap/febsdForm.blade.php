@extends ('layouts.admin')

@section ('contenido')

<h1>  
 Reset EFTs
</h1>

<div class="container">  
  <!-- will be used to show any messages -->
  @if (Session::has('message'))
  <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
  <br />  
  <form action="{{url('voids')}}" method="POST" >  
    {{ csrf_field() }}   
    @include('layouts.datetime')    
    <br />    
    <div class="row">       
      <div class="col-md-4 col-sm-4 col-xs-4">        
        @include('layouts.searchClient')        
      </div>      
    </div>    
    <br />    
    <div class="row">      
      <div class="col-md-8 col-sm-8 col-xs-8">        
        <button class="btn btn-primary btn-sm" type="submit">Reset</button>               
      </div>      
    </div>    
  </form>  
</div>

@endsection