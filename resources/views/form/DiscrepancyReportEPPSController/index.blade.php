@extends ('layouts.admin')
@section ('contenido')

<h1>
Discrepancy Report EPPS
</h1>
    <br />
    
    <div class="row">
      <button class="btn btn-success  btn-sm" name="action" value="export" type="button" onclick="exportTableToCSV('DiscrepancyReportEPPSController.csv')">Export</button>
  </div>
    <br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
    <div id="img" style="display:none"><img src="{{asset('img/success.png')}}" alt="Success"  width="15">  </div>
    <div class="table-responsive">
            <table id="example1" class="table table-striped table-bordered table-condensed table-hover">
            <thead>
            	<th></th>
                <th>
CRM_ID</th>
                <th style="min-width:150px;">Client</th>
                <th width="100">PayoutDate</th>
                <th width="80">Amount</th>
                <?php
					  for($i=1; $i<=6;$i++)
					  {
						  if($i==1)
						  {
							   echo '<th>DAN</th>';
						  }
						  if($i==2)
						  {
							   echo '<th>Nyxent</th>';
						  }
						  if($i==3)
						  {
							   echo '<th>KC Credit Services</th>';
						  }
						  if($i==4)
						  {
							   echo '<th>Company 1 name</th>';
							   echo '<th>Amount</th>';
						  }
						  if($i==5)
						  {
							   echo '<th>Company 2 name</th>';
							   echo '<th>Amount</th>';
						  }
						  if($i==6)
						  {
							   echo '<th>EPPS</th>';
						  }
					  }
				 ?>
                   <th width="80">Account balance</th>
            </thead>
    		
				<?php
					 $CountMonth=0;
					 if(! empty($itemsFile))
					 {
						  $PaymentsEPPS=array();
						 
						  foreach($itemsFile as $indice=>$item)
						  {					 
									  if($item->statusCode=='S')
									  {
										  echo '<tr>';
									  }
									  else
									  {
										  if($item->dateIshigher=='N')
										  {
											   echo '<tr class="warning">';
										  }
										  else
										  {
											  $CountMonth++;
											 echo '<tr>' ;
										  }
									  }
									
									
									echo   '<td> '.($indice+1).' </td><td>'.$item->idFile.'</td><td>'.$item->lastName.' '.$item->firstName.'</td>';
								   if($item->statusCode=='P' and $item->Type=='DEP')
									{
										
										echo  '<td>'.$item->payoutDate.'</td>';
										
										
									}
									else
									{
										 if($item->statusCode=='P' and $item->Type=='COM')
										 {
											echo  '<td>'.$item->payoutDate.'</td>';
										 }
										 else
										 {
											echo  '<td>'.$item->payoutDate.'</td>'; 
										 }
									}
									
									if($item->statusCode=='P' and $item->Type=='DEP')
									{
										if($item->dateIshigher=='N')
										{
											echo '<td align="right" id="amount'.$item->eftTransactionID.'">$'.$item->amount.'</td>';
										}
										else
										{
											echo '<td onclick="ShowChangeAmount('.$item->eftTransactionID.')" align="right" id="amount'.$item->eftTransactionID.'">$'.$item->amount.'</td>';
										}
									}
									else
									{
										echo '<td align="right">$'.$item->amount.'</td>';
									}
									for($i=1; $i<=6;$i++)
									  {
										  if($i==1)
										  {
											
											   if(array_key_exists('DAN',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												   if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['DAN']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
												   echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['DAN']['amount']) ;
													  echo '</td>';
											   }
											   else
											   {
												   echo '<td></td>';
											   }
										  }
										  if($i==2)
										  {
											   if(array_key_exists('Nyxent',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['Nyxent']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
												     echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['Nyxent']['amount']) ;
											   }
											   else
											   {
												   echo '<td></td>';
											   }
										  }
										  if($i==3)
										  {
											   if(array_key_exists('KcCredit',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['KcCredit']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
												    echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['KcCredit']['amount']) ;
											   }
											   else
											   {
												   echo '<td></td>';
											   }
										  }
										  if($i==4)
										  {
											   
											   if(array_key_exists('C1',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C1']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
											  	 echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C1']['affiliateCompanyName']) ;
											   }
											   else
											   {
												   echo '<td></td>';
											   }
											   
											   if(array_key_exists('C1',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C1']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
											  	 echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C1']['amount']) ;
											   }
											   else
											   {
												   echo '<td></td>';
											   }
											  
										  }
										  if($i==5)
										  {
											   if(array_key_exists('C2',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C2']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
											  	 echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C2']['affiliateCompanyName']) ;
											   }
											   else
											   {
												   echo '<td></td>';
											   }
											   
											   if(array_key_exists('C2',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												    if($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C2']['statusCode']=='P')
												   {
													   echo '<td class="danger">'; 
												   }
												   else
												   {
													    echo '<td>'; 
												   }
											  	 echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['C2']['amount']) ;
											   }
											    else
											   {
												   echo '<td></td>';
											   }
										  }
										  if($i==6)
										  {
											
											   if(array_key_exists('EPPS',$CompanysPaymentsDate[$item->payoutDate2][$item->idFile]))
											   {
												   echo '<td>'; 
												   echo($CompanysPaymentsDate[$item->payoutDate2][$item->idFile]['EPPS']['amount']) ;
												   echo '</td>';
											   }
											   else
											   {
												   echo '<td></td>';
											   }
										  }
										
									  }
									 
									  echo '<td>'.$item->balanceEPPS.'</td>';
									  
										
									echo'</tr>';
							}
							 
						  
					 }
                ?>
             
             </table>
             <?php
				 if(! empty($itemsFile))
				 {
					?>
				   
					  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
					  <?php 
				 }
			?>
          </div>
      </div>
  </div>
<script src="{{asset('js/html2csv.js?ver=8.0.0')}}"></script>   
@endsection