@extends ('layouts.admin')
@section ('contenido')
<h1>
Transaction list
</h1>
<div class="container">
		<br />
          <div class="row">
               <form action="{{url('form/paymentHistory')}}" method="get" >
                       @include('layouts.date')
                             <br />
                             <div class="row">
                                   <div class="col-md-8 col-sm-8 col-xs-8">
                                     @include('layouts.search')
                                   </div>
                                   
                              </div>
                              <br />
                              <div class="row">
                                  <button class="btn btn-primary  btn-sm" type="submit">Filter</button>
                                  <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
                              </div>
                </form>
          </div>
    
</div>
	  <br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                 <th>Type</th>
                <th>Client</th>
                <th>Affiliate company name</th>
                <th>PayoutDate</th>
                <th>Amount</th>
                <th>Comments</th>
            </thead>
            @foreach ($itemsFile as $item)
            <tr>
            	<td>{{$item->type}} </td>
                <td>{{$item->lastName}} {{$item->firstName}}</td>
                <td>{{$item->affiliateCompanyName}} </td>
                <td>{{$item->payoutDate}}</td>
                <td align="right">${{$item->amount}}</td>
                <td>{{$item->comments}}</td>
            </tr>
            {{--  @include('form.commissionEntry.modal') --}}
            @endforeach
            </table>
        </div>
		 {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
$("#searchClient").val('<?php echo $searchClient;?>');
$("#searchText").val('<?php echo $searchText;?>');
</script>
@endsection