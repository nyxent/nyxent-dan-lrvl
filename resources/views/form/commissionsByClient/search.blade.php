<div class="col-md-6 col-sm-6 col-xs-6">
{!! Form::open(array('url'=>'form/CommissionPayments','method'=>'get','autocomplete'=>'off','role'=>'search')) !!}

<div class="form-group">
	<div class="input-group">
        <select id="lunch" class="selectpicker" name="searchText" data-live-search="true" title="please select a company" onchange="">
          @foreach ($company as $compan)
                <option value="{{ $compan->affiliateCompanyId }}">{{ $compan->affiliateCompanyName }}</option>
            @endforeach
     	 </select>
		<span class="input-group-btn">
			<button type="submit" class="btn btn-primary  btn-xs">search</button>
		</span>
	</div>
</div>
{{Form::close()}}
</div>
<div class="col-md-6 col-sm-6 col-xs-6">
@if(! empty($client))
{!! Form::open(array('url'=>'form/CommissionPayments','method'=>'get','autocomplete'=>'off','role'=>'searchClient')) !!}
<div class="form-group">
	<div class="input-group">
        <select id="lunch" class="selectpicker" name="searchClient" data-live-search="true" title="please select client" onchange="">
          @foreach ($client as $item)
                <option value="{{ $item->idFile }}">{{ $item->client2 }}</option>
            @endforeach
     	 </select>
		<span class="input-group-btn">
			<button type="submit" class="btn btn-primary  btn-xs">search</button>
		</span>
	</div>
</div>
{{Form::close()}}
</div>
 @endif