@extends ('layouts.admin')
@section ('contenido')
<script>
	$.PaymentsEPPSTrasantion=<?php echo $PaymentsEPPSTrasantion;?>;	
	$.PaymentsEPPSChanges=Array();	
	$.CommissionEPPSTrasantion=<?php echo $CommissionEPPSTrasantion;?>;	
	$.CommissionEPPSChanges=Array();	
	$.CommissionAmountEPPSChanges=Array();
	<?php if(isset($AccountNumber) && $AccountNumber!=""): ?>
	$.AccountNumber=<?php echo $AccountNumber;?>;	
  $.RoutingNumber=<?php echo $RoutingNumber;?>;
  $.NewFEEAmount = 0;
  $.Reactive = 0;
	<?php endif; ?>
	$.Enviar='N';	
</script>
<h1>Active Snapshot	@if(! empty($NameClient)) --- <div id="NameClient">{{ $NameClient }}</div>@endif </h1>
<div class="container">
	<br />
	<div class="row">
		<div class="col-md-6">
			@include('form.commisionPaymentsByClient2.search')
		</div>
	</div>
</div>
<br />

<form action="{{url('form/commisionPaymentsByClient2')}}" id="FormConfirmSaveChange" method="post" enctype="multipart/form-data" onsubmit="return SaveNote();">
	<input name="action" type="hidden" value="saveChangePayment" />
	<input name="NewNote" id="NewNote" type="hidden" value="" />
	<input name="Reactive" id="Reactive" type="hidden" value="0" />
	<input name="PaymentsEPPSChanges" id="PaymentsEPPSChanges" type="hidden" value="" />
	<input name="CommissionEPPSChanges" id="CommissionEPPSChanges" type="hidden" value="" />
	{{csrf_field()}}
	<div class="row">
		<div class="col-md-4">
			<button class="btn btn-success btn-xs" name="synchronize" type="submit">Save changes</button>
		</div>
		<div class="col-md-4">
			<!--<button id="AccountInfoButton" class="btn btn-success btn-xs" name="synchronize" type="button" onclick="ChangeAccountInfo()">Change	account info</button>-->
		</div>
		<div align="right" class="col-md-4">
			<!--<button id="AccountInfoButton" class="btn btn-success btn-xs" name="synchronize" type="button" onclick="Renumbercommision()">Renumber</button>-->
		</div>
	</div>
	<div class="row" style="margin: 20px 0px">
		<div class="col-md-4">
			<h4>Account Balance: <span class="<@if($accountBalance<0) {{'text-danger'}} @else {{'text-success'}} @endif>"> ${{number_format($accountBalance,2)}}</span></h4>
		</div>
		<div class="col-md-4">
			<h4>Status: <span class=""> {{$fileStatusName}}</span></h4>
		</div>
		<div class="col-md-4">
			<?php 
				$countPending = 1;
				if($idFileStatus==18):
					foreach($itemsFile as $row):
						if($row['type']=='dep' && $row['statusCode']=='P' && $countPending):
				?>
				<button id="reactiveButton" class="btn btn-primary btn-xs" name="synchronize" type="button" onclick="ReactivateDate(<?php $values = explode('-',$row['amount']); echo $values[1]; ?>);">Reactivate</button>			
				<input id="reactivateDate" value="{{date('m/d/Y', strtotime($row['date']))}}"  readonly="readonly" type="text" style="width:100px;height:20px;display:none;" />
				<div id="img2" style="display:none">
					<img src="{{asset('img/success.png')}}" alt="Success" width="15"> 
				</div>
				<?php 
							$countPending =0;
						endif;
					endforeach;
				elseif($idFileStatus==19 || $idFileStatus==17):
					$numItems = count($itemsFile);
					$i=0;
					foreach($itemsFile as $row):
						if(++$i === $numItems):
				?>
				<button id="reactiveButton" class="btn btn-primary btn-xs" name="synchronize" type="button" onclick="ReactivateUser('{{date('Y-m-d', strtotime($row['date']))}}');">Reactivate</button>			
				<input id="reactivateDate" value="{{date('m/d/Y', strtotime($row['date']))}}"  readonly="readonly" type="text" style="width:100px;height:20px;display:none;" />
				<div id="img2" style="display:none">
					<img src="{{asset('img/success.png')}}" alt="Success" width="15"> 
				</div>
				<?php
						endif;
					endforeach;
				endif;
			?>
		</div>
	</div>
</form>

<form id="formAccountInfo" action="{{url('form/commisionPaymentsByClient2')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
	<input name="NewAccountNumber" id="NewAccountNumber" type="hidden" value="" />
	<input name="NewRoutingNumber" id="NewRoutingNumber" type="hidden" value="" />
	<input name="action" type="hidden" value="saveChangeAccountinfo" />
	{{csrf_field()}}
</form>

<div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
		<div id="img" style="display:none">
			<img src="{{asset('img/success.png')}}" alt="Success" width="15"> 
		</div>

		<div class="table-responsive">			
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th width="60"></th>
					<th width="15"></th>
					<!--<th>Type</th>-->
					<!--<th style="min-width:150px;">Client</th>-->
					<th width="100">CRMID</th>
					<th width="100">PayoutDate</th>
					<th width="15"></th>
					<th width="80">Amount</th>
					<?php					
					foreach($Companys as $indice=>$Company)					
					{						
						echo '<th>'.$Company->affiliateCompanyName.'</th>';						
					}					
					?>
					<th>Remarks</th>
					<th>Provider</th>
				</thead>
				<tbody>
				<?php $totalCommision = 0; ?>
					<?php $CountMonth =0; ?>
						@foreach($itemsFile as $item)
							<?php
								if($item['type']=='dep'){
									$totalCommision = 0;
									$amount =$item['amount'];
									$values = explode('-',$item['amount']);
									$eftID = isset($values[1])? $values[1]:'';
									$amount = isset($values[0])? $values[0]:'';
									$CountMonth = $item['paymentNumber'];
								}
							?>		
							<tr  class="<?php if($item['type']=='dep' && $item['statusCode']=='S'){echo 'success';} elseif($item['type']=='dep' && $item['statusCode']=='R'){echo 'danger';} elseif($item['type']=='com' && $item['statusCode']=='T'){echo 'warning';} if($item['type']=='com' ){echo 'com';}?>">
								<td>
									@if($item['type']=='dep')
										<a href="#"  onclick="CopyRow({{$eftID}})"><img src="../img/add.png" alt="Pendding" width="20"></a>
									@else
										<a href="#"  onclick="CopyRow('{{$item['date']}}')"><img src="../img/add.png" alt="Pendding" width="20"></a>
									@endif
									@if($item['statusCode']=='P')
										@if($item['type']=='dep')
											<a href="#"  onclick="DeleteRow({{$eftID}})"><img src="../img/delete.png" alt="Pendding" width="15"></a>
										@else
											<a href="#"  onclick="DeleteRow('{{$item['date']}}')"><img src="../img/delete.png" alt="Pendding" width="15"></a>
										@endif									
									@endif
								</td>
								<td>@if($item['type']=='dep' && $item['statusCode']!='R'){{$CountMonth}}@endif</td>
								<td>{{$idFile}}</td>
								<td id="<?php if($item['type']=='dep' && $item['statusCode']=='P'){echo $eftID;}elseif($item['type']=='com' && $item['statusCode']=='P'){echo 'fee-'.$item['date'];}?>" class="row-date" onclick="<?php if($item['type']=='dep' && $item['statusCode']=='P'){echo 'ShowChangeDate('.$eftID.');' ;}elseif($item['type']=='com'){echo "ShowChangeDateFee('".$item['date']."');";}?>">{{date('m/d/Y', strtotime($item['date']))}}</td>
								<td id="<?php if($item['type']=='dep' && $item['statusCode']=='P'){echo 'msg'.$eftID;}elseif($item['type']=='com' && $item['statusCode']=='P'){echo 'msg'.$item['date'];}?>"></td>
								@if($item['type']=='dep')
									<td id="amount{{$eftID}}" <?php if($item['statusCode']=='P'){ echo 'onclick="ShowChangeAmount('.$eftID.');"';} ?>>${{number_format($amount/100,2)}}</td>
								@else
									<td></td>
								@endif
								@foreach($Companys as $indice=>$Company)
									@if($item['type']=='com')
										<?php
										// dd($item);
											$commision =(isset($item[str_replace(' ','_',str_replace(',','',$Company->affiliateCompanyName))]))?$item[str_replace(' ','_',str_replace(',','',$Company->affiliateCompanyName))]:null;
											$values = explode('_',$commision);
											$feeStatus = isset($values[0])? $values[0]:'';
											$feeAmount = isset($values[1])? $values[1]:0;
											$feeds = isset($values[2])? explode(',',$values[2]): array();
											$feeID = isset($values[2])? explode(',',$values[2])[0]:'';
											$totalCommision = $totalCommision + $feeAmount;
										?>
										@if($feeStatus=='P')						
											<td id="amountFee{{$feeID}}" style="{{count($feeds)>1 && $feeAmount>0?'background-color:orange':''}}" onclick="ShowChangeAmountFee('{{date('m/d/Y', strtotime($item['date']))}}',{{$feeID}},'{{$Company->affiliateCompanyName}}')">
												${{number_format($feeAmount/100,2)}}
											</td>
										@elseif($feeStatus=='T')
											<td id="amountFee{{$feeID}}" style="{{count($feeds)>1 && $feeAmount>0?'background-color:orange':''}}">
												<img src="{{asset('img/success.png')}}" alt="Success" width="15"> 
												${{number_format($feeAmount/100,2)}}
											</td>
										@elseif(!isset($commision))
											<td id="amountFee{{$feeID}}" style="{{count($feeds)>1 && $feeAmount>0?'background-color:orange':''}}" onclick="CrearNewFEE('{{date('m/d/Y', strtotime($item['date']))}}','{{$Company->affiliateCompanyName}}','EPPS',{{$Company->affiliateCompanyId}})">${{number_format($feeAmount/100,2)}}</td>
										@else
										<td id="amountFee{{$feeID}}" style="{{count($feeds)>1 && $feeAmount>0?'background-color:orange':''}}">
												${{number_format($feeAmount/100,2)}}
											</td>
										@endif
									@else
										<td></td>
									@endif								
								@endforeach
								<td>@if($item['type']=='dep'){{$item['statusCodeDescription']}}@endif</td>
								<td>{{$item['provider']}}</td>
							</tr>
							<?php if($item['type']=='dep' && $item['statusCode']!='R'){$CountMonth++;} ?> 
						@endforeach
				</tbody>
			</table>		
		</div>		
	</div>	
</div>

<script>
	$.CountMonth = <?php echo $CountMonth ?>;
	$.copyFEE = 0;
</script>

<script>
	<?php	
		if($AccountNumber=='' and $RoutingNumber==''){
	?>		
		$("#AccountInfoButton").hide();	
	<?php		
		}
	?>	
	$("#searchClient").val('<?php echo $searchClient;?>');
</script>

<script>
	function CopyRow(eftTransactionID)
	{		
		$.NewRow = Array();		
		$.each($.PaymentsEPPSTrasantion, function (key, value){			
			if (eftTransactionID == value.EftTransactionID){
				$.NewRow.push({
					amount: value.EftAmount,
					type: 'EFT',
					idFile: value.CardHolderId,
					Oringen: value,
					Merge: 'N'
				});				
				$.each($.CommissionEPPSTrasantion, function (key2, value2){					
					if (eftTransactionID == value2.Fee_Date){						
						$.NewRow.push({
							amount: value2.FeeAmount,
							type: 'FEE',
							PaidToName: value2.PaidToName,
							Oringen: value2,
							Merge: 'N'
						});						
					}					
				});				
			}			
		});		
		if ($.NewRow.length == 0){			
			$.each($.CommissionEPPSTrasantion, function (key2, value2){		
				if ((value2.Fee_Date).substring(0, 10) == eftTransactionID){					
					$.NewRow.push({
						amount: value2.FeeAmount,
						type: 'FEE',
						PaidToName: value2.PaidToName,
						Oringen: value2,
						Merge: 'N'
					});					
				}				
			});
			$.copyFEE = 1;
		}
		$.Contenido = '<div class="form-group">';			
		$.Contenido += '<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';				
		$.Contenido += '<input name="NewEftDate" value="" id="NewEftDate" class="Newdate" type="text" style="width:140px;">';				
		$.Contenido += '</div>';				
		$.Contenido += '</div>';				
		$.Contenido += '</div>';				
		MostrarPopup('Confirm', $.Contenido, 'ConfirmCopyRow()', 'S', 'N', 'primary', 'N', 'N');				
		$('.Newdate').datepicker({					
			format: 'mm/dd/yyyy',					
			startDate: CurrentDateMoreTwoDays(),					
			autoclose: true,					
		});			
	}
			
			
			
	function DeleteRow(eftTransactionID)	
	{
		$.NewRow = Array();				
		$.each($.PaymentsEPPSTrasantion, function (key, value){					
			if (eftTransactionID == value.EftTransactionID){						
				$.NewRow.push({
					amount: value.EftAmount,
					type: 'EFT',
					idFile: value.CardHolderId,
					Oringen: value,
					Merge: 'N'
				});						
				$.each($.CommissionEPPSTrasantion, function (key2, value2){							
					if (value.EftDate == value2.Fee_Date){								
						$.NewRow.push({
							amount: value2.FeeAmount,
							type: 'FEE',
							PaidToName: value2.PaidToName,
							Oringen: value2,
							Merge: 'N'
						});								
					}							
				});						
			}					
		});				
		if ($.NewRow.length == 0){					
			$.each($.CommissionEPPSTrasantion, function (key2, value2){						
				if ((value2.Fee_Date).substring(0, 10) == eftTransactionID){							
					$.NewRow.push({
						amount: value2.FeeAmount,
						type: 'FEE',
						PaidToName: value2.PaidToName,
						Oringen: value2,
						Merge: 'N'
					});							
				}						
			});					
		}				
		$.Contenido = '<div class="form-group">';					
		$.Contenido += '<div class="form-group">';						
		$.Contenido += 'Confirm void EFT and FEEs.';						
		$.Contenido += '</div>';						
		$.Contenido += '</div>';						
		MostrarPopup('Confirm', $.Contenido, 'ConfirmDelete(' + eftTransactionID + ')', 'S', 'N', 'primary', 'N', 'N');						
	}
					
	function ConfirmDelete(eftTransactionID)			
	{			
		$.Url = 'commisionPaymentsByClient2';						
		datos = {							
			action: 'DeleteRow',							
			NewRow: $.NewRow,							
			_token: $('input[name="_token"]').val()							
		}
		ShowLoading();						
		EnviarPeticion(datos, datos.action);						
	}
					
	function ConfirmCopyRow()
	{
		$.Continue = 'S';
		$.each($.PaymentsEPPSTrasantion, function (key, value){
			$.dateCurrent = (($("#NewEftDate").val()).substring(0, 10)).split("/");
			$.CurrentDate = $.dateCurrent[2] + '-' + $.dateCurrent[0] + '-' + $.dateCurrent[1];
			if ((value.EftDate).substring(0, 10) == $.CurrentDate){
				if(!$.copyFEE){
					$.Continue = 'N';
				}
			
			}
		});
		$.each($.CommissionEPPSTrasantion, function (key, value){
			$.dateCurrent = (($("#NewEftDate").val()).substring(0, 10)).split("/");
			$.CurrentDate = $.dateCurrent[2] + '-' + $.dateCurrent[0] + '-' + $.dateCurrent[1];
			if ((value.Fee_Date).substring(0, 10) == $.CurrentDate){
				if($.copyFEE){
					$.Continue = 'N';
				}
			}
		});

		if ($.Continue == 'S'){
			if ($.trim($("#NewEftDate").val()) != ''){
				$.Url = 'commisionPaymentsByClient2';
				datos = {
					action: 'CopyRow',
					NewRow: $.NewRow,
					NewEftDate: $('#NewEftDate').val(),
					_token: $('input[name="_token"]').val()
				}
				ShowLoading();
				EnviarPeticion(datos, 'SaveReschedule')
			}
			else{
				MostrarPopup('Warning', 'Please insert a new date', 'N', 'S', 'N', 'primary', 'N', 'N');
			}
		} 
		else{
			$.NewEftDate = $("#NewEftDate").val();
			$.Contenido = '<div class="form-group">';
			$.Contenido += '<div class="form-group">';
			$.Contenido += 'Do you want to insert and displace other transactions or merge with existing one';
			$.Contenido += '</div>';
			$.Contenido += '</div>';
			$.Contenido += '</div>';
			MostrarPopup('Confirm', $.Contenido, "ReConfirmCopyRow('DisplaceRow')", "ReConfirmCopyRow('MergeRow')", 'S','primary', 'N', 'N', '', 'Displace', 'Merge');
		}
	}


	function ReConfirmCopyRow(action)
	{
		$.Url = 'commisionPaymentsByClient2';
		datos = {
			action: action,
			NewRow: $.NewRow,
			NewEftDate: $.NewEftDate,
			_token: $('input[name="_token"]').val()
		}
		ShowLoading();
		setTimeout(function () {ShowLoading();}, 500);
		ShowLoading();
		EnviarPeticion(datos, 'SaveReschedule')
	}

	function DecidirAccion(data, decidir)
	{
		if (decidir == 'SaveReschedule' || 'DeleteRow' || 'AddNewFEE'){
			ShowLoading();
			window.location = data.Save + '?searchClient=' + data.idfile;
			/*CerrarPopup();*/
		}
	}

	function SaveNote()
	{
		if ($.Enviar == 'S'){
			return true;
		}
		$.Contenido = '<div class="form-group">';
		$.Contenido += '<div class="form-group"> <label for="client">Note&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido += '<input id="NewNoteInto" class="text" type="text" style="width:280px;" />';
		$.Contenido += '</div>';
		$.Contenido += '</div>';
		MostrarPopup('Add Note', $.Contenido, 'ConfirmSaveChange()', 'S', 'N', 'primary', 'N', 'N');
		return false;
	}

	function ConfirmSaveChange()
	{
		if ($("#NewNoteInto").val() == ''){
			MostrarPopup('Warning', 'Please enter a note. ', 'N', 'S', 'N', 'primary', 'N', 'N');
			return false;
		}
		$("#NewNote").val($("#NewNoteInto").val());
		$.Enviar = 'S';
		$("#FormConfirmSaveChange").submit();
	}

	function CrearNewFEE(payoutDate, affiliateCompanyName, provider, affiliateCompanyId)
	{
		if (provider == 'SAS'){
			$.NewFEEDate = payoutDate;
			$.affiliateCompanyName = affiliateCompanyName;
			$.affiliateCompanyId = affiliateCompanyId;
			$.Contenido = '<div class="form-group">';
			$.Contenido += '<div class="form-group">  <label for="client">Client&nbsp;&nbsp;</label>';
			$.Contenido += ' ' + $("#NameClient").html();
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += payoutDate;
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group"><label for="client">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += '<input id="NewFEEAmount" value="" class="text" type="text" style="width:140px;" />';
			$.Contenido += '</div>';
			$.Contenido += '</div>';
			MostrarPopup('Add new FEE SAS', $.Contenido, 'ConfirmCrearNewFEESAS()', 'S', 'N', 'primary', 'N', 'N');
		} 
		else{
			$.NewFEEDate = payoutDate;
			$.affiliateCompanyName = affiliateCompanyName;
			$.Contenido = '<div class="form-group">';
			$.Contenido += '<div class="form-group">  <label for="client">Client&nbsp;&nbsp;</label>';
			$.Contenido += ' ' + $("#NameClient").html();
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += payoutDate;
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group"><label for="client">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += '<input id="NewFEEAmount" value="" class="text" type="text" style="width:140px;" />';
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group"> <label for="client">Account Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += '<input id="NewAccountNumber" onkeypress="javascript:return validarNro(event)" value="' + ($.AccountNumber) +'" class="text" type="text" style="width:140px;" />';
			$.Contenido += '</div>';
			$.Contenido += '<div class="form-group"> <label for="client">Routing Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido += '<input id="NewRoutingNumber" onkeypress="javascript:return validarNro(event)" value="' + ($.RoutingNumber) +'" class="text" type="text" style="width:140px;" />';
			$.Contenido += '</div>';
			$.Contenido += '</div>';
			MostrarPopup('Add new FEE', $.Contenido, ' $.NewFEEAmount = $("#NewFEEAmount").val(); $.AccountNumber = $("#NewAccountNumber").val(); $.RoutingNumber=$("#NewRoutingNumber").val(); ConfirmCrearNewFEE();', 'S', 'N', 'primary', 'N', 'N');
		}
  }
  
  $('#Popup').on('click', '.btn-primary', function(){
    $.NewFEEAmount = $("#NewFEEAmount").val();
  });

	function ConfirmCrearNewFEESAS()
	{
		if (!formatCurrency($('#NewFEEAmount').val())){
			MostrarPopup('Warning', 'Please enter a proper amount. "##.##"', 'N', 'S', 'N', 'primary', 'N', 'N');
			return false;
		}
		$.Url = 'commisionPaymentsByClient2';
		datos = {
			action: 'AddNewFEESAS',
			NewEftDate: $.NewFEEDate,
			NewFEEAmount: $('#NewFEEAmount').val(),
			affiliateCompanyId: $.affiliateCompanyId,
			_token: $('input[name="_token"]').val()
		}
		ShowLoading();
		setTimeout(function () {ShowLoading();}, 500);
		ShowLoading();
		EnviarPeticion(datos, 'AddNewFEESAS')
	}


  function ConfirmCrearNewFEE()
	{
    console.log($.NewFEEAmount);
    console.log($("#NewRoutingNumber"));
		if (($("#NewRoutingNumber").val()).length < 9){
      
			MostrarPopup('Warning', 'Bank Routing Number must be at least 9 digits. ', 'N', 'S', 'N', 'primary', 'N', 'N');
		} 
		else{      
			if (!formatCurrency($('#NewFEEAmount').val())){
				MostrarPopup('Warning', 'Please enter a proper amount. "##.##"', 'N', 'S', 'N', 'primary', 'N', 'N');
				return false;
			}
		}
    $.Url = 'commisionPaymentsByClient2';
    console.log($('#NewFEEAmount').val());
		datos = {
			action: 'AddNewFEE',
			NewAccountNumber: $.AccountNumber,
			NewRoutingNumber: $.RoutingNumber,
			NewEftDate: $.NewFEEDate,
			NewFEEAmount: $.NewFEEAmount,
			AffiliateCompanyName: $.affiliateCompanyName,
			_token: $('input[name="_token"]').val()
		}
    console.log(datos);
		ShowLoading();
		setTimeout(function () {ShowLoading();}, 500);
		ShowLoading();
		EnviarPeticion(datos, 'AddNewFEE')
	}

	function Renumbercommision()
	{
		$.Url = 'commisionPaymentsByClient2';
		datos = {
			action: 'Renumbercommision',
			_token: $('input[name="_token"]').val()
		}
		ShowLoading();
		setTimeout(function () {ShowLoading();}, 500);
		ShowLoading();
		EnviarPeticion(datos, 'AddNewFEE')
	}
</script>
<script src="{{asset('js/plugin/datejs/date.js?ver=7.3.1')}}"></script>
<script src="{{asset('js/CommissionPaymentsByClient2.js?ver='.rand(10,10))}}"></script>

@endsection