@extends ('layouts.admin')
@section ('contenido')

<h1>
	Discrepancy Report EPPS 2
</h1>
<br />

<div class="row">
	{{-- <button class="btn btn-success  btn-sm" name="action" value="export" type="button" onclick="exportTableToCSV('DiscrepancyReportEPPSController.csv')">Export</button> --}}
</div>
<br />
<div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
		<div class="table-responsive">
			<table id="example1" class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>File</th>
					<th>Client</th>
					<th>Deposit Date</th>
					<th>Deposit Amount [DA]</th>
					<th>Commision Amount [CA]</th>
					<th>EPPS Amount [EA]</th>
					<th>Difference [DA -(CA+EA)]</th>
				</thead>
				<tbody>
					@foreach ($rows as $item)
							<tr class="@php echo ($item->resta<0)?'danger':'success'; @endphp">
								<td>{{$item->idFile}}</td>
								<td>{{$item->name}}</td>
								<td>{{$item->date}}</td>
								<td>{{$item->depositeAmount/100}}</td>
								<td>{{$item->commisionAmount/100}}</td>
								<td>{{$item->eppsAmount/100}}</td>
								<td>{{$item->resta/100}}</td>
							</tr>
					@endforeach
				</tbody>				
			</table>
		</div>
	</div>
</div>
<script src="{{asset('js/html2csv.js?ver=8.0.0')}}"></script>   
@endsection