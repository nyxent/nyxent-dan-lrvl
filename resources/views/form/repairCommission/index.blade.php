@extends ('layouts.admin')
@section ('contenido')
<script src="{{asset('js/paymentCommissionAsign.js')}}" type="text/javascript"></script>
<h1>Commission Inspector
    @if(! empty($NameClient)) --- <div>{{ $NameClient }}</div>@endif </h1>
<div class="container">
    <br />
    <div class="row">
        <div class="col-md-6">
            @include('form.repairCommission.search')
        </div>
    </div>
</div>
<br />


<div class="row">
    <div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
        <div style="display:none">
            <img src="{{asset('img/success.png')}}" alt="Success" width="15"> 
        </div>

        <div class="table-responsive">			
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>

                <th width="15"></th>
                <!--<th>Type</th>-->
                <!--<th style="min-width:150px;">Client</th>-->
                <th width="100">CRMID</th>
                <th>Related Payment</th>
                <th width="100">PayoutDate</th>
                <th width="15"></th>
                <th width="80">Amount</th>
                <?php
                foreach ($Companys as $indice => $Company) {
                    echo '<th>' . $Company->affiliateCompanyName . '</th>';
                }
                ?>
                <th>Remarks</th>
                <th>Provider</th>
                </thead>
                <tbody>
                    <?php 
                    $totalCommision = 0; 
                    $contRows = 0;
                    ?>
                    
                    @foreach($itemsFile as $item)
                    <?php
                    if ($item['type'] == 'dep') {
                        $totalCommision = 0;
                        $amount = $item['amount'];
                        $values = explode('-', $item['amount']);
                        $eftID = isset($values[1]) ? $values[1] : '';
                        $amount = isset($values[0]) ? $values[0] : '';
                        $CountMonth = $item['paymentNumber'];
                    }
                    ?>		
                    <tr  class="<?php if ($item['type'] == 'dep' && $item['statusCode'] == 'S') {
                        echo 'success';
                    } elseif ($item['type'] == 'dep' && $item['statusCode'] == 'R') {
                        echo 'danger';
                    } elseif ($item['type'] == 'com' && $item['statusCode'] == 'T') {
                        echo 'warning';
                    } if ($item['type'] == 'com') {
                        echo 'com';
                    } ?>">

                        <td>@if($item['type']=='dep' && $item['statusCode']!='R'){{$CountMonth}}@endif</td>
                        <td>{{$idFile}}</td>
                        <td>
                            <div class="row">
                                @if($item['type']=='com')
                                    <div class="col-md-8">
                                        <select class="form-control" id="paymentSelecrt_{{$contRows}}">
                                            <option value=""> -- </option>
                                            @foreach($payments as $payment)

                                            @if($payment['paymentNumber'] >= 0 )
                                            <option value="{{$payment['id']}}">{{$payment['statusCode'] == 'R' ? 'R' : $payment['paymentNumber']}} - {{date('m/d/Y', strtotime($payment['date']))}} ({{$payment['id']}})</option>
                                            @endif

                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btnUpdateCommissions" onclick="return savePaymentAssign({{$contRows}});">Save</button>
                                    </div>
                                @endif
                                
                            </div>

                        </td>
                        <td class="row-date" >{{date('m/d/Y', strtotime($item['date']))}}</td>
                        <td ></td>
                        @if($item['type']=='dep')
                        <td >{{is_numeric($amount) ? "$" . number_format($amount/100,2) : ''}}</td>
                        
                        @else
                        <td></td>
                        @endif
                        @foreach($Companys as $indice=>$Company)
                            @if($item['type']=='com')
                                <?php
                                // dd($item);
                                $companyName = str_replace(' ', '_', str_replace(',', '', $Company->affiliateCompanyName));
                                $commision = (isset($item[$companyName])) ? $item[$companyName] : null;
                                $id = (isset($item["{$companyName}-id"])) ? $item["{$companyName}-id"] : null;
                                $eftTransactionID = (isset($item["{$companyName}-eftTransactionID"])) ? $item["{$companyName}-eftTransactionID"] : null;
                                $values = explode('_', $commision);
                                $feeStatus = isset($values[0]) ? $values[0] : '';
                                $feeAmount = isset($values[1]) ? $values[1] : 0;
                                $feeds = isset($values[2]) ? explode(',', $values[2]) : '';
                                $feeID = isset($values[2]) ? explode(',', $values[2])[0] : '';
                                $totalCommision = $totalCommision + $feeAmount;
                                ?>
                                <td  style="{{(!$id || $eftTransactionID || $item['provider'] == 'SAS' ) ? '' : 'background-color:red'}}  
                                     {{($eftTransactionID && !isset($payments[$eftTransactionID]) && $item['provider'] == 'EPPS' ) ? 'background-color:red':  ''}}" >
                                    <input type="hidden" class="commissionValue_{{$contRows}}" value="{{$id}}" />
                                    @if($feeStatus=='P')						
                                            ${{number_format($feeAmount/100,2)}}
                                    @elseif($feeStatus=='T')
                                            <img src="{{asset('img/success.png')}}" alt="Success" width="15"> 

                                            ${{number_format($feeAmount/100,2)}}
                                    @elseif(!isset($commision))

                                            ${{number_format($feeAmount/100,2)}}</td>
                                    @else

                                            ${{number_format($feeAmount/100,2)}}

                                    @endif
                                    
                                    
                                    @if($eftTransactionID && isset($payments[$eftTransactionID]))
                                        <br />
                                        @if($payments[$eftTransactionID]['statusCode'] == 'R')
                                            Assigned to Return
                                        @else
                                            Assigned to payment #{{$payments[$eftTransactionID]['paymentNumber']}}
                                        @endif
                                    
                                    @elseif ($eftTransactionID)
                                    <br />Invalid assignation 
                                    @endif
                                </td>
                            @else
                            <td></td>
                            @endif								
                        @endforeach
                        <td>@if($item['type']=='dep'){{$item['statusCodeDescription']}}@endif</td>
                        <td>{{$item['provider']}}</td>
                    </tr>
                    <?php 
                    
                    $contRows++;
                    ?>
                    @endforeach
                </tbody>
            </table>		
        </div>		
    </div>	
</div>

@endsection