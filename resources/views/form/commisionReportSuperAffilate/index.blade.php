@extends ('layouts.admin')

@section ('contenido')

<h1>Commission report affilate</h1>
<div class="container">
    <br />
    <!--	<form id="formFilters" action="{{secure_url('form/commisionReportAffilate')}}" method="get" > -->
    <form id="formFilters" action="{{url('form/commisionReportAffilate')}}" method="get" > 
        {{ csrf_field() }} 
        <input id="downloadInput" type="hidden" name="download" value="0">
        <div class="row">        
            <div  class="col-md-4 col-sm-4 col-xs-4">
                From Date:  <input name="date1" id="date1" class="date" type="text"  style="width:140px;" value="{{isset($request['date1'])?$request['date1']:''}}"  />
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                To Date:  <input name="date2"  id="date2"  class="date" type="text" style="width:140px;" value="{{isset($request['date2'])?$request['date2']:''}}"  />
            </div>
        </div>
        <br />
        <div class="row"> 
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            <select  id="searchAffiliate" class="selectpicker"   name="searchAffiliate" data-live-search="true" title="please select Affiliate Company" onchange="">
                                @if(! empty($affiliates))
                                    @foreach ($affiliates as $item)
                                        @if(isset ($request['searchAffiliate']))
                                            @php $selected =  $request['searchAffiliate'] ==$item->affiliateCompanyId? 'selected':''; @endphp
                                        @else 
                                            @php dump($item->affiliateCompanyId) @endphp
                                            @php $selected =  $item->affiliateCompanyId == 26? 'selected':''; @endphp
                                        @endif;
                                        <option value="{{ $item->affiliateCompanyId }}" {{$selected}}>{{ $item->affiliateCompanyName }}</option>
                                    @endforeach
                                @endif 
                            </select>
                        </div>
                    </div>
                </div>
            </div>			
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group">
                            @php $selected =  isset($request['searchClient']) ? $request['searchClient']:null; @endphp
                            <select  id="searchClient" class="selectpicker"   name="searchClient" data-live-search="true" title="please select client" onchange="">
                                @if(! empty($clients))
                                    @foreach ($clients as $item)
                                    <option value="{{ $item->idFile }}" {{$item->idFile == $selected ? 'selected':''}}>{{ $item->firstName }} {{ $item->lastName }}</option>
                                    @endforeach
                                @endif 
                            </select>
                        </div>
                    </div>
                </div>
            </div>			
        </div>		
        <br />		
        <div class="row">
            <button class="btn btn-primary  btn-sm" name="action" value="filter" type="submit"  onclick="$('#downloadInput').val(0);">Filter</button>
            <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
        </div>		
        <br />		
        <div class="row">
            <button class="btn btn-info downloadBtn"  onclick="$('#downloadInput').val(1);">Download</button>
        </div>	
    </form>	
</div>

<br />

<div class="row">	
    <div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">		
        @if(! empty($commisions))		
        <div class="table-responsive">			
            @include('form.commisionReportAffilate.table')				
        </div>
        {{ $commisions->appends(['date1' =>$request['date1'],'date2'=>$request['date2'],'searchAffiliate'=>$request['searchAffiliate'],'searchClient'=>$request['searchClient']])->links() }}
        @endif			
    </div>		
</div>

@endsection