@php use App\Http\Utils\Utils; @endphp

<table class="table table-striped table-bordered table-condensed table-hover">				
    <thead>	
    <tr>			
        <th>Referral</th>					
        <th>CRM_ID</th>					
        <th id="AffiliateName" class="active" onclick="OrderBy('AffiliateName')">Affiliate name</th>					
        <th id="FirstName" class="active" onclick="OrderBy('FirstName')">First name</th>					
        <th id="LastName" class="active" onclick="OrderBy('LastName')">Last name</th>					
        <th>Total enrolled debt</th>					
        <th>Program duration</th>					
        <th id="Date" class="active" onclick="OrderBy('Date')">Transaction date</th>					
        <th id="amount" class="active" onclick="OrderBy('amount')">Commission</th>					
        <th>Payment #</th>					
        <th>Payee Company Name</th>					
        <th>Approved date</th>
    </tr>										
</thead>
<tbody>
    @php $currentFile = 0; $count=0; @endphp				
    @foreach ($commisions as $indice=>$item)
        @if($currentFile != $item->idFile)
            @php $currentFile = $item->idFile; $count=Utils::isDownPayment($item->payoutDate,$item->idFile)?0:1; @endphp		
        @endif
        @if($item->manualPaymentNumber=='Y')
            @php $count = $item->paymentNumber; @endphp
        @endif											
        <tr>																
            <td>{{$item->idFileDAN}}</td>						
            <td>{{$item->idFile}}</td>						
            <td>{{$item->affiliateName}}</td>						
            <td>{{$item->firstName}}</td>						
            <td>{{$item->lastName}}</td>						
            <td>{{number_format($item->contractTotalDebt?$item->contractTotalDebt:$item->ppTotalDeb,2)}}</td>						
            <td>{{$item->contractProgramLenght? $item->contractProgramLenght:$item->ppProgramLenght}}</td>						
            <td>{{$item->payoutDate}}</td>						
            <td>{{number_format($item->amount/100,2)}}</td>					
            <td>{{$count}}</td>				
            <td>{{$item->provider}}</td>						
            <td>{{$item->enrollmentApprovedDate=='01/01/1900'?'':$item->enrollmentApprovedDate}}</td>													
        </tr>
        @php $count++; @endphp					
    @endforeach
</tbody>				
</table>