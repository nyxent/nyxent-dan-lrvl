@extends ('layouts.admin')
@section ('contenido')
<h1>
Payments by client
</h1>
    <div class="container">
          <br />
          <form action="{{url('form/paymentsByClient')}}" method="get" >  
          		   @include('layouts.date')
            <br />
           <div class="row"> 
            			<div class="col-md-4 col-sm-4 col-xs-4">
                            @include('layouts.searchClient')
                       </div>
            </div>
            <br />
           <div class="row">
           	       <div class="col-md-8 col-sm-8 col-xs-8">
                  <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                  <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
                  </div>
            </div>
            </form>
    </div>
      <br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
     
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                <th>Client</th>
                <th>Deposit date</th>
                <th>Amount</th>

                <th>comment</th>
                <th class="commenttd">Payment number</th>
            </thead>

            @foreach ($itemsFile as $item)
                  @if($item->statusCode=='S')
                      <tr class="success">
                  @else
                     <tr>
                  @endif  
                <td>{{$item->lastName}} {{$item->firstName}}</td>
                <td>{{$item->depositDate}}</td>
                <td align="right">{{$item->amount}}</td>
                <td>{{$item->comment}}</td>
                <td>{{$item->paymentNumber}}</td>
            </tr>
            {{--  @include('form.commissionEntry.modal') --}}
            @endforeach
            </table>
        </div>
		 {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
$("#searchClient").val('<?php echo $searchClient;?>');
</script>
@endsection