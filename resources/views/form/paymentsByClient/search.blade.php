
<div class="col-md-12">
{!! Form::open(array('url'=>'form/paymentsByClient','method'=>'get','autocomplete'=>'off','role'=>'searchClient')) !!}
<div class="form-group">
	<div class="input-group">
        <select id="lunch" class="selectpicker"   name="searchClient" data-live-search="true" title="please select client" onchange="">
          @foreach ($client as $item)
                <option value="{{ $item->idFile }}">{{ $item->client2 }}</option>
            @endforeach
     	 </select>
		<span class="input-group-btn">
			<button type="submit" class="btn btn-primary btn-xs">search</button>
		</span>
	</div>
</div>
{{Form::close()}}
</div>