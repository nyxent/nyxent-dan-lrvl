@extends ('layouts.admin')
@section ('contenido')
<script>
$.FindEftByStatusDateResult=<?php echo $FindEftByStatusDateResult;?>;
</script>
<h1>
Issues Report
</h1>
	   {{ csrf_field() }}
   
<br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                 <th class="clienttd">Client name</th>
                 <th class="amounttd">Payout date</th>
                 <th class="depositeDatetd">Amount</th>
                 <th>Message</th>
                 <th>Reschedule</th>
            </thead>
            @foreach ($itemsFile as $indice=> $item)
            <tr>
            	<td>{{$item->client}}</td>
                <td>{{$item->EftDate}}</td>
                <td>${{$item->EftAmount}}</td>
                <td>{{$item->LastMessage}}</td>
                <td><button class="btn btn-primary btn-xs" onclick="Reschedule({{$indice}})" type="button">Reschedule</button></td>
            </tr>
            @endforeach
            </table>
        </div>
		  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
			function Reschedule(indice)
			{
				$.Contenido='<div class="form-group">';
				
					$.Contenido+='<div class="form-group">  <label for="client">Client&nbsp;&nbsp;</label>';
					$.Contenido+=' '+$.FindEftByStatusDateResult[indice]['client'];
					$.Contenido+='</div>';
					$.Contenido+='<div class="form-group">  <label for="client">Old date&nbsp;&nbsp;</label>';
					$.Contenido+=' '+$.FindEftByStatusDateResult[indice]['EftDate'];
					$.Contenido+='</div>';
					$.Contenido+='<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
					$.Contenido+='<input name="NewEftDate" value="" id="NewEftDate" class="Newdate" type="text" style="width:140px;">';
					$.Contenido+='</div>';
					$.Contenido+='<div class="form-group"> <label for="client">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
					$.Contenido+='<input id="NewEftAmount" value="'+($.FindEftByStatusDateResult[indice]['EftAmount']).replace("$","")+'" class="text" type="text" style="width:140px;" />';
					$.Contenido+='</div>';
					$.Contenido+='<div class="form-group"> <label for="client">Account Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
					
					$.Contenido+='<input id="NewAccountNumber" onkeypress="javascript:return validarNro(event)" value="'+($.FindEftByStatusDateResult[indice]['AccountNumber'])+'" class="text" type="text" style="width:140px;" />';
					$.Contenido+='</div>';
					$.Contenido+='<div class="form-group"> <label for="client">Routing Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
					$.Contenido+='<input id="NewRoutingNumber" onkeypress="javascript:return validarNro(event)" value="'+($.FindEftByStatusDateResult[indice]['RoutingNumber'])+'" class="text" type="text" style="width:140px;" />';
					$.Contenido+='</div>';
					
					$.Contenido+='<br><div class="form-group">  <label for="client">Routing number of this EFT in EPPS</label>';
					$.Contenido+=' '+$.FindEftByStatusDateResult[indice]['RoutingNumberEPPS'];
					$.Contenido+='</div>';
					
					$.Contenido+='<div class="form-group">  <label for="client">Account number of this EFT in EPPS</label>';
					$.Contenido+=' '+$.FindEftByStatusDateResult[indice]['AccountNumberEPPS'];
					$.Contenido+='</div>';
					
					
					$.Contenido+='<div class="form-group"> <label for="client">Note&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
					$.Contenido+='<input id="NewNote" class="text" type="text" style="width:280px;" />';
					$.Contenido+='</div>';
					
				$.Contenido+='</div>';
				MostrarPopup('Reschedule EFT',$.Contenido,'ConfirmSaveReschedule('+indice+')','S','N','primary','N','N');
				$('.Newdate').datepicker({  
				  		format: 'mm/dd/yyyy',
	  					startDate: CurrentDateMoreTwoDays(),
	    				autoclose: true,
				 });  
			}
			function DecidirAccion(data,decidir)
			{
				if(decidir=='SaveReschedule')
				{
					 window.location = "/reportsTransactions/form/rescheduleEFT";
					CerrarPopup()
				}
			}
			function ConfirmSaveReschedule(indice)
			{
				if($.trim($("#NewEftAmount").val())!='' && $.trim($("#NewEftDate").val())!='')
				{
					$.UpdateNextAccount='N';
					 if(($("#NewRoutingNumber").val()).length<9)
					 {	
						MostrarPopup('Warning','Bank Routing Number must be at least 9 digits. ','N','S','N','primary','N','N');
					 }
					 else
					 {
						 if(!formatCurrency($('#NewEftAmount').val()))
						 {
							MostrarPopup('Warning','Please enter a proper amount. "##.##"','N','S','N','primary','N','N');
							return false;
						 }
						 if(
						 $.FindEftByStatusDateResult[indice]['AccountNumber']!=$("#NewAccountNumber").val()
						 || 
						  $.FindEftByStatusDateResult[indice]['RoutingNumber']!=$("#NewRoutingNumber").val()
						 )
						 {
							$.UpdateNextAccount='S';
							
						 }
						 
						 $.Url='rescheduleEFT';
						 datos={
							 FindEftByStatusDateResult:$.FindEftByStatusDateResult[indice],
							 NewEftDate :$('#NewEftDate').val(),
							 NewEftAmount:$('#NewEftAmount').val(),
							 action:'SaveReschedule',
							 _token :$('input[name="_token"]').val(),
							 UpdateNextAccount:$.UpdateNextAccount,
							 NewAccountNumber:$("#NewAccountNumber").val(),
							 NewRoutingNumber:$("#NewRoutingNumber").val(),
							 NewNote:$("#NewNote").val()
						}
						 ShowLoading();
						 EnviarPeticion(datos,'SaveReschedule')	
					 }
				}
				else
				{
					MostrarPopup('Warning','Please insert a new amount and new date','N','S','N','primary','N','N');
				}
			}
	</script>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
</script>
@endsection