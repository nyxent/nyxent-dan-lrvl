@extends ('layouts.admin')
@section ('contenido')
<h1>
Commission report affilate  KC Credit Services
</h1>
	   {{ csrf_field() }}
    <div class="container">
          <br />
          <form id="formFilters"  action="{{url('form/commisionReportAffilateKC')}}" method="get" >  
                    @include('layouts.order')
                    @include('layouts.date')
                    <br />
                    <div class="row"> 
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    @include('layouts.searchClient')
                               </div>
                    </div>
                    <br />
                    @include('layouts.buttons')
                    <br />
                    @include('layouts.buttonsExport')
            </form>
    </div>
<br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
            	 <th>CRM_ID</th>
            	 <th id="ClienteName" class="active" onclick="OrderBy('ClienteName')">Client name</th>
                 <th id="Date" class="active" onclick="OrderBy('Date')">Payout date</th>
                 <th id="amount" class="active" onclick="OrderBy('amount')">Commission</th>
                 <th>Payment #</th>
                 <th>Comment</th>
                 <th>Processor</th>
            </thead>
             @foreach ($itemsFile as $indice=>$item)
            	 @if($item->negative=='S')
          		  <tr class="danger">
            	@else
                <tr >
                @endif
            	<td>{{$item->CRM_ID}}</td>
            	<td >{{$item->lastName}} {{$item->firstName}}</td>
                <td>{{$item->payoutDate}}</td>
                <td >{{$item->Commission}}</td>
                <!--<td id='{{$indice}}' onclick="ShowPopupPaymentNumber('{{$indice}}','{{$item->CRM_ID}}','{{$item->payoutDate}}','{{$item->affiliateCompanyId}}')">{{$item->paymentNumber}}</td>-->
                <td>{{$item->paymentNumber}}</td>
                <td>{{$item->comment}}</td>
                <td>{{$item->Processor}}</td>
            </tr>
            @endforeach
            </table>
        </div>
		  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
 $.Url='commisionReportAffilate';
</script>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
$("#searchClient").val('<?php echo $searchClient;?>');
LoadOrderby('<?php echo $orderby;?>');
</script>
<script src="{{asset('js/ReportSavePaymentNumber.js?ver=10.1.1')}}"></script>    
@endsection