@extends ('layouts.admin')
@section ('contenido')
<h1>
Commission entry
</h1>
 <div class="container">
      <br />
    
     
              <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                           <form action="{{url('form/commissionEntry')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
                                  <input name="accion" type="hidden" value="import" />
                                  <div class="col-md-8 col-sm-8 col-xs-8">
                                    {{csrf_field()}}
                                    <input type="file" name="imported-file"/>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-4">
                                      <button class="btn btn-primary btn-xs" type="submit">Import</button>
                                  </div>
                            </form>
                    </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                            @if(! empty($Continue) and $Continue=='Y') 
                                     <form action="{{url('form/commissionEntry')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
                                          <input name="accion" type="hidden" value="success" />
                                         {{csrf_field()}}
                                          <div class="col-md-12">
                                             <button class="btn btn-primary btn-xs" name="export" value="ok" type="submit">Confirm Upload</button>
                                          
                                          </div>
                                      </form>
                                   
                              @endif 
                     </div>
              </div>
      
         		<br />
            	<div class="row">	
                      <div class="col-md-6 col-sm-6 col-xs-6">
                         <form action="{{url('form/commissionEntry')}}" id="SynchronizeSAS" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
                              <input name="accion" type="hidden" value="synchronizeSAS" />
                              <input name="number_send" id="number_send" type="hidden" value="1" />
                             {{csrf_field()}}
                              <div class="col-md-12">
                                 <button class="btn btn-success btn-xs" name="synchronize" type="submit">Synchronize SAS</button>
                              
                              </div>
                          </form> 
                       </div>
                       <div class="col-md-6  col-sm-6 col-xs-6">
                         <form action="{{url('form/commissionEntry')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
                              <input name="accion" type="hidden" value="synchronizeEPPS" />
                             {{csrf_field()}}
                              <div class="col-md-4">
                                 <button class="btn btn-success btn-xs" name="synchronize" type="submit">Synchronize EPPS</button>
                              
                              </div>
                          </form> 
                       </div>
                </div>
     </div>
    
   
      <br />
      <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
        
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                <th>Type</th>
                <th>Payment date</th>
                <th>Client</th>
                <th>Commission</th>
                <th>Company</th>
            </thead>
            @foreach ($itemsFile as $item)
            <tr>
                <td>{{$item->type}}</td>
                <td>{{$item->payment_date}}</td>
                <td>
                	 {{$item->client}}
               	 	 @if( $item->matchClient<>'Y')
                      <img src="{{asset('img/warning.png')}}" alt="Warning" height="25" width="25">
                    @endif
                  
                  	
                	
                 </td>
                <td align="right">${{$item->commission}}</td>
                <td>
                	 {{$item->company}}
                     @if ($item->duplicate=="Y")
                          	 <img src="{{asset('img/duplicate.png')}}" alt="Duplicate" title="Duplicate" width="15">
                        @elseif ($item->matchCompany<>"Y" )
                        	 <img src="{{asset('img/warning.png')}}" alt="Warning" title="No match"  width="15">
                           	
                        @else
                          	 <img src="{{asset('img/success.png')}}" alt="Success"  width="15">  
                      @endif
                     
                    {{-- empty($Continue) and $Continue=='Y'--}}
                

               </td>
            </tr>
            {{--  @include('form.commissionEntry.modal') --}}
            @endforeach
            </table>
          
       
        </div>
		 {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
 @if(!empty($number_send) and $number_send!=0 )
 	<script>
			$( document ).ready(function() {
				$("#number_send").val(<?php echo $number_send+1;?>)
				$("#SynchronizeSAS").submit();
			});

	</script>
 @endif
 
  @if(! empty($Continue) and $Continue=='EPPS_Y') 
 	<script>
			$( document ).ready(function() {
					var Contenido='please check the names of the companies	';
					MostrarPopup('Warning',Contenido,'N','S','N','warning','N','N');
			});

	</script>
 @endif
   @if(! empty($Continue) and $Continue=='Success') 
 	<script>
			$( document ).ready(function() {
					var Contenido='Synchronize success	';
					MostrarPopup('Confirm',Contenido,'N','S','N','primary','N','N');
			});

	</script>
 @endif
@endsection