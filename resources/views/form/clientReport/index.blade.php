@extends ('layouts.admin')
@section ('contenido')
<h1>
Client by date
</h1>
    <div class="container">
          <br />
          <form action="{{url('form/clientReport')}}" method="get" >  
          		   @include('layouts.date')
            <br />
            <br />
           <div class="row">
           	       <div class="col-md-8 col-sm-8 col-xs-8">
                  <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                  <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
                  </div>
            </div>
            </form>
    </div>
      <br />
    <div class="row">
	<div class="col-lg-6 col-md-6 col-dm-6 col-xs-6">
  	 @if(! empty($itemsFile))
     
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                <th>Id client</th>
            </thead>

            @foreach ($itemsFile as $item)
            <tr>
                <td>{{$item->idClient}}</td>
               
            </tr>
            @endforeach
            </table>
        </div>
		 {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
</script>
@endsection