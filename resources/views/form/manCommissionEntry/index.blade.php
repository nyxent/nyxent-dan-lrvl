@extends ('layouts.admin')
@section ('contenido')
<script>
 ShowLoading();
</script>
<script>
$.itemsFile=<?php echo json_encode($itemsFile);?>;
</script>
<h1>
Manual Commision Entry SAS
</h1>
	  {{ csrf_field() }}
    <div class="container">
          <br />
          <form action="{{url('form/manCommissionEntry')}}" method="get" onsubmit="return ValidateDays()" >  
          		   @include('layouts.date')
            <br />
           <div class="row"> 
            			<div class="col-md-4 col-sm-4 col-xs-4">
                            @include('layouts.searchClient')
                       </div>
            </div>
            <br />
           <div class="row">
           	       <div class="col-md-8 col-sm-8 col-xs-8">
                  <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                  <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
                  </div>
            </div>
             <br />
             <div class="row"> 
            			<div class="col-md-4 col-sm-4 col-xs-4">
                          @include('layouts.buttonsExport')
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                        	This date for all commissions listed
                         <input name="DateToReply" value="" id="DateToReply" class="Newdate" type="text" style="width:80px;">
                         <button class="btn btn-primary btn-sm" type="button" onclick="ReplyDate()">Apply</button>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        	Update all
                         
                        	 <button class="btn btn-primary btn-sm" type="button" onclick="ApplyAll()">Apply</button>
                        </div>
             </div>
            </form>
            <script>
			function ReplyDate()
			{
				if($("#DateToReply").val()=='')
				{
					MostrarPopup('Warning','Please enter a date.','N','S','N','primary','N','N');
					return false;
				}
				$(".Newdate").val($("#DateToReply").val());
			}
			</script>
    </div>
      <br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
     
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                <th>Affiliate Name</th>
                <th class="clienttd">Client</th>
                <th class="depositeDatetd">Deposit date</th>
                <th class="amounttd">Amount</th>
                <!--<th class="amounttd">Commission paid</th>-->
                <th>Commission date</th>
                <th>Affiliate commission</th>
                <th>Super Affiliate Name</th>
                <th>Super Affiliate commission</th>
                <th>DAN</th>
                <th>KC Credit Services</th> 
                <th>Nyxent</th>
                <th>Comment</th>
               <!--<th>Option</th>-->
            </thead>

            @foreach ($itemsFile as $indice=> $item)
           <?php 
		     if($item->commissionPaid=='Y')
              {
                  echo '<tr id="'.$indice.'" class="success">';
              }
			  else
			  {
				  echo '<tr id="'.$indice.'">';  
			  }
			?>
                <td  class="clienttd">{{$item->AffiliateName}}</td>
                <td>{{$item->lastName}} {{$item->firstName}}</td>
                <td>{{$item->depositDate}}</td>
                <td align="right">{{$item->amount}}</td>
                <!--<td align="right"><select id="ApplieNextMonth"><option>Y</option><option>N</option></select></td>-->
                <td><input name="NewFeeDate" value=""  data-indice="{{$indice}}" data-date="{{$item->depositDate2}}" data-idfile="{{$item->idFile}}" id="NewFeeDate{{$item->idFile}}{{$item->depositDate2}}" class="Newdate" type="text" style="width:80px;"></td>
                 <td><input id="paymentAffiliate{{$item->idFile}}{{$item->depositDate2}}" value="{{$item->paymentAffiliate}}" class="text" type="text" style="width:60px;" /></td>
                 <td class="clienttd">{{$item->SuperAffiliateName}}</td>
                <td><input id="paymentSuperAffiliate{{$item->idFile}}{{$item->depositDate2}}" value="{{$item->paymentSuperAffiliate}}" class="text" type="text" style="width:60px;" /></td>
                <td><input id="PaymentDAN{{$item->idFile}}{{$item->depositDate2}}" value="{{$item->PaymentDAN}}" class="text" type="text" style="width:60px;" />
               </td>
               
                <td><input id="paymentFinder{{$item->idFile}}{{$item->depositDate2}}" value="{{$item->paymentFinder}}" class="text" type="text" style="width:60px;" /></td> 
				
               
                <td><input id="paymentNyxent{{$item->idFile}}{{$item->depositDate2}}" value="{{$item->paymentNyxent}}" class="text" type="text" style="width:60px;" /></td>
                  <td>
                  	<input id="NewNoteInto{{$indice}}" class="text" type="text" style="width:140px;" />
                  </td>
                  <!--<td>
                   <?php
				   if($item->commissionPaid!='Y')
				   {
					?>
                 	 <button class="btn btn-primary btn-xs" onclick="SaveCommision({{$indice}},{{$item->idFile}},'{{$item->depositDate2}}')" type="button">Ok</button>
                     
                     </td>
                    <?php
				   }
				   ?>-->
            </tr>
            @endforeach
            </table>
        </div>
		 {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
     @if(! empty(Session::get('userIdProfile')) and Session::get('userIdProfile')=='1' or 1==1) 
 	<script>
			function parseDate(str) {
				var mdy = str.split('-');
				return new Date(mdy[2], mdy[0]-1, mdy[1]);
			}
			function ValidateDays()
			{
				days=datediff(parseDate($("#date1").val()), parseDate($("#date2").val()));
				if(days<60)
				{
					/*ShowLoading();*/
					return true;	
				}
				else
				{
					MostrarPopup('Warning','maximum interval 60 days','N','S','N','primary','N','N');
					return false;
				}
			}
			function datediff(first, second) {
				// Take the difference between the dates and divide by milliseconds per day.
				// Round to nearest whole number to deal with DST.
				return Math.round((second-first)/(1000*60*60*24));
			}
	
			function SaveCommision(indice,idFile,date)	
			{
				$.items=$.itemsFile['data'][indice];
				$.SumCommision=
				parseFloat($("#PaymentDAN"+idFile+date).val())+
				parseFloat($("#paymentSuperAffiliate"+idFile+date).val())+
				parseFloat($("#paymentFinder"+idFile+date).val())+
				parseFloat($("#paymentAffiliate"+idFile+date).val())+
				parseFloat($("#paymentNyxent"+idFile+date).val());

				$.amountPayment=($.items['amount']).replace("$","");
				
				$.NewFeeDate=$("#NewFeeDate"+idFile+date).val();
				
				if($.amountPayment!=$.SumCommision)
				{
					MostrarPopup('Warning','Deposit Amount: '+$.amountPayment +' <br>is direference to <br>sum commision:'+$.SumCommision,'N','S','N','primary','N','N');
					return false;
				}
				if($("#NewFeeDate"+idFile+date).val()=='' )
				{
					MostrarPopup('Warning','Please select a commission date','N','S','N','primary','N','N');
					return false;
				}
				
				if($("#PaymentDAN"+idFile+date).val()=='')
				{
					MostrarPopup('Warning','Please select a commission','N','S','N','primary','N','N');
					return false;
				}
				if($("#paymentSuperAffiliate"+idFile+date).val()=='' )
				{
					MostrarPopup('Warning','Please select a commission','N','S','N','primary','N','N');
					return false;
				}
				if($("#paymentFinder"+idFile+date).val()=='')
				{
					MostrarPopup('Warning','Please select a commission','N','S','N','primary','N','N');
					return false;
				}
				if($("#paymentAffiliate"+idFile+date).val()=='')
				{
					MostrarPopup('Warning','Please select a commission','N','S','N','primary','N','N');
					return false;
				}
				if($("#paymentNyxent"+idFile+date).val()=='')
				{
					MostrarPopup('Warning','Please select a commission','N','S','N','primary','N','N');
					return false;
				}
				
				/*
					*  VALIDATE CURRENCY 
				*/
				if(!formatCurrency2($("#PaymentDAN"+idFile+date).val()))
				{
					MostrarPopup('Warning','Please enter a proper amount. "##.##"','N','S','N','primary','N','N');
					return false;
				}
				if(!formatCurrency2($("#paymentSuperAffiliate"+idFile+date).val()))
				{
					MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					return false;
				}
				if(!formatCurrency2($("#paymentFinder"+idFile+date).val()))
				{
					MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					return false;
				}
				if(!formatCurrency2($("#paymentAffiliate"+idFile+date).val()))
				{
					MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					return false;
				}
				if(!formatCurrency2($("#paymentNyxent"+idFile+date).val()))
				{
					MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					return false;
				}
				 
				if($("#NewNoteInto"+indice).val()=='')
				{
					MostrarPopup('Warning','Please enter a note','N','S','N','primary','N','N');
					return false;
				}
				 
				MostrarPopup('Save commision','Do you confirm to save changes?','ConfirmSaveCommision('+Comilla+indice+Comilla+','+Comilla+idFile+Comilla+','+Comilla+date+Comilla+')','S','N','primary','N','N');
				
			}
			function ConfirmSaveCommision (indice,idFile,date)
			{
				$.indice=indice;
				$.CompanysPayments=Array();
				$.CompanysPayments[0]={affiliateCompanyId:$.items['affiliateCompanyIdDAN'],
				amount:parseFloat($("#PaymentDAN"+idFile+date).val()).toFixed(2)*100}
				$.CompanysPayments[1]={affiliateCompanyId:$.items['affiliateCompanyIdFinder'],
				amount:parseFloat($("#paymentFinder"+idFile+date).val()).toFixed(2)*100}
				$.CompanysPayments[2]={affiliateCompanyId:$.items['affiliateCompanyId'],
				amount:parseFloat($("#paymentAffiliate"+idFile+date).val()).toFixed(2)*100}
				$.CompanysPayments[3]={affiliateCompanyId:$.items['affiliateCompanyIdNyxent'],
				amount:parseFloat($("#paymentNyxent"+idFile+date).val()).toFixed(2)*100}
				$.CompanysPayments[4]={affiliateCompanyId:$.items['SuperAffiliateCompanyId'],
				amount:parseFloat($("#paymentSuperAffiliate"+idFile+date).val()).toFixed(2)*100}
					
				$.Note=$("#NewNoteInto"+indice).val();
				 $.Url='manCommissionEntry';
				 datos={
					 ApplieNextMonth:$("#ApplieNextMonth").val(),
					 idFile:idFile,
					 idDate:date,
					 NewFeeDate:$.NewFeeDate,
					 _token :$('input[name="_token"]').val(),
					 CompanysPayments:$.CompanysPayments,
					 Note:$.Note
					}
				 ShowLoading();
				 EnviarPeticion(datos,'SaveAmount')	
				
			}
			function DecidirAccion(data,decidir)
			{
				if(decidir=='SaveAmount')
				{
					$("#"+$.indice).remove();
					CerrarPopup();
				}
				if(decidir=='SaveAmount2')
				{
					$( $.ListIndice ).each(function( index , item) {
					$("#"+item).remove();
					});
					CerrarPopup();
				}
			}
			function formatCurrency2(money) {
			    var regex = /^\d+(?:\.\d{0,2})$/;
				return regex.test(money); 
			}
			
	</script>
 @endif
	</div>	
</div>
<script>
$('.Newdate').datepicker({  
				  		format: 'mm/dd/yyyy',
	    				autoclose: true,
				 });  
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
$("#searchClient").val('<?php echo $searchClient;?>');
</script>
<script>
setTimeout(function(){ CerrarPopup(); }, 1000);
function ApplyAll()
{
		
	MostrarPopup('Warning',"Are you sure to save all commission's dates?",'ConfirmUpdateAll()','S','N','primary','N','N');
	return false;
	
}
function ConfirmUpdateAll()
{
	$.DatosToSave=Array();
	$.ListIndice=Array();
	$( ".Newdate" ).each(function( index , item) {
		if(item.value!='' && item.value!=undefined && $( this ).data("indice")!=undefined)
		{
				$.items=$.itemsFile['data'][$( this ).data("indice")];

				$.NewFeeDate=$("#NewFeeDate"+$( this ).data("idfile")+$( this ).data("date")).val();
				
				$.Note=$("#NewNoteInto"+$(this).data("indice")).val();

				
				idFile=$( this ).data("idfile");
				date=$( this ).data("date");
				indice=$( this ).data("indice");
				
				$.ListIndice[$.ListIndice.length]=indice;
				
				$.Continue='S'
				if(!formatCurrency2($("#PaymentDAN"+idFile+date).val()))
				{
					//MostrarPopup('Warning','Please enter a proper amount. "##.##"','N','S','N','primary','N','N');
					$("#PaymentDAN"+idFile+date).parent().css( "background-color", "#F2DEDE" );
					$.Continue='N';
				}
				if(!formatCurrency2($("#paymentSuperAffiliate"+idFile+date).val()))
				{
					//MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					$("#paymentSuperAffiliate"+idFile+date).parent().css( "background-color", "#F2DEDE" );
					$.Continue='N';
				}
				if(!formatCurrency2($("#paymentFinder"+idFile+date).val()))
				{
					//MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					$("#paymentFinder"+idFile+date).parent().css( "background-color", "#F2DEDE" );
					$.Continue='N';
				}
				if(!formatCurrency2($("#paymentAffiliate"+idFile+date).val()))
				{
					//MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					$("#paymentAffiliate"+idFile+date).parent().css( "background-color", "#F2DEDE" );
					$.Continue='N';
				}
				if(!formatCurrency2($("#paymentNyxent"+idFile+date).val()))
				{
					//MostrarPopup('Warning','Please enter a proper amount','N','S','N','primary','N','N');
					$("#paymentNyxent"+idFile+date).parent().css( "background-color", "#F2DEDE" );
					$.Continue='N';
				}
				
				if($.Continue=='S')
				{	 			
					$.CompanysPayments=Array();
					$.CompanysPayments[0]={affiliateCompanyId:$.items['affiliateCompanyIdDAN'],
					amount:parseFloat($("#PaymentDAN"+idFile+date).val()).toFixed(2)*100}
					$.CompanysPayments[1]={affiliateCompanyId:$.items['affiliateCompanyIdFinder'],
					amount:parseFloat($("#paymentFinder"+idFile+date).val()).toFixed(2)*100}
					$.CompanysPayments[2]={affiliateCompanyId:$.items['affiliateCompanyId'],
					amount:parseFloat($("#paymentAffiliate"+idFile+date).val()).toFixed(2)*100}
					$.CompanysPayments[3]={affiliateCompanyId:$.items['affiliateCompanyIdNyxent'],
					amount:parseFloat($("#paymentNyxent"+idFile+date).val()).toFixed(2)*100}
					$.CompanysPayments[4]={affiliateCompanyId:$.items['SuperAffiliateCompanyId'],
					amount:parseFloat($("#paymentSuperAffiliate"+idFile+date).val()).toFixed(2)*100}
					
					
					$.Note=$("#NewNoteInto"+indice).val();	
					$.DatosToSave[$.DatosToSave.length]={
						idFile:$(this).data("idfile"),
						idDate:$(this).data("date"),
						CompanysPayments:$.CompanysPayments,
						Note:$.Note,
						NewFeeDate:$.NewFeeDate
						};
					
				}
				
		}
	});
	
	if($.DatosToSave.length>0)
	{
	 	$.Url='manCommissionEntry';
				 datos={
					DatosToSave:$.DatosToSave,
					 _token :$('input[name="_token"]').val(),
					}
				 ShowLoading();
				 EnviarPeticion(datos,'SaveAmount2')	
	}
	else
	{
		CerrarPopup();
	}
}
</script>
@endsection