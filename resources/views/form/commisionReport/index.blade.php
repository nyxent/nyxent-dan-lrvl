@extends ('layouts.admin')
@section ('contenido')

<h1>
Commissions report
</h1>
  {{ csrf_field() }}
<div class="container">
		<br />
          <div class="row">
          	 <!--onsubmit="ShowLoading();"-->
               <form id="formFilters" action="{{url('form/commisionReport')}}" method="get"   >
               
               			 @include('layouts.order')
               			 @include('layouts.date')
                         <br />
                         <div class="row">
                               <div class="col-md-8 col-sm-8 col-xs-8">
                                 @include('layouts.search')
                               </div>
                          </div>
                          <br />
                          @include('layouts.buttons')
                          <br />
                          @include('layouts.buttonsExport')
                </form>
          </div> 
</div>
<br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
            	 <th>CRM_ID</th>
                 <th>Sales person</th>
                 <th id="FirstName" class="active" onclick="OrderBy('FirstName')">First name</th>
                 <th id="LastName" class="active" onclick="OrderBy('LastName')">Last name</th>
                 <th id="Date" class="active" onclick="OrderBy('Date')">Transaction date</th>
                 <th id="amount" class="active" onclick="OrderBy('amount')">amount</th>
                 <th>Payment #</th>
                 <th> # of payments</th>
                 <th>Total debt</th>
                 <th>1st approval date</th>
                 <!--<th id="affiliateCompanyName" class="active" onclick="OrderBy('affiliateCompanyName')">Affiliate company name</th>
                 <th>Processor</th>
                 <th>Comment</th>-->
                 <th>status account</th>
            </thead>
            @foreach ($itemsFile  as $indice=>$item)
            	 @if($item->negative=='S')
          		  <tr class="danger">
            	@else
                <tr >
                @endif
            	<td>{{$item->CRM_ID}}</td>
                <td>{{$item->SalesPerson}}</td>
                <td>{{$item->lastName}}</td>
                <td>{{$item->firstName}}</td>
                <td>{{$item->Transactiondate}}</td>
                <td>${{$item->amount}}</td>
                <td id='{{$indice}}' onclick="ShowPopupPaymentNumber('{{$indice}}','{{$item->CRM_ID}}','{{$item->Transactiondate}}','{{$item->affiliateCompanyId}}')">{{$item->paymentNumber}}</td>
                <td>{{$item->Nofpayments}}</td>
                <td>${{$item->totalEnrolledDebt}}</td>
                <td>{{$item->enrollmentApprovedDate}}</td>
                
                <!--<td >{{$item->affiliateCompanyName}}</td>
                <td>{{$item->provider}}</td>
                <td>{{$item->comment}}</td>-->
                <td>{{$item->fileStatus}}</td>
            </tr>
            @endforeach
            </table>
        </div>
		  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
<script>
 $.Url='savePaymentNumber';
</script>
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
$("#searchClient").val('<?php echo $searchClient;?>');
$("#searchText").val('<?php echo $searchText;?>');
LoadOrderby('<?php echo $orderby;?>');
</script>
<script src="{{asset('js/ReportSavePaymentNumber.js?ver=10.1.1')}}"></script>    
@endsection