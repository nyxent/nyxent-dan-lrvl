@extends ('layouts.admin')
@section ('contenido')
<script>
$.PaymentsEPPSTrasantion=<?php echo $PaymentsEPPSTrasantion;?>;
$.PaymentsEPPSChanges=Array();

$.CommissionEPPSTrasantion=<?php echo $CommissionEPPSTrasantion;?>;
$.CommissionEPPSChanges=Array();
$.CommissionAmountEPPSChanges=Array();
$.AccountNumber=<?php echo $AccountNumber;?>;
$.RoutingNumber=<?php echo $RoutingNumber;?>;
$.Enviar='N';
</script>
<h1>
Active Snapshot 
@if(! empty($NameClient))
---  <div id="NameClient">{{ $NameClient }}</div>
@endif 
</h1>
<div class="container">
	  <br />
      <div class="row">
        
           <div class="col-md-6">
                @include('form.commisionPaymentsByClient.search')
           </div>
      	</div>
      </div>
      <br />
       <form action="{{url('form/commisionPaymentsByClient')}}" id="FormConfirmSaveChange" method="post" enctype="multipart/form-data" onsubmit="return SaveNote();">
         <input name="action" type="hidden" value="saveChangePayment" />
         <input name="NewNote" id="NewNote" type="hidden" value="" />
         <input name="PaymentsEPPSChanges" id="PaymentsEPPSChanges" type="hidden" value="" />
         <input name="CommissionEPPSChanges" id="CommissionEPPSChanges" type="hidden" value="" />
         {{csrf_field()}}
          <div class="col-md-4">
             <button class="btn btn-success btn-xs" name="synchronize" type="submit">Save changes</button>
          </div>
          <div class="col-md-4">
             <!--<button id="AccountInfoButton" class="btn btn-success btn-xs" name="synchronize" type="button" onclick="ChangeAccountInfo()">Change account info</button> -->
          </div>
          <div align="right" class="col-md-4">
            <!-- <button id="AccountInfoButton" class="btn btn-success btn-xs" name="synchronize" type="button" onclick="Renumbercommision()">Renumber</button> -->
          </div>
      </form> 
       <form id="formAccountInfo" action="{{url('form/commisionPaymentsByClient')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
       	<input name="NewAccountNumber" id="NewAccountNumber" type="hidden" value="" />
        <input name="NewRoutingNumber" id="NewRoutingNumber" type="hidden" value="" />
         <input name="action" type="hidden" value="saveChangeAccountinfo" />
         {{csrf_field()}}
      </form> 
       <br />
       <br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
    <div id="img" style="display:none"><img src="{{asset('img/success.png')}}" alt="Success"  width="15">  </div>
    <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
            	<th width="60"></th>
            	<th width="15"></th>
                <!--<th>Type</th>-->
                <!--<th style="min-width:150px;">Client</th>-->
                <th width="100">CRMID</th>
                <th width="100">PayoutDate</th>
                <th width="15"></th>
                <th width="80">Amount</th>
                  <?php
					  foreach($Companys as $indice=>$Company)
					  {
						  echo '<th>'.$Company->affiliateCompanyName.'</th>';
					  }
				 ?>
                <th>Remarks</th>
                 <th>Provider</th>
            </thead>
    		
				<?php
					 $CountMonth=0;
           $eftNumber=0;
					 if(! empty($itemsFile))
					 {
						  $PaymentsEPPS=array();
						 
						  foreach($itemsFile as $indice=>$item)
						  {				
						  	 	if($item->provider=='EPPS')
								{
									  if($item->statusCode=='S')
									  {
										  echo '<tr class="success">';
										  echo   '<td><a href="#"  onclick="CopyRow('.$item->eftTransactionID.')"><img src="../img/add.png" alt="Pendding" width="20"></a></td>'; 
									  }
									  else
									  {
										  if($item->statusCode=='R')
										  {
											  echo '<tr class="danger">';
											  echo   '<td></td>';
										  }
										  else
										  {
											  if($item->dateIshigher=='N')
											  {
												  /* echo '<tr class="warning">';
												   echo   '<td></td>';*/
												   
												   
												    echo   '<tr class="warning"><td><a href="#"  onclick="CopyRow('.$item->eftTransactionID.')"><img src="../img/add.png" alt="Pendding" width="20"></a> </td>';
											  }
											  else
											  {
												  $CountMonth++;
												 echo '<tr>' ;
												 if($item->statusCode=='P' and $item->Type=='DEP')
												 {
												
												 echo   '<td><a href="#"  onclick="CopyRow('.$item->eftTransactionID.')"><img src="../img/add.png" alt="Pendding" width="20"></a> <a href="#"  onclick="DeleteRow('.$item->eftTransactionID.')"><img src="../img/delete.png" alt="Pendding" width="15"></a></td>';
												 }
												 else
												 {
													  echo   '<tr><td><a href="#"  onclick="CopyRow('."'".$item->eftTransactionID."'".')"><img src="../img/add.png" alt="Pendding" width="20"></a> <a href="#"  onclick="DeleteRow('."'".$item->eftTransactionID."'".')"><img src="../img/delete.png" alt="Pendding" width="15"></a></td>';
												 }
											  }
										  }
									  }
								}
								else
								{
									 if($item->statusCode=='S')
									  {
										  echo '<tr class="success">';
										  echo   '<td></td>'; 
									  }
									  else
									  {
										  if($item->statusCode=='R')
										  {
											  echo '<tr class="danger">';
											  echo   '<td></td>';
										  }
										  else
										  {
											  if($item->dateIshigher=='N')
											  {
												    echo   '<tr class="warning"><td></td>';
											  }
											  else
											  {
												  $CountMonth++;
												 echo '<tr>' ;												
												 echo   '<td></td>';
											  }
										  }
									  }
								}
									if($item->Type=='DEP')
									{
                    if($item->statusCode=='P'){
                      $eftNumber++;
                      echo   '<td> '.($eftNumber).' </td>';
                    }
                    else{
                     echo   '<td> '.($item->paymentNumber).($item->paymentNumberCommission).' </td>';
                     $eftNumber = $item->paymentNumber;
                    }
									}
									if($item->Type=='COM')
									{
									   echo   '<td> '.($item->paymentNumberCommission).' </td>';
									}
								   echo   '<td> '.($item->idFile).' </td>';
								   if($item->statusCode=='P' and $item->Type=='DEP')
									{
										if($item->dateIshigher=='N')
										{
										/*echo  '<td>'.$item->payoutDate.'</td><td ></td>';*/
										echo  '<td onclick="ShowChangeDate('.$item->eftTransactionID.')" id="'.$item->eftTransactionID.'">'.$item->payoutDate.'</td><td id="msg'.$item->eftTransactionID.'"></td>';
										}
										else
										{
										echo  '<td onclick="ShowChangeDate('.$item->eftTransactionID.')" id="'.$item->eftTransactionID.'">'.$item->payoutDate.'</td><td id="msg'.$item->eftTransactionID.'"></td>';
										}
									}
									else
									{
										 if($item->statusCode=='P' and $item->Type=='COM')
										 {
											if($item->dateIshigher=='N')
											{
												echo  '<td>'.$item->payoutDate.'</td><td ></td>';
											}
											else
											{
												echo  '<td onclick="ShowChangeDateFee('."'".$item->eftTransactionID."'".')" id="DateFee'.$item->eftTransactionID.'">'.$item->payoutDate.'</td><td id="msg'.$item->eftTransactionID.'"></td>';
											}
										 }
										 else
										 {
											echo  '<td>'.$item->payoutDate.'</td><td ></td>'; 
										 }
									}
									
									if($item->statusCode=='P' and $item->Type=='DEP')
									{
										if($item->dateIshigher=='N')
										{
											echo '<td align="right"  id="amount'.$item->eftTransactionID.'">$'.$item->amount.'</td>';
										}
										else
										{
											echo '<td onclick="ShowChangeAmount('.$item->eftTransactionID.')" align="right" id="amount'.$item->eftTransactionID.'">$'.$item->amount.'</td>';
										}
									}
									else
									{
										if($item->amount==0)
										{
											echo '<td align="right" class="warning">$'.$item->amount.'</td>';
										}
										else
										{
											echo '<td align="right">$'.$item->amount.'</td>';
										}
									}
									
									foreach($Companys as $indice=>$Company)
									{		
										
										
										if(array_key_exists($item->payoutDate.$Company->affiliateCompanyId,$CompanysPayments))
										{
											$imgPendding='';
											if($CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['statusCode']=='P')
											{
												$imgPendding='<img src="../img/duplicate.png" alt="Pendding" width="15">';
											}
											if($CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['statusCode']=='T')
											{
												$imgPendding='<img src="../img/success.png" alt="Pendding" width="15">';
											}
											if($item->dateIshigher=='N')
											{
													echo '<td align="right" id="amountFee'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['feeID'].'">'.$imgPendding.'$'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['amountFee'].'</td>';
											}
											else
											{
											echo '<td align="right" onclick="ShowChangeAmountFee('."'".$item->payoutDate."'".','.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['feeID'].','."'".$Company->affiliateCompanyName."'".')" id="amountFee'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['feeID'].'">$'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId]['amountFee'].'</td>';
											
											}
										}
										else
										{
											/*
												* VERIFICAR QUE EXISTE EL FEE CON EL ID EFT
											*/
											if(array_key_exists($item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID,$CompanysPayments))
											{
												$imgPendding='';
												if($CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['statusCode']=='P')
												{
													$imgPendding='<img src="../img/duplicate.png" alt="Pendding" width="15">';
												}
												if($CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['statusCode']=='T')
												{
													$imgPendding='<img src="../img/success.png" alt="Pendding" width="15">';
												}
												if($item->dateIshigher=='N')
												{
														echo '<td align="right" id="amountFee'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['feeID'].'">'.$imgPendding.'$'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['amountFee'].'</td>';
												}
												else
												{
													
												echo '<td align="right" onclick="ShowChangeAmountFee('."'".$item->payoutDate."'".','.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['feeID'].','."'".$Company->affiliateCompanyName."'".')" id="amountFee'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['feeID'].'">$'.$CompanysPayments[$item->payoutDate.$Company->affiliateCompanyId.$item->eftTransactionID]['amountFee'].'</td>';
												}
											}
											else
											{
												echo '<td  align="right" onclick="CrearNewFEE('."'".$item->payoutDate."'".','."'".$Company->affiliateCompanyName."',"."'".$item->provider."','".$Company->affiliateCompanyId."'".')"></td>';
											}
										}
									}
									echo '<td align="right">'.$item->statusCodeDescription.'</td>';
									echo '<td align="right">'.$item->provider.'</td>';
									echo'</tr>';
							}
							 
						  
					 }
                ?>
             
             </table>
             <?php
				 if(! empty($itemsFile))
				 {
					?>
				   
					  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
					  <?php 
				 }
			?>
          </div>
      </div>
  </div>
<script>
$.CountMonth=<?php echo $CountMonth;?>;
</script>
<script>
<?php 
if($AccountNumber=='' and $RoutingNumber=='')
{
	?>
    $("#AccountInfoButton").hide();
    <?php
}
?>
$("#searchClient").val('<?php echo $searchClient;?>');
</script>
<script>
function CopyRow(eftTransactionID)
{
	$.NewRow=Array();
	$.each($.PaymentsEPPSTrasantion, function( key, value ) 
	{
		if(eftTransactionID==value.EftTransactionID)
		{
			$.NewRow.push({amount:value.EftAmount,type:'EFT',idFile:value.CardHolderId,Oringen:value,Merge:'N'});
			$.each($.CommissionEPPSTrasantion, function( key2, value2 ) 
			{
				if(value.EftDate==value2.Fee_Date)
				{
					$.NewRow.push({amount:value2.FeeAmount,type:'FEE',PaidToName:value2.PaidToName,Oringen:value2,Merge:'N'});
				}
			});
		}
	});
	if($.NewRow.length==0)
	{
		$.each($.CommissionEPPSTrasantion, function( key2, value2 ) 
		{
			if((value2.Fee_Date).substring(0, 10)==eftTransactionID)
			{
				$.NewRow.push({amount:value2.FeeAmount,type:'FEE',PaidToName:value2.PaidToName,Oringen:value2,Merge:'N'});
			}
		});
	}
	$.Contenido='<div class="form-group">';	
		
	$.Contenido+='<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
	$.Contenido+='<input name="NewEftDate" value="" id="NewEftDate" class="Newdate" type="text" style="width:140px;">';
	$.Contenido+='</div>';
		
	$.Contenido+='</div>';
		
	$.Contenido+='</div>';
	MostrarPopup('Confirm',$.Contenido,'ConfirmCopyRow()','S','N','primary','N','N');
	$('.Newdate').datepicker({  
			format: 'mm/dd/yyyy',
			startDate: CurrentDateMoreTwoDays(),
			autoclose: true,
	 });  
}

function DeleteRow(eftTransactionID)
{
	$.NewRow=Array();
	$.each($.PaymentsEPPSTrasantion, function( key, value ) 
	{
		if(eftTransactionID==value.EftTransactionID)
		{
			$.NewRow.push({amount:value.EftAmount,type:'EFT',idFile:value.CardHolderId,Oringen:value,Merge:'N'});
			$.each($.CommissionEPPSTrasantion, function( key2, value2 ) 
			{
				if(value.EftDate==value2.Fee_Date)
				{
					$.NewRow.push({amount:value2.FeeAmount,type:'FEE',PaidToName:value2.PaidToName,Oringen:value2,Merge:'N'});
				}
			});
		}
	});
	if($.NewRow.length==0)
	{
		$.each($.CommissionEPPSTrasantion, function( key2, value2 ) 
		{
			if((value2.Fee_Date).substring(0, 10)==eftTransactionID)
			{
				$.NewRow.push({amount:value2.FeeAmount,type:'FEE',PaidToName:value2.PaidToName,Oringen:value2,Merge:'N'});
			}
		});
	}
	$.Contenido='<div class="form-group">';	
		$.Contenido+='<div class="form-group">';
		$.Contenido+='Confirm void EFT and FEEs.';
		$.Contenido+='</div>';		
	$.Contenido+='</div>';
	MostrarPopup('Confirm',$.Contenido,'ConfirmDelete('+eftTransactionID+')','S','N','primary','N','N');
}
function ConfirmDelete(eftTransactionID)
{
	$.Url='commisionPaymentsByClient';
	 datos={
		 action:'DeleteRow',
		 NewRow:$.NewRow,
		 _token :$('input[name="_token"]').val()
	 }
	 ShowLoading();
	 EnviarPeticion(datos,datos.action);
}
function ConfirmCopyRow()
{
	$.Continue='S';
	$.each($.PaymentsEPPSTrasantion, function( key, value ) 
	{
		$.dateCurrent=(($("#NewEftDate").val()).substring(0, 10)).split("/");
		$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
		if((value.EftDate).substring(0, 10)==$.CurrentDate)
		{
			$.Continue='N';
		}
	});
	
	$.each($.CommissionEPPSTrasantion, function( key, value ) 
	{
		$.dateCurrent=(($("#NewEftDate").val()).substring(0, 10)).split("/");
		$.CurrentDate=$.dateCurrent[2]+'-'+$.dateCurrent[0]+'-'+$.dateCurrent[1];
		if((value.Fee_Date).substring(0, 10)==$.CurrentDate)
		{
			$.Continue='N';
		}
	});
	
	
	if($.Continue=='S')
	{
		if($.trim($("#NewEftDate").val())!='')
		{
			$.Url='commisionPaymentsByClient';
			 datos={
				 action:'CopyRow',
				 NewRow:$.NewRow,
				 NewEftDate :$('#NewEftDate').val(),
				 _token :$('input[name="_token"]').val()
			}
			 ShowLoading();
			 EnviarPeticion(datos,'SaveReschedule')	
		}
		else
		{
			MostrarPopup('Warning','Please insert a new date','N','S','N','primary','N','N');	
		}
	}
	else
	{
		$.NewEftDate=$("#NewEftDate").val();
		$.Contenido='<div class="form-group">';	
			$.Contenido+='<div class="form-group">';
			$.Contenido+='Do you want to insert and displace other transactions or merge with existing one';
			$.Contenido+='</div>';
		$.Contenido+='</div>';
			
		$.Contenido+='</div>';
		MostrarPopup('Confirm',$.Contenido,"ReConfirmCopyRow('DisplaceRow')","ReConfirmCopyRow('MergeRow')",'S','primary','N','N','','Displace','Merge');
	}
}

function ReConfirmCopyRow(action)
{
	$.Url='commisionPaymentsByClient';
			 datos={
				 action:action,
				 NewRow:$.NewRow,
				 NewEftDate :$.NewEftDate,
				 _token :$('input[name="_token"]').val()
			}
			 ShowLoading();
			 setTimeout(function(){  ShowLoading(); }, 500);
			 ShowLoading();
			 EnviarPeticion(datos,'SaveReschedule')	
}
function DecidirAccion(data,decidir)
{
	if(decidir=='SaveReschedule' || 'DeleteRow' || 'AddNewFEE')
	{
		 ShowLoading();
		window.location = data.Save+'?searchClient='+data.idfile;
		/*CerrarPopup();*/
	}
}
function SaveNote()
{
	if($.Enviar=='S')
	{
		return true;
	}
	$.Contenido='<div class="form-group">';
						
		$.Contenido+='<div class="form-group"> <label for="client">Note&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
		$.Contenido+='<input id="NewNoteInto" class="text" type="text" style="width:280px;" />';
		$.Contenido+='</div>';
		
	$.Contenido+='</div>';
	MostrarPopup('Add Note',$.Contenido,'ConfirmSaveChange()','S','N','primary','N','N');
	return false;
}

function ConfirmSaveChange()
{
	 if($("#NewNoteInto").val()=='')
	 {	
		MostrarPopup('Warning','Please enter a note. ','N','S','N','primary','N','N');
		return false;
	 }
	 $("#NewNote").val($("#NewNoteInto").val());
	 $.Enviar='S';
	 $("#FormConfirmSaveChange").submit();
}
function CrearNewFEE(payoutDate,affiliateCompanyName,provider,affiliateCompanyId)
{
	if(provider=='SAS')
	{
		$.NewFEEDate=payoutDate;
		$.affiliateCompanyName=affiliateCompanyName;
		$.affiliateCompanyId=affiliateCompanyId;
		$.Contenido='<div class="form-group">';
			$.Contenido+='<div class="form-group">  <label for="client">Client&nbsp;&nbsp;</label>';
			$.Contenido+=' '+$("#NameClient").html();
			$.Contenido+='</div>';
			
			$.Contenido+='<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+=payoutDate;
			$.Contenido+='</div>';
			
			$.Contenido+='<div class="form-group"><label for="client">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+='<input id="NewFEEAmount" value="" class="text" type="text" style="width:140px;" />';
			$.Contenido+='</div>';
		$.Contenido+='</div>';
		
		MostrarPopup('Add new FEE SAS',$.Contenido,'ConfirmCrearNewFEESAS()','S','N','primary','N','N');
	}
	else
	{
		$.NewFEEDate=payoutDate;
		$.affiliateCompanyName=affiliateCompanyName;
		$.Contenido='<div class="form-group">';
			$.Contenido+='<div class="form-group">  <label for="client">Client&nbsp;&nbsp;</label>';
			$.Contenido+=' '+$("#NameClient").html();
			$.Contenido+='</div>';
			
			$.Contenido+='<div class="form-group">  <label for="client">New date&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+=payoutDate;
			$.Contenido+='</div>';
			
			$.Contenido+='<div class="form-group"><label for="client">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+='<input id="NewFEEAmount" value="" class="text" type="text" style="width:140px;" />';
			$.Contenido+='</div>';
			
			
			$.Contenido+='<div class="form-group"> <label for="client">Account Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+='<input id="NewAccountNumber" onkeypress="javascript:return validarNro(event)" value="'+($.AccountNumber)+'" class="text" type="text" style="width:140px;" />';
			$.Contenido+='</div>';
			
			$.Contenido+='<div class="form-group"> <label for="client">Routing Number in DataBase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>';
			$.Contenido+='<input id="NewRoutingNumber" onkeypress="javascript:return validarNro(event)" value="'+($.RoutingNumber)+'" class="text" type="text" style="width:140px;" />';
			$.Contenido+='</div>';
		$.Contenido+='</div>';
		
		MostrarPopup('Add new FEE',$.Contenido,'ConfirmCrearNewFEE()','S','N','primary','N','N');
	}
}

function ConfirmCrearNewFEESAS()
{
	 if(!formatCurrency($('#NewFEEAmount').val()))
	 {
		MostrarPopup('Warning','Please enter a proper amount. "##.##"','N','S','N','primary','N','N');
		return false;
	 }
	$.Url='commisionPaymentsByClient';
			 datos={
				 action:'AddNewFEESAS',
				 NewEftDate :$.NewFEEDate,
				 NewFEEAmount:$('#NewFEEAmount').val(),
				 affiliateCompanyId:$.affiliateCompanyId,
				 _token :$('input[name="_token"]').val()
			}
			 ShowLoading();
			 setTimeout(function(){  ShowLoading(); }, 500);
			 ShowLoading();
			 EnviarPeticion(datos,'AddNewFEESAS')		
}
function ConfirmCrearNewFEE()
{
	if(($("#NewRoutingNumber").val()).length<9)
	{	
		MostrarPopup('Warning','Bank Routing Number must be at least 9 digits. ','N','S','N','primary','N','N');
	}
	 else
	 {
		 if(!formatCurrency($('#NewFEEAmount').val()))
		 {
			MostrarPopup('Warning','Please enter a proper amount. "##.##"','N','S','N','primary','N','N');
			return false;
		 }
	 }
	$.Url='commisionPaymentsByClient';
			 datos={
				 action:'AddNewFEE',
				 NewAccountNumber:$("#NewAccountNumber").val(),
				 NewRoutingNumber:$("#NewRoutingNumber").val(),
				 NewEftDate :$.NewFEEDate,
				 NewFEEAmount:$('#NewFEEAmount').val(),
				 AffiliateCompanyName:$.affiliateCompanyName,
				 _token :$('input[name="_token"]').val()
			}
			 ShowLoading();
			 setTimeout(function(){  ShowLoading(); }, 500);
			 ShowLoading();
			 EnviarPeticion(datos,'AddNewFEE')	
}
function Renumbercommision()
{
	$.Url='commisionPaymentsByClient';
			 datos={
				 action:'Renumbercommision',
				 _token :$('input[name="_token"]').val()
			}
			 ShowLoading();
			 setTimeout(function(){  ShowLoading(); }, 500);
			 ShowLoading();
			 EnviarPeticion(datos,'AddNewFEE')	
}
</script>
<script src="{{asset('js/CommissionPaymentsByClient.js?ver=7.3.1')}}"></script>    
@endsection