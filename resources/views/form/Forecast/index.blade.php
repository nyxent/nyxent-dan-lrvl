@extends ('layouts.admin')
@section ('contenido')
<h1>
Forecast report EPPS
</h1>
<div class="container">
<br />
    <form id="formFilters" action="{{url('form/Forecast')}}" method="get" >  
           @include('layouts.order')
      
           <br />
           <div class="row"> 
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            @include('layouts.searchClient')
                       </div>
            </div>
            <br />
            @include('layouts.buttons')
            <br />
            <input type="button"  class="btn btn-success  btn-sm" value="Select table"
             onclick="selectElementContents( document.getElementById('TableForeCast') );">
    </form>
</div>
<br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table id="TableForeCast"  width="<?php echo (($CantidadMayor[0]->CantidadMayor-1)*120)+300;?>"
             class=" table-striped table-bordered table-condensed table-hover"
             >
            <thead>
                 <th id="CRMID" class="activeTd" onclick="OrderBy('CRMID')">CMRID</th>
                 <th id="ClienteName" class="activeTd" onclick="OrderBy('ClienteName')">Client name</th>
                 <th class="clienttd" colspan="2">Downpayment</th>
                 <th class="clienttd">Monthly</th>
                  @for($i=0;$i<($CantidadMayor[0]->CantidadMayor-1);$i++)
                  	<th class="depositeDatetd"> {{$i}}</th>
                  @endfor
            </thead>
            @foreach ($itemsFile as $indice=> $item)
            <tr>
            	<td class="clienttd">{{$item->idfile}} </td>
            	<td class="clienttd">{{$item->firstName}} {{$item->lastName}}</td>
                <td class="clienttd">
                <?php 
				$var=$item->idfile;
                if(isset($ClientPayments[(integer)$var])){
                if(array_key_exists(0, $ClientPayments[(integer)$var]))
				    {
					echo $ClientPayments[(integer)$var][0]->depositDate;
                    }
                }
                ?>
                </td>
                <td class="clienttd"  align="right">{{$item->ppDownPayment}}</td>
            	<td class="clienttd"  align="right">
				<?php 
				$var=$item->idfile;
                if(isset($ClientPayments[(integer)$var])){
                if(array_key_exists(0, $ClientPayments[(integer)$var]))
				{
					echo $ClientPayments[(integer)$var][0]->amount;
                }
                }
                ?>
                </td>
                 @for($i=0;$i<($CantidadMayor[0]->CantidadMayor-1);$i++)
                 	
                  
					<?php  
                        $var=$item->idfile;
                        if(isset($ClientPayments[(integer)$var])){
						if(array_key_exists($i, $ClientPayments[(integer)$var]))
						{
							switch ($ClientPayments[(integer)$var][$i]->statusCode) {
								case 'S':
									echo '<td  class="depositeDatetd successTd" >';
									break;
								case 'P':
									echo '<td class="depositeDatetd" >';
									break;
								case 'R':
									echo '<td  class=" depositeDatetd warningTd">';
									break;
							}
						 	echo '<div data-toggle="tooltip" data-html="true" title="Amount:'.$ClientPayments[(integer)$var][$i]->amount.' <br> Message:'.$ClientPayments[(integer)$var][$i]->lastMessage.'">'.$ClientPayments[(integer)$var][$i]->depositDate.'</div>'; 
						}
						else
						{
							echo 	'<td class="depositeDatetd">';
                        }
                    }
					?></td>
                 @endfor
            </tr>
            @endforeach
            </table>
        </div>
		  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
$("#searchClient").val('<?php echo $searchClient;?>');
LoadOrderby('<?php echo $orderby;?>');
</script>
@endsection