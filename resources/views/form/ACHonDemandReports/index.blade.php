@extends ('layouts.admin')
@section ('contenido')
<h1>
ACH on demand reports EPPS
</h1>
    <div class="container">
          <br />
          <form action="{{url('form/ACHonDemandReports')}}" method="get" >  
          		<div class="row"> 
                        <div class="col-md-4 col-sm-4 col-xs-4">
                           @include('layouts.searchACHonDemand')
                       </div>
                       
                 </div>
          		     @include('layouts.date')
                  
            <br />
            <br />
           <div class="row">
           	       <div class="col-md-8 col-sm-8 col-xs-8">
                  <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                  <button class="btn btn-default btn-sm" type="button" onclick="clearFields()">clear</button>
                  </div>
            </div>
            </form>
    </div>
    <br />
<script>
$("#date1").val('<?php echo $date1;?>');
$("#date2").val('<?php echo $date2;?>');
</script>
@endsection