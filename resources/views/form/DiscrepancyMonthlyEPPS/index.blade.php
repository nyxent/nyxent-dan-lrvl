@extends ('layouts.admin')
@section ('contenido')
<h1>
Discrepancy monthly EPPS
</h1>
<div class="container">
<form action="{{url('form/DiscrepancyMonthly')}}" method="GET" >  
    {{ csrf_field() }}       
    <div class="row">       
      <div class="col-md-4 col-sm-4 col-xs-4">        
        <select id="month" class="selectpicker"   name="month" data-live-search="true" title="please select month" onchange="">
            @for ($i=1; $i <= 12; $i++)
                <option value="{{ $i}}">{{ $i }}</option>
            @endfor
     	 </select>     
      </div>      
      <div class="col-md-4 col-sm-4 col-xs-4">        
        <select id="year" class="selectpicker"   name="year" data-live-search="true" title="please select year" onchange="">
            @for ($i=2018; $i <= 2025; $i++)
                <option value="{{ $i}}">{{ $i }}</option>
            @endfor
        </select>     
      </div>             
      <div class="col-md-4 col-sm-4 col-xs-4">        
        <button class="btn btn-primary btn-sm" type="submit">Search!</button>               
      </div>      
    </div>    
  </form>  

<br />
    <div class="row">
	<div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
  	 @if(! empty($itemsFile))
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                 <th id="CRMID">CMRID</th>
                 <th id="ClienteName">Client name</th>
                 <th class="clienttd">Deposit date</th>
                 <th class="clienttd">Downpayment</th>
                 <th class="clienttd">Monthly</th>
                 <th class="clienttd">Payment</th>
                 <th class="clienttd">Commission</th>
                 <th class="clienttd">Commission + EPPS</th>
                 <th class="clienttd">Diff</th>
            </thead>
            @foreach ($itemsFile as $indice=> $item)
                @php 
                 $diff = $item->amountReal2  - $item->comepps;
                 @endphp
                 @if($diff != 0)
                 @if($diff > 0)
                 <tr  class="success">
                 @else
                 <tr  class="danger">
                 @endif
            	<td>{{$item->idFile}} </td>
            	<td>{{$item->firstName}} {{$item->lastName}}</td>
                <td>{{$item->depositDate}}</td>
                <td>${{$item->ppDownPayment}}</td>
                <td>${{$item->ppMontlyPayment}}</td>
                <td>${{number_format($item->amountReal2, 2)}}</td>
                <td>${{number_format($item->amountCommission2, 2)}}</td>
                <td>${{number_format($item->comepps, 2)}}</td>
                <td>{{number_format($diff, 2)}}</td>
                </tr>
                @endif
            @endforeach
            </table>
        </div>
		  {{$itemsFile->appends(Request::capture()->except('page'))->render()}}
	 @endif
	</div>	
</div>
@endsection