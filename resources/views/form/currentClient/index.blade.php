@extends ('layouts.admin')
@section ('contenido')
<h1>
    Payments history
</h1>
<br />
<br />

<form id="formAccountInfo" action="{{url('form/paymentCurrentClient')}}" method="post" enctype="multipart/form-data" onsubmit="ShowLoading();">
    <input name="action" type="hidden" value="renumber" />
    {{csrf_field()}}
    <div align="right">          
        @if(! empty(Session::get('userIdProfile')) and (Session::get('userIdProfile')=='1' or Session::get('iduser')=='146'))            
<!--        <button class="btn btn-success btn-xs" name="synchronize" type="submit">Renumber</button>                  -->
        @endif
    </div>
</form> 
<br />
<div class="row">
    <div class="col-lg-12 col-md-12 col-dm-12 col-xs-12">
        {{ csrf_field() }}
        @if(! empty($itemsFile))

        <div class="table-responsive">
            
            
            <a href="/reportsTransactions/sinchronizeCustomClientSAS/{{$idFile}}" class="btn btn-primary">Reload SAS information</a>
            
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th class="clienttd">Client</th>
                <th class="depositeDatetd">Deposit date</th>
                <th class="amounttd">Amount</th>
                <th class="statusCode">Status code</th>
                <th class="commenttd">Comments</th>
                <th class="commenttd">Provider</th>
                <th class="commenttd">Payment number</th>
                </thead>

                @foreach ($itemsFile as $indice=>$item)

                @if($item['statusCode']=='S')
                    <tr class="success">
                @else
                    <tr>
                @endif  
                    <td>{{$item['file']['lastName']}} {{$item['file']['firstName']}}</td>
                    <td>{{$item['depositDate']}}</td>
                    <td align="right">{{$item['amount']}}</td>
                    <td align="right">{{$item['statusCodeDescription']}}</td>
                    @if(! empty(Session::get('userIdProfile')) and (Session::get('userIdProfile')=='1' or Session::get('iduser')=='146' or Session::get('userIdProfile')=='12')) 
                    <td style="cursor: pointer;"  onclick="ShowPopupComment('{{$indice}}','{{$item['id']}}')" id="comment_{{$indice}}">{{$item['comment'] ? $item['comment']->comment : ''}}</td>
                    @else
                    <td >{{$item['comment'] ? $item['comment']->comment : ''}}</td>
                    @endif
                    <td>{{$item['provider']}}</td>
                    
                    @if($item['manualPaymentNumber']=='Y')
                        <td class="info">{{$item['paymentNumber']}}<a href="#"  onclick="DeletePaymentNumber('{{$indice}}','{{$item['id']}}')">
                            &nbsp;<img src="../img/undo.png" alt="Pendding" width="15"></a></td>
                    @else
                    
                        
                    
                        <td id='{{$indice}}' @if($item['statusCode'] != 'R' && $item['statusCode'] != 'RF') style="cursor: pointer;" onclick="ShowPopupPaymentNumber('{{$indice}}','{{$item['id']}}')" @endif>{{$item['paymentNumber']}}</td>
                    @endif 

                </tr>
                {{--  @include('form.commissionEntry.modal') --}}
                @endforeach
            </table>
        </div>
        
        @endif
    </div>	
</div>
@if(! empty($MessageClientEmpty) and $MessageClientEmpty=='Y') 
<script>
                                    $(document).ready(function() {
                            var Contenido = 'Please select a client	';
                                    MostrarPopup('Warning', Contenido, 'N', 'N', 'N', 'warning', 'N', 'N');
                            });	</script>
@endif
@if(! empty(Session::get('userIdProfile')) and (Session::get('userIdProfile')=='1' or Session::get('iduser')=='146' or Session::get('userIdProfile')=='12')) 
<script>
                                            $.Url = 'paymentCurrentClient';	</script>
<script src="{{asset('js/ReportSavePaymentNumber.js?ver=10.1.5')}}"></script>  
@endif
@endsection