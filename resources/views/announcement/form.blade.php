@extends ('layouts.admin')

@section ('contenido')
<h2>
  Send announcement to: <strong>{{$to}}</strong>
</h2>
@if(Session::has('message'))
  <p>{{ Session::get('message') }}</p>
@endif
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="">
        <div class="">
          <div class="">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('announcement.save') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="col-sm-2 control-label">Subject</label>            
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="subject" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Message</label>            
                <div class="col-sm-10">
                  <textarea  class="form-control" name="message" required></textarea>
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">                
                  <input type="checkbox" class="form-check-input" id="all" name="all" value="true">
                  <label class="form-check-label" for="all">For all customers?</label>
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                  <button class="btn btn-white" type="submit">Cancel</button>
                  <button class="btn btn-primary" type="submit">Send</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection