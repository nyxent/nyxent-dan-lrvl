CREATE TABLE crmlexco_dan_dev.creditRepairDocument (
	idDocument int(10) unsigned auto_increment NOT NULL,
	idFile int(10) unsigned NOT NULL,
	name varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	fileName varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	created_at datetime NULL,
	updated_at datetime NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (idDocument)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci
COMMENT='';
CREATE INDEX idFile USING BTREE ON crmlexco_dan_dev.creditRepairDocument (idFile);

ALTER TABLE file
ADD COLUMN contractSignDateCr DATETIME NULL ;

create view vwCreditRepairClient as 
SELECT
	
    f.idFile,
    f.firstName,
    f.lastName,
    f.provider,
    f.contractSignDateCr,
    f.idFileDAN,
    f.email,
    f.homePhone,
    f.businessPhone,
    f.cellPhone,
    f.enrollmentApprovedDate,
    c.idUser,
    c2.idUser as sands,
    c2.creationDate as sandsCreationDate,
    c.creationDate as goldCreationDate,            
    s.name AS statusName,
    a.hideInfoDonna1 as hide,
    u.hideInfoDonna1 as userHide,

    (
        case 

            when c.idUser is not null and DATE_SUB(NOW(),INTERVAL 730 DAY) < f.enrollmentApprovedDate then c.creationDate
            when c.idUser is not null then date_add(f.enrollmentApprovedDate, interval 730 day)
            when c2.idUser is not null then c2.creationDate
            else date_add(f.enrollmentApprovedDate, interval 730 day)
    end) AS sentDate

FROM

file f
INNER JOIN fileStatus s ON f.idFileStatus = s.idFileStatus

LEFT JOIN fileChecked c ON f.idFile = c.idFile AND c.idUser = 170

LEFT JOIN fileChecked c2 ON f.idFile = c2.idFile AND c2.idUser = 140
AND c2.idFile NOT IN (SELECT idFile from fileChecked WHERE idUser = 170)

LEFT JOIN affiliateCompany a ON a.affiliateCompanyId = f.affiliateCompanyId

LEFT JOIN user AS u ON u.idUser = f.fileOwner

WHERE
    f.enrollmentApprovedDate IS NOT NULL
    AND f.enrollmentApprovedDate <> '1900-01-01'
    AND (f.enrollmentApprovedDate < DATE_SUB(NOW(),INTERVAL 730 DAY) OR f.idFile IN (SELECT idFile from fileChecked WHERE idUser =170 OR idUser =140))
    AND f.idFileStatus NOT IN (11,17)

